-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Servidor: mysql1002.mochahost.com:3306
-- Tiempo de generación: 28-03-2022 a las 12:52:43
-- Versión del servidor: 10.3.31-MariaDB
-- Versión de PHP: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sicoinet_kyocera2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `f_complementopago`
--

CREATE TABLE `f_complementopago` (
  `complementoId` bigint(20) NOT NULL,
  `FacturasId` int(11) NOT NULL,
  `ImpPagado` float NOT NULL COMMENT 'monto',
  `Folio` varchar(15) NOT NULL,
  `Fecha` datetime NOT NULL,
  `Sello` text NOT NULL,
  `NoCertificado` varchar(30) NOT NULL,
  `Certificado` text NOT NULL,
  `SubTotal` float NOT NULL DEFAULT 0 COMMENT 'por defecto',
  `Moneda` varchar(5) NOT NULL DEFAULT 'XXX' COMMENT 'por defecto',
  `Total` float NOT NULL DEFAULT 0 COMMENT 'por defecto',
  `TipoDeComprobante` varchar(3) NOT NULL DEFAULT 'P' COMMENT 'por defecto',
  `LugarExpedicion` varchar(7) NOT NULL,
  `E_rfc` varchar(30) NOT NULL,
  `E_nombre` varchar(300) NOT NULL,
  `E_regimenfiscal` varchar(10) NOT NULL,
  `R_rfc` varchar(30) NOT NULL,
  `R_nombre` varchar(300) NOT NULL,
  `R_regimenfiscal` varchar(10) NOT NULL,
  `ClaveUnidad` varchar(10) NOT NULL DEFAULT 'ACT' COMMENT 'por defecto',
  `ClaveProdServ` varchar(20) NOT NULL DEFAULT '84111506' COMMENT 'por defecto',
  `Cantidad` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'por defecto',
  `Descripcion` varchar(30) NOT NULL DEFAULT 'Pago' COMMENT 'por defecto',
  `ValorUnitario` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'por defecto',
  `Importe` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'por defecto',
  `FechaPago` datetime NOT NULL,
  `FormaDePagoP` varchar(10) NOT NULL,
  `MonedaP` varchar(10) NOT NULL,
  `Monto` float NOT NULL,
  `NumOperacion` varchar(30) NOT NULL,
  `CtaBeneficiario` varchar(30) NOT NULL,
  `IdDocumento` varchar(50) NOT NULL,
  `Serie` varchar(50) NOT NULL,
  `Foliod` varchar(50) NOT NULL,
  `MonedaDR` varchar(10) NOT NULL,
  `MetodoDePagoDR` varchar(10) NOT NULL,
  `NumParcialidad` int(11) NOT NULL,
  `ImpSaldoAnt` float NOT NULL,
  `ImpSaldoInsoluto` float NOT NULL,
  `UsoCFDI` varchar(100) DEFAULT NULL,
  `Estado` int(11) NOT NULL DEFAULT 0,
  `rutaXml` text DEFAULT NULL,
  `uuid` varchar(300) DEFAULT NULL,
  `sellosat` text DEFAULT NULL,
  `nocertificadosat` varchar(300) DEFAULT NULL,
  `fechatimbre` datetime DEFAULT NULL,
  `cadenaoriginal` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `f_complementopago`
--

INSERT INTO `f_complementopago` (`complementoId`, `FacturasId`, `ImpPagado`, `Folio`, `Fecha`, `Sello`, `NoCertificado`, `Certificado`, `SubTotal`, `Moneda`, `Total`, `TipoDeComprobante`, `LugarExpedicion`, `E_rfc`, `E_nombre`, `E_regimenfiscal`, `R_rfc`, `R_nombre`, `R_regimenfiscal`, `ClaveUnidad`, `ClaveProdServ`, `Cantidad`, `Descripcion`, `ValorUnitario`, `Importe`, `FechaPago`, `FormaDePagoP`, `MonedaP`, `Monto`, `NumOperacion`, `CtaBeneficiario`, `IdDocumento`, `Serie`, `Foliod`, `MonedaDR`, `MetodoDePagoDR`, `NumParcialidad`, `ImpSaldoAnt`, `ImpSaldoInsoluto`, `UsoCFDI`, `Estado`, `rutaXml`, `uuid`, `sellosat`, `nocertificadosat`, `fechatimbre`, `cadenaoriginal`) VALUES
(1, 60, 1.16, '60', '2020-08-20 09:24:15', 'hX+NcImkP+rqKIJqPSU+bVZ55p3817tkbsskqB/tUpL8qQ6ADDnN3wy05PbSKlxVvaSv5GzydxG5vQVdnAZE48KWGi6PPLeHC+6p2CB7chwZZLaV9bio3wJ6FKjuc2Unpl6SdAVjU3T2TdkbVJlkKUfPLHvhlWQq2QGk/7rVoat3xobbdrN2lRADSvG3Y/fNRmbtLBmAk/XU1sZN2HeSQ9CVwJriwS5mOMwl8Oqhtcw3cpYdb86G+ja/7/57GgpMWV00gyHRkzkRudLD6o26vgHYAaccZPknYkMlOXxkn4BS1LJtk3jv4dlDsiG+TTuUkZNiULkX/o7JGKiHk/MnuQ==', '00001000000409540787', '00001000000409540787', 0, 'XXX', 0, 'P', '72050', 'HERD890308UAA', 'DANIEL JAASIEL HERNANDEZ RODRIGUEZ', '621', 'ROCJ830308CP1', 'JUAN CARLOS ROJAS CAMPOS', '', 'ACT', '84111506', 1, 'Pago', 0, 0, '2020-08-19 18:06:43', '01', 'MXN', 1.16, '1', '2', '0E7CFDEF-30C7-43E6-86C6-38095DD209F4', 'XXX', '60', 'MXN', 'PUE', 1, 1.16, 0, 'G03', 1, 'kyocera/facturas/complemento_0E7CFDEF-30C7-43E6-86C6-38095DD209F4_1.xml', '3C58328F-536F-4D4C-B6B9-D8B2DBB88E28', 'l6rP9F3wd5HxgiJ0ekBYS68tblZ7AfXYneqlEPrVHdWpMq9hHyY3durScJTDxUylw8zfy+J4WTXA7CGOPIBUdOk86Qu13DZi4Mp8bIbo8svohQ1D7eeDLutE/mdo/HU0uLxeH2+QQ8B+Yn6hAqcjmpogzNypneRc/TlFu7tWj1Wnni56fskW8iBGVq37XHN8INMvRSTSWHJ9OhSn6lNfX6yYbu0Ew7JRNreWkn2+EPYZmTYBugEBIxm/4D5BurWMjYY9aAGofDNoqBTizAHhP+zYB7H9paVahP3esGEsKwjKwMOfeLOG1tql3E8XrbJGzmlUpZ0TWeBrqi8OVP48Mw==', '00001000000504317583', '2020-08-20 10:54:58', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `f_complementopago`
--
ALTER TABLE `f_complementopago`
  ADD PRIMARY KEY (`complementoId`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `f_complementopago`
--
ALTER TABLE `f_complementopago`
  MODIFY `complementoId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
