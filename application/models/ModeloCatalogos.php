<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloCatalogos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function categorias_all() {
        $strq = "SELECT * FROM categoria where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function categoriadell($id){
        $strq = "UPDATE categoria SET activo=0 WHERE categoriaId=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function categoriaadd($nom){
        $strq = "INSERT INTO categoria(categoria) VALUES ('$nom')";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function updatenota($use,$nota){
        $strq = "UPDATE notas SET mensaje='$nota',usuario='$use',reg=NOW()";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function configticket(){
        $strq = "SELECT * FROM ticket";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function configticketupdate($titulo,$mensaje1,$mensaje2,$fuente,$tamano,$margsup){
        $strq = "UPDATE ticket SET titulo='$titulo',mensaje='$mensaje1',mensaje2='$mensaje2',fuente='$fuente',tamano='$tamano',margensup='$margsup'";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    
    public function getselectwheren($tables,$where){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($where);
        $query=$this->db->get();
        return $query;
    }

    public function getselectwhereRow($tables){
        $this->db->select("*");
        $this->db->from($tables);
        $query=$this->db->get();
        return $query->row();
    }

    function ultimoFolio() {
        $strq = "SELECT Folio FROM f_facturas WHERE activo=1 and Folio!=0 ORDER BY FacturasId DESC limit 1";
        $Folio = 0;
        $query = $this->db->query($strq);
        foreach ($query->result() as $row) {
            $Folio =$row->Folio;
        } 
        return $Folio;
    }

    function nombreunidadfacturacion($id){
        $sql="SELECT nombre
                from f_unidades
                WHERE UnidadId='$id'";
        $query=$this->db->query($sql);
        $nombre='';
        foreach ($query->result() as $item) {
            $nombre=$item->nombre;
        }
        return $nombre;
    }

    function traeProductosFactura($FacturasId){
        $sql="SELECT 
                dt.Unidad AS ClaveUnidad, 
                ser.Clave AS ClaveProdServ,
                dt.Descripcion2 as Descripcion, 
                dt.Cu, 
                dt.descuento, 
                dt.Cantidad, 
                dt.Importe, 
                u.nombre,
                u.Clave AS cunidad 
                FROM f_facturas_servicios AS dt 
                left JOIN f_unidades AS u ON dt.Unidad = u.Clave 
                LEFT JOIN f_servicios AS ser ON ser.Clave = dt.ServicioId 
                WHERE dt.FacturasId =$FacturasId";
        $query=$this->db->query($sql);
        return $query;
    }
    function updateCatalogo($Tabla,$data,$where){
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($Tabla);
        //return $id;
    }
    function Insert($Tabla,$data){
        $this->db->insert($Tabla, $data);
        $id=$this->db->insert_id();
        return $id;
    }
    function complementofacturas($facturaId){
        $sql="SELECT comp.complementoId,comp.FechaPago,comp.rutaXml,compd.NumParcialidad,compd.ImpPagado
                FROM f_complementopago AS comp
                inner join f_complementopago_documento as compd on compd.complementoId=comp.complementoId
                where comp.Estado=1 and compd.facturasId=$facturaId
            ";
        $query=$this->db->query($sql);
        return $query;
    }

    function configPuntosUpdate($data){
        // Seleccionar el máximo valor del campo 'id' en la tabla 'Config'
        $this->db->select_max('id');
        $query = $this->db->get('config_puntos');
        $row = $query->row();
        
        // Actualizar Activo en la ultima entrada
        if (isset($row)) {
            $last_id = $row->id;
            $this->db->set('activo', 0 , FALSE);
            $this->db->where('id', $last_id);
            $this->db->update('config_puntos');
        }

        //Inserta nueva Configuracion
        $this->db->insert('config_puntos', $data);
        $id = $this->db->insert_id();

        return $id;
    }

    function getDataConfigPuntos(){
        $this->db->select('id, porcentaje, checkFecha, fechaIni, fechaFin');
        $this->db->from('config_puntos');
        $this->db->where('activo','1');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->row();
    }

}