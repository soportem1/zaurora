<?php
$a = session_id();
if (empty($a)) session_start();
defined('BASEPATH') or exit('No direct script access allowed');

class ModeloVentas extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    /*
    function ingresarventa($uss,$cli,$mpago,$sbtotal,$desc,$descu,$total,$efectivo,$tarjeta){
    	$strq = "INSERT INTO ventas(id_personal, id_cliente, metodo, subtotal, descuento,descuentocant, monto_total,pagotarjeta,efectivo) 
                VALUES ($uss,$cli,$mpago,$sbtotal,$desc,$descu,$total,'$tarjeta','$efectivo')";
        $query = $this->db->query($strq);
        $id=$this->db->insert_id();
        $this->db->close();
        return $id;
    }*/
    /*
    function ingresarventad($idventa,$producto,$cantidad,$precio){
    	$strq = "INSERT INTO venta_detalle(id_venta, id_producto, cantidad, precio) VALUES ($idventa,$producto,$cantidad,$precio)";
        $query = $this->db->query($strq);
        $this->db->close();

        $strq = "UPDATE productos SET stock=stock-$cantidad WHERE productoid=$producto";
        $query = $this->db->query($strq);
        $this->db->close();
    }*/
    function configticket()
    {
        $strq = "SELECT * FROM ticket";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function clientepordefecto()
    {
        $strq = "SELECT * FROM  clientes LIMIT 1";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function getventas($id)
    {
        $strq = "SELECT * FROM ventas where id_venta=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function getventasVendedor($id)
    {
        $strq = "SELECT v.*, concat(p.nombre,' ',p.apellidos) as cajero, concat(pv.nombre,' ',pv.apellidos) as vendedor, c.nom as cliente FROM ventas as v left join personal as p on p.personalId=v.id_personal left join personal as pv on pv.personalId=v.vendedor left join clientes as c on c.ClientesId=v.id_cliente 
        where v.id_venta=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function getventas_credito($inicio, $fin, $sucursal)
    {   
        if ($sucursal == 0) {
            $wheres = '';
        } else {
            $wheres = 'v.sucursalid=' . $sucursal . ' and ';
        }
        $strq = "SELECT SUM(v.monto_total) AS monto_total FROM ventas as v
        where $wheres v.metodo=6 AND v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado!=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function getventas_credito_pagos($inicio, $fin, $sucursal)
    {   
        if ($sucursal == 0) {
            $wheres = '';
        } else {
            $wheres = 'vt.sucursalid=' . $sucursal . ' and ';
        }
        $strq = "SELECT SUM(v.monto) AS monto FROM venta_pagos_credito as v
        inner join ventas AS vt ON vt.id_venta=v.id_venta
        where $wheres vt.metodo=6 AND v.fecha between '$inicio 00:00:00' and '$fin 23:59:59' and v.activo=1 and vt.cancelado!=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function get_restante_venta($id)
    {
        $strq = "SELECT SUM(p.monto) AS monto FROM venta_pagos_credito as p  
        where p.activo=1 AND p.id_venta=$id";
        $query = $this->db->query($strq);
        return $query;
    }

    function getsucs($id)
    {
        $strq = "SELECT * FROM sucursal where idsucursal=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getventasd($id)
    {
        $strq = "SELECT vendell.cantidad, pro.nombre, vendell.precio, vendell.status, vendell.descuento, concat(p.nombre,' ',p.apellidos) as vendedor 
        FROM venta_detalle as vendell
        inner join ventas as v on v.id_venta=vendell.id_venta
        inner join productos as pro on pro.productoid=vendell.id_producto
        left join personal as p on p.personalId=v.vendedor
        where vendell.id_venta=$id";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function ingresarcompra($uss, $prov, $personal, $total, $sucursal)
    {
        $strq = "INSERT INTO compras(id_proveedor, monto_total, id_personal, idsucursal) VALUES ($prov,$total, $personal, $sucursal)";
        $query = $this->db->query($strq);
        $id = $this->db->insert_id();
        $this->db->close();
        return $id;
    }
    function ingresarcomprad($idcompra, $producto, $cantidad, $precio, $cantInicial, $sucursal)
    {
        $strq = "INSERT INTO compra_detalle(id_compra, id_producto, cantidad, precio_compra, cantInicial, idsucursal) VALUES ($idcompra,$producto,$cantidad,$precio, $cantInicial, $sucursal)";
        $query = $this->db->query($strq);
        $this->db->close();

        /*
        $strq = "UPDATE productos SET stock=stock+$cantidad WHERE productoid=$producto";
        $query = $this->db->query($strq);
        $this->db->close();
        */

        $strq = "UPDATE productos_sucursales SET existencia=existencia+$cantidad WHERE idproducto=$producto AND idsucursal=$sucursal";
        $query = $this->db->query($strq);
        $this->db->close();

    }
    function turnos()
    {
        $strq = "SELECT * FROM turno ORDER BY id DESC LIMIT 1";
        $query = $this->db->query($strq);
        $this->db->close();
        $status = 'cerrado';
        foreach ($query->result() as $row) {
            $status = $row->status;
        }
        return $status;
    }
    function turnoss()
    {
        $strq = "SELECT * FROM turno ORDER BY id DESC LIMIT 1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function abrirturno($cantidad, $nombre, $fecha, $horaa)
    {
        $strq = "INSERT INTO turno(fecha, horaa, cantidad, nombre, status, user) VALUES ('$fecha','$horaa','$cantidad','$nombre','abierto','user')";
        $query = $this->db->query($strq);
        //$this->db->close();
        $id = $this->db->insert_id();
        return $id;
    }
    function cerrarturno($id, $horaa)
    {
        $fechac = date('Y-m-d');
        $strq = "UPDATE turno SET horac='$horaa',fechacierre='$fechac', status='cerrado' WHERE id=$id";
        $query = $this->db->query($strq);
        //$this->db->close();
    }

    function corte($inicio, $fin, $sucursal, $socios, $cajero)
    {
        if ($cajero == 0) {
            $wherec = '';
        } else {
            $wherec = 'v.id_personal=' . $cajero . ' and ';
        }
        if ($socios == 0) {
            $joinso = '';
            $whereso = '';
        } else {
            $joinso = 'inner join productos as pro on pro.productoid=vd.id_producto';
            $whereso = 'pro.iddepartamento=' . $socios . ' and ';
        }
        if ($sucursal == 0) {
            $wheres = '';
        } else {
            $wheres = 'v.sucursalid=' . $sucursal . ' and ';
        }
        $strq = "SELECT DISTINCT(v.id_venta), cl.ClientesId,cl.Nom, v.reg, v.id_venta, v.id_cliente, v.id_personal, v.monto_total,v.pagotarjeta, v.pagopuntos, v.efectivo, v.subtotal, p.personalId, concat(p.nombre,p.apellidos) as vendedor, vd.cantidad,vd.precio,
            sum(vd.cantidad*vd.precio) as totalvd, v.descuentocant, SUM(DISTINCT v.descuentocant) as desctotal/*sum(v.descuentocant) as desctotal*/
                FROM ventas as v 
                left join personal as p on v.id_personal=p.personalId 
                join clientes as cl on cl.ClientesId=v.id_cliente
                join venta_detalle as vd on vd.id_venta=v.id_venta /* agregado para nuevo reporte*/
                $joinso

                WHERE  $wherec $wheres $whereso v.metodo!=6 AND v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado!=1 and vd.status=1
                group by v.id_venta
                ORDER BY v.id_venta";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function corte_credito($inicio, $fin, $sucursal, $socios, $cajero)
    {
        if ($cajero == 0) {
            $wherec = '';
        } else {
            $wherec = 'v.id_personal=' . $cajero . ' and ';
        }
        if ($socios == 0) {
            $joinso = '';
            $whereso = '';
        } else {
            $joinso = 'inner join productos as pro on pro.productoid=vd.id_producto';
            $whereso = 'pro.iddepartamento=' . $socios . ' and ';
        }
        if ($sucursal == 0) {
            $wheres = '';
        } else {
            $wheres = 'v.sucursalid=' . $sucursal . ' and ';
        }
        $strq = "SELECT DISTINCT(v.id_venta), cl.ClientesId,cl.Nom, v.reg, v.id_venta, v.id_cliente, v.id_personal, v.monto_total,v.pagotarjeta, v.pagopuntos, v.efectivo, v.subtotal, p.personalId, concat(p.nombre,p.apellidos) as vendedor, vd.cantidad,vd.precio,
            sum(vd.cantidad*vd.precio) as totalvd, v.descuentocant, sum(v.descuentocant) as desctotal
                FROM ventas as v 
                left join personal as p on v.id_personal=p.personalId 
                join clientes as cl on cl.ClientesId=v.id_cliente
                join venta_detalle as vd on vd.id_venta=v.id_venta /* agregado para nuevo reporte*/
                $joinso

                WHERE  $wherec $wheres $whereso v.metodo=6 AND v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado!=1 and vd.status=1
                group by v.id_venta
                ORDER BY v.id_venta";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function corte_credito_pagos($inicio, $fin, $sucursal)
    {

        if ($sucursal == 0) {
            $wheres = '';
        } else {
            $wheres = 'v.sucursalid=' . $sucursal . ' and ';
        }
        $strq = "SELECT vc.*
                FROM ventas as v  
                inner join venta_pagos_credito as vc on vc.id_venta=v.id_venta
                WHERE $wheres vc.activo=1 AND vc.fecha between '$inicio' and '$fin' and v.cancelado!=1";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }


    function corte_bonos($inicio, $fin)
    {

        $strq = "SELECT b.*
                FROM bonos_descuentos as b  
                WHERE b.estatus=1 AND b.fecha between '$inicio' and '$fin'";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }


    function corteEfectivo($inicio, $fin, $sucursal, $socios){
        if ($socios == 0) {
            $joinso = '';
            $whereso = '';
        } else {
            $joinso = 'inner join productos as pro on pro.productoid=vd.id_producto';
            $whereso = 'pro.iddepartamento=' . $socios . ' and ';
        }
        if ($sucursal == 0) {
            $wheres = '';
        } else {
            $wheres = 'v.sucursalid=' . $sucursal . ' and ';
        }
        $strq = "SELECT DISTINCT(v.id_venta), cl.ClientesId,cl.Nom, v.reg, v.id_venta, v.id_cliente, v.id_personal, v.monto_total,v.pagotarjeta,v.efectivo, v.subtotal, p.personalId, concat(p.nombre,p.apellidos) as vendedor, vd.cantidad,vd.precio,
            sum(vd.cantidad*vd.precio) as totalvd
                FROM ventas as v 
                left join personal as p on v.id_personal=p.personalId 
                join clientes as cl on cl.ClientesId=v.id_cliente
                join venta_detalle as vd on vd.id_venta=v.id_venta /* agregado para nuevo reporte*/
                $joinso

                WHERE $wheres $whereso v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado!=1 and vd.status=1
                and efectivo > 0 
                group by v.id_venta
                ORDER BY v.id_venta";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function cortePuntos($inicio, $fin, $sucursal, $socios)
    {
        if ($socios == 0) {
            $joinso = '';
            $whereso = '';
        } else {
            $joinso = 'inner join productos as pro on pro.productoid=vd.id_producto';
            $whereso = 'pro.iddepartamento=' . $socios . ' and ';
        }
        if ($sucursal == 0) {
            $wheres = '';
        } else {
            $wheres = 'v.sucursalid=' . $sucursal . ' and ';
        }
        $strq = "SELECT DISTINCT(v.id_venta), cl.ClientesId,cl.Nom, v.reg, v.id_venta, v.id_cliente, v.id_personal, v.monto_total,v.pagotarjeta,v.efectivo, v.subtotal, p.personalId, concat(p.nombre,p.apellidos) as vendedor, vd.cantidad,vd.precio, v.pagopuntos, v.porcentaje_puntos, v.puntos_total, v.pagopuntos, s.nombre as sucursal, v.metodo,
            sum(vd.cantidad*vd.precio) as totalvd
                FROM ventas as v 
                left join personal as p on v.id_personal=p.personalId 
                join clientes as cl on cl.ClientesId=v.id_cliente
                join venta_detalle as vd on vd.id_venta=v.id_venta /* agregado para nuevo reporte*/
                join sucursal as s on s.idsucursal = v.sucursalid
                $joinso

                WHERE $wheres $whereso v.metodo!=6 AND v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado!=1
                /*and pagopuntos > 0*/
                /*AND v.metodo IN (4, 5)*/
                group by v.id_venta
                ORDER BY v.id_venta DESC";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function corteTarjeta($inicio, $fin, $sucursal, $socios){
        if ($socios == 0) {
            $joinso = '';
            $whereso = '';
        } else {
            $joinso = 'inner join productos as pro on pro.productoid=vd.id_producto';
            $whereso = 'pro.iddepartamento=' . $socios . ' and ';
        }
        if ($sucursal == 0) {
            $wheres = '';
        } else {
            $wheres = 'v.sucursalid=' . $sucursal . ' and ';
        }
        $strq = "SELECT DISTINCT(v.id_venta), cl.ClientesId,cl.Nom, v.reg, v.id_venta, v.id_cliente, v.id_personal, v.monto_total,v.pagotarjeta,v.efectivo, v.subtotal, p.personalId, concat(p.nombre,p.apellidos) as vendedor, vd.cantidad,vd.precio,
            sum(vd.cantidad*vd.precio) as totalvd
                FROM ventas as v 
                left join personal as p on v.id_personal=p.personalId 
                join clientes as cl on cl.ClientesId=v.id_cliente
                join venta_detalle as vd on vd.id_venta=v.id_venta /* agregado para nuevo reporte*/
                $joinso

                WHERE $wheres $whereso v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado!=1 and vd.status=1
                and pagotarjeta>0
                group by v.id_venta
                ORDER BY v.id_venta";
        $query = $this->db->query($strq);
        return $query;
    }
    function cortemixtos($inicio, $fin, $sucursal, $socios)
    {
        if ($socios == 0) {
            $joinso = '';
            $whereso = '';
        } else {
            $joinso = 'inner join productos as pro on pro.productoid=vd.id_producto';
            $whereso = 'pro.iddepartamento=' . $socios . ' and ';
        }
        if ($sucursal == 0) {
            $wheres = '';
        } else {
            $wheres = 'v.sucursalid=' . $sucursal . ' and ';
        }
        $strq = "SELECT DISTINCT(v.id_venta), cl.ClientesId,cl.Nom, v.reg, v.id_venta, v.id_cliente, v.id_personal, v.monto_total,v.pagotarjeta,v.efectivo, v.subtotal, p.personalId, concat(p.nombre,p.apellidos) as vendedor, vd.cantidad,vd.precio,
            sum(vd.cantidad*vd.precio) as totalvd
                FROM ventas as v 
                left join personal as p on v.id_personal=p.personalId 
                join clientes as cl on cl.ClientesId=v.id_cliente
                join venta_detalle as vd on vd.id_venta=v.id_venta

                $joinso

                WHERE $wheres $whereso v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado!=1 and vd.status=1
                /*and efectivo > 0 and pagotarjeta>0*/
                AND (
                    (efectivo > 0 AND pagotarjeta > 0) OR
                    (efectivo > 0 AND pagopuntos > 0) OR
                    (pagotarjeta > 0 AND pagopuntos > 0) OR
                    (efectivo > 0 AND pagotarjeta > 0 AND pagopuntos > 0)
                )

                group by v.id_venta
                ORDER BY v.id_venta";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function corteTotalEfectCancel($inicio, $fin, $sucursal, $socios)
    {
        if ($socios == 0) {
            $joinso = '';
            $whereso = '';
        } else {
            /*$joinso='inner join venta_detalle as vd on vd.id_venta=v.id_venta*/
            $joinso = 'inner join productos as pro on pro.productoid=venta_detalle.id_producto';
            $whereso = 'pro.iddepartamento=' . $socios . ' and ';
        }
        if ($sucursal == 0) {
            $wheres = '';
        } else {
            $wheres = 'v.sucursalid=' . $sucursal . ' and ';
        }
        $strq = "SELECT  venta_detalle.cantidad,venta_detalle.precio,sum(venta_detalle.cantidad*venta_detalle.precio) as totalvd 
                FROM ventas as v 
                left join personal as p on v.id_personal=p.personalId 
                join clientes as cl on cl.ClientesId=v.id_cliente
                join venta_detalle on venta_detalle.id_venta=v.id_venta 
                $joinso

                WHERE $wheres $whereso v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado=2 and venta_detalle.status=0
                ORDER BY venta_detalle.id_venta";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    /*function corteTotalCanceladoP($inicio,$fin,$sucursal,$socios){
        if ($socios==0) {
            $joinso='';
            $whereso='';
        }else{
            $joinso='inner join venta_detalle as vd on vd.id_venta=v.id_venta
                    inner join productos as pro on pro.productoid=vd.id_producto';
            $whereso='pro.iddepartamento='.$socios.' and ';
        }
        if ($sucursal==0) {
            $wheres='';
        }else{
            $wheres='v.sucursalid='.$sucursal.' and ';
        }
        $strq = "vd.cantidad, vd.precio, productos.codigo, v.reg, v.id_venta, vd.id_detalle_venta, sum(cantidad * precio) as totalCGP
                FROM venta_detalle AS vd
                JOIN ventas AS v ON v.id_venta = vd.id_venta
                $joinso

                WHERE $wheres $whereso v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado=0 and vd.status=1";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }*/
    function corteCParcial($inicio, $fin, $sucursal, $socios)
    {
        if ($socios == 0) {
            $joinso = '';
            $whereso = '';
        } else {
            $joinso = 'inner join venta_detalle as vd on vd.id_venta=v.id_venta
                    inner join productos as pro on pro.productoid=vd.id_producto';
            $whereso = 'pro.iddepartamento=' . $socios . ' and ';
        }
        if ($sucursal == 0) {
            $wheres = '';
        } else {
            $wheres = 'v.sucursalid=' . $sucursal . ' and ';
        }
        $strq = "SELECT v.reg, v.id_venta, v.id_cliente, v.id_personal, v.monto_total,v.pagotarjeta,v.efectivo, v.subtotal, vt.id_detalle_venta
                FROM ventas as v
                join venta_detalle as vt on vt.id_venta=v.id_venta
                $joinso
                WHERE $wheres $whereso v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado=2";
        $query = $this->db->query($strq);
        return $query;
    }

    function detaVentaCP($id_detv)
    {
        $this->db->select('vt.*, SUM(cantidad * precio) as totVPC, v.pagotarjeta, v.efectivo');
        $this->db->from('venta_detalle as vt');
        $this->db->join('ventas as v', 'v.id_venta=vt.id_venta');
        $this->db->where('vt.id_detalle_venta', $id_detv);
        $this->db->where('vt.status', 0);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return 0;
        }
    }
    function cortev($inicio, $fin, $sucursal, $socios)
    {
        if ($socios == 0) {
            $joinso = '';
            $whereso = '';
        } else {
            $joinso = '';
            $whereso = 'pr.iddepartamento=' . $socios . ' and ';
        }
        if ($sucursal == 0) {
            $wheres = '';
        } else {
            $wheres = 'v.sucursalid=' . $sucursal . ' and ';
        }
        $strq = "SELECT v.id_venta,concat(p.nombre,' ',p.apellidos) as cajero,  v.reg,concat(pv.nombre,' ',pv.apellidos) as vendedor,
                vd.cantidad,vd.precio,pr.nombre as producto, v.monto_total, v.descuentocant, v.descuento
                FROM ventas as v 
                left join personal as p on v.id_personal=p.personalId 
                join clientes as cl on cl.ClientesId=v.id_cliente
                join venta_detalle as vd on vd.id_venta=v.id_venta
                left join personal as pv on v.vendedor=pv.personalId
                join productos as pr on pr.productoid=vd.id_producto
                $joinso
                WHERE $wheres $whereso v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado=0";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function cortevp($inicio, $fin, $sucursal, $socios)
    { //ventas por vendedores
        if ($socios == 0) {
            $joinso = '';
            $whereso = '';
        } else {
            $joinso = '
                    inner join productos as pro on pro.productoid=vd.id_producto';
            $whereso = 'pro.iddepartamento=' . $socios . ' and ';
        }
        if ($sucursal == 0) {
            $wheres = '';
        } else {
            $wheres = 'v.sucursalid=' . $sucursal . ' and ';
        }
        $strq = "SELECT concat(pv.nombre,' ',pv.apellidos) as vendedor,
                sum(vd.cantidad*vd.precio) as total, sum(v.descuentocant) as desctotal
                FROM ventas as v 
                join venta_detalle as vd on vd.id_venta=v.id_venta
                left join personal as pv on v.vendedor=pv.personalId
                $joinso
                WHERE $wheres $whereso v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado!=1 and vd.status = 1
                GROUP by pv.personalId
                ";

        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function cortets($inicio, $fin, $sucursal, $socios)
    { //corte por socios
        if ($socios == 0) {
            $joinso = '';
            $whereso = '';
        } else {
            $joinso = '';
            $whereso = 'productos.iddepartamento=' . $socios . ' and ';
        }
        if ($sucursal == 0) {
            $wheres = '';
        } else {
            $wheres = 'v.sucursalid=' . $sucursal . ' and ';
        }
        $strq = "SELECT departamento.iddepartamento,suc.nombre as sucursal, departamento.nombre as depto,
                sum(vd.cantidad*vd.precio) as total, v.descuentocant, v.subtotal, sum(v.descuentocant) as desctotal
                FROM ventas as v 
                
                join venta_detalle as vd on vd.id_venta=v.id_venta
                join sucursal as suc on suc.idsucursal=v.sucursalid
                join productos on productos.productoid=vd.id_producto
                join departamento on departamento.iddepartamento=productos.iddepartamento
    
                WHERE $wheres $whereso v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado!=1 and vd.status=1
                GROUP by departamento.iddepartamento";

        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function cortets2($inicio, $fin, $sucursal, $socios)
    {
        if ($socios == 0) {
            $joinso = '';
            $whereso = '';
        } else {
            $joinso = '
                    inner join productos as pro on pro.productoid=vd.id_producto';
            $whereso = 'pro.iddepartamento=' . $socios . ' and ';
        }
        if ($sucursal == 0) {
            $wheres = '';
        } else {
            $wheres = 'v.sucursalid=' . $sucursal . ' and ';
        }
        $strq = "SELECT sum(vd.monto_total) as total
                FROM ventas as v 
                WHERE $wheres $whereso v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado=0
               ";

        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function cortegastos($inicio, $fin, $sucursal)
    {
        if ($sucursal == 0) {
            $wheres = '';
        } else {
            $wheres = 'gastos.id_sucursal=' . $sucursal . ' and ';
        }
        $strq = "SELECT sum(gastos.monto) as total, sucursal.nombre as sucursal
                FROM gastos 
                LEFT JOIN sucursal on sucursal.idsucursal=gastos.id_sucursal 
                WHERE $wheres fecha between '$inicio 00:00:00' and '$fin 23:59:59' and gastos.status=1
                GROUP BY sucursal.idsucursal";

        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function cortetc($inicio, $fin, $cajero)
    { //corte - ventas por cajero
        if ($cajero == 0) {
            $wheres = '';
        } else {
            $wheres = 'v.id_personal=' . $cajero . ' and ';
        }
        $strq = "SELECT concat(pv.nombre,' ',pv.apellidos) as vendedor, v.monto_total, suc.nombre as sucursal,
                personal.nombre as cajero, sum(vd.cantidad*vd.precio) as total, v.descuentocant, sum(v.descuentocant) as desctotal
                FROM ventas as v 
                left join personal as pv on v.vendedor=pv.personalId
                join venta_detalle as vd on vd.id_venta=v.id_venta
                join sucursal as suc on suc.idsucursal=v.sucursalid
                join productos on productos.productoid=vd.id_producto
                join departamento on departamento.iddepartamento=productos.iddepartamento
                join personal on personal.personalId=v.id_personal

                WHERE $wheres v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado!=1 AND vd.status = 1
                GROUP by personal.personalId";

        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function cortecancel($inicio, $fin, $sucursal, $socios)
    {
        if ($socios == 0) {
            $whereso = '';
        }
        if ($sucursal == 0) {
            $wheres = '';
        }/*else{
            $whereso='productos.iddepartamento='.$socios.'';
        }*/
        if ($sucursal != 0 && $socios != 0) {
            $wheres = 'and v.sucursalid=' . $sucursal . ' and';
            $whereso = 'productos.iddepartamento=' . $socios . '';
        }
        if ($sucursal == 0 && $socios != 0) {
            //$wheres='';
            $whereso = 'and productos.iddepartamento=' . $socios . '';
        }
        if ($sucursal != 0 && $socios == 0) {
            $wheres = 'and v.sucursalid=' . $sucursal . ' ';
            //$whereso='';
        }

        $strq = "SELECT * from(
            SELECT v.*, concat(pv.nombre,' ',pv.apellidos) as personal, usuarios.Usuario as cajero,
            sum(vd.cantidad*vd.precio) as total, productos.codigo as prod, vd.cantidad as cant
                FROM ventas as v 
                left join personal as pv on v.vendedor=pv.personalId
                join venta_detalle as vd on vd.id_venta=v.id_venta
                left join productos on productos.productoid=vd.id_producto
                /*left join personal on personal.personalId=v.id_personal*/
                left join usuarios on usuarios.UsuarioID=v.cancela_personal
                WHERE v.hcancelacion between '$inicio 00:00:00' and '$fin 23:59:59' and vd.status=0 $wheres $whereso GROUP BY vd.id_detalle_venta
            ) as datos WHERE cancelado=2 or cancelado=1";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function cortevmm($inicio, $fin, $sucursal, $socios, $tipo)
    {
        if ($socios == 0) {
            $joinso = '';
            $whereso = '';
        } else {
            $joinso = '';
            $whereso = 'pr.iddepartamento=' . $socios . ' and ';
        }
        if ($sucursal == 0) {
            $wheres = '';
        } else {
            $wheres = 'v.sucursalid=' . $sucursal . ' and ';
        }
        if ($tipo == 1) {
            $orden = 'DESC';
        } else {
            $orden = 'ASC';
        }
        $strq = "SELECT pr.nombre as producto,sum(vd.cantidad) as cantidad
                FROM ventas as v 
                join clientes as cl on cl.ClientesId=v.id_cliente
                join venta_detalle as vd on vd.id_venta=v.id_venta
                join productos as pr on pr.productoid=vd.id_producto

                $joinso

                WHERE $wheres $whereso v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado=0
                GROUP by vd.id_producto
                ORDER BY `cantidad` $orden
                /*limit 10*/
                ";
        //log_message('error','checar'.$strq);
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function corteEgresosCajero($inicio, $fin, $cajero){
        if ($cajero == 0) {
            $wheres = '';
        } else {
            $wheres = 'gastos.id_usuario=' . $cajero . ' and ';
        }
        $strq = "SELECT concat(p.nombre,' ',p.apellidos) as cajero, sum(monto) as total
                FROM gastos 
                left join usuarios u on u.UsuarioID=gastos.id_usuario
                left join personal p on p.personalId=u.personalId
                WHERE $wheres fecha between '$inicio 00:00:00' and '$fin 23:59:59' and status=1
                GROUP by gastos.id_usuario";

        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function consultarturnoname($inicio, $fin)
    {
        $strq = "SELECT * FROM turno where fecha>='$inicio' and fechacierre<='$fin'";
        $query = $this->db->query($strq);
        $this->db->close();
        //$nombre=$cfecha.'/'.$chora;
        return $query;
    }
    /*
    function cortesum($inicio,$fin,$sucursal){
        if ($sucursal==0) {
            $where='';
        }else{
            $where='sucursalid='.$sucursal.' and ';
        }
        $strq = "SELECT sum(monto_total) as total , sum(subtotal) as subtotal, sum(descuentocant) as descuento 
                FROM ventas 
                where $where reg between '$inicio 00:00:00' and '$fin 23:59:59' and cancelado=0";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }*/
    function filas()
    {
        $strq = "SELECT COUNT(*) as total FROM ventas";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total = $row->total;
        }
        return $total;
    }
    function total_paginados($por_pagina, $segmento)
    {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento != '') {
            $segmento = ',' . $segmento;
        } else {
            $segmento = '';
        }
        $strq = "SELECT ven.id_venta,ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,ven.metodo,ven.subtotal, ven.monto_total,ven.cancelado, suc.nombre as sucursal
                FROM ventas as ven 
                inner join personal as per on per.personalId=ven.id_personal
                inner join clientes as cli on cli.ClientesId=ven.id_cliente
                inner join sucursal as suc on suc.idsucursal=ven.sucursalid
                /*inner join venta_detalle as vt on vt.id_venta=ven.id_venta
                inner join productos as prods on prods.productoid=vt.id_producto*/
                ORDER BY ven.id_venta DESC

                LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function detallesVentas($id)
    {

        $strq = "SELECT ven.id_venta, ven.sucursalid, ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,ven.metodo,ven.subtotal, ven.monto_total,ven.cancelado, suc.nombre as sucursal, vt.id_detalle_venta, vt.cantidad, vt.precio, vt.id_producto as id_productovd, vt.status as status_vd, ven.facturado,
            prods.codigo, prods.descripcion, prods.nombre
                FROM ventas as ven 
                inner join personal as per on per.personalId=ven.id_personal
                inner join clientes as cli on cli.ClientesId=ven.id_cliente
                inner join sucursal as suc on suc.idsucursal=ven.sucursalid
                inner join venta_detalle as vt on vt.id_venta=ven.id_venta
                inner join productos as prods on prods.productoid=vt.id_producto
                where ven.id_venta=$id and vt.status=1
                group by vt.id_detalle_venta
                ORDER BY ven.id_venta DESC";

        $query = $this->db->query($strq);
        return $query;
    }

    function detallesVentasId($id)
    {
        $strq = "SELECT *
                FROM venta_detalle 
                inner join ventas as ven on ven.id_venta=venta_detalle.id_venta
                where venta_detalle.id_detalle_venta=$id";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }


    function filastur()
    {
        $strq = "SELECT COUNT(*) as total FROM turno";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total = $row->total;
        }
        return $total;
    }
    function cancelarventa($id, $personal)
    {
        $fecha = date('Y-m-d H:i:s');
        $strq = "UPDATE ventas SET cancelado=1,cancela_personal='$personal',hcancelacion='$fecha' WHERE id_venta=$id";
        $query = $this->db->query($strq);
        //$this->db->close();
    }
    function cancelarventa2($id, $personal)
    {
        $fecha = date('Y-m-d H:i:s');
        $strq = "UPDATE ventas SET cancelado=2,cancela_personal='$personal',hcancelacion='$fecha' WHERE `id_venta`=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function ventadetalles($id)
    {
        $strq = "SELECT * FROM venta_detalle
        inner join ventas as ven on ven.id_venta=venta_detalle.id_venta
        where venta_detalle.id_venta=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function regresarpro($id, $can, $id_suc)
    {
        $strq = "UPDATE productos set stock=stock+$can where productoid=$id and idsucursal=$id_suc";
        $query = $this->db->query($strq);
        $this->db->close();
    }

    function regresarpro2($id, $can, $id_suc)
    {
        $sql = "UPDATE productos_sucursales SET existencia=existencia+$can WHERE idproducto=$id and idsucursal=$id_suc";
        return $this->db->query($sql);
    }
    function filasturnos()
    {
        $strq = "SELECT COUNT(*) as total FROM turno";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total = $row->total;
        }
        return $total;
    }
    function total_paginadosturnos($por_pagina, $segmento)
    {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento != '') {
            $segmento = ',' . $segmento;
        } else {
            $segmento = '';
        }
        $strq = "SELECT * FROM turno ORDER BY id DESC
                LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function consultarturno($id)
    {
        $strq = "SELECT * FROM turno where id=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function consultartotalturno($fecha, $horaa, $horac, $fechac)
    {
        $strq = "SELECT sum(monto_total) as total FROM ventas where reg between '$fecha $horaa' and '$fechac $horac' and cancelado=0";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total = $row->total;
        }
        return $total;
    }
    function consultartotalturno2($fecha, $horaa, $horac, $fechac)
    {
        $strq = "SELECT sum(p.preciocompra*vd.cantidad) as preciocompra 
                from venta_detalle as vd
                inner join productos as p on vd.id_producto=p.productoid
                inner join ventas as v on vd.id_venta=v.id_venta
                where v.cancelado=0 and v.reg>='$fecha $horaa' and v.reg<='$fechac $horac'";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total = $row->preciocompra;
        }
        return $total;
    }
    function consultartotalturnopro($fecha, $horaa, $horac, $fechac)
    {
        $strq = "SELECT p.nombre as producto,vd.cantidad, vd.precio
                from venta_detalle as vd
                inner join productos as p on vd.id_producto=p.productoid
                inner join ventas as v on vd.id_venta=v.id_venta
                where v.cancelado=0 and v.reg>='$fecha $horaa' and v.reg<='$fechac $horac'";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function consultartotalturnopromas($fecha, $horaa, $horac, $fechac)
    {
        $strq = "SELECT p.nombre as producto, sum(vd.cantidad) as total 
                from venta_detalle as vd 
                inner join productos as p on vd.id_producto=p.productoid 
                inner join ventas as v on vd.id_venta=v.id_venta 
                where v.cancelado=0 and v.reg>='$fecha $horaa' and v.reg<='$fechac $horac' GROUP BY producto ORDER BY `total` DESC LIMIT 1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function filaslcompras()
    {
        $strq = "SELECT COUNT(*) as total FROM compra_detalle";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total = $row->total;
        }
        return $total;
    }
    function total_paginadoslcompras($por_pagina, $segmento)
    {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento != '') {
            $segmento = ',' . $segmento;
        } else {
            $segmento = '';
        }
        $strq = "SELECT compdll.id_detalle_compra,comp.reg,pro.nombre as producto,prov.razon_social,compdll.cantidad,compdll.precio_compra 
                FROM compra_detalle as compdll 
                inner join compras as comp on comp.id_compra=compdll.id_compra 
                inner join productos as pro on pro.productoid=compdll.id_producto 
                inner join proveedores as prov on prov.id_proveedor=comp.id_proveedor 
                ORDER BY compdll.id_detalle_compra DESC
                LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function lcomprasconsultar($inicio, $fin)
    {

        $strq = "SELECT compdll.id_detalle_compra,comp.reg,pro.nombre as producto,prov.razon_social,compdll.cantidad,compdll.precio_compra 
                FROM compra_detalle as compdll 
                inner join compras as comp on comp.id_compra=compdll.id_compra 
                inner join productos as pro on pro.productoid=compdll.id_producto 
                inner join proveedores as prov on prov.id_proveedor=comp.id_proveedor 
                where comp.reg>='$inicio 00:00:00' and comp.reg<='$fin 23:59:59'
                ORDER BY compdll.id_detalle_compra DESC";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function ventassearch($search)
    {
        $strq = "SELECT ven.id_venta,ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,ven.metodo,ven.subtotal, ven.monto_total,ven.cancelado, suc.nombre as sucursal
                FROM ventas as ven 
                inner join personal as per on per.personalId=ven.id_personal
                inner join clientes as cli on cli.ClientesId=ven.id_cliente
                inner join sucursal as suc on suc.idsucursal=ven.sucursalid
                where ven.id_venta like '%" . $search . "%' or 
                      ven.reg like '%" . $search . "%' or
                      cli.Nom like '%" . $search . "%' or
                      suc.nombre like '%" . $search . "%' or
                      per.nombre like '%" . $search . "%' or
                      ven.monto_total like '%" . $search . "%'
                ";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function getventasMax()
    {
        $strq = "SELECT MAX(id_venta) as id_venta
        
        FROM ventas 

        ";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    public function cancelaParcial($id)
    {
        $this->db->where('id_detalle_venta', $id);
        $this->db->set('status', 0);
        $this->db->update('venta_detalle');
    }

    public function cancelaParcial2($id)
    {
        $this->db->where('id_venta', $id);
        $this->db->set('status', 0);
        $this->db->update('venta_detalle');
    }

    public function update_stock($id_prod, $cantidad, $id_suc)
    {
        $sql = "UPDATE productos_sucursales SET existencia=existencia+$cantidad WHERE idproducto=$id_prod and idsucursal=$id_suc";
        return $this->db->query($sql);
    }

    function obtenerPrecioProd($id, $idsucu)
    {
        /*$this->db->select('precio_venta');
        $this->db->from('productos_sucursales');
        $this->db->where('idproducto', $id);
        $this->db->where('idsucursal',$idsucu);*/

        $srq = "SELECT precio_venta FROM productos_sucursales WHERE idproducto=$id AND idsucursal=$idsucu";

        //$query = $this->db->get();
        //return $query->result_array();
        $query = $this->db->query($srq);
        return $query->result();
        /*$query = $this->db->get();
        return $query;*/
    }

    function getVentasGral($id)
    {
        $strq = "SELECT v.*, cli.*, cli.Nom as cliente_name, concat(p.nombre,' ',p.apellidos) as vendedor FROM ventas as v
        left join clientes as cli on cli.ClientesId=v.id_cliente
        left join personal as p on p.personalId=v.id_personal
        where id_venta=$id";
        $query = $this->db->query($strq);
        return $query->row();
    }

    public function getseleclikeunidadsat($search)
    {
        $strq = "SELECT * from f_unidades WHERE activo=1 and (Clave like '%" . $search . "%' or nombre like '%" . $search . "%' )";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    public function getseleclikeconceptosa($search)
    {
        $strq = "SELECT * from f_servicios WHERE activo=1 and (Clave like '%" . $search . "%' or nombre like '%" . $search . "%' )";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    public function getRFC($id)
    {
        $this->db->select("rfcdf");
        $this->db->from('clientes');
        $this->db->where('ClientesId', $id);
        $query = $this->db->get();
        return $query->row();
    }
    function ultimoFolio()
    {
        $strq = "SELECT Folio FROM f_facturas where activo=1 and Folio!=0 ORDER BY FacturasId DESC limit 1";
        $Folio = 0;
        $query = $this->db->query($strq);
        foreach ($query->result() as $row) {
            $Folio = $row->Folio;
        }
        return $Folio;
    }

    function facturadetalle($facturaId)
    {
        $strq = "SELECT * FROM `f_facturas_servicios` as fas 
                inner join f_unidades as uni on uni.Clave=fas.Unidad
                WHERE fas.FacturasId=" . $facturaId;
        $query = $this->db->query($strq);
        return $query;
    }

    function get_data($params)
    {
        $columns = array(
            0 => 'ven.id_venta',
            1 => 'ven.reg',
            2 => 'cli.Nom as cliente',
            3 => 'concat(per.nombre," ",per.apellidos) as vendedor',
            4 => 'ven.metodo',
            5 => 'ven.subtotal',
            6 => 'ven.monto_total',
            7 => 'ven.cancelado',
            8 => 'suc.nombre as sucursal',
        );

        $select = "";
        foreach ($columns as $c) {
            $select .= "$c, ";
        }

        $columns2 = array( 
            0 => 'ven.id_venta',
            1 => 'ven.reg',
            2 => 'cli.Nom',
            3 => 'per.nombre',
            4 => 'ven.metodo',
            5 => 'ven.subtotal',
            6 => 'ven.monto_total',
            7 => 'ven.cancelado',
            8 => 'suc.nombre',
            9 => 'per.apellidos',
        );


        $this->db->select($select);
        $this->db->from('ventas ven ');
        $this->db->join('personal per','per.personalId = ven.vendedor');
        $this->db->join('clientes cli','cli.ClientesId = ven.id_cliente');
        $this->db->join('sucursal suc','suc.idsucursal = ven.sucursalid');
        //$this->db->join('venta_detalle vt','vt.id_venta = ven.id_venta');
        //$this->db->join('productos prods','prods.productoid = vt.id_producto');

        //$this->db->where('ven.cancelado = 0');
        if($params['metodo']==6){
            $this->db->where('ven.metodo = 6');
        }
        if (!empty($params['search']['value'])) {
            $search = $params['search']['value'];
            $this->db->group_start();
            foreach ($columns2 as $c) {
                $this->db->or_like($c, $search);
            }
            $this->db->group_end();
        }
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'], $params['start']);
        $query = $this->db->get();
        return $query;
    }


    public function total_data($params)
    {
        $columns2 = array( 
            0 => 'ven.id_venta',
            1 => 'ven.reg',
            2 => 'cli.Nom',
            3 => 'per.nombre',
            4 => 'ven.metodo',
            5 => 'ven.subtotal',
            6 => 'ven.monto_total',
            7 => 'ven.cancelado',
            8 => 'suc.nombre',
            9 => 'per.apellidos',
        );

        $this->db->select('COUNT(1) as total');
        $this->db->from('ventas ven ');
        $this->db->join('personal per','per.personalId = ven.id_personal');
        $this->db->join('clientes cli','cli.ClientesId = ven.id_cliente');
        $this->db->join('sucursal suc','suc.idsucursal = ven.sucursalid');
        //$this->db->join('venta_detalle vt','vt.id_venta = ven.id_venta');
        //$this->db->join('productos prods','prods.productoid = vt.id_producto');

        //$this->db->where('ven.cancelado = 0');
        if($params['metodo']==6){
            $this->db->where('ven.metodo = 6');
        }
        if (!empty($params['search']['value'])) {
            $search = $params['search']['value'];
            $this->db->group_start();
            foreach ($columns2 as $c) {
                $this->db->or_like($c, $search);
            }
            $this->db->group_end();
        }
        $query = $this->db->get();
        return $query->row()->total;
    }

    public function totalPagosP($fechaIni,$fechaFin,$sucursal){
        //$this->db->select('IFNULL(SUM(cantidadHoras * pagoHora), 0) AS total');
        $this->db->select('IFNULL(SUM(pagoTotal), 0) AS total');
        $this->db->from('bitacora_pago');
        $this->db->where('estatus', 1);
        $this->db->where('fechaIni >=', $fechaIni);
        $this->db->where('fechaFin <=', $fechaFin);
    
        $query = $this->db->get();
        return $query->row()->total;
    }

    public function cortePagosP($fechaIni,$fechaFin,$sucursal){
        $this->db->select('bp.*,CONCAT(per.nombre,per.apellidos) as empleado');
        $this->db->from('bitacora_pago bp');
        $this->db->join('personal per','per.personalId = bp.empleadoId');
        $this->db->where('bp.estatus', 1);
        $this->db->where('bp.fechaIni >=', $fechaIni);
        $this->db->where('bp.fechaFin <=', $fechaFin);
    
        $query = $this->db->get();
        return $query;
    }


    public function checkStock($id,$sucursal){
        $this->db->select('IFNULL(existencia,0) AS existencia');
        $this->db->from('productos_sucursales');
        $this->db->where('idproducto', $id);
        $this->db->where('idsucursal', $sucursal);
        
        $query = $this->db->get();
        return $query->row()->existencia;
    }


    public function getVentaCancelPar($id){
        $this->db->select('vd.id_detalle_venta, vd.id_producto , v.sucursalid');
        $this->db->from('venta_detalle vd');
        $this->db->join('ventas v','v.id_venta = vd.id_venta');
        $this->db->where('vd.id_venta', $id);
        $this->db->where('vd.status', 1);
        
        $query = $this->db->get();
        return $query->result();
    }

}
