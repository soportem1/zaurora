<?php
$a = session_id();
if (empty($a)) session_start();
defined('BASEPATH') or exit('No direct script access allowed');

class ModeloKardex extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }


    function userAllSearch($usu)
    {
        $this->db->select('p.nombre, p.apellidos, u.Usuario, u.UsuarioID');
        $this->db->from('usuarios u');
        $this->db->join('personal p', 'p.personalId = u.personalId', 'left');
        $this->db->where('p.estatus', '1');
        $this->db->group_start();
        $this->db->like('u.Usuario', $usu);
        $this->db->or_like('p.nombre', $usu);
        $this->db->or_like('p.apellidos', $usu);
        $this->db->group_end();
        $this->db->order_by('u.Usuario', 'ASC');

        $query = $this->db->get();
        return $query;
    }


    function personalAllSearch($search)
    {
        $this->db->select('p.personalId, p.nombre, p.apellidos');
        $this->db->from('personal p');
        $this->db->where('p.estatus', '1');
        $this->db->group_start();
        $this->db->like('p.nombre', $search);
        $this->db->or_like('p.apellidos', $search);
        $this->db->group_end();
        $this->db->order_by('p.nombre', 'ASC');

        $query = $this->db->get();
        return $query;
    }



    //GET DATA------------------->
    function get_result($params)
    {

        $ventas_query = $this->build_ventas_query($params);
        $compras_query = $this->build_compras_query($params);
        $devolucion_query = $this->build_devolucion_query($params);
        $eliminacion_query = $this->build_eliminacion_query($params);
        $edicion_query = $this->build_edicion_query($params);

        $columns_search = array(
            0 => 'fecha',            // Fecha
            1 => 'hora',             // Hora
            2 => 'personal',         // Personal
            3 => 'codigo',           // Código
            4 => 'descripcion',      // Descripción
            4 => 'sucursal',      // Sucursal
            5 => 'cantInicial',      // Cantidad inicial
            6 => 'tipo',             // Tipo (venta o compra)
            7 => 'cantidad',         // Cantidad
            8 => 'stock',            // Stock
        );

        //$union_query = $this->db->query("($ventas_query) UNION ($compras_query)");
        //$union_query = $this->db->query("($ventas_query) UNION ($compras_query) ORDER BY {$columns_search[$params['order'][0]['column']]} {$params['order'][0]['dir']}");
        //$union_query = $this->db->query("($ventas_query) UNION ($compras_query) ORDER BY {$columns_search[$params['order'][0]['column']]} {$params['order'][0]['dir']} LIMIT {$params['start']}, {$params['length']}");

        if (!empty($params['tipo'])) {

            $union_query = $this->db->query(
                "SELECT * 
                FROM (
                    $ventas_query
                    UNION
                    $compras_query
                    UNION
                    $devolucion_query
                    UNION
                    $eliminacion_query
                    UNION
                    $edicion_query
                ) AS resultados
                WHERE tipo = '" . $params['tipo'] . "'
                ORDER BY {$columns_search[$params['order'][0]['column']]} {$params['order'][0]['dir']} 
                LIMIT {$params['start']}, {$params['length']}"
            );
        } else {
            $union_query = $this->db->query(
                "($ventas_query) UNION ($compras_query) UNION ($devolucion_query) UNION ($eliminacion_query) UNION ($edicion_query) 
                ORDER BY {$columns_search[$params['order'][0]['column']]} {$params['order'][0]['dir']} 
                LIMIT {$params['start']}, {$params['length']}"
            );
        }

        return $union_query->result();
    }




    private function build_ventas_query($params)
    {
        $columns = array(
            0 => 'v.reg AS fecha',
            1 => 'TIME(v.reg) AS hora',
            2 => 'CONCAT(ps.nombre, ps.apellidos) AS personal',
            3 => 'p.codigo',
            4 => 'p.nombre as descripcion',
            5 => 's.nombre as sucursal',
            6 => 'vd.cantInicial',
            7 => '"1" AS tipo',
            8 => 'vd.cantidad',
            9 => '(vd.cantInicial - vd.cantidad) AS stock',

            10 => 'v.id_venta',
        );

        $select = "";
        foreach ($columns as $c) {
            $select .= "$c, ";
        }

        $columns2 = array(
            0 => 'DATE(v.reg)',
            1 => 'TIME(v.reg)',
            2 => 'CONCAT(ps.nombre, ps.apellidos)',
            3 => 'p.codigo',
            4 => 'p.nombre',
            5 => 's.nombre',
            6 => 'vd.cantInicial',
            7 => '"1"',
            8 => 'vd.cantidad',
            9 => '(vd.cantInicial - vd.cantidad)',

            10 => 'v.id_venta',
        );

        $this->db->select($select);
        $this->db->from('ventas v');
        $this->db->join('venta_detalle vd', 'vd.id_venta = v.id_venta', 'left');
        $this->db->join('productos p', 'p.productoid = vd.id_producto', 'left');
        $this->db->join('personal ps', 'ps.personalId = v.id_personal', 'left');
        $this->db->join('sucursal s', 's.idsucursal = vd.sucursalid', 'left');
        //$this->db->where('v.cancelado', '0');

        $this->apply_filters($params, 'v', $columns2);

        return $this->db->get_compiled_select();
    }


    private function build_compras_query($params)
    {
        $columns = array(
            0 => 'c.reg AS fecha',
            1 => 'TIME(c.reg) AS hora',
            2 => 'CONCAT(ps.nombre, ps.apellidos) AS personal',
            3 => 'p.codigo',
            4 => 'p.nombre as descripcion',
            5 => 's.nombre as sucursal',
            6 => 'cd.cantInicial',
            7 => '"2" AS tipo',
            8 => 'cd.cantidad',
            9 => '(cd.cantInicial + cd.cantidad) AS stock',

            10 => 'c.id_compra',
        );

        $select = "";
        foreach ($columns as $c) {
            $select .= "$c, ";
        }

        $columns2 = array(
            0 => 'DATE(c.reg)',
            1 => 'TIME(c.reg)',
            2 => 'CONCAT(ps.nombre, ps.apellidos)',
            3 => 'p.codigo',
            4 => 'p.nombre',
            5 => 's.nombre',
            6 => 'cd.cantInicial',
            7 => '"2"',
            8 => 'cd.cantidad',
            9 => '(cd.cantInicial + cd.cantidad)',

            10 => 'c.id_compra',
        );

        $this->db->select($select);
        $this->db->from('compras c');
        $this->db->join('compra_detalle cd', 'cd.id_compra = c.id_compra', 'left');
        $this->db->join('productos p', 'p.productoid = cd.id_producto', 'left');
        $this->db->join('personal ps', 'ps.personalId = c.id_personal', 'left');
        $this->db->join('sucursal s', 's.idsucursal = cd.idsucursal', 'left');

        $this->apply_filters($params, 'c', $columns2);
        return $this->db->get_compiled_select();
    }

    private function build_devolucion_query($params)//Parcial
    {
        $columns = array(
            0 => 'vd.hcancelacion AS fecha',
            1 => 'TIME(vd.hcancelacion) AS hora',
            2 => 'CONCAT(ps.nombre, ps.apellidos) AS personal',
            3 => 'p.codigo',
            4 => 'p.nombre as descripcion',
            5 => 's.nombre as sucursal',
            6 => '(vd.cantIniCancel) AS cantInicial',
            7 => '"3" AS tipo',
            8 => 'vd.cantidad',
            9 => '(vd.cantIniCancel + vd.cantidad) AS stock',

            10 => 'vc.id_venta',
        );

        $select = "";
        foreach ($columns as $c) {
            $select .= "$c, ";
        }

        $columns2 = array(
            0 => 'DATE(vd.hcancelacion)',
            1 => 'TIME(vd.hcancelacion)',
            2 => 'CONCAT(ps.nombre, ps.apellidos)',
            3 => 'p.codigo',
            4 => 'p.nombre',
            5 => 's.nombre',
            6 => 'vd.cantIniCancel',
            7 => '"3"',
            8 => 'vd.cantidad',
            9 => '(vd.cantIniCancel + vd.cantidad)',

            10 => 'vc.id_venta',
        );

        $this->db->select($select);
        $this->db->from('ventas vc');
        $this->db->join('venta_detalle vd', 'vd.id_venta = vc.id_venta', 'left');
        $this->db->join('productos p', 'p.productoid = vd.id_producto', 'left');
        $this->db->join('personal ps', 'ps.personalId = vc.cancela_personal', 'left');
        $this->db->join('sucursal s', 's.idsucursal = vd.sucursalid', 'left');
        //$this->db->where('vc.cancelado', '2');
        $this->db->where('vc.cancelado >', '0');
        $this->db->where('vd.status', '0');

        $this->apply_filters($params, 'vc', $columns2);

        return $this->db->get_compiled_select();
    }

    private function build_eliminacion_query($params)
    {
        $columns = array(
            0 => 'bd.reg AS fecha',
            1 => 'TIME(bd.reg) AS hora',
            2 => 'CONCAT(ps.nombre, ps.apellidos) AS personal',
            3 => 'p.codigo',
            4 => 'p.nombre as descripcion',
            5 => 's.nombre as sucursal',
            6 => 'bd.cantInicial',
            7 => '"4" AS tipo',
            8 => 'bd.cantidad',
            9 => '(bd.cantInicial - bd.cantidad) AS stock',

            10 => 'bd.id',
        );

        $select = "";
        foreach ($columns as $c) {
            $select .= "$c, ";
        }

        $columns2 = array(
            0 => 'DATE(bd.reg)',
            1 => 'TIME(bd.reg)',
            2 => 'CONCAT(ps.nombre, ps.apellidos)',
            3 => 'p.codigo',
            4 => 'p.nombre',
            5 => 's.nombre',
            6 => 'bd.cantInicial',
            7 => '"4"',
            8 => 'bd.cantidad',
            9 => '(bd.cantInicial - bd.cantidad)',

            10 => 'bd.id',
        );

        $this->db->select($select);
        $this->db->from('bitacora_eliminacion bd');
        $this->db->join('productos p', 'p.productoid = bd.id_producto', 'left');
        $this->db->join('personal ps', 'ps.personalId = bd.id_personal', 'left');
        $this->db->join('sucursal s', 's.idsucursal = bd.id_sucursal', 'left');
        $this->db->where('bd.tipo', 0);

        $this->apply_filters($params, 'bd', $columns2);

        return $this->db->get_compiled_select();
    }

    private function build_edicion_query($params)
    {
        $columns = array(
            0 => 'be.reg AS fecha',
            1 => 'TIME(be.reg) AS hora',
            2 => 'CONCAT(ps.nombre, ps.apellidos) AS personal',
            3 => 'p.codigo',
            4 => 'p.nombre as descripcion',
            5 => 's.nombre as sucursal',
            6 => 'be.cantInicial',
            7 => '"5" AS tipo',
            8 => '(be.cantInicial - be.cantidad) AS cantidad',
            9 => 'be.cantidad AS stock',

            10 => 'be.id',
        );

        $select = "";
        foreach ($columns as $c) {
            $select .= "$c, ";
        }

        $columns2 = array(
            0 => 'DATE(be.reg)',
            1 => 'TIME(be.reg)',
            2 => 'CONCAT(ps.nombre, ps.apellidos)',
            3 => 'p.codigo',
            4 => 'p.nombre',
            5 => 's.nombre',
            6 => 'be.cantInicial',
            7 => '"5"',
            8 => '(be.cantInicial - be.cantidad)',
            9 => 'be.cantidad',

            10 => 'be.id',
        );

        $this->db->select($select);
        $this->db->from('bitacora_eliminacion be');
        $this->db->join('productos p', 'p.productoid = be.id_producto', 'left');
        $this->db->join('personal ps', 'ps.personalId = be.id_personal', 'left');
        $this->db->join('sucursal s', 's.idsucursal = be.id_sucursal', 'left');
        $this->db->where('be.tipo', 1);

        $this->apply_filters($params, 'be', $columns2);

        return $this->db->get_compiled_select();
    }


    private function apply_filters($params, $alias, $columns_search)
    {

        if ($alias == 'vc') {
            if (!empty($params['fInicio']) && !empty($params['fFinal'])) {
                $this->db->where("DATE(" . $alias . ".hcancelacion) >= '" . $this->db->escape_str($params['fInicio']) . "' AND DATE(" . $alias . ".hcancelacion) <= '" . $this->db->escape_str($params['fFinal']) . "'");
            }
        } else {
            if (!empty($params['fInicio']) && !empty($params['fFinal'])) {
                $this->db->where("DATE(" . $alias . ".reg) >= '" . $this->db->escape_str($params['fInicio']) . "' AND DATE(" . $alias . ".reg) <= '" . $this->db->escape_str($params['fFinal']) . "'");
            }
        }


        if (!empty($params['id'])) {
            $this->db->where($alias . '.id_personal', $params['id']);
        }


        if (!empty($params['sucursal'])) {
            if ($alias == 'v') {
                $this->db->where('vd.sucursalid', $params['sucursal']);
            } else if ($alias == 'vc') {
                $this->db->where('vd.sucursalid', $params['sucursal']);
            } else if ($alias == 'c') {
                $this->db->where('c.idsucursal', $params['sucursal']);
            } else if ($alias == 'bd') {
                $this->db->where('bd.id_sucursal', $params['sucursal']);
            }else if ($alias == 'be') {
                $this->db->where('be.id_sucursal', $params['sucursal']);
            }
        }


        if (!empty($params['search']['value'])) {
            $search = $params['search']['value'];
            $this->db->group_start();
            foreach ($columns_search as $c) {
                $this->db->or_like($c, $search);
            }
            $this->db->group_end();
        }

        //$this->db->order_by($columns_search[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'], $params['start']);
    }









    //--->totalS
    function total_result($params)
    {
        $ventas_total_query = $this->build_total_ventas_query($params);
        $compras_total_query = $this->build_total_compras_query($params);
        $devolucion_total_query = $this->build_total_devolucion_query($params);
        $eliminacion_total_query = $this->build_total_eliminacion_query($params);
        $edicion_total_query = $this->build_total_edicion_query($params);


        $union_query = $this->db->query("($ventas_total_query) UNION ($compras_total_query) UNION ($devolucion_total_query) UNION ($eliminacion_total_query) UNION ($edicion_total_query)");


        $total = 0;
        foreach ($union_query->result() as $row) {
            //log_message('error','row: '.json_encode($row));
            if (!empty($params['tipo'])) {
                if($row->tipo == $params['tipo']){
                    $total += $row->total;
                }
            }else{
                $total += $row->total;
            }
            
        }
        return $total;
    }




    private function build_total_ventas_query($params)
    {
        $columns2 = array(
            0 => 'DATE(v.reg)',
            1 => 'TIME(v.reg)',
            2 => 'CONCAT(ps.nombre, ps.apellidos)',
            3 => 'p.codigo',
            4 => 'p.nombre',
            5 => 's.nombre',
            6 => 'vd.cantInicial',
            7 => '"1"',
            8 => 'vd.cantidad',
            9 => '(vd.cantInicial - vd.cantidad)',

            10 => 'v.id_venta',
        );

        $this->db->select('COUNT(1) as total, "1" as tipo');
        $this->db->from('ventas v');
        $this->db->join('venta_detalle vd', 'vd.id_venta = v.id_venta', 'left');
        $this->db->join('productos p', 'p.productoid = vd.id_producto', 'left');
        $this->db->join('personal ps', 'ps.personalId = v.id_personal', 'left');
        $this->db->join('sucursal s', 's.idsucursal = vd.sucursalid', 'left');
        //$this->db->where('v.cancelado', '0');

        $this->apply_total_filters($params, 'v', $columns2);

        return $this->db->get_compiled_select();
    }


    private function build_total_compras_query($params)
    {
        $columns2 = array(
            0 => 'DATE(c.reg)',
            1 => 'TIME(c.reg)',
            2 => 'CONCAT(ps.nombre, ps.apellidos)',
            3 => 'p.codigo',
            4 => 'p.nombre',
            5 => 's.nombre',
            6 => 'cd.cantInicial',
            7 => '"2"',
            8 => 'cd.cantidad',
            9 => '(cd.cantInicial + cd.cantidad)',

            10 => 'c.id_compra',
        );

        $this->db->select('COUNT(1) as total, "2" as tipo');
        $this->db->from('compras c');
        $this->db->join('compra_detalle cd', 'cd.id_compra = c.id_compra', 'left');
        $this->db->join('productos p', 'p.productoid = cd.id_producto', 'left');
        $this->db->join('personal ps', 'ps.personalId = c.id_personal', 'left');
        $this->db->join('sucursal s', 's.idsucursal = cd.idsucursal', 'left');

        $this->apply_total_filters($params, 'c', $columns2);
        return $this->db->get_compiled_select();
    }

    private function build_total_devolucion_query($params)
    {
        $columns2 = array(
            0 => 'DATE(vd.hcancelacion)',
            1 => 'TIME(vd.hcancelacion)',
            2 => 'CONCAT(ps.nombre, ps.apellidos)',
            3 => 'p.codigo',
            4 => 'p.nombre',
            5 => 's.nombre',
            6 => 'vd.cantIniCancel',
            7 => '"3"',
            8 => 'vd.cantidad',
            9 => '(vd.cantIniCancel + vd.cantidad)',

            10 => 'vc.id_venta',
        );

        $this->db->select('COUNT(1) as total, "3" as tipo');
        $this->db->from('ventas vc');
        $this->db->join('venta_detalle vd', 'vd.id_venta = vc.id_venta', 'left');
        $this->db->join('productos p', 'p.productoid = vd.id_producto', 'left');
        $this->db->join('personal ps', 'ps.personalId = vc.cancela_personal', 'left');
        $this->db->join('sucursal s', 's.idsucursal = vd.sucursalid', 'left');
        //$this->db->where('vc.cancelado', '2');
        $this->db->where('vc.cancelado >', '0');
        $this->db->where('vd.status', '0');

        $this->apply_total_filters($params, 'vc', $columns2);

        return $this->db->get_compiled_select();
    }

    private function build_total_eliminacion_query($params)
    {
        $columns2 = array(
            0 => 'DATE(bd.reg)',
            1 => 'TIME(bd.reg)',
            2 => 'CONCAT(ps.nombre, ps.apellidos)',
            3 => 'p.codigo',
            4 => 'p.nombre',
            5 => 's.nombre',
            6 => 'bd.cantInicial',
            7 => '"4"',
            8 => 'bd.cantidad',
            9 => '(bd.cantInicial - bd.cantidad)',

            10 => 'bd.id',
        );

        $this->db->select('COUNT(1) as total, "4" as tipo');
        $this->db->from('bitacora_eliminacion bd');
        $this->db->join('productos p', 'p.productoid = bd.id_producto', 'left');
        $this->db->join('personal ps', 'ps.personalId = bd.id_personal', 'left');
        $this->db->join('sucursal s', 's.idsucursal = bd.id_sucursal', 'left');
        $this->db->where('bd.tipo', 0);

        $this->apply_total_filters($params, 'bd', $columns2);

        return $this->db->get_compiled_select();
    }


    private function build_total_edicion_query($params)
    {
        $columns2 = array(
            0 => 'DATE(be.reg)',
            1 => 'TIME(be.reg)',
            2 => 'CONCAT(ps.nombre, ps.apellidos)',
            3 => 'p.codigo',
            4 => 'p.nombre',
            5 => 's.nombre',
            6 => 'be.cantInicial',
            7 => '"5"',
            8 => '(be.cantInicial - bd.cantidad)',
            9 => 'be.cantidad',

            10 => 'be.id',
        );

        $this->db->select('COUNT(1) as total, "5" as tipo');
        $this->db->from('bitacora_eliminacion be');
        $this->db->join('productos p', 'p.productoid = be.id_producto', 'left');
        $this->db->join('personal ps', 'ps.personalId = be.id_personal', 'left');
        $this->db->join('sucursal s', 's.idsucursal = be.id_sucursal', 'left');
        $this->db->where('be.tipo', 1);

        $this->apply_total_filters($params, 'be', $columns2);

        return $this->db->get_compiled_select();
    }


    private function apply_total_filters($params, $alias, $columns_search)
    {
        if ($alias == 'vc') {
            if (!empty($params['fInicio']) && !empty($params['fFinal'])) {
                $this->db->where("DATE(" . $alias . ".hcancelacion) >= '" . $this->db->escape_str($params['fInicio']) . "' AND DATE(" . $alias . ".hcancelacion) <= '" . $this->db->escape_str($params['fFinal']) . "'");
            }
        } else {
            if (!empty($params['fInicio']) && !empty($params['fFinal'])) {
                $this->db->where("DATE(" . $alias . ".reg) >= '" . $this->db->escape_str($params['fInicio']) . "' AND DATE(" . $alias . ".reg) <= '" . $this->db->escape_str($params['fFinal']) . "'");
            }
        }


        if (!empty($params['id'])) {
            $this->db->where($alias . '.id_personal', $params['id']);
        }

        if (!empty($params['sucursal'])) {
            if ($alias == 'v') {
                $this->db->where('vd.sucursalid', $params['sucursal']);
            } else if ($alias == 'vc') {
                $this->db->where('vd.sucursalid', $params['sucursal']);
            } else if ($alias == 'c') {
                $this->db->where('c.idsucursal', $params['sucursal']);
            } else if ($alias == 'bd') {
                $this->db->where('bd.id_sucursal', $params['sucursal']);
            }else if ($alias == 'be') {
                $this->db->where('be.id_sucursal', $params['sucursal']);
            }
        }

        if (!empty($params['search']['value'])) {
            $search = $params['search']['value'];
            $this->db->group_start();
            foreach ($columns_search as $c) {
                $this->db->or_like($c, $search);
            }
            $this->db->group_end();
        }

    }
}
