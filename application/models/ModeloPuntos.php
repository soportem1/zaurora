<?php
$a = session_id();
if (empty($a)) session_start();
defined('BASEPATH') or exit('No direct script access allowed');

class ModeloPuntos extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getDataPuntos($id, $fIni, $fFin)
    {
        $this->db->select('distinct(v.id_venta), v.reg, v.monto_total, v.puntos_total, v.cancelado, v.porcentaje_puntos, v.descuento, v.en_monedero, v.metodo, v.pagotarjeta,v.efectivo, v.pagopuntos, CONCAT(p.nombre," ",p.apellidos) as vendedor, s.nombre as sucursal');
        $this->db->from('ventas v');
        $this->db->join('personal p', 'p.personalId = v.vendedor');
        $this->db->join('sucursal s', 's.idsucursal = v.sucursalid');
        //$this->db->where('v.cancelado',0);
        $this->db->where('v.id_cliente', $id);
        //$this->db->where("v.reg BETWEEN '" . $this->db->escape_str($fIni) . "' AND '" . $this->db->escape_str($fFin) . "'");
        $this->db->where("DATE(v.reg) >= '" . $this->db->escape_str($fIni) . "' AND DATE(v.reg) <= '" . $this->db->escape_str($fFin) . "'");
        $this->db->where('v.cancelado !=', '1');


        $this->db->order_by('v.id_venta', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }

    function getPuntosVenta($id_cliente, $id_venta, $minFecha)
    {
        $this->db->select('v.puntos_total');
        $this->db->from('ventas v');
        $this->db->where('v.id_cliente', $id_cliente);
        $this->db->where('v.id_venta', $id_venta);
        $this->db->where("DATE(v.reg) >= '" . $this->db->escape_str($minFecha) . "'");
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row()->puntos_total;
        } else {
            return 'E';
        }
    }

    function addPuntosCliente($id_cliente, $puntos)
    {
        $this->db->set('puntos', 'puntos + ' . (float) $puntos, FALSE);
        //$this->db->set('CurrentYPoints', 'CurrentYPoints + ' . (float) $puntos, FALSE);
        $this->db->where('ClientesId', $id_cliente);
        $this->db->update('clientes');
        //return $id;
    }

    function addPuntosClienteAnioAnt($id_cliente, $puntos)
    {
        $this->db->set('LastYPoints', 'LastYPoints + ' . (float) $puntos, FALSE);
        $this->db->where('ClientesId', $id_cliente);
        $this->db->update('clientes');
        //return $id;
    }

    function addPuntosClienteAnioAct($id_cliente, $puntos)
    {
        $this->db->set('CurrentYPoints', 'CurrentYPoints + ' . (float) $puntos, FALSE);
        $this->db->where('ClientesId', $id_cliente);
        $this->db->update('clientes');
        //return $id;
    }

    
    function subPuntosCliente($id_cliente, $puntos)
    {
        $this->db->set('puntos', 'puntos - ' . (float) $puntos, FALSE);
        //$this->db->set('CurrentYPoints', 'CurrentYPoints - ' . (float) $puntos, FALSE);
        $this->db->where('ClientesId', $id_cliente);
        $this->db->update('clientes');
        //return $id;
    }

    function subPuntosClienteAnioAnt($id_cliente, $puntos)
    {
        $this->db->set('LastYPoints', 'LastYPoints - ' . (float) $puntos, FALSE);
        $this->db->where('ClientesId', $id_cliente);
        $this->db->update('clientes');
        //return $id;
    }

    function subPuntosClienteAnioAct($id_cliente, $puntos)
    {
        $this->db->set('CurrentYPoints', 'CurrentYPoints - ' . (float) $puntos, FALSE);
        $this->db->where('ClientesId', $id_cliente);
        $this->db->update('clientes');
        //return $id;
    }

    function subPuntosVenta($id_venta, $puntos)
    {
        $this->db->set('puntos_total', 'puntos_total - ' . (float) $puntos, FALSE);
        $this->db->where('id_venta', $id_venta);
        $this->db->update('ventas');
        //return $id;
    }

    function setPuntosVenta($id_venta, $puntos)
    {
        $this->db->set('puntos_total', (float)$puntos, FALSE);
        $this->db->where('id_venta', $id_venta);
        $this->db->update('ventas');
        //return $id;
    }

    function subPuntosUsados($id_venta, $puntos)
    {
        $this->db->set('pagopuntos', 'pagopuntos - ' . (float) $puntos, FALSE);
        $this->db->where('id_venta', $id_venta);
        $this->db->update('ventas');
        //return $id;
    }

    function getTotalPuntos($id_cliente)
    {
        $this->db->select('c.puntos');
        $this->db->from('clientes c');
        $this->db->where('c.ClientesId', $id_cliente);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row()->puntos;
        } else {
            return 0;
        }
    }

    function checkTotalParcial($id_venta)
    {
        $this->db->select('SUM(vd.precio * vd.cantidad) AS total');
        $this->db->from('venta_detalle vd');
        $this->db->where('vd.id_venta', $id_venta);
        $this->db->where('vd.status', '1');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row()->total;
        } else {
            return '0';
        }
    }

    function getPorcentajePuntos()
    {
        $this->db->select('porcentaje');
        $this->db->from('config_puntos');
        $this->db->where('activo', '1');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row()->porcentaje;
        } else {
            return 0;
        }
    }

    function getDataConfigPuntos()
    {
        $this->db->select('id, porcentaje, checkFecha, fechaIni, fechaFin');
        $this->db->from('config_puntos');
        $this->db->where('activo', '1');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->row();
    }

    function getDataConfigPuntosCliente($id)
    {
        $this->db->select('ClientesId, porcentaje');
        $this->db->from('clientes');
        $this->db->where('activo', '1');
        $this->db->where('ClientesId', $id);
        $this->db->order_by('ClientesId', 'DESC');
        $query = $this->db->get();
        return $query->row();
    }

    function getDataVentaGeneral($id_venta)
    {
        $strq = "SELECT v.id_venta, v.porcentaje_puntos, v.id_cliente, v.descuento, v.metodo, v.monto_total, v.pagopuntos, v.puntos_total, v.cancelado, v.subtotal, vd.precio, vd.cantidad, v.reg
                FROM ventas v
                INNER JOIN venta_detalle vd ON v.id_venta = vd.id_venta
                WHERE vd.status = 1
                AND v.id_venta = $id_venta";

        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function getDataVentaParticular($id_venta, $id_venta_detalle)
    {
        $strq = "SELECT v.id_venta, v.porcentaje_puntos, v.id_cliente, v.descuento, v.metodo, v.monto_total, v.pagopuntos, v.puntos_total, v.cancelado, v.subtotal, vd.precio, vd.cantidad, v.reg
                FROM ventas v
                INNER JOIN venta_detalle vd ON v.id_venta = vd.id_venta
                WHERE vd.status = 1
                AND v.id_venta = $id_venta
                AND vd.id_detalle_venta = $id_venta_detalle";

        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function getSumaPrecios($id){
        $this->db->select('IFNULL(SUM(precio * cantidad), 0) AS total');
        $this->db->from('venta_detalle');
        $this->db->where('id_venta', $id);
        $this->db->where('status', 0);
        $query = $this->db->get();
        $result = $query->row();
        return $result->total;
    }

    function getPuntosGastadosVenta($id_venta){
        $this->db->select('IFNULL(pagopuntos, 0) AS pago');
        $this->db->from('ventas');
        $this->db->where('id_venta', $id_venta);//metodo
        $query = $this->db->get();
        return $query->row()->pago;
    }


    function getAnioCliente($idCliente){
        $this->db->select('IFNULL(anio_puntos, 2024) AS anio');
        $this->db->from('clientes');
        $this->db->where('ClientesId', $idCliente);
        $this->db->where('activo', 1);
        $query = $this->db->get();
        return $query->row()->anio;
    }

    /*
    function setAnioYPuntosCliente($idCliente, $anio){
        $this->db->set('puntos', 0, FALSE);
        $this->db->set('anio_puntos', $anio);    
        $this->db->where('ClientesId', $idCliente);
        $this->db->update('clientes');
        //return $id;
    }
    */

    function setAnioYPuntosCliente($idCliente, $anio){
        $this->db->set('puntos', 'puntos - LastYPoints', FALSE);
        $this->db->set('LastYPoints', 'CurrentYPoints ', FALSE);
        $this->db->set('CurrentYPoints', 0, FALSE);

        $this->db->set('anio_puntos', $anio, FALSE);    
        $this->db->where('ClientesId', $idCliente);
        $this->db->update('clientes');
        //return $id;
    }


    function auxPuntosY2024($idCliente){
        log_message("error","AUXILIAR 2024");
        $this->db->set('CurrentYPoints', 'puntos', FALSE);
        $this->db->where('ClientesId', $idCliente);
        $this->db->where('anio_puntos', "2024");
        $this->db->where('CurrentYPoints', 0);
        $this->db->where('LastYPoints', 0);
        $this->db->update('clientes');
        //return $id;
    }

    function reviewNegativePoints($idCliente){
        log_message("error","Set a 0");
        $this->db->set('puntos', 0, FALSE);
        $this->db->where('ClientesId', $idCliente);
        $this->db->where('puntos <', 0);
        $this->db->update('clientes');
        //return $id;
    }

    //-------------------------------------------------------------------

    function existsCliente($tel) {
        $this->db->select('ClientesId');
        $this->db->from('clientes');
        $this->db->where('activo', 1);
        $this->db->like('telefonoc', $tel);
        $query = $this->db->get();
        return $query->num_rows() > 0;
    }

    function getDataCliente($tel){
        $this->db->select('ClientesId, puntos, anio_puntos');
        $this->db->from('clientes');
        $this->db->where('activo', 1);
        $this->db->where('telefonoc', $tel);
        //$this->db->like('telefonoc', $tel);
        $query = $this->db->get();
        return $query->row();
    }


    //-------------------------------------------------------------------


    function getPuntosClienteAnio($id_cliente){
        $this->db->select('LastYPoints, CurrentYPoints');
        $this->db->from('clientes');
        $this->db->where('ClientesId', $id_cliente);
        //$this->db->where('activo', 1);
        $query = $this->db->get();
        return $query->row();
    }


}
