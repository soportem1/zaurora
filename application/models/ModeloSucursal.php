<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloSucursal extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    public function insertar($contenido)
    {
      $this->db->insert('sucursal', $contenido);
      return $this->db->insert_id();
    }

    public function update($id,$contenido)
    {
        $this->db->where('id', $id);
        $this->db->insert($contenido);
        $this->db->update('sucursal');
    }

      public function verSucursales()
      {
        $this->db->select('*');
        $this->db->from('sucursal');
        $this->db->where('estatus', 1);

        $query = $this->db->get();
        return $query;
      }

      public function getEmpleado()
      {
        $this->db->select('*');
        $this->db->from('usuarios');
        //$this->db->where('estatus', 1);

        $query = $this->db->get();
        return $query;
      }

      public function eliminar($id)
      {
        $this->db->where('id', $id);
        $this->db->set('estatus' , 0);
        $this->db->update('sucursal');
      }

    public function get_table_active(){
        $sql = "SELECT * FROM sucursal
        LEFT JOIN usuarios on usuarios.UsuarioID = sucursal.encargado
        WHERE estatus=1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getSucursal($id){
        $sql = "SELECT * FROM sucursal WHERE estatus=1 and idsucursal=$id";
        $query = $this->db->query($sql);
        return $query;
    }
    function updateCatalogo($data,$idname,$id,$catalogo){
        $this->db->set($data);
        $this->db->where($idname, $id);
        $this->db->update($catalogo);
        return $id;
    }

}
