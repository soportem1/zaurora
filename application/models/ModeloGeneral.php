<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModeloGeneral extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function log_movs($tabla,$data){ // funcion para insertar log de movs realizados dentro del sistema
        $this->db->insert($tabla,$data);   
        return $this->db->insert_id();
    } 


    public function tabla_inserta($tabla,$data){
        $this->db->insert($tabla,$data);   
        return $this->db->insert_id();
    } 

    public function genSelect($tables){
        $this->db->select("*");
        $this->db->from($tables);;
        $query=$this->db->get();
        return $query->result();
    }

    public function getselectwhere($tables,$cols,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($cols,$values);
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    public function getselectwheren($tables,$where){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($where);
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    public function getselectwheren2($tables,$where){
        $this->db->select("*, sucursal.nombre as nomsuc");
        $this->db->from($tables);
        $this->db->join('productos_sucursales','productos_sucursales.idproducto=productos.productoid');
        $this->db->join('sucursal','productos_sucursales.idsucursal=sucursal.idsucursal');
        $this->db->where($where);
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    public function getselectwheren3($tables,$where){
        $this->db->select("*, sucursal.nombre as nomsuc");
        $this->db->from($tables);
        $this->db->join('productos','productos.productoid=productos_sucursales.idproducto');
        $this->db->join('sucursal','productos_sucursales.idsucursal=sucursal.idsucursal');
        $this->db->where($where);
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    function updateCatalogo($data,$idname,$id,$catalogo){
        $this->db->set($data);
        $this->db->where($idname, $id);
        $this->db->update($catalogo);
        return $id;
    }
    function updateCatalogon($data,$where,$catalogo){
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($catalogo);
        //return $id;
    }
    function updateCatalogostock($colm,$masmenos,$value,$catalogo,$where){
        $this->db->set($colm,$colm.$masmenos.$value,FALSE);
        $this->db->where($where);
        $this->db->update($catalogo);
        //return $id;
    }

    public function selectestados(){
        $sql = "SELECT * FROM estado";
        $query = $this->db->query($sql);
        return $query->result();
    } 

    public function producto_existencia($id,$sucursal){
        $strq = "SELECT * FROM productos_sucursales WHERE idproducto =$id and idsucursal=$sucursal";
        $query = $this->db->query($strq);
        $idproductosucursal='';
        $precio_venta='';
        $mayoreo='';
        $cuantos='';
        $existencia='';
        $minimo='';
        foreach ($query->result() as $item) {
            $idproductosucursal=$item->idproductosucursal;
            $precio_venta=$item->precio_venta;
            $mayoreo=$item->mayoreo;
            $cuantos=$item->cuantos;
            $existencia=$item->existencia;
            $minimo=$item->minimo;
        }
        $arrayproductosucursal = array(
                            'idproductosucursal' =>$idproductosucursal,
                            'precio_venta' =>$precio_venta,
                            'mayoreo' =>$mayoreo,
                            'cuantos' =>$cuantos,
                            'existencia' =>$existencia,
                            'minimo' =>$minimo 
                           );
        return $arrayproductosucursal;     
    }

    function searchPersonal($per){
        $strq = "SELECT *
        FROM personal as p
        where estatus=1 and (nombre like '%".$per."%' or 
        apellidos like '%".$per."%')";
        $query = $this->db->query($strq);
        return $query;
    }

    public function getVentasVendedor2($id,$ini,$fin){
        $this->db->select("IFNULL(sum(cantidad),0) as tot_prods, IFNULL(sum(cantidad*precio),0) as tot_venta");
        $this->db->from("venta_detalle vd");
        $this->db->join("ventas v","v.id_venta=vd.id_venta");
        $this->db->where("id_vendedor",$id);
        $this->db->where("status",1);
        $this->db->where("v.reg between '$ini 00:00:00' and '$fin 23:59:59' and v.cancelado!=1");
        $query=$this->db->get();
        return $query->result();
    }

    public function getVentasVendedor($id,$ini,$fin){
        $this->db->select("IFNULL(sum(cantidad),0) as tot_prods, IFNULL(sum(cantidad*precio),0) as tot_venta");
        $this->db->from("venta_detalle vd");
        $this->db->join("ventas v","v.id_venta=vd.id_venta");
        $this->db->where("id_vendedor",$id);
        $this->db->where("status",1);
        $this->db->where("v.reg between '$ini 00:00:00' and '$fin 23:59:59' and v.cancelado!=1");
        $query=$this->db->get();
        return $query->row();
    }

    public function getDescsVentasVendedor($id,$ini,$fin){
        $this->db->select("sum(descuentocant) as tot_descs");
        $this->db->from("ventas v");
        $this->db->where("vendedor",$id);
        $this->db->where("v.reg between '$ini 00:00:00' and '$fin 23:59:59' and v.cancelado!=1");
        $query=$this->db->get();
        return $query->row();
    }

    /*public function getVentasVendedorPDF($id,$ini,$fin){
        $this->db->select("IFNULL(sum(cantidad),0) as tot_prods, IFNULL(sum(cantidad*precio),0) as tot_venta, p.nombre, p.apellidos, p.celular, p.correo,p.salario_dia,p.hora_extra,p.hora_doble,p.hora_dia_feriado,p.comision_venta");
        $this->db->from("venta_detalle vd");
        $this->db->join("ventas v","v.id_venta=vd.id_venta");
        $this->db->join("personal p","p.personalId=vd.id_vendedor");
        $this->db->where("id_vendedor",$id);
        $this->db->where("status",1);
        $this->db->where("v.reg between '$ini 00:00:00' and '$fin 23:59:59' and v.cancelado!=1");
        $query=$this->db->get();
        return $query->result();
    }*/

    public function getDatosVende($id){
        $this->db->select("p.nombre, p.apellidos, p.celular, p.correo,p.salario_dia,p.salario_hora,p.hora_extra,p.hora_doble,p.hora_dia_feriado,p.comision_venta");
        $this->db->from("personal p");
        $this->db->where("personalId",$id);
        $query=$this->db->get();
        return $query->result();
    }

    public function detallesVentas($id,$ini,$fin){
        $this->db->select("p.nombre, vd.cantidad,vd.precio");
        $this->db->from("venta_detalle vd");
        $this->db->join("ventas v","v.id_venta=vd.id_venta");
        $this->db->join("productos p","p.productoid=vd.id_producto");
        $this->db->where("id_vendedor",$id);
        $this->db->where("vd.status",1);
        $this->db->where("v.reg between '$ini 00:00:00' and '$fin 23:59:59'");
        $query=$this->db->get();
        return $query->result();
    }

    public function getTotalBonosDescs($id,$ini,$fin){
        $this->db->select("IFNULL(sum(cant_bono),0) as tot_bonos, IFNULL(sum(cant_desc),0) as tot_descs");
        $this->db->from("bonos_descuentos b");
        $this->db->where("id_empleado",$id);
        $this->db->where("estatus",1);
        $this->db->where("fecha between '$ini 00:00:00' and '$fin 23:59:59'");
        $query=$this->db->get();
        return $query->result();
    }

    public function getTotalNolaborales($id,$ini,$fin){
        $this->db->select("IFNULL(sum(tot_descuento),0) as tot_no_labora");
        $this->db->from("vacaciones_empleado v");
        $this->db->where("id_empleado",$id);
        $this->db->where("estatus",1);
        $this->db->where("(inicio >= '$ini' or fin <= '$fin')");
        $query=$this->db->get();
        return $query->result();
    }

    public function getBonosDescs($id,$ini,$fin){
        $this->db->select("*");
        $this->db->from("bonos_descuentos b");
        $this->db->where("id_empleado",$id);
        $this->db->where("estatus",1);
        if(isset($ini) && $ini>0 && $ini!=""){
            $this->db->where("fecha between '$ini 00:00:00' and '$fin 23:59:59'");
        }
        $query=$this->db->get();
        return $query->result();
    }

    public function getNolaborales($id,$ini,$fin){
        $this->db->select("*");
        $this->db->from("vacaciones_empleado v");
        $this->db->where("id_empleado",$id);
        $this->db->where("estatus",1);
        $this->db->where("(inicio >= '$ini' or fin <= '$fin')");
        $query=$this->db->get();
        return $query->result();
    }

    public function getDataVacaciones($id){
        $this->db->select("v.*, concat(p.nombre, ' ',p.apellidos) as empleado, date_format(inicio,'%d/%m/%Y') as inicio, date_format(fin,'%d/%m/%Y') as fin");
        $this->db->from("vacaciones_empleado v");
        $this->db->join("personal p","p.personalId=v.id_empleado");
        $this->db->where("id_empleado",$id);
        $this->db->where("v.estatus",1);
        //$this->db->where("reg between '$ini 00:00:00' and '$fin 23:59:59'");
        $query=$this->db->get();
        return $query->result();
    }

    public function getDataAsistencias($id){
        $this->db->select("a.id, a.personal_id, a.entrada, a.salida, a.check_in, a.check_out, a.fecha, date_format(a.fecha,'%d/%m/%Y') as fechaF, date_format(a.entrada, '%h:%i %p') as entradaF, date_format(a.salida, '%h:%i %p') as salidaF, concat(p.nombre, ' ',p.apellidos) as empleado");
        $this->db->from("asistencias a");
        $this->db->join("personal p","p.personalId = a.personal_id");
        $this->db->where("a.personal_id",$id);
        $this->db->where("a.estatus",1);
        $this->db->order_by('fecha', 'DESC');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }

    public function checkEntradas($id){
        $this->db->select("MAX(CASE WHEN check_out = 0 THEN 1 ELSE 0 END) AS resultado");
        $this->db->from('asistencias');
        $this->db->where('estatus', 1);
        $this->db->where('personal_id', $id);
    
        $query = $this->db->get();
        $result = $query->row()->resultado;
    
        return $result == null ?  '0' : $result;
    }

    public function checkInOutToday($id, $date) {
        $this->db->select('IFNULL(check_out, 0) AS check_out');
        $this->db->from('asistencias');
        $this->db->where('estatus', 1);
        $this->db->where('personal_id', $id);
        $this->db->where('fecha', $date);
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
    
        $query = $this->db->get();
        return $query->row();
    }


    function getAsistenciaLista($params){
        $columns = array( 
            0=>'a.id',
            1=>'concat(p.nombre," ",p.apellidos) as empleado',
            2=>"date_format(a.fecha,'%d/%m/%Y') as fechaF",
            3=>"date_format(a.entrada, '%h:%i %p') as entradaF",
            4=>"date_format(a.salida, '%h:%i %p') as salidaF",
            5=>'a.fecha',
            6=>'a.entrada',
            7=>'a.salida'
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }

        $this->db->select($select);
        $this->db->from('asistencias a');
        $this->db->join("personal p","p.personalId = a.personal_id");
        //$this->db->where("a.personal_id",$id);
        $this->db->where("a.estatus",1);

        if($params["fIni"]!="0" && $params["fFin"]!="0"){
            $this->db->where('a.fecha BETWEEN '.'"'.$params["fIni"].'"'.' AND '.'"'.$params["fFin"].'"');
        }

        if($params["idP"]!="0"){
            $this->db->where('a.personal_id',$params["idP"]);
        }

        $this->db->order_by('fecha', 'DESC');
        $this->db->order_by('id', 'DESC');

        $query=$this->db->get();
        return $query->result();
    }



    function getHorasLaboradas($idP, $fechaIni, $fechaFin){
        $this->db->select('IFNULL(SUM(GREATEST(0, IFNULL(TIMESTAMPDIFF(HOUR, entrada, salida), 0))), 0) AS total_horas', FALSE);
        $this->db->select('IFNULL(SUM(GREATEST(0, IFNULL(TIMESTAMPDIFF(MINUTE, entrada, salida), 0))), 0) AS total_minutos', FALSE);
        $this->db->from('asistencias');
        $this->db->where('personal_id', $idP);
        $this->db->where('check_in', 1);
        $this->db->where('check_out', 1);
        $this->db->where('estatus', 1);
        $this->db->where('fecha >=', $fechaIni);
        $this->db->where('fecha <=', $fechaFin);
        $query = $this->db->get();
        $result = $query->result();

        // Calcular el total de minutos en función de las horas
        if (!empty($result)) {
            //$total_horas = (int)$result[0]->total_horas;
            $total_minutos = (int)$result[0]->total_minutos;

            // Ajustar horas y minutos
            $result[0]->total_horas = floor($total_minutos / 60);
            $result[0]->total_minutos = $total_minutos % 60;

            return $result;

        } else {
            return $result;
        }
    }

    function checkPagos($idP, $fechaIni, $fechaFin){
        $this->db->select('COUNT(*) AS total');
        $this->db->from('bitacora_pago');
        $this->db->where('estatus', 1);
        $this->db->where('fechaIni', $fechaIni);
        $this->db->where('fechaFin', $fechaFin);
        $this->db->where('empleadoId', $idP);

        $query = $this->db->get();
        return $query->row()->total;
    }

    function log_eliminacion($dataEliminacion){
        $this->db->insert('bitacora_eliminacion',$dataEliminacion);   
        return $this->db->insert_id();
    }

    function kardex_add_devolucion($id_vd, $id_personal, $stock, $reg){
        $data = array(
            'cancela_personal' => $id_personal,
            'cantIniCancel' => $stock,
            'hcancelacion' => $reg
        );

        $this->db->set($data);
        $this->db->where('id_detalle_venta', $id_vd);
        $this->db->update('venta_detalle');
        //return $id;
    }

}