<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Modelofacturas extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getfacturas($params){
        $columns = array( 
            0=>'f.FacturasId',
            1=>'f.Folio',
            2=>'f.Nombre',
            3=>'f.Rfc',
            4=>'f.total',
            5=>'f.Estado',
            6=>'f.fechatimbre',
            7=>'f.rutaXml',
            8=>'f.rutaAcuseCancelacion',
            9=>'id_venta',
            10=>'u.Usuario',
            11=>'f.sellosat',
            12=>'cl.Correo'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('f_facturas f');
        $this->db->join('usuarios u','u.UsuarioID=f.usuario_id','left');
        $this->db->join('clientes cl','cl.ClientesId=f.Clientes_ClientesId','left');

        /*if($params["idcliente"]!=""){
            $this->db->where('facturaabierta',1);
        }*/
        if($params["idcliente"]!=""){
            $this->db->where('f.Clientes_ClientesId',$params["idcliente"]);
        }
        if($params["fecha_fact"]!=""){
            $this->db->where(f.'fechatimbre BETWEEN '.'"'.$params["fecha_fact"].' 00:00:00" '.' AND '.'"'.$params["fecha_fact"].' 23:59:59"');
        }
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function total_facturas($params){
        $columns = array( 
            0=>'FacturasId',
            1=>'Folio',
            2=>'Nombre',
            3=>'Rfc',
            4=>'total',
            5=>'Estado',
            6=>'fechatimbre',
            7=>'rutaXml',
            10=>'u.Usuario',
            11=>'sellosat'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('f_facturas');
        $this->db->join('usuarios u','u.UsuarioID=f_facturas.usuario_id','left');
        
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if($params["idcliente"]!=""){
            $this->db->where('Clientes_ClientesId',$params["idcliente"]);
        }
        if($params["fecha_fact"]!=""){
            $this->db->where('fechatimbre BETWEEN '.'"'.$params["fecha_fact"].' 00:00:00" '.' AND '.'"'.$params["fecha_fact"].' 23:59:59"');
        }
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }

    public function getFactura($idFact){
        $this->db->select('*');
        $this->db->from('f_facturas');
        $this->db->where('FacturasId',$idFact);
        $query = $this->db->get();
        return $query->result();
    }

    function saldocomplemento($factura){
        $strq = "SELECT doc.ImpPagado
                FROM f_complementopago_documento as doc 
                INNER JOIN f_complementopago as com on com.complementoId=doc.complementoId 
                WHERE com.Estado=1 AND doc.facturasId=$factura
                "; 
        $datoscop = $this->db->query($strq);
        $saldo=0;
        $numcomplem=0;
        foreach ($datoscop->result() as $item) {
            //log_message('error', 'validar saldo0: '.$item->ImpPagado);
            $saldo=$saldo+$item->ImpPagado;
            $numcomplem++;
        }
        //log_message('error', 'validar saldo1: '.$saldo);
        return $saldo; 
    }

    function saldocomplementonum($factura){
        $strq = "SELECT doc.ImpPagado
                FROM f_complementopago_documento as doc 
                INNER JOIN f_complementopago as com on com.complementoId=doc.complementoId 
                WHERE com.Estado=1 AND doc.facturasId=$factura
                "; 
        $datoscop = $this->db->query($strq);
        $saldo=0;
        $numcomplem=0;
        foreach ($datoscop->result() as $item) {
            $saldo=$saldo+$item->ImpPagado;
            $numcomplem++;
        }
        return $numcomplem; 
    }
    function documentorelacionado($complemento){
        $strq = "SELECT 
                    compd.facturasId,
                    compd.IdDocumento,
                    compd.NumParcialidad,
                    compd.ImpSaldoAnt,
                    compd.ImpPagado,
                    compd.ImpSaldoInsoluto,
                    compd.MetodoDePagoDR,
                    fac.moneda,
                    fac.Folio,
                    fac.serie,
                    fac.FormaPago
                FROM f_complementopago_documento as compd
                inner join f_facturas as fac on fac.FacturasId=compd.facturasId
                where compd.complementoId=$complemento";
        $query = $this->db->query($strq);
        return $query;
    }



}