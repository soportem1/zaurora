<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloTraspasos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function filas() {
        $strq = "SELECT COUNT(*) as total FROM traspasos";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function listado($params){
        /*
            SELECT tra.traspasoId,pro.nombre,suc_s.nombre as suc_salida,suc_e.nombre as suc_entrada,per.nombre,per.apellidos,tra.cantidad,tra.reg 
            FROM traspasos as tra 

            inner join productos as pro on pro.productoid=tra.productoId
            inner JOIN sucursal as suc_s on suc_s.idsucursal=tra.suc_salida
            inner JOIN sucursal as suc_e on suc_e.idsucursal=tra.suc_entrada
            inner JOIN personal as per on per.personalId=tra.personalId
        */
        $columns = array( 
            0 => 'tra.traspasoId',
            1 => 'pro.nombre',
            2 => 'suc_s.nombre as suc_salida',
            3 => 'suc_e.nombre as suc_entrada',
            4 => "concat(per.nombre,' ',per.apellidos) as personal",
            5 => 'tra.cantidad',
            6 => 'tra.reg',   
        );
        $columns2 = array( 
            0 => 'tra.traspasoId',
            1 => 'pro.nombre',
            2 => 'suc_s.nombre',
            3 => 'suc_e.nombre',
            4 => "concat(per.nombre,' ',per.apellidos)",
            5 => 'tra.cantidad',
            6 => 'tra.reg',    
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }

        $this->db->select($select);
        $this->db->from('traspasos tra');

        $this->db->join('productos pro','pro.productoid=tra.productoId');
        $this->db->join('sucursal suc_s','suc_s.idsucursal=tra.suc_salida');
        $this->db->join('sucursal suc_e','suc_e.idsucursal=tra.suc_entrada');
        $this->db->join('personal per','per.personalId=tra.personalId');


        
         if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
            
        }
        
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();

       // print_r($query); die;
        return $query;
    }
    
    
}