<?php
/*$a=session_id();
if(empty($a)) session_start();*/
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloReportes extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    function filas() {
        $strq = "SELECT COUNT(*) as total FROM log_session";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    
    function total_paginados($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT log_session.*, usuarios.Usuario, concat(personal.nombre,' ',personal.apellidos) as empleado
        FROM log_session 
        LEFT JOIN usuarios on usuarios.UsuarioID=log_session.id_usuario
        LEFT JOIN personal on personal.personalId=log_session.id_usuario  
        JOIN sucursal on sucursal.idsucursal=log_session.id_sucursal
        LIMIT $por_pagina $segmento";

        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    /*function gastosallsearch($con){
        $strq = "SELECT log_session.*, usuarios.Usuario, concat(personal.nombre,' ',personal.apellidos) as empleado, sucursal.nombre as sucursal 
        FROM log_session
        LEFT JOIN usuarios on usuarios.UsuarioID=log_session.id_usuario
        LEFT JOIN personal on personal.personalId=log_session.id_usuario 
        join sucursal on sucursal.idsucursal=log_session.id_sucursal

        WHERE personal.nombre like '%".$con."%' or personal.apellidos like '%".$con."%'";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }*/

    function listado($params){
        $columns = array( 
            0=> 'log_session.id',
            1=> 'concat(personal.nombre," ",personal.apellidos) as empleado',
            2=> 'log_session.fecha_ini',
            3=> 'log_session.fecha_fin', 
            4=> 'sucursal.nombre as sucursal',
            5=> 'log_session.id_sucursal',
        );
        $columns2 = array( 
            0=> 'log_session.id',
            1=> 'log_session.fecha_ini',
            2=> 'log_session.fecha_fin', 
            3=> 'sucursal.nombre',
            4=> 'personal.nombre', 
            5=> 'personal.apellidos'
        );
        
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('log_session');
        $this->db->join('usuarios','usuarios.UsuarioID=log_session.id_usuario','left');
        $this->db->join('personal','personal.personalId=usuarios.personalId','left');
        $this->db->join('sucursal','sucursal.idsucursal=log_session.id_sucursal','left');
        $this->db->order_by('log_session.id', 'DESC');
        
         if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            //log_message('error','BUSQUEDA:'.$search);
            $this->db->group_start();
            foreach($columns2 as $c){
                $search = ltrim($search, '0');
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
            
        }
        
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();

       // print_r($query); die;
        return $query;
    }


    function filasMovs() {
        $strq = "SELECT COUNT(*) as total FROM log_cambios";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }

    function filasMovsFecha($fi,$ff) {
        $strq = "SELECT COUNT(*) as total FROM log_cambios
        WHERE fecha between '$fi 00:00:00' and '$ff 23:59:59'";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    
    function total_paginadosMovs($por_pagina,$segmento) {
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT log_session.*, usuarios.Usuario, concat(personal.nombre,' ',personal.apellidos) as empleado
        FROM log_cambios 
        LEFT JOIN usuarios on usuarios.UsuarioID=log_session.id_usuario
        LEFT JOIN personal on personal.personalId=log_session.id_usuario  
        JOIN sucursal on sucursal.idsucursal=log_session.id_sucursal
        LIMIT $por_pagina $segmento";

        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function listadoMovs($params){
        $columns = array( 
            0=> 'log_cambios.id',
            1=> 'concat(personal.nombre," ",personal.apellidos) as empleado',
            2=> 'log_cambios.fecha',
            3=> 'log_cambios.tabla',
            4=> 'sucursal.nombre as sucursal',
            5=> 'log_cambios.id_sucursal',
            6=> 'log_cambios.modificacion',
            7=> 'log_cambios.id_reg'
        );
        $columns2 = array( 
            0=> 'log_cambios.id',
            1=> 'log_cambios.fecha',
            2=> 'log_cambios.tabla', 
            3=> 'log_cambios.id_sucursal',
            4=> 'log_cambios.modificacion',
            5=> 'sucursal.nombre',
            6=> 'personal.nombre', 
            7=> 'personal.apellidos',
            8=> 'log_cambios.id_reg'
        );
        
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('log_cambios');
        $this->db->join('usuarios','usuarios.UsuarioID=log_cambios.id_usuario','left');
        $this->db->join('personal','personal.personalId=usuarios.personalId','left');
        $this->db->join('sucursal','sucursal.idsucursal=log_cambios.id_sucursal','left');
        $this->db->order_by('log_cambios.id', 'DESC');
        
         if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            //log_message('error','BUSQUEDA:'.$search);
            $this->db->group_start();
            foreach($columns2 as $c){
                $search = ltrim($search, '0');
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
            
        }
        
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();

       // print_r($query); die;
        return $query;
    }

    function movimientosFechas($params){
        $txtInicio =$params['txtInicio'];
        $txtFin =$params['txtFin'];
        $columns = array( 
            0=> 'log_cambios.id',
            1=> 'concat(personal.nombre," ",personal.apellidos) as empleado',
            2=> 'log_cambios.fecha',
            3=> 'log_cambios.tabla',
            4=> 'sucursal.nombre as sucursal',
            5=> 'log_cambios.id_sucursal',
            6=> 'log_cambios.modificacion',
            7=> 'log_cambios.id_reg'
        );
        $columns2 = array( 
            0=> 'log_cambios.id',
            1=> 'log_cambios.fecha',
            2=> 'log_cambios.tabla', 
            3=> 'log_cambios.id_sucursal',
            4=> 'log_cambios.modificacion',
            5=> 'sucursal.nombre',
            6=> 'personal.nombre', 
            7=> 'personal.apellidos',
            8=> 'log_cambios.id_reg'
        );
        
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('log_cambios');
        $this->db->join('usuarios','usuarios.UsuarioID=log_cambios.id_usuario','left');
        $this->db->join('personal','personal.personalId=usuarios.personalId','left');
        $this->db->join('sucursal','sucursal.idsucursal=log_cambios.id_sucursal','left');
        $this->db->order_by('log_cambios.id', 'DESC');
        $this->db->where("log_cambios.fecha between '{$txtInicio} 00:00:00' and '{$txtFin} 23:59:59'");
         if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            //log_message('error','BUSQUEDA:'.$search);
            $this->db->group_start();
            foreach($columns2 as $c){
                $search = ltrim($search, '0');
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
            
        }
        //$this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();

       // print_r($query); die;
        return $query;
    }

    public function getReportes($params){
        $start=$params["start"];
        $end=$params["end"];
        /*SELECT `id`, `id_reg`, `tabla` as `title`, `modificacion`, '#cfa520' as color, CONCAT(date_format(fecha, '%Y-%m-%d'), 'T', date_format(fecha, '%H:%i:%s')) as start, CONCAT(date_format(fecha, '%Y-%m-%d'), 'T', date_format(fecha, DATE_FORMAT(DATE_ADD(STR_TO_DATE(date_format(fecha, '%H:%i:%s'), '%H:%i:%s'),INTERVAL 12 HOUR), '%H:%i:%s') )) as end, `tabla`, 
        DATE_FORMAT(DATE_ADD(STR_TO_DATE(date_format(fecha, '%H:%i:%s'), '%H:%i:%s'),INTERVAL 2 HOUR), '%H:%i:%s') hora
        FROM `log_cambios`
        WHERE `fecha` >= '2022-06-15' AND `fecha` <= '2022-06-16'*/
        $this->db->select("id, id_reg, tabla as title, modificacion, IF(modificacion='elimina' or modificacion='elimina multiple','#FF0000','#cfa520') as color,
            CONCAT(date_format(fecha,'%Y-%m-%d'),'T',date_format(fecha, DATE_FORMAT(DATE_ADD(STR_TO_DATE(date_format(fecha, '%H:%i:%s'), '%H:%i:%s'),INTERVAL 12 HOUR), '%H:%i:%s') )) as start, 
            CONCAT(date_format(fecha, '%Y-%m-%d'), 'T', date_format(fecha, DATE_FORMAT(DATE_ADD(STR_TO_DATE(date_format(fecha, '%H:%i:%s'), '%H:%i:%s'),INTERVAL 12 HOUR), '%H:%i:%s') )) as end, tabla, u.Usuario");
        $this->db->from('log_cambios lc');
        $this->db->join("usuarios u","lc.id_usuario=u.UsuarioID","left");
        $this->db->where("fecha >= '$start' AND fecha <= '$end'");

        $query = $this->db->get();
        return $query->result();
    }

    public function getDetalleLog($id){
        $this->db->select("codigo, nombre");
        $this->db->from('productos');
        $this->db->where("productoid",$id);

        $query = $this->db->get();
        return $query->row();
    }

}
