<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloProductos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    function filas($params) {
        $tipoProd=$params['tipoProd'];
        if($tipoProd!='0'){
            $where = "and tipo_prod= '$tipoProd'";
        }
        else{
           $where=""; 
        }
        $strq = "SELECT COUNT(*) as total FROM productos where activo=1 $where";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function filas2() {
        $strq = "SELECT COUNT(*) as total FROM productos where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }

    function filasStock($tipostock,$sucurs) {
        if($tipostock==0){
            $where = " and s.idsucursal=$sucurs and ps.existencia <= 5";
        }
        else{
            $where = " and s.idsucursal=$sucurs and ps.existencia > 5";
        }
        $strq = "SELECT COUNT(*) as total 
        FROM productos as p
        inner join productos_sucursales as ps on ps.idproducto = p.productoid 
        inner join sucursal as s on s.idsucursal=ps.idsucursal 
        where p.activo=1 $where";

        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }

    /*
    function total_paginados($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT pro.productoid,pro.codigo, pro.nombre, d.nombre as depa, pro.fecharegistro FROM productos as pro
        inner join departamento as d on d.iddepartamento = pro.iddepartamento
        

        where pro.activo=1 LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }*/
    function listado($params){
        $departamento=$params['departamento'];
        $tipoProd=$params['tipoProd'];
        $columns = array( 
            0=> 'pro.productoid',
            1=> 'pro.productoid',            
            2=> 'pro.nombre',
            3=> 's1.nombre suc_1',
            4=> 'ps1.precio_venta as pv_1',
            5=> 'ps1.existencia as exi_1',
            6=> 's2.nombre suc_2',
            7=> 'ps2.precio_venta as pv_2',
            8=> 'ps2.existencia as exi_2',
            9=> 's3.nombre suc_3',
            10=> 'ps3.precio_venta as pv_3',
            11=> 'ps3.existencia as exi_3',
            12=> 'd.nombre as depa', 
            13=> 'pro.fecharegistro',
            14=> 'pro.tipo_prod',
            15=> 'pro.codigo'
        );
        $columns2 = array( 
            0=> 'pro.productoid',
            1=> 'pro.productoid',
            2=> 'pro.codigo', 
            3=> 'pro.nombre',
            4=> 's1.nombre',
            5=> 'ps1.precio_venta',
            6=> 'ps1.existencia',
            7=> 's2.nombre',
            8=> 'ps2.precio_venta',
            9=> 'ps2.existencia',
            10=> 's3.nombre',
            11=> 'ps3.precio_venta',
            12=> 'ps3.existencia',
            13=> 'd.nombre', 
            14=> 'pro.fecharegistro',
            15=> 'pro.tipo_prod'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('productos pro');
        $this->db->join('departamento d', 'd.iddepartamento = pro.iddepartamento');
        $this->db->join('productos_sucursales ps1', 'ps1.idproducto=pro.productoid and ps1.idsucursal=1');
        $this->db->join('sucursal s1', 's1.idsucursal=ps1.idsucursal');

        $this->db->join('productos_sucursales ps2', 'ps2.idproducto=pro.productoid and ps2.idsucursal=2');
        $this->db->join('sucursal s2', 's2.idsucursal=ps2.idsucursal');

        $this->db->join('productos_sucursales ps3', 'ps3.idproducto=pro.productoid and ps3.idsucursal=3');
        $this->db->join('sucursal s3', 's3.idsucursal=ps3.idsucursal');
        //$this->db->order_by('ps1.existencia', 'DESC');
        /*$where = array(
                        'pro.activo'=>1, 
                        's1.idsucursal'=>1, 
                        's2.idsucursal'=>2, 
                        's3.idsucursal'=>3
                        );
        $this->db->where($where);*/
        $this->db->where('pro.activo',1);
        if($departamento!=0) {
            $this->db->where('pro.iddepartamento',$departamento);
        }
        if($tipoProd!='0'){
           $this->db->where('pro.tipo_prod',$tipoProd); 
        }/*else{
            $this->db->where('pro.tipo_prod!=',''); 
        }*/
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $search = ltrim($search, '0');
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    function total_prods($params){
        $departamento=$params['departamento'];
        $tipoProd=$params['tipoProd'];
        $columns2 = array( 
            0=> 'pro.productoid',
            1=> 'pro.productoid',
            2=> 'pro.codigo', 
            3=> 'pro.nombre',
            4=> 's1.nombre',
            5=> 'ps1.precio_venta',
            6=> 'ps1.existencia',
            7=> 's2.nombre',
            8=> 'ps2.precio_venta',
            9=> 'ps2.existencia',
            10=> 's3.nombre',
            11=> 'ps3.precio_venta',
            12=> 'ps3.existencia',
            13=> 'd.nombre', 
            14=> 'pro.fecharegistro',
            15=> 'pro.tipo_prod',
        );
        $this->db->select("count(1)");
        $this->db->from('productos pro');
        $this->db->join('departamento d', 'd.iddepartamento = pro.iddepartamento');
        $this->db->join('productos_sucursales ps1', 'ps1.idproducto=pro.productoid and ps1.idsucursal=1');
        $this->db->join('sucursal s1', 's1.idsucursal=ps1.idsucursal');

        $this->db->join('productos_sucursales ps2', 'ps2.idproducto=pro.productoid and ps2.idsucursal=2');
        $this->db->join('sucursal s2', 's2.idsucursal=ps2.idsucursal');

        $this->db->join('productos_sucursales ps3', 'ps3.idproducto=pro.productoid and ps3.idsucursal=3');
        $this->db->join('sucursal s3', 's3.idsucursal=ps3.idsucursal');
        //$this->db->order_by('ps1.existencia', 'DESC');
        /*$where = array(
                        'pro.activo'=>1, 
                        's1.idsucursal'=>1, 
                        's2.idsucursal'=>2, 
                        's3.idsucursal'=>3
                        );
        $this->db->where($where);*/
        $this->db->where('pro.activo',1);
        if($departamento!=0) {
            $this->db->where('pro.iddepartamento',$departamento);
        }
        if($tipoProd!='0'){
           $this->db->where('pro.tipo_prod',$tipoProd); 
        }/*else{
            $this->db->where('pro.tipo_prod!=',''); 
        }*/
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $search = ltrim($search, '0');
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }   
        return $this->db->count_all_results();
    }
    function productosall() {
        $strq = "SELECT pro.productoid,pro.nombre,pro.img,pro.codigo,pro.descripcion,cat.categoria,pro.precioventa,pro.stock FROM productos as pro
        inner join categoria as cat on cat.categoriaId=pro.categoria 
        where pro.activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function total_paginadosp($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT pro.productoid,pro.nombre,pro.img,pro.codigo FROM productos as pro
        inner join categoria as cat on cat.categoriaId=pro.categoria 
        where pro.activo=1 LIMIT $por_pagina $segmento";
        return $strq;
    }
    function productoallsearch($sucu,$depa,$pro)
    {   
        if($sucu!=0){
            $innerjoinsucu = "inner join productos_sucursales as ps on ps.idproducto = p.productoid
                    inner join sucursal as s on s.idsucursal=ps.idsucursal";
            $where_sucu =" s.idsucursal = ".$sucu." and ";
        }else{
            $innerjoinsucu ='';
            $where_sucu ='';
        }
        if($depa!=0){
            $where_depa=" d.iddepartamento = ".$depa." and ";
        }else{
            $where_depa='';
        }
        
        if($pro!=''){
            $wheresearch=$where_sucu.' '.$where_depa.'p.activo=1';
            $wheresearchb=$wheresearch."  and p.codigo like '%$pro%' or";
            $wheresearchb.=$wheresearch." and p.nombre like '%$pro%' or";
            $wheresearchb.=$wheresearch." and d.nombre like '%$pro%'";
        }else{
            $wheresearchb=$where_sucu.' '.$where_depa.'p.activo=1';
        }
        $strq = "SELECT p.*, d.nombre as depa 
        FROM productos as p
        inner join departamento as d on d.iddepartamento = p.iddepartamento
        $innerjoinsucu
        where $wheresearchb
        /* inner join sucursal as s on s.idsucursal = ds.idsucursal */ 
        ";

        /*
        $strq = "SELECT p.*, d.nombre as depa FROM productos as p
        inner join departamento as d on d.iddepartamento = p.iddepartamento
        inner join productos_sucursales as ds on ds.idproducto = p.productoid
        inner join sucursal as s on s.idsucursal = ds.idsucursal 
        where p.activo=1 and  ds.idsucursal = $sucu  and d.iddepartamento = $depa and p.codigo like '%".$pro."%' or p.activo=1 and  ds.idsucursal = $sucu  and d.iddepartamento = $depa and p.nombre like '%".$pro."%'";
        $query = $this->db->query($strq);
        */

        $query = $this->db->query($strq);
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    /*function productoallsearchStock($sucurs,$tipostock)
    {   

        $strq = "SELECT p.*, d.nombre as depa, p.fecharegistro, s.nombre as suc_1,ps.precio_venta as pv_1,
        ps.mayoreo as my_1,ps.cuantos as ct_1, ps.existencia as exi_1, ps.minimo as min_1
        FROM productos as p
        inner join departamento as d on d.iddepartamento = p.iddepartamento 
        inner join productos_sucursales as ps on ps.idproducto = p.productoid 
        inner join sucursal as s on s.idsucursal=ps.idsucursal 
        where s.idsucursal = 1 and ps.existencia <= 5";

        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }*/

    function productoallsearchStock($params){
        $sucurs=$params['sucurs'];
        $stock=$params['tipostock'];
        $columns = array( 
            0=> '',
            2=> 'p.*',
            3=> 'd.nombre as depa',
            4=> 'p.fecharegistro',
            5=> 's.nombre as suc_1', 
            6=> 'precio_venta as pv_1',
            7=> 'ps.mayoreo as my_1',
            8=> 'ps.cuantos as ct_1',
            9=> 'ps.existencia as exi_1',
            10=> 'ps.minimo as min_1',
            10=> 'p.tipo_prod'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('productos p');
        $this->db->join('departamento as d','d.iddepartamento = p.iddepartamento');
        $this->db->join('productos_sucursales as ps','ps.idproducto = p.productoid');
        $this->db->join('sucursal as s','s.idsucursal=ps.idsucursal');
        $this->db->where('s.idsucursal',$stock);
        if($stock!=0)    
           $this->db->where('ps.existencia<=',5); 
        /*else
            $this->db->where('ps.existencia>',5); */  

         if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            //log_message('error','BUSQUEDA:'.$search);
            $this->db->group_start();
            foreach($columns2 as $c){
                $search = ltrim($search, '0');
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
            
        }
        //$this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();

       // print_r($query); die;
        return $query;
    }


    function productoallsearchlike($pro){
        $strq = "SELECT * FROM productos where activo=1 and codigo like '%".$pro."%' or activo=1 and nombre like '%".$pro."%'";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function categorias() {
        $strq = "SELECT * FROM categoria where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    public function productosinsert($cod,$pfiscal,$nom,$des,$catego,$stock,$preciocompra,$porcentaje,$precioventa,$pmmayoreo,$cpmmayoreo,$pmayoreo,$cpmayoreo){
            $strq = "INSERT INTO productos(codigo, productofiscal, nombre, descripcion, categoria, stock, preciocompra, porcentaje, precioventa, mediomayoreo, canmediomayoreo, mayoreo, canmayoreo) 
                                   VALUES ('$cod',$pfiscal,'$nom','$des',$catego,$stock,'$preciocompra',$porcentaje,$precioventa,$pmmayoreo,$cpmmayoreo,$pmayoreo,$cpmayoreo)";
            $this->db->query($strq);
            $id=$this->db->insert_id();
            return $id;
    }
    public function productosupdate($id,$cod,$pfiscal,$nom,$des,$catego,$stock,$preciocompra,$porcentaje,$precioventa,$pmmayoreo,$cpmmayoreo,$pmayoreo,$cpmayoreo){
            $strq = "UPDATE productos SET codigo='$cod',productofiscal=$pfiscal,nombre='$nom',descripcion='$des',categoria=$catego,stock=$stock,preciocompra=$preciocompra,porcentaje=$porcentaje,precioventa=$precioventa,mediomayoreo=$pmmayoreo,canmediomayoreo=$cpmmayoreo,mayoreo=$pmayoreo,canmayoreo=$cpmayoreo WHERE productoid=$id";
            $this->db->query($strq);
    }
    public function imgpro($file,$pro){
        $strq = "UPDATE productos SET img='$file' WHERE productoid=$pro";
        $this->db->query($strq);
    }
    function totalproductosenexistencia() {
        //$strq = "SELECT ROUND(sum(stock),2) as total FROM `productos` where activo=1 ";
        $strq = "SELECT sum(pros.cuantos) as total
                FROM productos_sucursales as pros
                inner join productos as pro on pro.productoid=pros.idproducto
                WHERE pro.activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function totalproductopreciocompra() {
        $strq = "SELECT sum(pros.precio_venta) as total
                FROM productos_sucursales as pros
                inner join productos as pro on pro.productoid=pros.idproducto
                WHERE pro.activo=1";
        //$strq = "SELECT ROUND(sum(preciocompra),2) as total FROM `productos` where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function totalproductoporpreciocompra(){
        $strq = "SELECT sum(pros.precio_venta*pros.cuantos) as total
                FROM productos_sucursales as pros
                inner join productos as pro on pro.productoid=pros.idproducto
                WHERE pro.activo=1";
        //$strq = "SELECT ROUND(sum(preciocompra*stock),2) as total FROM `productos` where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function getproducto($id){
        $strq = "SELECT * FROM productos where productoid=$id";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function productosdelete($id){
        $strq = "UPDATE productos SET activo=0 WHERE productoid=$id";
        $this->db->query($strq);
    }
    function tablasucursal($buscar){
        $strq = "SELECT ps.idproductosucursal, ps.idproducto, s.nombre,ps.precio_venta,ps.mayoreo,ps.cuantos,ps.existencia,ps.minimo
                    FROM productos_sucursales as ps
                    INNER JOIN sucursal as s on s.idsucursal=ps.idsucursal
                    where ps.idproducto = $buscar ";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function aupdatetabla(){
        $strq = "UPDATE productos SET activo=0 WHERE activo=1";
        $this->db->query($strq);
    }
    public function Addproductos($codigo, $nombre, $preciocompra, $gananciaefectivo, $porcentaje, $precioventa, $mayoreo , $canmayoreo, $fecharegistro, $iddepartamento, $tipo_ganancia){
        //$strq = "CALL SP_GET_ADDPRODUCTOS(".$Articulo.",'".$Descripcion."','".$Talla."',".$Precio.",".$ConteoIn.",".$InventarioSis.",".$diferencia.");";
        $strq = "INSERT INTO productos(codigo, nombre, preciocompra, gananciaefectivo, porcentaje, precioventa, mayoreo , canmayoreo, fecharegistro, iddepartamento, tipo_ganancia) VALUES('$codigo', '$nombre', '$preciocompra', $gananciaefectivo, $porcentaje, $precioventa, $mayoreo, $canmayoreo, '$fecharegistro', $iddepartamento , $tipo_ganancia);";
        //log_message('error','---------------'.$strq);
        $this->db->query($strq);
        $id=$this->db->insert_id();
        return $id;
    }
    public function Addsucursales($idsucursal, $idp, $precio_venta, $mayoreo, $cuantos, $existencia, $minimo){
        //$strq = "CALL SP_GET_ADDPRODUCTOS(".$Articulo.",'".$Descripcion."','".$Talla."',".$Precio.",".$ConteoIn.",".$InventarioSis.",".$diferencia.");";
        $strq = "INSERT INTO productos_sucursales(idsucursal, idproducto, precio_venta, mayoreo, cuantos, existencia,  minimo) VALUES($idsucursal, $idp, $precio_venta, $mayoreo, $cuantos, $existencia, $minimo);";
        //log_message('error','---------------'.$strq);
        $this->db->query($strq);
    }
    function getSucursales(){
        $strq = "SELECT * FROM sucursal where estatus=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getProducto_excel(){
        $strq = "SELECT p.codigo, p.nombre, p.preciocompra,  p.gananciaefectivo, p.porcentaje, p.precioventa,  p.mayoreo,  p.canmayoreo,p.fecharegistro, p.iddepartamento, p.tipo_ganancia, s.precio_venta as precio_s1,  s.mayoreo as mayoreo_s1,  s.cuantos as cuantos_s1,  s.existencia as existencia_s1, s.minimo as minimo_s1 FROM productos AS p 
                INNER JOIN productos_sucursales AS s ON s.idproducto = p.productoid
                WHERE p.activo=1 AND s.idsucursal = 1 GROUP BY p.productoid, s.idproductosucursal";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getProducto_excel2(){
        $strq = "SELECT su.precio_venta,  su.mayoreo, su.cuantos, su.existencia,  su.minimo FROM productos AS p 
                INNER JOIN productos_sucursales AS su ON su.idproducto = p.productoid
                WHERE p.activo=1 AND su.idsucursal=2 GROUP BY p.productoid, su.idproductosucursal";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getProducto_excel3(){
        $strq = "SELECT suc.precio_venta, suc.mayoreo, suc.cuantos, suc.existencia, suc.minimo FROM productos AS p 
                 INNER JOIN productos_sucursales AS suc ON suc.idproducto = p.productoid
                 WHERE p.activo=1 AND suc.idsucursal=3 GROUP BY p.productoid, suc.idproductosucursal";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function getStockProducto($producto, $sucursal){
        $this->db->select('suc.existencia');
        $this->db->from('productos p');
        $this->db->join('productos_sucursales suc', 'suc.idproducto = p.productoid');
        $this->db->where('p.productoid', $producto);
        $this->db->where('suc.idsucursal', $sucursal);
        //$this->db->where('p.activo',1);
    
        $query = $this->db->get();
        $result = $query->row();
        return $result ? $result->existencia : '0';
    }

    function getStocksProducto($producto){
        $this->db->select('suc.existencia, suc.idsucursal');
        $this->db->from('productos p');
        $this->db->join('productos_sucursales suc', 'suc.idproducto = p.productoid');
        $this->db->where('p.productoid', $producto);
        //$this->db->where('p.activo',1);
    
        $query = $this->db->get();
        return $query->result();
    }

}