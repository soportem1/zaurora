<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloSession extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function login($usu,$pass) {
        //$strq = "CALL SP_GET_SESSION('$usu');";
        /*$strq="SELECT usu.UsuarioID,per.personalId,per.nombre, usu.perfilId, usu.contrasena,per.idsucursal
        FROM usuarios as usu
        inner JOIN personal as per on per.personalId = usu.personalId
        where per.estatus=1 AND usu.Usuario = '$usu'";*/
        $strq="SELECT usu.UsuarioID,per.personalId,per.nombre, per.almacen, usu.perfilId, usu.contrasena,per.idsucursal
                FROM usuarios as usu
                inner JOIN personal as per on per.personalId = usu.personalId
                where per.estatus=1 AND usu.Usuario = '$usu'";
        $count = 0;
        $passwo =0;
        $query = $this->db->query($strq);
        foreach ($query->result() as $row) {
            $passwo =$row->contrasena;
            $id = $row->UsuarioID;
            $nom =$row->nombre;
            $perfil = $row->perfilId; 
            $idpersonal = $row->personalId;
            $id_user = $row->UsuarioID; 
            $idsucursal= $row->idsucursal;
            $almacen=$row->almacen;

            $verificar = password_verify($pass,$passwo);
            if ($verificar) {

                /*date_default_timezone_set('America/Mexico_City');
                $date = date('Y-m-d h:i:s \G\M\T');

                $data2 = array(
                    'id_usuario' => $id,
                    'fecha_ini' => $date,
                    'fecha_fin'=>'',
                    'id_sucursal'=> $idsucursal,
                    'status' => 1
                );
                $this->insertLogData($data2);*/
                $count=1;
                $data = array(
                        'logeado' => true,
                        'usuarioid_tz' => $id,
                        'usuario_tz' => $nom,
                        'perfilid_tz'=>$perfil,
                        'idpersonal_tz'=>$idpersonal,
                        'id_user'=>$id_user,
                        'idsucursal_tz'=>$idsucursal,
                        'almacen'=>$almacen
                    );
                $this->session->set_userdata($data);
                
                //$count=$passwo.'/'.$id.'/'.$nom.'/'.$perfil.'/'.$idpersonal;
            }
            /*
                si es cero aqui se agregara los mismos parametros pero para el aseso de los residentes si es que lo piden
            */
        } 
        
        echo $count;
        
        //$query->next_result(); 
        //$query->free_result(); 
        //end of new code

        return $query;
    }

    function verificaPassDesc($usu,$pass) {
        $strq = "CALL SP_GET_SESSION('$usu');";
        $count = 0;
        $passwo =0;
        $query = $this->db->query($strq);
        foreach ($query->result() as $row) {
            $passwo =$row->contrasena;
            $id = $row->UsuarioID;
            $nom =$row->nombre;

            $verificar = password_verify($pass,$passwo);
            if ($verificar) {
                $count=1;
            }

        } 
        
        echo $count;
        $query->next_result(); 
        $query->free_result(); 
        //end of new code
        return $query;
    }

    public function insertLogData($data2){
        $this->db->insert('log_session',$data2);//ingresa datos para el log de sesion
        return $this->db->insert_id();
    }

    public function cerrarLogSess($id_usuario,$ff)
    {
        $this->db->where('id_usuario', $id_usuario);
        $this->db->where('status', 1);
        $this->db->set('fecha_fin' , $ff);
        $this->db->set('status' , 0);
        $this->db->update('log_session');
    }

    public function menus($perfil){
        //$strq ="SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, Perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='$perfil' ORDER BY men.MenuId ASC";
        $strq ="SELECT distinct men.MenuId,men.Nombre,men.Icon 
                from menu as men, menu_sub as mens, personal_menu as perfd 
                where men.MenuId=mens.MenuId and perfd.MenuId=mens.MenusubId and perfd.personalId='$perfil' ORDER BY men.MenuId ASC";
        //$strq="CALL SP_GET_MENUS($perfil)";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;

    }
    public function submenus($perfil,$menu){
        $strq ="SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
                from menu_sub as menus, personal_menu as perfd 
                WHERE perfd.MenuId=menus.MenusubId and perfd.personalId='$perfil' and menus.MenuId='$menu' ORDER BY menus.MenusubId ASC";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;

    }
    function alertproductos(){
        //$strq = "SELECT count(*) as total FROM productos where stock<=3 and activo=1";
        $strq = "SELECT count(*) as total 
                    FROM productos as pro
                    inner JOIN productos_sucursales as pros on pros.idproducto=pro.productoid
                    WHERE pro.activo=1 and pros.existencia<10";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function alertproductosall(){
        //$strq = "SELECT * FROM productos where stock<=3 and activo=1";
        $strq ="SELECT pro.nombre as producto,suc.nombre as sucursal, pros.existencia  
                FROM productos as pro
                inner JOIN productos_sucursales as pros on pros.idproducto=pro.productoid
                inner JOIN sucursal as suc on suc.idsucursal=pros.idsucursal
                WHERE pro.activo=1 and pros.existencia<10 limit 40";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function notas(){
        $strq = "SELECT * FROM notas";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function permisoadmin(){
        $strq = "SELECT * FROM usuarios WHERE perfilId=1";
        $query = $this->db->query($strq);
        return $query->result();
    }
    function getviewpermiso($perfil,$modulo){
        $strq = "SELECT COUNT(*) as total FROM `personal_menu` WHERE personalId=$perfil AND MenuId=$modulo";
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total; 
    }

}
