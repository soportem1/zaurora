<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloPersonal extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function personalnuevo($nom,$ape,$fnaci,$sexo,$domic,$ciudad,$estado,$copo,$telcasa,$telcel,$email,$turno,$fechain,$fechaba,$sueldo,$idsucursal,$num_checador,$salario_dia,$salario_hora,$hora_extra,$hora_doble,$hora_dia_feriado,$comision_venta,$vacas,$almacen){
            $strq = "INSERT INTO personal(nombre, apellidos, fechanacimiento, sexo, domicilio, ciudad, estado, codigopostal, telefono, celular, correo, turno, fechaingreso, fechabaja, sueldo, idsucursal, num_checador,salario_dia,salario_hora,hora_extra,hora_doble,hora_dia_feriado,comision_venta,dias_vaca,almacen) 
            VALUES('$nom','$ape','$fnaci',$sexo,'$domic','$ciudad','$estado','$copo','$telcasa','$telcel','$email',$turno,'$fechain','$fechaba','$sueldo','$idsucursal','$num_checador','$salario_dia','$salario_hora','$hora_extra','$hora_doble','$hora_dia_feriado','$comision_venta','$vacas','$almacen')";
            $this->db->query($strq);
            $id=$this->db->insert_id();
            return $id;
    }
    public function personalupdate($id,$nom,$ape,$fnaci,$sexo,$domic,$ciudad,$estado,$copo,$telcasa,$telcel,$email,$turno,$fechain,$fechaba,$sueldo,$idsucursal,$num_checador,$salario_dia,$salario_hora,$hora_extra,$hora_doble,$hora_dia_feriado,$comision_venta,$vacas,$almacen){
            $strq = "UPDATE personal SET nombre='$nom', apellidos='$ape',fechanacimiento='$fnaci',sexo=$sexo, domicilio='$domic',ciudad='$ciudad',estado=$estado, codigopostal=$copo,telefono='$telcasa',celular='$telcel',correo='$email',turno=$turno,fechaingreso='$fechain',fechabaja='$fechaba',sueldo='$sueldo', idsucursal=$idsucursal, num_checador='$num_checador',salario_dia='$salario_dia',salario_hora='$salario_hora',hora_extra=$hora_extra,hora_doble='$hora_doble',hora_dia_feriado='$hora_dia_feriado',comision_venta='$comision_venta',dias_vaca='$vacas',almacen='$almacen'
            WHERE personalId=$id";
            $this->db->query($strq);
    }
    /*
    function getpersonal() {
        $strq = "CALL SP_GET_ALL_PERSONAL";
        $query = $this->db->query($strq);
        return $query;
    }
    */

    function getpersonal(){
        $strq = "SELECT per.personalId,per.nombre,per.apellidos,usu.Usuario 
                FROM personal as per 
                LEFT JOIN usuarios as usu on usu.personalId=per.personalId
                WHERE  per.estatus=1";
        $query = $this->db->query($strq);
        return $query;
        
    }
    
    function personalview($id) {
        $strq = "SELECT *  FROM personal where personalId='$id'";
        $query = $this->db->query($strq);
        return $query;
    }
    function personalusuview($id) {
        $strq = "SELECT * FROM usuarios where personalId='$id'";
        $query = $this->db->query($strq);
        return $query;
    }
    /*
    function estados() {
        $strq = "SELECT *  FROM estado";
        $query = $this->db->query($strq);
        return $query;
    }*/
    
    /*
    public function personaldelete($id){
            $strq = "CALL SP_DEL_PERSONAL($id)";
            $this->db->query($strq);
    }
    */

    public function personaldelete($id){
        $strq = "UPDATE personal SET estatus=0 where personalId=$id";
        $this->db->query($strq);
    }

    function getAllmenu(){
        $strq = "SELECT menu_sub.MenusubId,menu_sub.MenuId,menu.nombre as menu,menu_sub.nombre as submenu,menu_sub.Icon 
                from menu_sub
                inner join menu on menu_sub.MenuId = menu.MenuId;";
        $query = $this->db->query($strq);
        return $query;
    }
    function getMenusPerfil($id){
        $strq = "SELECT personal_menu.personalmenuId,menu_sub.MenusubId,menu_sub.MenuId,menu.Nombre as menu,menu_sub.Nombre as submenu,menu_sub.Icon 
                from menu_sub
                inner join menu on menu_sub.MenuId = menu.MenuId

                inner join personal_menu on menu_sub.MenusubId = personal_menu.MenuId
                where personal_menu.personalId=$id;";
        $query = $this->db->query($strq);
        return $query;

    }
    
    
    

}
