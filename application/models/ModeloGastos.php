<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloGastos extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    function filas() {
        $strq = "SELECT COUNT(*) as total  FROM gastos 
   
        where status=1 ";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function getGasto($id){
        $this->db->select('gastos.*, usuarios.Usuario, sucursal.nombre as sucursal, concat(personal.nombre," ",personal.apellidos) as personal');
        $this->db->from('gastos');
        $this->db->join('usuarios','usuarios.UsuarioID=gastos.id_usuario','left');
        $this->db->join('personal','personal.personalId=usuarios.personalId','left');
        $this->db->join('sucursal','sucursal.idsucursal=gastos.id_sucursal');
        $this->db->join('sucursal sucPer','sucPer.idsucursal = personal.idsucursal','left');
        $this->db->where('status', 1);
        $this->db->where('id', $id);

        $query = $this->db->get();
        return $query;
    }
    
    function total_paginados($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT gastos.*, usuarios.Usuario, sucursal.nombre as sucursal, concat(personal.nombre,' ',personal.apellidos) as personal
        FROM gastos 
        LEFT JOIN usuarios on usuarios.UsuarioID=gastos.id_usuario
        left join personal on personal.personalId=gastos.id_usuario
        LEFT JOIN sucursal on sucursal.idsucursal=gastos.id_sucursal 
        
        where status=1 LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function gastosallsearch($con){
        $strq = "SELECT gastos.*, usuarios.Usuario, sucursal.nombre as sucursal,  concat(personal.nombre,' ',personal.apellidos) as personal
        FROM gastos
        LEFT JOIN usuarios on usuarios.UsuarioID=gastos.id_usuario
        left join personal on personal.personalId=gastos.id_usuario
        LEFT JOIN sucursal on sucursal.idsucursal=gastos.id_sucursal 

        where status=1 and concepto like '%".$con."%' ORDER BY concepto ASC";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function listado($params){
        $columns = array( 
            0=> 'g.concepto',
            1=> 'g.monto',
            2=> 'p.nombre as sucursalP',
            3=> 'g.fecha',
            4=> 'u.Usuario', 
            5=> 's.nombre',
            6=> 'g.id',
            7=> 'p.nombre',
            8=> 'p.apellidos',
            9=> 'concat (p.nombre, " ",p.apellidos) as personal',
            10=> 'g.id',
        );
        
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }

        $columns2 = array( 
            0=> 'g.concepto',
            1=> 'g.monto',
            2=> 'p.nombre',
            3=> 'g.fecha',
            4=> 'u.Usuario', 
            5=> 's.nombre',
            6=> 'g.id',
            7=> 'p.nombre',
            8=> 'p.apellidos',
            9=> 'p.nombre',
            10=> 'g.id',
        );

        $this->db->select($select);
        $this->db->from('gastos g');
        $this->db->join('usuarios u','u.UsuarioID=g.id_usuario','left');
        $this->db->join('personal p','p.personalId=u.personalId','left');
        $this->db->join('sucursal s','s.idsucursal=g.id_sucursal','left');
        //$this->db->join('sucursal sucPer','sucPer.idsucursal = p.idsucursal','left');
        //$this->db->order_by('g.id', 'DESC');
        $where = array(
                        'g.status'=>1
                        );
        $this->db->where($where);
        
         if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            //log_message('error','BUSQUEDA:'.$search);
            $this->db->group_start();
            foreach($columns2 as $c){
                $search = ltrim($search, '0');
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
            
        }
        
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();

       // print_r($query); die;
        return $query;
    }


    function listado_total($params){
        $columns = array( 
            0=> 'g.concepto',
            1=> 'g.monto',
            2=> 'p.nombre',
            3=> 'g.fecha',
            4=> 'u.Usuario', 
            5=> 's.nombre',
            6=> 'g.id',
            7=> 'p.nombre',
            8=> 'p.apellidos',
            9=> 'p.nombre',
            10=> 'g.id',
        );

        $this->db->select('COUNT(1) as total');
        $this->db->from('gastos g');
        $this->db->join('usuarios u','u.UsuarioID=g.id_usuario','left');
        $this->db->join('personal p','p.personalId=u.personalId','left');
        $this->db->join('sucursal s','s.idsucursal=g.id_sucursal','left');
        //$this->db->join('sucursal sucPer','sucPer.idsucursal = p.idsucursal','left');
        $where = array(
                        'g.status'=>1
                        );
        $this->db->where($where);
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            //log_message('error','BUSQUEDA:'.$search);
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
            
        }
        
        $query=$this->db->get();
        return $query->row()->total;
    }


    public function insertarGasto($contenido)
    {
      $this->db->insert('gastos', $contenido);
      return $this->db->insert_id();
    }

    public function updateGasto($id,$contenido)
    {
        $this->db->where('id', $id);
        $this->db->insert($contenido);
        $this->db->update('gastos');
    }

  public function verGastos()
  {
    $this->db->select('gastos.*, usuarios.Usuario, sucursal.nombre, concat(personal.nombre, " ",personal.apellidos) as personal');
    $this->db->from('gastos');
    $this->db->join('usuarios','usuarios.UsuarioID=gastos.id_usuario','left');
    $this->db->join('personal','personal.personalId=gastos.id_usuario','left');
    $this->db->join('sucursal','sucursal.idsucursal=gastos.id_sucursal');
    $this->db->where('status', 1);

    $query = $this->db->get();
    return $query;
  }

  public function eliminarGasto($id)
  {
    $this->db->where('id', $id);
    $this->db->set('status' , 0);
    $this->db->update('gastos');
  }

}
