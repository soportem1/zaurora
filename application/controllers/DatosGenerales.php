<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DatosGenerales extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloSession');
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal_tz');
            $permiso=$this->ModeloSession->getviewpermiso($this->idpersonal,22);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Sistema');
            }
        }else{
            redirect('/Sistema');
        }
    }

	public function index(){
        $data['btn_active']=4;
        $data['btn_active_sub']=8;
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];
        $data['result']=$datosconfiguracion;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('config/dg');
        $this->load->view('templates/footer');
        $this->load->view('config/dgjs');
    }
    function updatedatos(){
        $params = $this->input->post();
        $ConfiguracionesId=$params['ConfiguracionesId'];
        unset($params['ConfiguracionesId']);
        unset($params['profile_avatar_remove']);

        $this->ModeloCatalogos->updateCatalogo('f_configuraciones',$params,array('ConfiguracionesId'=>$ConfiguracionesId));
    }
    function do_upload(){
        $config['upload_path']="./public/img";
        $config['allowed_types']='gif|jpg|png';
        $config['encrypt_name'] = TRUE;
         
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file")){
            $data = array('upload_data' => $this->upload->data());
 
            $title= $this->input->post('title');
            $image= $data['upload_data']['file_name']; 
            
            $result= $this->ModeloCatalogos->updateCatalogo('f_configuraciones',array('logotipo'=>$image),array('ConfiguracionesId'=>1));
            //echo json_decode($result);
        }
 
     }
}