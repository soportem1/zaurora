<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ventas extends CI_Controller {
	public function __construct()    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloClientes');
        $this->load->model('ModeloProductos');
        $this->load->model('ModeloVentas');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloPuntos');

        $this->load->model('ModeloGeneral');
        date_default_timezone_set("America/Mexico_City");
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        $this->current_anio = date('Y');
        //$this->fechahoy = date('Y-m-d');
        $this->fechahoyc = date('Y-m-d H:i:s');
        $this->fechahoyL = date('Y-m-d_H_i_s');

        $this->trunquearredondear=0;//0 redondeo 1 trunquear (dos dijitos)
        //==============================================================
                $logueo = $this->session->userdata();
                if (isset($logueo['perfilid_tz'])) {
                    $perfilid=$logueo['perfilid_tz'];
                    $this->sucursal=$logueo['idsucursal_tz'];
                    $this->idpersonal=$this->session->userdata('idpersonal_tz');
                    if ($this->sucursal==0) {
                        $this->sucursalid=1;
                    }else{
                        $this->sucursalid=$logueo['idsucursal_tz'];
                    }
                    $this->personal=$logueo['idpersonal_tz'];
                    
                }else{
                    $perfilid=0;
                }/*
                $permiso=$this->ModeloCatalogos->getviewpermiso($perfilid,19);// 13 es el id del menu
                if ($permiso==0) {
                    redirect('/Sistema');
                }*/
        //===================================================
    }
	public function index(){
        $data['sturno']=$this->ModeloVentas->turnos();
        $data['clientedefault']=$this->ModeloVentas->clientepordefecto();
        $wherev = array('estatus' => 1,'idsucursal'=>$this->sucursalid);
        $data['vendedores']=$this->ModeloGeneral->getselectwheren('personal',$wherev);
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('ventas/ventas',$data);
        $this->load->view('templates/footer');
        $this->load->view('ventas/jsventas');
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();
        unset($_SESSION['vendedor']);
        $_SESSION['vendedor']=array();
        unset($_SESSION['vendedor_name']);
        $_SESSION['vendedor_name']=array();
	}
    public function searchcli(){
        $usu = $this->input->get('search');
        $results=$this->ModeloClientes->clientesallsearch($usu);
        echo json_encode($results->result());
    }

    public function searchprodsucu(){
        //$results=$this->ModeloVentas->obtenerPrecioProd($idprod,$this->session->userdata('idsucursal_tz'));
        //echo json_encode($results->result());
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);

        $idsucu = $this->session->userdata('idsucursal_tz');
        if($this->session->userdata('idsucursal_tz')==0)
            $idsucu=1;
        for ($i=0;$i<count($DATA);$i++) { 
            $data = $this->ModeloVentas->obtenerPrecioProd($DATA[$i]->producto,$idsucu);
            /*foreach ($data->result() as $fila) {
                $total = $fila->total;
                //$total2 = $fila->total + $fila->total;
            }*/
            
        }
        //header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function searchproducto(){
        $pro = $this->input->get('search');
        $pro = ltrim($pro, '0');
        $results=$this->ModeloProductos->productoallsearchlike($pro);
        echo json_encode($results->result());
    }
    function searchproductobar(){
        $codigo = $this->input->post('prod');
        $codigo = ltrim($codigo, '0');
        $where = array('codigo' => $codigo,'activo'=>1);
        $results=$this->ModeloGeneral->getselectwheren('productos',$where);
        $pro=0;
        $proid=0;
        $prorow=0;
        foreach ($results as $item){
            $pro=1;
            $proid=$item->productoid;
            $prorow++;
        }
        $json_data = array(
            "pro"=>$pro,
            "proid"=>$proid,
            "prorow"=>$prorow 
        );
        
        echo json_encode($json_data);
    }
    public function addproducto(){
        $cant = $this->input->post('cant');
        //log_message('error', '....cant: '.$cant);
        $prod = $this->input->post('prod');
        $vendedor = $this->input->post('vendedor');
        $vendedor_name = $this->input->post('vendedor_name');

        $personalv=$this->ModeloProductos->getproducto($prod);
        //$oProducto = new Producto();
        $existencia_v=0;
        foreach ($personalv->result() as $item){
              $id = $item->productoid;
              $codigo = $item->codigo;
              $nombre = $item->nombre;
              $precioventa = $item->precioventa;
              $mediomayoreo = $item->mediomayoreo;
              $canmediomayoreo = $item->canmediomayoreo;
              $mayoreo = $item->mayoreo;
              $canmayoreo = $item->canmayoreo;

                $where_suc = array('idproducto' => $id);
                $results_suc=$this->ModeloGeneral->getselectwheren('productos_sucursales',$where_suc);
                log_message('error', 'Results->'.json_encode($results_suc));
                //log_message('error', '....sucursal: '.$this->sucursalid);
                foreach ($results_suc as $items) {
                    if ($this->sucursalid==$items->idsucursal) {
                        $precioventa = $items->precio_venta;
                        //$mediomayoreo = $item->mediomayoreo;
                        $existencia = $items->existencia;
                        $mayoreo = $items->mayoreo;
                        $canmayoreo = $items->cuantos;
                        $existencia_v=$items->existencia;
                    }
                    
                }
                //log_message('error', '....precio: '.$precioventa);  

                $oProducto=array("id"=>$id,"codigo"=>$codigo,"nombre"=>$nombre,'precioventa'=>$precioventa,'mediomayoreo'=>$mediomayoreo,'canmediomayoreo'=>$canmediomayoreo,'mayoreo'=>$mayoreo,'canmayoreo'=>$canmayoreo,'existencia'=>$existencia,"vendedor"=>$vendedor,"vendedor_name"=>$vendedor_name);

        }
        $html='';
        $restriccion=0;
        $validar=0;
        if($existencia_v<=0){
            $validar=1;
        }else{
            //$validar=0;
             
            if(in_array($oProducto, $_SESSION['pro'])){  // si el producto ya se encuentra en la lista de compra suma las cantidades
                $idx = array_search($oProducto, $_SESSION['pro']);
                $suma=$_SESSION['can'][$idx]+$cant;
                if($suma<=$existencia_v){
                    $_SESSION['can'][$idx]+=$cant;
                }else{
                    $validar=1;      
                }
                
            }
            else{ //sino lo agrega
                $suma=$cant;
                if($suma<=$existencia_v){
                    array_push($_SESSION['pro'],$oProducto);
                    array_push($_SESSION['can'],$cant);
                    array_push($_SESSION['vendedor'],$vendedor);
                    array_push($_SESSION['vendedor_name'],$vendedor_name);
                }else{
                    $validar=1;      
                }
            }
            //======================================================================
            $count = 0;
            $n =array_sum($_SESSION['can']);
            $html='';
            $restriccion=0;
            foreach ($_SESSION['pro'] as $fila){
                $Cantidad=$_SESSION['can'][$count];
                
                if ($Cantidad>$fila['existencia']) {
                    //$Cantidad=$fila['existencia'];
                }
                /*
                if ($Cantidad>=$fila['canmayoreo']) {
                    log_message('error', '....mayoreo: ');
                    $precio=$fila['mayoreo'];
                }else{
                    $precio=$fila['precioventa'];
                    log_message('error', '....menudeo: ');
                }*/
                if ($fila['canmayoreo']==0) {
                    $precio=$fila['precioventa'];
                    $descuento=0;
                }else{
                    $precio=$fila['mayoreo'];
                    $descuento=$fila['precioventa']-$fila['mayoreo'];
                }
                $cantotal=$Cantidad*$precio;
                $vendedor = $fila['vendedor'];
                $vendedor_name = $fila['vendedor_name'];
                if ($Cantidad==0) {
                    $style='style="background: #ff00002e;"';
                }else{
                    $style='';
                }
                $idpro=$fila['id'];
                $namepro=$fila['nombre'];

                $html.='<tr class="producto_'.$count.'" '.$style.'>';
                    $html.='<td>';
                        $html.='<input type="hidden" name="vsproid" id="vsproid" value="'.$idpro.'">';
                        $html.='<input type="hidden" name="vsdesc" id="vsdesc" value="'.$descuento.'">';
                        $html.= $fila['codigo'];
                    $html.='</td>';
                    $html.='<td>';
                        $html.='<input type="number" name="vscanti" id="vscanti" value="'.$Cantidad.'" readonly style="background: transparent;border: 0px; width: 80px;">';
                        $html.='<input type="hidden" name="cantIni" id="cantIni" value="'.$fila['existencia'].'" readonly style="background: transparent;border: 0px; width: 80px;">';
                    $html.='</td>';
                    $html.='<td>'.$namepro.'</td>';
                    $html.='<td><input type="hidden" name="id_vendedor" id="id_vendedor" value="'.$vendedor.'">'.$vendedor_name.'</td>';
                    $html.='<td>$ <input type="text" name="vsprecio" id="vsprecio" value="'.$precio.'" readonly style="background: transparent;border: 0px;width: 100px;"></td>';
                    $html.='<td>$ <input type="text" class="vstotal" name="vstotal" id="vstotal" value="'.$cantotal.'" readonly style="background:transparent;border: 0px;width: 100px;"></td>';
                    $html.='<td>';
                        $html.='<a class="danger" data-original-title="Eliminar" title="Eliminar" onclick="deletepro('.$count.')">';
                            $html.='<i class="ft-trash font-medium-3"></i>';
                        $html.='</a>';
                    $html.='</td>';
                $html.='</tr>';
            $count++;     
            }

            log_message('error','DATA SESION: ' . json_encode($this->session->userdata()));
        }

        $datos  = array('datos' => $html,'restriccion'=>$restriccion,'validar'=>$validar);
        echo json_encode($datos);
    }
    function productoclear(){
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();
        unset($_SESSION['vendedor']);
        $_SESSION['vendedor']=array();
        unset($_SESSION['vendedor_name']);
        $_SESSION['vendedor_name']=array();
    }
    function deleteproducto(){
        $idd = $this->input->post('idd');
        unset($_SESSION['pro'][$idd]);
        $_SESSION['pro']=array_values($_SESSION['pro']);
        unset($_SESSION['can'][$idd]);
        $_SESSION['can'] = array_values($_SESSION['can']); 
        unset($_SESSION['vendedor'][$idd]);
        $_SESSION['vendedor'] = array_values($_SESSION['vendedor']); 
        unset($_SESSION['vendedor_name'][$idd]);
        $_SESSION['vendedor_name'] = array_values($_SESSION['vendedor_name']); 
    }
    function ingresarventa(){
        $idClienteGeneral = 45;
        $data['vendedor']=$this->input->post('vend');
        $data['id_personal']=$this->personal;
        $data['id_cliente'] = $this->input->post('cli');
        $data['metodo'] = $this->input->post('mpago');
        $data['subtotal'] = $this->input->post('sbtotal');
        $data['descuento'] = $this->input->post('desc');
        $data['descuentocant'] = $this->input->post('descu');
        $data['monto_total'] = $this->input->post('total');
        $data['pagotarjeta'] = $this->input->post('tarjeta');
        $data['pagopuntos'] = $this->input->post('puntos');
        $data['efectivo'] = $this->input->post('efectivo');
        $data['sucursalid']=$this->sucursalid;
        $data['reg']=$this->fechahoy;

        $this->checkVigenciaPuntos($data['id_cliente']);

        $porcentajePuntos = $this->checkPorcentajePuntos($data['reg']);
        $porcentajePuntosCliente = $this->checkPorcentajePuntosCliente($data['id_cliente']);


        if($data['id_cliente'] != $idClienteGeneral){
          if($porcentajePuntosCliente > 0){
            $data['porcentaje_puntos'] = $porcentajePuntosCliente * 100;
            $data['puntos_total'] = ($data['monto_total'] - $data['pagopuntos']) * $porcentajePuntosCliente;
          }else{
            $data['porcentaje_puntos'] = $porcentajePuntos * 100;
            $data['puntos_total'] = ($data['monto_total'] - $data['pagopuntos']) * $porcentajePuntos;
          }
        }else{
          $data['porcentaje_puntos'] = 0;
          $data['puntos_total'] = 0;
        }
        
        log_message('error','Puntos%: '.json_encode($data['porcentaje_puntos']));
        $data['en_monedero'] = '1';
        $currentPuntos = $this->getPuntosActuales($data['id_cliente']) > 0 ? $this->getPuntosActuales($data['id_cliente']) : 0;


        if($data['pagopuntos'] > $currentPuntos){
          if($data['metodo'] == 5){
            echo 'Error1';
            return;
          }else if($data['metodo'] == 4){//}else{
            echo 'Error2';
            return;
          }
        }


      //Gasto de Puntos
        if( $data['pagopuntos'] > 0 && ($data['metodo'] == 5 || $data['metodo'] == 4) ){
          log_message('error',"PAGO PUNTOS: ".$data['monto_total']);
          $this->ModeloPuntos->subPuntosCliente($data['id_cliente'], $data['pagopuntos']);

          //$pagoPuntosAnt = 0;
          //$pagoPuntosAct = 0;
          $puntosPorAnio = $this->ModeloPuntos->getPuntosClienteAnio($data['id_cliente']);
          $pagoPuntosAux = $data['pagopuntos'] - $puntosPorAnio->LastYPoints;

          //log_message('error','ANTaño: '.json_encode(($pagoPuntosAux)));
          //log_message('error','ACTaño: '.json_encode(($data['pagopuntos'] - $pagoPuntosAux)));

          if($pagoPuntosAux > 0){
            $this->ModeloPuntos->subPuntosClienteAnioAnt($data['id_cliente'],  ($data['pagopuntos'] - $pagoPuntosAux));
            $this->ModeloPuntos->subPuntosClienteAnioAct($data['id_cliente'],  ($pagoPuntosAux));
          }else{
            $this->ModeloPuntos->subPuntosClienteAnioAnt($data['id_cliente'],  $data['pagopuntos']);
          }

        }

        //$id=$this->ModeloVentas->ingresarventa($uss,$cli,$mpago,$sbtotal,$desc,$descu,$total,$efectivo,$tarjeta);
        log_message('error','Data Venta:' . json_encode($data));
        $id =$this->ModeloGeneral->tabla_inserta('ventas',$data);

        //Update puntos Cliente---------->
        log_message('error','ADD PUNTOS CLIENTE:' . json_encode($data['id_cliente']));
        $this->ModeloPuntos->addPuntosCliente($data['id_cliente'], $data['puntos_total']);//puntos en clientes
        $this->ModeloPuntos->addPuntosClienteAnioAct($data['id_cliente'], $data['puntos_total']);
        log_message('error','ADD PUNTOS PUNTOS:' . json_encode($data['puntos_total']));
        //Update puntos Cliente---------->

        echo $id;
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d h:i:s \G\M\T');
        $array = array("id_reg"=>$id,
                        "tabla"=>'ventas',
                        "modificacion"=>'insertar',
                        "campo_ant"=>'',
                        "id_producto"=>'0',
                        "id_usuario"=>$this->session->userdata('usuarioid_tz'),
                        "id_sucursal"=>$this->session->userdata('idsucursal_tz'),
                        'fecha'=>$date
        );
        $this->ModeloGeneral->log_movs('log_cambios',$array);
    }

    function ingresarventapro(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        log_message('error','DAtOS: '.json_encode($datos));
        for ($i=0;$i<count($DATA);$i++) { 
            $data['id_venta'] = $DATA[$i]->idventa;
            $data['id_producto'] = $DATA[$i]->producto;
            $data['id_vendedor'] = $DATA[$i]->id_vendedor;
            $data['cantidad'] =$DATA[$i]->cantidad;
            $data['cantInicial'] =$DATA[$i]->cantIni;
            $data['precio'] =$DATA[$i]->precio;
            $data['descuento'] =$DATA[$i]->descuento;
            $data['sucursalid'] =$this->sucursalid;
            
            $this->ModeloGeneral->tabla_inserta('venta_detalle',$data);
            $where= array('idproducto'=>$DATA[$i]->producto,'idsucursal'=>$this->sucursalid);
            //$datas['existencia']='existencia-'.$DATA[$i]->cantidad;
            //$this->ModeloGeneral->updateCatalogomm($datas,$where,'productos_sucursales');
            $this->ModeloGeneral->updateCatalogostock('existencia','-',$DATA[$i]->cantidad,'productos_sucursales',$where);
            //disminucion de estou de tucursal
            //$this->ModeloVentas->ingresarventad($idventa,$producto,$cantidad,$precio);
           /* date_default_timezone_set('America/Mexico_City');
            $date = date('Y-m-d h:i:s \G\M\T');
            $array = array("id_reg"=>$id,
                            "tabla"=>'productos_sucursales',
                            "modificacion"=>'modifica stock',
                            "campo_ant"=>'',
                            "id_usuario"=>$this->session->userdata('usuarioid_tz'),
                            "id_sucursal"=>$this->session->userdata('idsucursal_tz'),
                            'fecha'=>$date
            );
            $this->ModeloGeneral->log_movs('log_cambios',$array);*/
        }
    }
    function abrirturno(){
        $cantidad = $this->input->post('cantidad');
        $nombre = $this->input->post('nombre');
        $fecha =date('Y').'-'.date('m'). '-'.date('d'); 
        $horaa =date('H:i:s');
        $d_resp = $this->ModeloVentas->abrirturno($cantidad,$nombre,$fecha,$horaa);
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d h:i:s \G\M\T');
        $array = array("id_reg"=>$d_resp,
                        "tabla"=>'abre turno',
                        "modificacion"=>'insertar',
                        "campo_ant"=>'',
                        "id_producto"=>'0',
                        "id_usuario"=>$this->session->userdata('usuarioid_tz'),
                        "id_sucursal"=>$this->session->userdata('idsucursal_tz'),
                        'fecha'=>$date
        );
        $this->ModeloGeneral->log_movs('log_cambios',$array);
    }
    function cerrarturno(){
        $id = $this->input->post('id');
        $fecha =date('Y').'-'.date('m'). '-'.date('d'); 
        $horaa =date('H:i:s');
        $this->ModeloVentas->cerrarturno($id,$horaa);
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d h:i:s \G\M\T');
        $array = array("id_reg"=>$id,
                        "tabla"=>'cierra turno',
                        "modificacion"=>'insertar',
                        "campo_ant"=>'',
                        "id_producto"=>'0',
                        "id_usuario"=>$this->session->userdata('usuarioid_tz'),
                        "id_sucursal"=>$this->session->userdata('idsucursal_tz'),
                        'fecha'=>$date
        );
        $this->ModeloGeneral->log_movs('log_cambios',$array);
    }
    function consultarturno(){
        $id = $this->input->post('id');
        $vturno=$this->ModeloVentas->consultarturno($id);
        foreach ($vturno->result() as $item){
            $fecha= $item->fecha;
            $fechac= $item->fechacierre;
            $horaa= $item->horaa;
            $horac= $item->horac;
        }
        if ($horac=='00:00:00') {
            $horac =date('H:i:s');
            $fechac =date('Y-m-d');
        }
        $totalventas=$this->ModeloVentas->consultartotalturno($fecha,$horaa,$horac,$fechac);
        echo "<p><b>Total: ".number_format($totalventas,2,'.',',')."</b></p>";
        $totalpreciocompra=$this->ModeloVentas->consultartotalturno2($fecha,$horaa,$horac,$fechac);
        $obed =$totalventas-$totalpreciocompra;
        echo "<p><b>Utilidad: ".number_format($obed,2,'.',',')."</b></p>";
        $productos =$this->ModeloVentas->consultartotalturnopro($fecha,$horaa,$horac,$fechac);
        echo "<div class='panel-body table-responsive' id='table'>
                            <table class='table table-striped table-bordered table-hover' id='data-tables2'>
                            <thead class='vd_bg-green vd_white'>
                                <tr>
                                    <th>Producto</th>
                                    <th>Cantidad</th>
                                    <th>Precio</th>
                                    
                                </tr>
                            </thead>
                            <tbody>";
        foreach ($productos->result() as $item){
            echo "<tr><td>".$item->producto."</td><td>".$item->cantidad."</td><td> $".$item->precio."</td></tr>";
        }
        echo "</tbody> </table></div>";
        echo "<div style='    position: absolute; float: left; top: 0; left: 800px; background: white; width: 250px; z-index: 20;'>
                            <table class='table table-striped table-bordered table-hover' id='data-tables'>
                            <thead>
                                <tr>
                                    <td colspan='4' style='text-align: center;'>Productos mas vendido</td>
                                </tr>
                            </thead>
                            <tbody>";
        $productosmas =$this->ModeloVentas->consultartotalturnopromas($fecha,$horaa,$horac,$fechac);
        foreach ($productosmas->result() as $item){
            echo "<tr><td colspan='2' style='text-align: center;'>".$item->total." </td><td colspan='2' style='text-align: center;'>".$item->producto." </td></tr>";
        }
        echo "</tbody> </table></div>";

    }
    function checador(){
        $pro = $this->input->post('codigo');
        $pro = ltrim($pro, '0');
        $where = array('codigo' => $pro,'activo'=>1 );
        $results=$this->ModeloGeneral->getselectwheren2('productos',$where);
        $result=0;
        $html='';
        foreach ($results as $item) {
            $id = $item->productoid;
            $codigo = $item->codigo;
            $nompro = $item->nombre;
            $descripcion = $item->descripcion;
            $nomsuc = $item->nomsuc;
            $cant = $item->existencia;
            $result=1;
        }
        if ($result==1) {
            $html.='<div class="col-md-12">';
               $html.='<p>Producto: <b>'.$nompro.'</b></p>';
               $html.='<p>Código: <b>'.$codigo.'</b></p>';
               $html.='<p>Descripción: <b>'.$descripcion.'</b></p>';
               //$html.='<p>Existencia: <b>'.$cant.'</b></p>';
           $html.='</div>';
          
             // falta la sucursal
            $where_suc = array('idproducto' => $id);
            $results_suc=$this->ModeloGeneral->getselectwheren3('productos_sucursales',$where_suc);
            foreach ($results_suc as $item) {
                if ($this->sucursal==0) {
                    $html.='<div class="col-md-12">';
                       $html.='<p>Precio '.$item->idsucursal.': <b> $ '.$item->precio_venta.'</b></p>';
                   $html.='</div>';
                   $html.='<div class="col-md-12">';
                       $html.='<p>Sucursal: <b>'.$item->nomsuc.'</b></p>';
                       $html.='<p>Existencias '.$item->existencia.'</b></p>';
                   $html.='</div>';
                }elseif ($this->sucursal==$item->idsucursal) {
                    $html.='<div class="col-md-12">';
                       $html.='<p>Precio : <b> $ '.$item->precio_venta.'</b></p>';
                   $html.='</div>';
                   $html.='<div class="col-md-12">';
                       $html.='<p>Sucursal: <b>'.$item->nomsuc.'</b></p>';
                       $html.='<p>Existencias '.$item->existencia.'</b></p>';
                   $html.='</div>';
                   
                } 
                
            }
             /*$html.='<div class="col-md-6">';
               $html.='<p>Producto: <b>'.$nompro.'</b></p>';
               $html.='<p>Código: <b>'.$codigo.'</b></p>';
               $html.='<p>Existencia: <b>'.$cant.'</b></p>';
           $html.='</div>';*/
           //$html.='<hr>';
            $html.='<div class="col-md-12">';
                $html.='<button class="btn btn-raised gradient-green-tea white sidebar-shadow modalchecador" onclick="addproductocheck('.$id.');" >Agregar</button>';      
            $html.='</div>';

        }else{
            $html.='No se encontro resultados';
        }

        echo $html;
    }

    public function facturar($idventa){
        $data['fact']=$this->ModeloVentas->getVentasGral($idventa);
        $data["det_venta"]=$this->ModeloVentas->detallesVentas($idventa);
        $data['cfdi']=$this->ModeloGeneral->getselectwheren('f_uso_cfdi',array("activo"=>1)); 
        $data["id_venta"]=$idventa;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('ventas/ventasFactura',$data);
        $this->load->view('templates/footer');
        $this->load->view('ventas/jsventasFactura');
    }

    function searchunidadsat(){
        $search = $this->input->get('search');
        $results=$this->ModeloVentas->getseleclikeunidadsat($search);
        echo json_encode($results->result());
    }
    function searchconceptosat(){
        $search = $this->input->get('search');
        $results=$this->ModeloVentas->getseleclikeconceptosa($search);
        echo json_encode($results->result());
    }
    function obtenerrfc(){
        $id = $this->input->post('id');
        $r=$this->ModeloVentas->getRFC($id);
        echo $r->rfcdf;
    }
    //=============================================================
    //===================tratar de no mover apartir de aqui =======
    //=============================================================
    public function prefactura(){
        /*$datosconfiguracion=$this->ModeloGeneral->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];*/
        $logotipo = base_url().'public/img/ops.png';
        $data["logotipo"]=$logotipo;
        /*$data["Nombrerasonsocial"]=$datosconfiguracion->Nombre;
        $data['rrfc']=$datosconfiguracion->Rfc;
        $data['rdireccion']=$datosconfiguracion->Calle.' '.$datosconfiguracion->Municipio.' '.$datosconfiguracion->Estado.' '.$datosconfiguracion->PaisExpedicion.' C.P. '.$datosconfiguracion->CodigoPostal;
        $data['regimenf']=$this->gt_regimenfiscal($datosconfiguracion->Regimen);*/
        $data["Nombrerasonsocial"]="ZAPATERIAS AURORA SA DE CV";
        $data['rrfc']="ZAAU101010XXX";
        $data['rdireccion']="CALLE VENUSTIANO CARRANZA No. 15, SAN BARTOLOMÉ, C.P. 90970 SAN PABLO DEL MONTE, TLAXCALA";
        $data['regimenf']="601 General de Ley Personas Morales";
        $data['Folio']=0001;
        $data['folio_fiscal']='XX00X000-000X-0X00-0X0X-X0X00XXXX000';
        $data['nocertificadosat']='00001000000000000000';
        $data['certificado']='00001000000000000000';
        $data['cfdi']=$this->gt_uso_cfdi($_GET['uso_cfdi']);
        $data['fechatimbre']='0000-00-00 00:00:00';

        $dcliente=$this->ModeloGeneral->getselectwheren('clientes',array('ClientesId'=>$_GET['idcliente']));
        foreach ($dcliente as $item) {
            $cliente=$item->razon_social;
            $direccion=$item->direccion_fiscal;
            $cp = $item->cp_fiscal;    
        }

        /*
        $destado=$this->ModeloCatalogos->getselectwheren('estado',array('EstadoId'=>$idestado));
        foreach ($destado->result() as $item) {
            $estado=$item->Nombre;
        }
        */

        //$resultcli=$this->ModeloCatalogos->getselectwheren('cliente_has_datos_fiscales',array('id'=>$_GET['rfc']));
        //$cliente='';
        $clirfc=$_GET['rfc'];
        //$clidireccion='';
        /*
        foreach ($resultcli->result() as $item) {
            $cliente=$item->razon_social;
            $clirfc=$item->rfc;
            $cp = $item->cp;
            $num_ext = $item->num_ext;
            $num_int = $item->num_int;
            $colonia = $item->colonia;
            $calle = $item->calle;
            $cp = $item->cp;

            if($item->estado!=null){
                $estado=$item->estado;
            }
            if($item->municipio!=null){
              $municipio=$item->municipio;   
            }
        }
        */
        
        $clidireccion     =       $direccion ;
        $data['cp']             =   $cp;
        $data['cliente']        =   $cliente;
        $data['clirfc']         =   $clirfc;
        $data['clidireccion']   =   $clidireccion;
        //$data['isr']            =   $_GET['visr'];
        $data['numordencompra'] =   $_GET['numordencompra'];
        //$data['ivaretenido']    =   $_GET['vriva'];
        $data['cedular']        =   0;
       //$data['cincoalmillarval']        =   $_GET['v5millar'];
        //$data['outsourcing']        =   $_GET['outsourcing'];
        $data['numproveedor']   =   $_GET['numproveedor'];
        //$data['observaciones']  =   $_GET['observaciones'];
        $data['observaciones']  =   '';     
        $data['iva']            =   $_GET['iva'];
        $data['subtotal']       =   $_GET['subtotal'];
        $data['total']          =   $_GET['total'];
        $data['moneda']         =   $_GET['moneda'];
        //$data['tarjeta']        =   $_GET['tarjeta'];
        $data['tarjeta']        =   '';
        $data['FormaPago']      =   $_GET['MetodoPago'];
        $data['FormaPagol']     =   $this->gf_FormaPago($_GET['MetodoPago']);
        $data['MetodoPago']     =   $_GET['FormaPago'];
        $data['MetodoPagol']    =   $this->gt_MetodoPago($_GET['FormaPago']);
        $data['tipoComprobante']='I-Ingreso';

        //$data['Caducidad']      =   $_GET['Caducidad'];
        $data['Lote']           =   $_GET['lote'];

        $conseptos=json_decode($_GET['conceptos']);
        $arrayconceptos=array();
        foreach ($conseptos as $item) {
            $arrayconceptos[]=array(
                                    'Cantidad'=>$item->Cantidad,
                                    'Unidad'=>$item->Unidad,
                                    'nombre'=>$this->get_f_undades($item->Unidad),
                                    'servicioId'=>$item->servicioId,
                                    'Descripcion'=>$item->Descripcion,
                                    'Descripcion2'=>$item->Descripcion2,
                                    'Cu'=>$item->Cu,
                                    'descuento'=>$item->descuento
                                );
        }
        $arrayconceptos=json_encode($arrayconceptos);
        $arrayconceptos=json_decode($arrayconceptos);

        $data['facturadetalles']=$arrayconceptos;
        $data['selloemisor']    =   'MDExMTAxMDAgMDExMDAxMDEgMDExMTEwMDAgMDExMTAxMDAgMDExMDExMTEgMDAxMDAwMDAgMDExMDAxMDAgMDExMDAxMDEgMDAxMDAwMDAgMDExMTAwMDAgMDExMTAwMTAgMDExMTAxMDEgMDExMDAxMDEgMDExMDAwMTAgMDExMDAwMDEgMDAxMDAwMDAgMDEwMTEwMTEgMDExMTAwMTEgMDExMDAxMDAgMDExMTAwMTAgMDExMDAxMDEgMDExMTAxMTAgMDExMDAwMDEgMDExMDAxMDEgMDExMDAwMTAgMDEwMTExMDEgMDAxMDAwMDAgMDEwMDAwMDEgMDEwMDAxMTEgMDEwMDAwMTA=';
        $data['sellosat']    =   'MDExMTAxMDAgMDExMDAxMDEgMDExMTEwMDAgMDExMTAxMDAgMDExMDExMTEgMDAxMDAwMDAgMDExMDAxMDAgMDExMDAxMDEgMDAxMDAwMDAgMDExMTAwMDAgMDExMTAwMTAgMDExMTAxMDEgMDExMDAxMDEgMDExMDAwMTAgMDExMDAwMDEgMDAxMDAwMDAgMDEwMTEwMTEgMDExMTAwMTEgMDExMDAxMDAgMDExMTAwMTAgMDExMDAxMDEgMDExMTAxMTAgMDExMDAwMDEgMDExMDAxMDEgMDExMDAwMTAgMDEwMTExMDEgMDAxMDAwMDAgMDEwMDAwMDEgMDEwMDAxMTEgMDEwMDAwMTA=';
        $data['cadenaoriginal'] = '||3.3|U|819|0000-00-00T00:00:00|99|00001000000000000000|CONTADO|'.$_GET['subtotal'].'|MXN|'.$_GET['total'].'|I|'.$_GET['FormaPago'].'|72090|APR980122KZ6|ZAPATERIAS AURORA SA DE CV|'.$_GET['uso_cfdi'].'|'.$clirfc.'|'.$cliente.'|G03|44103103|1.00|H87|Pieza|EJEMPLO|1562.88|1562.88|1562.88|002|Tasa|0.160000|250.06|002|Tasa|0.160000|250.06|250.06||';



        $this->load->view('reportes/factura',$data);
    }   

    function gt_uso_cfdi($text){
          if ($text=='G01') {
            $textl=' G01 Adquisición de mercancias';
          }elseif ($text=='G02') {
            $textl=' G02 Devoluciones, descuentos o bonificaciones';
          }elseif ($text=='G03') {
            $textl=' G03 Gastos en general';
          }elseif ($text=='I01') {
            $textl=' I01 Construcciones';
          }elseif ($text=='I02') {
            $textl=' I02 Mobilario y equipo de oficina por inversiones';
          }elseif ($text=='I03') {
            $textl=' I03 Equipo de transporte';
          }elseif ($text=='I04') {
            $textl=' I04 Equipo de computo y accesorios';
          }elseif ($text=='I05') {
            $textl=' I05 Dados, troqueles, moldes, matrices y herramental';
          }elseif ($text=='I06') {
            $textl=' I06 Comunicaciones telefónicas';
          }elseif ($text=='I07') {
            $textl=' I07 Comunicaciones satelitales';
          }elseif ($text=='I08') {
            $textl=' I08 Otra maquinaria y equipo';
          }elseif ($text=='D01') {
            $textl=' D01 Honorarios médicos, dentales y gastos hospitalarios.';
          }elseif ($text=='D02') {
            $textl=' D02 Gastos médicos por incapacidad o discapacidad';
          }elseif ($text=='D03') {
            $textl=' D03 Gastos funerales.';
          }elseif ($text=='D04') {
            $textl=' D04 Donativos.';
          }elseif ($text=='D05') {
            $textl=' D05 Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).';
          }elseif ($text=='D06') {
            $textl=' >D06 Aportaciones voluntarias al SAR.';
          }elseif ($text=='D07') {
            $textl=' D07 Primas por seguros de gastos médicos.';
          }elseif ($text=='D08') {
            $textl=' D08 Gastos de transportación escolar obligatoria.';
          }elseif ($text=='D09') {
            $textl=' D09 Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.';
          }elseif ($text=='D10') {
            $textl=' D10 Pagos por servicios educativos (colegiaturas)';
          }elseif ($text=='P01') {
            $textl=' P01 Por definir';
          }else{
            $textl='';
          }
          return $textl;
    }
    function gf_FormaPago($text){
      //log_message('error', 'gf_FormaPago:'.$text);
        if ($text=='Efectivo') {
          $textl='01 Efectivo';
        }elseif ($text=='ChequeNominativo') {
          $textl='02 Cheque Nominativo';
        }elseif ($text=='TransferenciaElectronicaFondos') {
          $textl='03 Transferencia ElectronicaFondos';
        }elseif ($text=='TarjetasDeCredito') {
          $textl='04 Tarjetas De Credito';
        }elseif ($text=='MonederoElectronico') {
          $textl='05 Monedero Electronico';
        }elseif ($text=='DineroElectronico') {
          $textl='06 Dinero Electronico';
        }elseif ($text=='Tarjetas digitales') {
          $textl='07 Tarjetas digitales';
        }elseif ($text=='ValesDeDespensa') {
          $textl='08 Vales De Despensa';
        }elseif ($text=='TarjetaDebito') {
          $textl='28 Tarjeta Debito';
        }elseif ($text=='TarjetaServicio') {
          $textl='29 Tarjeta Servicio';
        }elseif ($text=='Otros') {
          $textl='99 Otros';
        }elseif ($text=='DacionPago') {
          $textl='12 Dacion Pago';
        }elseif ($text=='PagoSubrogacion') {
          $textl='13 Pago Subrogacion';
        }elseif ($text=='PagoConsignacion') {
          $textl='14 Pago Consignacion';
        }elseif ($text=='Condonacion') {
          $textl='15 Condonacion';
        }elseif ($text=='Compensacion') {
          $textl='17 Compensacion';
        }elseif ($text=='Novacion') {
          $textl='23 Novacion';
        }elseif ($text=='Confusion') {
          $textl='24 Confusion';
        }elseif ($text=='RemisionDeuda') {
          $textl='25 RemisionDeuda';
        }elseif ($text=='PrescripcionoCaducidad') {
          $textl='26 Prescripciono Caducidad';
        }elseif ($text=='SatisfaccionAcreedor') {
          $textl='27 Satisfaccion Acreedor';
        }elseif ($text=='AplicacionAnticipos') {
          $textl='30 Aplicacion Anticipos';
        }elseif ($text=='PorDefinir') {
          $textl='99 Por definir';
        }
        if ($text=='01') {
          $textl='01 Efectivo';
        }elseif ($text=='02') {
          $textl='02 Cheque Nominativo';
        }elseif ($text=='03') {
          $textl='03 Transferencia ElectronicaFondos';
        }elseif ($text=='04') {
          $textl='04 Tarjetas De Credito';
        }elseif ($text=='05') {
          $textl='05 Monedero Electronico';
        }elseif ($text=='06') {
          $textl='06 Dinero Electronico';
        }elseif ($text=='07') {
          $textl='07 Tarjetas digitales';
        }elseif ($text=='08') {
          $textl='08 Vales De Despensa';
        }elseif ($text=='28') {
          $textl='28 Tarjeta Debito';
        }elseif ($text=='29') {
          $textl='29 Tarjeta Servicio';
        }elseif ($text=='99') {
          $textl='99 Otros';
        }elseif ($text=='12') {
          $textl='12 Dacion Pago';
        }elseif ($text=='13') {
          $textl='13 Pago Subrogacion';
        }elseif ($text=='14') {
          $textl='14 Pago Consignacion';
        }elseif ($text=='15') {
          $textl='15 Condonacion';
        }elseif ($text=='17') {
          $textl='17 Compensacion';
        }elseif ($text=='23') {
          $textl='23 Novacion';
        }elseif ($text=='24') {
          $textl='24 Confusion';
        }elseif ($text=='25') {
          $textl='25 RemisionDeuda';
        }elseif ($text=='26') {
          $textl='26 Prescripciono Caducidad';
        }elseif ($text=='27') {
          $textl='27 Satisfaccion Acreedor';
        }elseif ($text=='30') {
          $textl='30 Aplicacion Anticipos';
        }elseif ($text=='99') {
          $textl='99 Por definir';
        }
        //log_message('error', 'gf_FormaPago:'.$textl);
        return $textl; 
    }
    function gt_regimenfiscal($text){
          if($text="601"){ 
            $textl='601 General de Ley Personas Morales';

          }elseif($text="603"){ 
            $textl='603 Personas Morales con Fines no Lucrativos';

          }elseif($text="605"){ 
            $textl='605 Sueldos y Salarios e Ingresos Asimilados a Salarios';

          }elseif($text="606"){ 
            $textl='606 Arrendamiento';

          }elseif($text="607"){ 
            $textl='607 Régimen de Enajenación o Adquisición de Bienes';

          }elseif($text="608"){ 
            $textl='608 Demás ingresos';

          }elseif($text="609"){ 
            $textl='609 Consolidación';

          }elseif($text="610"){ 
            $textl='610 Residentes en el Extranjero sin Establecimiento Permanente en México';

          }elseif($text="611"){ 
            $textl='611 Ingresos por Dividendos (socios y accionistas)';

          }elseif($text="612"){ 
            $textl='612 Personas Físicas con Actividades Empresariales y Profesionales';

          }elseif($text="614"){ 
            $textl='614 Ingresos por intereses';

          }elseif($text="615"){ 
            $textl='615 Régimen de los ingresos por obtención de premios';

          }elseif($text="616"){ 
            $textl='616 Sin obligaciones fiscales';

          }elseif($text="620"){ 
            $textl='620 Sociedades Cooperativas de Producción que optan por diferir sus ingresos';

          }elseif($text="621"){ 
            $textl='selected="">621 Incorporación Fiscal';

          }elseif($text="622"){ 
            $textl='622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras';

          }elseif($text="623"){ 
            $textl='623 Opcional para Grupos de Sociedades';

          }elseif($text="624"){ 
            $textl='624 Coordinados';

          }elseif($text="628"){ 
            $textl='628 Hidrocarburos';

          }elseif($text="629"){ 
            $textl='629 De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales';

          }elseif($text="630"){ 
            $textl='630 Enajenación de acciones en bolsa de valores';
          }else{
            $textl='';
          }
          return $textl;
    }
    function gt_MetodoPago($text){
      //log_message('error', 'gt_MetodoPago:'.$text);
      if ($text=='PUE') {
          $textl='PUE Pago en una sola exhibicion';
      }else{
          $textl='PPD Pago en parcialidades o diferido';
      }
      //log_message('error', 'gt_MetodoPago:'.$textl);
      return $textl;
    }

    function get_f_undades($unidad){
        $result=$this->ModeloGeneral->getselectwheren('f_unidades',array('Clave'=>$unidad));
        $nombre='';
        foreach ($result as $item) {
            $nombre=$item->nombre;
        }
        return $nombre;
    }

    function generafacturarabierta(){
        $datas = $this->input->post();
        if(isset($datas['conceptos'])){
            $conceptos = $datas['conceptos'];
            unset($datas['conceptos']);
        }
        $id_venta=$datas["id_venta"];

        $idcliente=$datas['idcliente'];
        $rfc=$datas['rfc'];
        //log_message('error', 'idcliente:'.$idcliente);
        $dcliente=$this->ModeloCatalogos->getselectwheren('clientes',array('clientesId'=>$idcliente));
        foreach ($dcliente->result() as $item) {
            $razon_social=$item->razon_social;
            $direccion=$item->direccion_fiscal;
            $cp=$item->cp_fiscal;
            $rfc=$item->rfcdf;
        }
      
        $pais = 'MEXICO';
        $TipoComprobante='I-Ingresos';

        $ultimoFolioval=$this->ModeloCatalogos->ultimoFolio() + 1;
        
        $data = array(
                
                "nombre"        => $razon_social,
                "direccion"     => $direccion, 
                "cp"            => $cp,
                "rfc"           => $rfc,
                "folio"         => $ultimoFolioval,
                "PaisReceptor"  =>$pais,
                "Clientes_ClientesId"       => $idcliente,
                "serie" =>'H',
                //"status"        => 1,
                "TipoComprobante"=>$TipoComprobante,
                "usuario_id"       => $this->idpersonal,
                "rutaXml"           => '',
                "FormaPago"         => $datas['FormaPago'],
                "tarjeta"       => '',
                "MetodoPago"        => $datas['MetodoPago'],
                "ordenCompra"   => '',//no se ocupa
                "moneda"        => $datas['moneda'],
                "observaciones" => '',
                "numproveedor"  => $datas['numproveedor'],
                "numordencompra"=> $datas['numordencompra'],
                "Lote"          => $datas['lote'],
                "Paciente"      => '',//no se ocupa
                //"Caducidad"     => $datas['Caducidad'],//no se ocupa
                "Caducidad"     => '',//no se ocupa
                "uso_cfdi"      => $datas['uso_cfdi'],
                "subtotal"      => $datas['subtotal'],
                "iva"           => $datas['iva'],
                "total"         => $datas['total'],
                "honorario"     => $datas['subtotal'],
                /*"ivaretenido"   => $datas['vriva'],
                "isr"           => $datas['visr'],
                "cincoalmillarval"  => $datas['v5millar'],*/
                "ivaretenido"   => '',
                "isr"           => '',
  
                "CondicionesDePago" => $datas['CondicionesDePago'],
                //"outsourcing" => $datas['outsourcing'],
                "facturaabierta"=>1,
                "id_venta"=>$datas["id_venta"],
                "f_relacion"    => $datas['f_r'],
                "f_r_tipo"      => $datas['f_r_t'],
                "f_r_uuid"      => $datas['f_r_uuid']
            );
        if(isset($datas['pg_periodicidad'])){
            $data['pg_periodicidad']=$datas['pg_periodicidad'];
        }
        if(isset($datas['pg_meses'])){
            $data['pg_meses']=$datas['pg_meses'];
        }
        if(isset($datas['pg_anio'])){
            $data['pg_anio']=$datas['pg_anio'];
        }
        $FacturasId=$this->ModeloGeneral->tabla_inserta('f_facturas',$data);
        $DATAc = json_decode($conceptos);
        for ($i=0;$i<count($DATAc);$i++) {

            $dataco['FacturasId']=$FacturasId;
            $dataco['Cantidad'] =$DATAc[$i]->Cantidad;
            $dataco['Unidad'] =$DATAc[$i]->Unidad;
            $dataco['servicioId'] =$DATAc[$i]->servicioId;
            $dataco['Descripcion'] =rtrim($DATAc[$i]->Descripcion);
            $dataco['Descripcion2'] =$DATAc[$i]->Descripcion2;
            $dataco['Cu'] =$DATAc[$i]->Cu;
            $dataco['descuento'] =$DATAc[$i]->descuento;
            $dataco['Importe'] =($DATAc[$i]->Cantidad*$DATAc[$i]->Cu);
            $dataco['iva'] =$DATAc[$i]->iva;
            $this->ModeloGeneral->tabla_inserta('f_facturas_servicios',$dataco);
        }
        $respuesta=$this->emitirfacturas($FacturasId,$id_venta); 
        echo json_encode($respuesta);
    }

    function retimbrar(){
        $factura = $this->input->post('factura');
        $id_venta = $this->input->post('id_venta');
        $facturaresult=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$factura));
        foreach ($facturaresult->result() as $item) {
            $Folio=$item->Folio;
            if($Folio==0){
            $newfolio=$this->ModeloCatalogos->ultimoFolio() + 1;
            $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Folio'=>$newfolio),array('FacturasId'=>$factura));
            }
        }
        $respuesta=$this->emitirfacturas($factura,$id_venta);
        echo json_encode($respuesta);
    }

    function emitirfacturas($facturaId,$id_venta){
        $productivo=0;//0 demo 1 producccion
        //$this->load->library('Nusoap');
        //require_once APPPATH."/third_party/nusoap/nusoap.php"; 
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];
        //========================
        $datosFacturaa=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$facturaId));
        $datosFacturaa=$datosFacturaa->result();
        $datosFacturaa=$datosFacturaa[0];
        //============
        $datosFactura = array(
          'carpeta'=>'zaurora_sat',
          'pwskey'=>'',
          'archivo_key'=>'',
          'archivo_cer'=>'',
          'factura_id'=>$facturaId
        );
        //================================================
          $respuesta=array();
          $passwordLlavePrivada   = $datosFactura['pwskey'];
          $nombreArchivoKey       = base_url() . $datosFactura['archivo_key'];
          $nombreArchivoCer       = base_url() . $datosFactura['archivo_cer'];
          // ---------------- INICIAN COMANDOS OPENSSL --------------------
          //Generar archivo que contendra cadena de certificado (Convertir Certificado .cer a .pem)
          $comando='openssl x509 -inform der -in '.$nombreArchivoCer.' -out ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/certificate.pem';    
          //echo '<br><br>' . $comando .'<br><br>';
          //exec($comando,$salida_script,$valor_exit);
          $comando='openssl x509 -inform DER -in '.$nombreArchivoCer.' -noout -startdate > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/IniciaVigencia.txt';
          //echo '<br><br>' . $comando .'<br><br>';
          //exec($comando,$salida_script,$valor_exit);
          $comando='openssl x509 -inform DER -in '.$nombreArchivoCer.' -noout -enddate > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/FinVigencia.txt';
          //echo '<br><br>' . $comando .'<br><br>';
          //exec($comando,$salida_script,$valor_exit);
          $comando='openssl x509 -inform DER -in '.$nombreArchivoCer.' -noout -serial > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/Serial.txt';
          //echo '<br><br>' . $comando .'<br><br>';
          //exec($comando,$salida_script,$valor_exit);
          $comando='openssl x509 -inform DER -in '.$nombreArchivoCer.' -noout -text > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/datos.txt';
          //echo '<br><br>' . $comando .'<br><br>';
          //exec($comando,$salida_script,$valor_exit);
          $comando='openssl pkcs8 -inform DER -in '.$nombreArchivoKey.' -passin pass:'.$passwordLlavePrivada.' > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/llaveprivada_pem.txt';
          //exec($comando,$salida_script,$valor_exit);
          $comando='openssl pkcs8 -inform DER -in '.$nombreArchivoKey.' -passin pass:'.$passwordLlavePrivada.' > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/llaveprivada.pem';
          //echo '<br><br>' . $comando .'<br><br>';
          //exec($comando,$salida_script,$valor_exit);
        // ---------------- TERMINAN COMANDOS OPENSSL --------------------      
          //GUARDAR NUMERO DE SERIE EN ARCHIVO     
          //$archivo=fopen(base_url() . $datosFactura['carpeta'] . '/temporalsat/Serial.txt','r');
          //$numeroCertificado= fread($archivo, filesize(base_url() . $datosFactura['carpeta'] . '/temporalsat/Serial.txt'));   
          $numeroCertificado=file_get_contents(base_url() . $datosFactura['carpeta'] . '/temporalsat/Serial.txt');
          //fclose($archivo);
          $numeroCertificado=  str_replace("serial=", "", $numeroCertificado);//quitar letras
          $temporal=  str_split($numeroCertificado);//Pasar todos los digitos a un array
          $numeroCertificado="";
          $i=0;  
          foreach ($temporal as $value) {
              if(($i%2))
                  $numeroCertificado .= $value;
              $i++;
          }
        
        $numeroCertificado = str_replace('\n','',$numeroCertificado);     
        $numeroCertificado = trim($numeroCertificado);
        
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('certificado'=>$numeroCertificado),array('FacturasId'=>$facturaId));
        //$this->model_mango_invoice->actualizaCampoFactura('certificado',$numeroCertificado,$datosFactura['factura_id']);
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('nocertificado'=>$numeroCertificado),array('FacturasId'=>$facturaId));
        //$this->model_mango_invoice->actualizaCampoFactura('nocertificado',$numeroCertificado,$datosFactura['factura_id']);
        
        $respuesta['numeroCertificado']=$numeroCertificado;
        //$archivo = fopen(base_url() . $datosFactura['carpeta'] . '/temporalsat/certificate.pem', "r");
        //$certificado = fread($archivo, filesize(base_url() . $datosFactura['carpeta'] . '/temporalsat/certificate.pem'));
        $certificado=file_get_contents(base_url() . $datosFactura['carpeta'] . '/temporalsat/certificate.pem');
        
        //fclose($archivo);
        $certificado=  str_replace("-----BEGIN CERTIFICATE-----", "", $certificado);
        $certificado=  str_replace("-----END CERTIFICATE-----", "", $certificado);
        
        $certificado=  str_replace("\n", "", $certificado);
        $certificado=  str_replace(" ", "", $certificado);
        $certificado=  str_replace("\n", "", $certificado);
        $certificado= preg_replace("[\n|\r|\n\r]", '', $certificado);
        $certificado= trim($certificado);
        
        $respuesta['certificado']=$certificado;
            
        $arrayDatosFactura=  $this->getArrayParaCadenaOriginal($facturaId);  
        //=================================================================
          $horaactualm1=date('H:i:s');
          $horaactualm1 = strtotime ( '-1 minute' , strtotime ( $horaactualm1 ) ) ;
          $horaactualm1=date ( 'H:i:s' , $horaactualm1 );
          $datosfacturaadd['fechageneracion']=date('Y-m-d').'T'.$horaactualm1;
        //==================================================================
        $xml=  $this->getXML($facturaId,'',$certificado,$numeroCertificado,$datosfacturaadd);
        //echo "contruccion de xml ===========================";  //borrar despues 
        //echo $xml;
        //echo "=====";
        
        log_message('error', 'generaCadenaOriginalReal xml1: '.$xml);
        //log_message('error', '-');log_message('error', '-');log_message('error', '-');log_message('error', '-');log_message('error', '-');
        $str = trim($this->generaCadenaOriginalReal($xml,$datosFactura['carpeta']));    
        log_message('error', 'generaCadenaOriginalReal xml2: '.$str);
        $cadena=$str;
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('cadenaoriginal'=>$str),array('FacturasId'=>$datosFactura['factura_id']));
        //$clienteSOAP = new nusoap_client('https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL',array('soap_version' => SOAP_1_2)); //productivo
        //$clienteSOAP = new nusoap_client('https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL');//productivo
        if($productivo==0){
          $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
        }elseif ($productivo==1) {
          //Produccion
          $URL_WS='https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL';
        }else{
          $URL_WS='';
        }
        
        //$clienteSOAP = new nusoap_client('https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL');//pruebas
        $clienteSOAP = new SoapClient($URL_WS);
        //$clienteSOAP->soap_defencoding='UTF-8';
        $referencia=$datosFactura['factura_id']."-C-". $this->idpersonal;
        
        if($productivo==0){
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }elseif ($productivo==1) {
          //Produccion
          $password= "@fuJXsS@";
          $usuario= "HERD890308UAA";
        }else{
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }
        
        $fp = fopen(base_url() . $datosFactura['carpeta'] . '/temporalsat/llaveprivada.pem', "r");       
        $priv_key = fread($fp, 8192); 
        fclose($fp); 
        $pkeyid = openssl_get_privatekey($priv_key);
    
        //openssl_sign($cadena, $sig, $pkeyid);
        openssl_sign($cadena, $sig, $pkeyid,'sha256');
        openssl_free_key($pkeyid);
        
        $sello = base64_encode($sig);
        //echo $sello;
        $respuesta['sello']=$sello;
        
        //$this->model_mango_invoice->actualizaCampoFactura('sellocadena',$sello,$datosFactura['factura_id']);
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('sellocadena'=>$sello),array('FacturasId'=>$datosFactura['factura_id']));
        //Gererar XML 2013-12-05T12:05:56
        $xml=  $this->getXML($facturaId,$sello,$certificado,$numeroCertificado,$datosfacturaadd);                
        //$fp = fopen(DIR_FILES . $datosFactura['carpeta'] . '/facturas/preview_'. $datosFactura['rfcEmisor'].'_'. $datosFactura['folio'].'.xml', 'w');//Cadena original 
        file_put_contents($datosFactura['carpeta'].'/facturas/preview_'. $datosconfiguracion->Rfc.'_'. $datosFacturaa->Folio.'.xml',$xml);
                
        //Preparar parametros para web service    
        //cadenaXML

        $result = $clienteSOAP->TimbrarCFDI(array(
                    'usuario' => $usuario,
                    'password' => $password,
                    'cadenaXML' => $xml,
                    'referencia' => $referencia));
        file_put_contents($datosFactura['carpeta'] . '/facturaslog/'.'log_facturas_result_'.$this->fechahoyL.'_.txt', json_encode($result));
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Referencia'=>$referencia),array('FacturasId'=>$datosFactura['factura_id']));
    
        if( $result->TimbrarCFDIResult->CodigoRespuesta != '0' ) {                       
            
            $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->TimbrarCFDIResult->CodigoRespuesta,
                          'MensajeError'=>$result->TimbrarCFDIResult->MensajeError,
                          'Mensajeadicional'=>'PORFAVOR RETIMBRAR MAS TARDE',
                          'facturaId'=>$facturaId,
                          'info'=>$result
                          );
            //$this->session->data['error'] = $resultado['CodigoRespuesta'] . ' ' . $resultado['MensajeError'] . ' ' . $resultado['MensajeErrorDetallado']. ' No se puede timbrar la factura verifique la informacion por favor <BR/> PORFAVOR RETIMBRAR MAS TARDE';
                        
            //$this->load->model("setting/general");  
            
            //$this->model_setting_general->saveLog($this->session->data['id_usuario'],$this->session->data['activo'],$resultado,2);
            $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Estado'=>2),array('FacturasId'=>$datosFactura['factura_id']));
            //$this->model_mango_invoice->actualizaCampoFactura('Estado','2',$datosFactura['factura_id']);
            
            //quirar restriccion de salida de sesion por error 12/02/2014 david garduño
            //$this->redirect($this->url->link('common/logout', 'token=' . $this->session->data['token'], 'SSL'));
            
            return $resultado;
        
        } else {
            try {
                $xmlCompleto=utf8_decode($result->TimbrarCFDIResult->XMLResultado);//Contiene XML
                //Guardar en archivo     y la ruta en la bd
                $ruta = $datosFactura['carpeta'] . '/facturas/'.$datosconfiguracion->Rfc.'_'.$datosFacturaa->Folio.'.xml';
                file_put_contents($ruta,"\xEF\xBB\xBF".$xmlCompleto);
                $this->ModeloCatalogos->updateCatalogo('f_facturas',array('rutaXml'=>$ruta),array('FacturasId'=>$datosFactura['factura_id']));       

                //$sxe = new SimpleXMLElement($xmlCompleto);
                //$ns = $sxe->getNamespaces(true);
                //$sxe->registerXPathNamespace('c', $ns['cfdi']);
                //$sxe->registerXPathNamespace('t', $ns['tfd']);
                //$uuid = '';
                //foreach ($sxe->xpath('//t:TimbreFiscalDigital') as $tfd) {          
                    //Actualizamos el Folio Fiscal UUID
                    //$uuid = $tfd['UUID'];
                    $updatedatossat=array(
                      'uuid'=>$result->TimbrarCFDIResult->Timbre->UUID,
                      'sellosat'=>$result->TimbrarCFDIResult->Timbre->SelloSAT,
                      'nocertificadosat'=>$result->TimbrarCFDIResult->Timbre->NumeroCertificadoSAT,
                      'nocertificado'=>$numeroCertificado,
                      'certificado'=>$numeroCertificado,
                      'fechatimbre'=>date('Y-m-d G:i:s'),
                      'Estado'=>1
                    );
                    $this->ModeloCatalogos->updateCatalogo('f_facturas',$updatedatossat,array('FacturasId'=>$datosFactura['factura_id'])); 
                //} 
            
                //$this->ModeloCatalogos->updateCatalogo('f_facturas',array('Estado'=>1),array('FacturasId'=>$datosFactura['factura_id']));

                $resultado = array(
                          'resultado'=>'correcto',
                          'Mensajeadicional'=>'Factura Generada',
                          'facturaId'=>$facturaId
                          );
            return $resultado;
            
           }  catch (Exception $e) {
              $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->TimbrarCFDIResult->CodigoRespuesta,
                          'MensajeError'=>$result->TimbrarCFDIResult->MensajeError,
                          'Mensajeadicional'=>'PORFAVOR RETIMBRAR MAS TARDE',
                          'facturaId'=>$facturaId,
                          'info'=>$result
                          );
             
            $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Estado'=>2),array('FacturasId'=>$datosFactura['factura_id']));
            return $resultado;
           }
           
        }
        return $resultado;
    }

    function getArrayParaCadenaOriginal($facturaId){
      $datosFactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$facturaId));
      $datosFactura=$datosFactura->result();
      $datosFactura=$datosFactura[0];

      $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
      $datosconfiguracion=$datosconfiguracion->result();
      $datosconfiguracion=$datosconfiguracion[0];
      //=======================================================================
            $conceptos = array();
            $datosconceptos=$this->ModeloCatalogos->getselectwheren('f_facturas_servicios',array('FacturasId'=>$facturaId));
            foreach ($datosconceptos->result() as $item) {
              $unidad=$this->ModeloCatalogos->nombreunidadfacturacion($item->Unidad);
              $conceptos[]    =   array(
                                       'cantidad' => $item->Cantidad,
                                       'unidad' =>  $unidad,
                                       'descripcion' =>  $item->Descripcion,
                                       'valorUnnitario' =>  $item->Cu,
                                       'importe' =>  $item->Importe
                                    );
            }
      //=======================================================================
      $datos=array(
                  'comprobante' => array(
                                            'version' => '3.2',  //Requerido                     
                                            'fecha' => $this->fechahoyc,       //Requerido                
                                            'tipoDeComprobante' => $datosFactura->TipoComprobante,//Requedido                       
                                            'formaDePago' => $datosFactura->FormaPago,                       
                                            'subtotal' => $datosFactura->subtotal,                       
                                            'total' => $datosFactura->total,                       
                                            'metodoDePago' => $datosFactura->MetodoPago,                       
                                            'lugarExpedicion' => $datosconfiguracion->LugarExpedicion                       
                                        ),
                  'emisor'    => array(
                                            'rfc' => $datosconfiguracion->Rfc,//Requedido
                                            'domocilioFiscal' => array(
                                                                        'calle' => $datosconfiguracion->Calle,//Requerido
                                                                        'municipio' => $datosconfiguracion->Municipio,//Requerido 
                                                                        'estado' => $datosconfiguracion->Estado,//Requerido 
                                                                        'pais' => $datosconfiguracion->PaisExpedicion,//Requedido
                                                                        'codigoPostal' => $datosconfiguracion->CodigoPostal//Requedido
                                                                        ),
                                            'expedidoEn' => array(
                                                                        'pais' => $datosconfiguracion->PaisExpedicion//Requedido
                                                                 ),
                                            'regimenFiscal' => array(
                                                                        'regimen' => $datosconfiguracion->CodigoPostal//Requedido
                                                                    )
                                      ),
                  'receptor'    => array(
                                            'rfc' => $datosFactura->Rfc,//Requedido
                                            'nombreCliente' => $datosFactura->Nombre,//Opcional
                                            'domicilioFiscal' => array( 
                                                            'calle' => '',//Opcional
                                                            'noExterior' => '',//Opcional
                                                            'colonia' => '',//Opcional                   todos son datos del receptor
                                                            'municipio' => '',//Opcional
                                                            'estado' => '',//Opcional
                                                            'pais' => $datosFactura->PaisReceptor //Requedido
                                                           )
                                      ),
                  'conceptos' => $conceptos,
                  'traslados' => array(
                                        'impuesto' => '002',//iva
                                        'tasa' => 16.00,
                                        'importe' => $datosFactura->subtotal,
                                        'totalImpuestosTrasladados' => $datosFactura->iva
                                      )
                                      
                );
         return $datos;
    }
    public function generaCadenaOriginalReal($xml,$carpeta) {  
        error_reporting(0); 
        $paso = new DOMDocument();
        //log_message('error', 'generaCadenaOriginalReal xml: '.$xml);
        $paso->loadXML($xml);
        $xsl = new DOMDocument();            
        //$file= base_url().$carpeta."/xslt33/cadenaoriginal_3_3.xslt";
        $file= base_url().$carpeta."/xslt40/cadenaoriginal_4_0.xslt";
        $xsl->load($file);
        $proc = new XSLTProcessor();
        // activar php_xsl.dll
        $proc->importStyleSheet($xsl); 
        $cadena_original = $proc->transformToXML($paso);
        //echo '<br><br>' . $cadena_original . '<br><br>';
        $str = mb_convert_encoding($cadena_original, 'UTF-8');                      
        //GUARDAR CADENA ORIGINAL EN ARCHIVO  
        $sello = utf8_decode($str);
        $sello = str_replace('#13','',$sello);
        $sello = str_replace('#10','',$sello);
        //$fp = fopen(DIR_FILES . $carpeta . '/temporalsat/CadenaOriginal.txt', 'w');//Cadena original 
        /*
        $fp = fopen(base_url() . $carpeta . '/temporalsat/CadenaOriginal.txt', 'w');//Cadena original 
        fwrite($fp, $sello);
        fclose($fp);
        */
        file_put_contents( $carpeta . '/temporalsat/CadenaOriginal.txt',$sello);
        return $sello;    
    }

    private function getXML($facturaId,$sello,$certificado,$numeroCertificado,$datosfacturaadd) {
      log_message('error', '$sello c: '.$sello);
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];
        //==============================================================================================================

        $datosFactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$facturaId));
        $datosFactura=$datosFactura->result();
        $datosFactura=$datosFactura[0];
        //====================================
        $clienteId=$datosFactura->Clientes_ClientesId;
        $datoscliente=$this->ModeloCatalogos->getselectwheren('clientes',array('clientesId'=>$clienteId));
        $datoscliente=$datoscliente->result();
        $datoscliente=$datoscliente[0];
        //===============================================================================================================
            $xmlConceptos = '';
            $conceptos = array();
            $datosconceptos=$this->ModeloCatalogos->getselectwheren('f_facturas_servicios',array('FacturasId'=>$facturaId));
            foreach ($datosconceptos->result() as $item) {
                $unidad=$this->ModeloCatalogos->nombreunidadfacturacion($item->Unidad);
                $xmlConceptos.= '<cfdi:Concepto cantidad="' . chop(ltrim(trim($item->Cantidad))) . '" ';
                $xmlConceptos.= 'unidad="' . chop(ltrim($unidad)) . '" descripcion="' . chop(ltrim($item->servicioId)) . '"';
                $xmlConceptos.= ' valorUnitario="' . chop(ltrim(trim(round($item->Cu, 2)))) . '" importe="' . round(($item->Importe), 2) . '"/>';

              $conceptos[]    =   array(
                                       'cantidad' => $item->Cantidad,
                                       'unidad' =>  $item->Unidad,
                                       'descripcion' =>  $item->Descripcion,
                                       'valorUnnitario' =>  $item->Cu,
                                       'importe' =>  $item->Importe
                                    );
            }
        //===============================================================================================================
        
        /******************************************************/
        /*          INICIA ASIGNACION DE VARIABLES           */
        /****************************************************/
        //$certificado=str_replace("\n", "", $certificado);
        $caract   = array('&','ñ','Ñ','°','á','é','í','ó','ú','Á','É','Í','Ó','Ú','  ',' ');
        $caract2  = array('','n','N','','a','e','i','o','u','A','E','I','O','U',' ','');
        $caractc   = array('&','ñ','Ñ','°','á','é','í','ó','ú','Á','É','Í','Ó','Ú');
        $caract2c  = array('','n','N','','a','e','i','o','u','A','E','I','O','U');
        $fecha=$this->fechahoyc;
        $calleEmisor=$datosconfiguracion->Calle;
        $NoexteriorEmisor=$datosconfiguracion->Noexterior;
        $coloniaEmisor=$datosconfiguracion->Colonia;
        $localidadEmisor=$datosconfiguracion->localidad;
        $estadoEmisor=$datosconfiguracion->Estado;
        $municipioEmisor=$datosconfiguracion->Municipio;
        $codigoPostalEmisor=$datosconfiguracion->CodigoPostal;
        $formaPago=$datosFactura->FormaPago;
        $subtotal=$datosFactura->subtotal;
        $total=$datosFactura->total;
        $uso_cfdi=$datosFactura->uso_cfdi;
        $FacturasId=$datosFactura->FacturasId;
        $serie=$datosFactura->serie;
        $f_relacion=$datosFactura->f_relacion;
        $f_r_tipo=$datosFactura->f_r_tipo;
        $f_r_uuid=$datosFactura->f_r_uuid;
        $impuesto='002';
        $tasa=16.00;
        $impuestosTrasladados=$datosFactura->iva;
        if($this->trunquearredondear==0){
          $impuestosTrasladados=round($impuestosTrasladados,2);
        }else{
          $impuestosTrasladados=floor(($impuestosTrasladados*100))/100;
        }
        $tipoComprobante='Ingreso';
        $metodoPago=$datosFactura->MetodoPago;
        $lugarExpedicion=$datosconfiguracion->LugarExpedicion;
        $rfcEmisor=$datosconfiguracion->Rfc;
        $nombreEmisor=$datosconfiguracion->Nombre;
        $paisEmisor=$datosconfiguracion->Pais;
        $paisExpedicion=$datosconfiguracion->PaisExpedicion;
        $regimenFiscal=$datosconfiguracion->Regimen;
        $rfcReceptor=str_replace(' ', '', $datosFactura->Rfc);
        $paisReceptor=$datosFactura->PaisReceptor;
        
        $localidadReceptor='';
        
        $xmlConceptos=$xmlConceptos;
        $nombreCliente=strval(str_replace($caract, $caract2, $datosFactura->Nombre));
        $CondicionesDePago = strval(str_replace($caractc, $caract2c, $datosFactura->CondicionesDePago));
        //$calleReceptor=$datosFactura['calleReceptor'];
        //$noExteriorReceptor=$datosFactura['noExteriorReceptor'];
        //$municipioReceptor=$datosFactura['municipioReceptor'];
        //$estadoReceptor=$datosFactura['estadoReceptor'];
        //$coloniaReceptor=$datosFactura['coloniaReceptor'];
        $folio=$datosFactura->Folio;

        $isr=$datosFactura->isr;
        $ivaretenido=$datosFactura->ivaretenido;
        $cedular=$datosFactura->cedular;
        $cincoalmillarval=$datosFactura->cincoalmillarval;
        $outsourcing=$datosFactura->outsourcing;
        $totalimpuestosretenido=$isr+$ivaretenido+$outsourcing;

        if ($totalimpuestosretenido==0) {
            $TotalImpuestosRetenidoss='';
        }else{
            $TotalImpuestosRetenidoss='TotalImpuestosRetenidos="'.round($totalimpuestosretenido,2).'"';
        }
        if ($cedular!=0) {
            $impuestoslocal='xmlns:implocal="http://www.sat.gob.mx/implocal"';
            $impuestoslocal2=' http://www.sat.gob.mx/implocal http://www.sat.gob.mx/sitio_internet/cfd/implocal/implocal.xsd';
        }else{
            $impuestoslocal='';
            $impuestoslocal2='';
        }
        if ($cincoalmillarval>0) {
            $impuestoslocal='xmlns:implocal="http://www.sat.gob.mx/implocal"';
            $impuestoslocal2=' http://www.sat.gob.mx/implocal http://www.sat.gob.mx/sitio_internet/cfd/implocal/implocal.xsd';
        }else{
            $impuestoslocal='';
            $impuestoslocal2='';
        }

        
        //cambios para poner el acr?nimo de la moneda en el XML
        if ($datosFactura->moneda=="USD") {
            $moneda="USD";
            $TipoCambio='TipoCambio="19.35"';
        }elseif ($datosFactura->moneda=="EUR") {
            $moneda="EUR";
            $TipoCambio='';
        }else{
            $moneda="MXN";
            $TipoCambio='';
        }
        //DATOS DE TIMBRE
        if(isset($datosFactura->uuid)) { 
        
            $uuid = $datosFactura->uuid;
            
        } else {
            
            $uuid= '';   
        }
        if(isset($datosFactura->sellosat)) {
            
            $selloSAT = $datosFactura->sellosat;
        } else {
            
            $selloSAT = '';
        }
        if(isset($datosFactura->certificado)) {   
            $certificadoSAT=$datosFactura->certificado;
        } else {
            $certificadoSAT='';
        }
        /*
        if(!empty($datosFactura['noInteriorReceptor'])) {   
            $noInt = '" noInterior="' . $datosFactura['noInteriorReceptor'] . '';
        } else {
            
            $noInt = '';
        }
        */
        /*  $minuto=date('i');
        $m=$minuto-1;
        $contar=strlen($m);
        if($contar==1)
        {
        $min='0'.$m;
        }else{$min=$m;} */  
        
        $importabasetotal=0;
        
        //.date('Y-m-d').'T'.date('H:i').':'.$segundos.
        
        /******************************************************/
        /*          TERMINA ASIGNACION DE VARIABLES          */
        /****************************************************/   
        /*
        $horaactualm1=date('H:i:s');
        $horaactualm1 = strtotime ( '-1 minute' , strtotime ( $horaactualm1 ) ) ;
        $horaactualm1=date ( 'H:i:s' , $horaactualm1 );
        */
        $factura_detalle = $this->ModeloCatalogos->traeProductosFactura($FacturasId); 
        $descuentogeneral=0;
        foreach ($factura_detalle->result() as $facturadell) {
          $descuentogeneral=$descuentogeneral+$facturadell->descuento;
        }

        if ($descuentogeneral>0) {
          $xmldescuentogene=' Descuento="'.$descuentogeneral.'" ';
        }else{
          $xmldescuentogene=' ';
        }
        if($rfcReceptor=='XAXX010101000'){
            $DomicilioFiscalReceptor=$codigoPostalEmisor;
            $RegimenFiscalReceptor=616;
            $uso_cfdinew='S01';
        }else{
            $DomicilioFiscalReceptor=$datoscliente->cp_fiscal;
            $RegimenFiscalReceptor=$datoscliente->RegimenFiscalReceptor;
            $uso_cfdinew=$uso_cfdi;
        }

        $fechageneracion=$datosfacturaadd['fechageneracion'];
        $xml= '<?xml version="1.0" encoding="utf-8"?>';
        $xml.='<cfdi:Comprobante xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '.$impuestoslocal.' xsi:schemaLocation="http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd'.$impuestoslocal2.'" Exportacion="01" ';
        $xml .= 'Version="4.0" Serie="'.$serie.'" Folio="'.$folio.'" Fecha="'.$fechageneracion.'" FormaPago="'.$metodoPago.'" CondicionesDePago="'.$CondicionesDePago.'" ';
        //$xml .= 'SubTotal="'.$subtotal.'" Descuento="0.00" Moneda="'.$moneda.'" TipoCambio="1" Total="'.$total.'" TipoDeComprobante="I" MetodoPago="'.$formaPago.'" LugarExpedicion="' .$codigoPostalEmisor . '" ';
        $xml .= 'SubTotal="'.$subtotal.'" Moneda="'.$moneda.'" '.$xmldescuentogene.' '.$TipoCambio.'  Total="'.round($total,2).'" TipoDeComprobante="I" MetodoPago="'.$formaPago.'" LugarExpedicion="' .$codigoPostalEmisor . '" ';
        $xml .='xmlns:cfdi="http://www.sat.gob.mx/cfd/4" NoCertificado="'.$numeroCertificado.'" Certificado="'.$certificado.'" Sello="'.$sello.'">';
                if($f_relacion==1){
                    $xml .='<cfdi:CfdiRelacionados TipoRelacion="'.$f_r_tipo.'">
                                <cfdi:CfdiRelacionado UUID="'.$f_r_uuid.'"/>
                            </cfdi:CfdiRelacionados>';
                }
                if($rfcReceptor=='XAXX010101000'){
                    $xml .= '<cfdi:InformacionGlobal Año="'.$datosFactura->pg_anio.'" Meses="'.$datosFactura->pg_meses.'" Periodicidad="'.$datosFactura->pg_periodicidad.'"/>';
                }
        $xml .= '<cfdi:Emisor Nombre="'.$nombreEmisor.'" RegimenFiscal="'.$regimenFiscal.'" Rfc="'.$rfcEmisor.'"></cfdi:Emisor>';
        $xml .= '<cfdi:Receptor Rfc="'.$rfcReceptor.'" Nombre="'.$datoscliente->razon_social.'" DomicilioFiscalReceptor="'.$DomicilioFiscalReceptor.'" RegimenFiscalReceptor="'.$RegimenFiscalReceptor.'" UsoCFDI="'.$uso_cfdinew.'" ></cfdi:Receptor>';
        $xml .='<cfdi:Conceptos>';
        
        //$isr=$datosFactura['isr'];
        //$ivaretenido=$datosFactura['ivaretenido'];
        //$cedular=$datosFactura['cedular'];

        
        $partidaproducto=1;
        $niva=$tasa/100;
        $comparariva = $datosFactura->iva;//colocar
        foreach ($factura_detalle->result() as $facturadell) {
            $valorunitario=$facturadell->Cu;
            if($facturadell->descuento>0){
              $xmldescuento=' Descuento="'.$facturadell->descuento.'" ';
            }else{
              $xmldescuento='';
            }
            if($comparariva>0){
                $ObjetoImp='02';//si, objeto de impuestos
            }else{
                $ObjetoImp='01';//no objeto de impuesto
            }
            
            //$xml .='<cfdi:Concepto ClaveUnidad="'.$facturadell['cunidad'].'" Unidad="'.$facturadell['nombre'].'" ClaveProdServ="'.$facturadell['ClaveProdServ'].'" NoIdentificacion="'.str_pad((int) $partidaproducto,10,"0",STR_PAD_LEFT).'" Cantidad="'.$facturadell['Cantidad'].'" Descripcion="'.$facturadell['Descripcion'].'" ValorUnitario="'.$facturadell['Cu'].'" Importe="'.$facturadell['Importe'].'" Descuento="0.00">';
            $xml .='<cfdi:Concepto ClaveUnidad="'.$facturadell->cunidad.'" ClaveProdServ="'.$facturadell->ClaveProdServ.'"';
             //$xml.='NoIdentificacion="'.str_pad((int) $partidaproducto,10,"0",STR_PAD_LEFT).'" ';
             $xml.=' Cantidad="'.$facturadell->Cantidad.'" Unidad="'.$facturadell->nombre.'" Descripcion="'.utf8_encode(str_replace($caract, $caract2, $facturadell->Descripcion)).'" ValorUnitario="'.$valorunitario.'" Importe="'.round($facturadell->Importe,2).'" '.$xmldescuento.' ObjetoImp="'.$ObjetoImp.'">';
                if($comparariva>0){
                  $importabase=$facturadell->Importe-$facturadell->descuento;
                  if($this->trunquearredondear==0){
                    $trasladadoimporte=round($importabase*$niva,4);
                  }else{
                    $trasladadoimporte=$importabase*$niva;
                    $trasladadoimporte=floor(($trasladadoimporte*100))/100;
                  }
                $xml .='<cfdi:Impuestos>';
                    $xml .='<cfdi:Traslados>';
                    $importabasetotal=$importabasetotal+$importabase;
                        $xml .='<cfdi:Traslado Base="'.$importabase.'" Impuesto="002" TipoFactor="Tasa" TasaOCuota="'.$niva.'0000" Importe="'.$trasladadoimporte.'"></cfdi:Traslado>';
                    $xml .='</cfdi:Traslados>';
                    //-----------------------------------------------------------------------------------
                    if($isr!=0 ||$ivaretenido!=0||$outsourcing>0){
                        $xml .='<cfdi:Retenciones>';
                        if ($isr!=0) {
                            $xml .='<cfdi:Retencion Base="'.$facturadell->Importe.'" Impuesto="001" TipoFactor="Tasa" TasaOCuota="0.100000" Importe="'.round($facturadell->Importe*0.100000,2).'"></cfdi:Retencion>';
                        }
                        if ($ivaretenido!=0) {
                            $xml .='<cfdi:Retencion Base="'.$facturadell->Importe.'" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.106666" Importe="'.round($facturadell->Importe*0.106666,2).'"></cfdi:Retencion>'; 
                        }   
                        if ($outsourcing!=0) {
                            $xml .='<cfdi:Retencion Base="'.$facturadell->Importe.'" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.060000" Importe="'.round($facturadell->Importe*0.06,2).'"></cfdi:Retencion>'; 
                        }      
                        $xml .='</cfdi:Retenciones>';
                    }
                    //------------------------------------------------------------------------------------- 
                    //$xml .='<cfdi:Retenciones>';
                    //    $xml .='<cfdi:Retencion Base="1000" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.160000" Importe="0.00" />';      
                    //$xml .='</cfdi:Retenciones>';
                $xml .='</cfdi:Impuestos>';
            }
            $xml .='</cfdi:Concepto>';
            $partidaproducto++;
        }
        $xml .='</cfdi:Conceptos>';
        if($comparariva>0){
            //$xml .='<cfdi:Impuestos TotalImpuestosRetenidos="0.00" TotalImpuestosTrasladados="'.$impuestosTrasladados.'">';
            $xml .='<cfdi:Impuestos '.$TotalImpuestosRetenidoss.' TotalImpuestosTrasladados="'.$impuestosTrasladados.'">';
                if($isr!=0 ||$ivaretenido!=0||$outsourcing>0){
                    $xml .='<cfdi:Retenciones>';
                    if ($isr!=0) {
                        $xml .='<cfdi:Retencion Impuesto="001" Importe="'.number_format(round($isr,2), 2, ".", "").'"></cfdi:Retencion>';
                    }
                    if ($ivaretenido!=0) {
                        $xml .='<cfdi:Retencion Impuesto="002" Importe="'.number_format(round($ivaretenido,2), 2, ".", "").'"></cfdi:Retencion>'; 
                    } 
                    if ($outsourcing>0) {
                        $xml .='<cfdi:Retencion Impuesto="002" Importe="'.number_format(round($outsourcing,2), 2, ".", "").'"></cfdi:Retencion>'; 
                    }      
                    $xml .='</cfdi:Retenciones>';
                }
               
                
                
                $xml .='<cfdi:Traslados>';
                    
                        $xml .='<cfdi:Traslado Base="'.$importabasetotal.'" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.160000" Importe="'.$impuestosTrasladados.'"></cfdi:Traslado>';    
                $xml .='</cfdi:Traslados>';
            $xml .='</cfdi:Impuestos>';
        }
        /*
        if($cedular!=0){
            
        }
        */
        if($cedular!=0||$uuid||$cincoalmillarval>0) {            
            $xml .= '<cfdi:Complemento>';
            if ($cedular!=0) {
                //$xml .= '<cfdi:Complemento xmlns:cfdi="http://www.sat.gob.mx/cfd/3" >';
                $xml .= '<implocal:ImpuestosLocales version="1.0" TotaldeTraslados="0.00" TotaldeRetenciones="'.$cedular.'" >';
                $xml .= '<implocal:RetencionesLocales  TasadeRetencion="01.00" ImpLocRetenido="CEDULAR" Importe="'.$cedular.'"/>';
                $xml .= '</implocal:ImpuestosLocales>';
            }
            if ($cincoalmillarval>0) {
                $xml .= '<implocal:ImpuestosLocales version="1.0" TotaldeTraslados="0.00" TotaldeRetenciones="'.$cincoalmillarval.'" >';
                  $xml .= '<implocal:RetencionesLocales Importe="'.$cincoalmillarval.'" TasadeRetencion="0.50" ImpLocRetenido=".005 Insp y Vig" />';
                $xml .= '</implocal:ImpuestosLocales>';
            }
            if ($uuid) {
                $xml .='<tfd:TimbreFiscalDigital Version="1.1" RfcProvCertif="FLI081010EK2" ';
                $xml .='UUID="'.$uuid.'" FechaTimbrado="'.$fecha.'" ';
                $xml .='SelloCFD="'.$sello.'" NoCertificadoSAT="'.$certificadoSAT.'" SelloSAT="'.$selloSAT.'" ';
                $xml .='xmlns:tfd="http://www.sat.gob.mx/TimbreFiscalDigital" ';
                $xml .= 'xsi:schemaLocation="http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/cfd/TimbreFiscalDigital/TimbreFiscalDigitalv11.xsd" />';
            }
            $xml .= '</cfdi:Complemento>';
        }
        $xml .= '</cfdi:Comprobante>';
        return $xml;
    }

    /*function cancelarCfdi(){
      $rutainterna= $_SERVER['DOCUMENT_ROOT'].'/zaurora';//rutalocal
      //$rutainterna= $_SERVER['DOCUMENT_ROOT'];//rutaserver
      $ruta=$rutainterna.'/zaurora_sat/temporalsat/';
      $rutaf=$rutainterna.'/zaurora_sat/facturas/';
      $rutalog=$rutainterna.'/zaurora_sat/facturaslog/';

      $productivo=1;//0 demo 1 producccion
      $datas = $this->input->post();
      $facturas=$datas['facturas'];
      unset($datas['facturas']);

      $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
      $datosconfiguracion=$datosconfiguracion->result();
      $datosconfiguracion=$datosconfiguracion[0];
      $rFCEmisor=$datosconfiguracion->Rfc;
      $passwordClavePrivada=$datosconfiguracion->paswordkey;

      $datosCancelacion=array();
      $DATAf = json_decode($facturas);
      for ($i=0;$i<count($DATAf);$i++) {
        $datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$DATAf[$i]->FacturasIds));
        $datosfactura=$datosfactura->result();
        $datosfactura=$datosfactura[0];
        $FacturasId=$datosfactura->FacturasId;
        $datosCancelacion[] = array(
                                  'Propiedad'=>'cancelacion',
                                  'RFCReceptor'=>$datosfactura->Rfc,
                                  'Total'=>round($datosfactura->total, 2),
                                  'UUID'=>$datosfactura->uuid

                                );
      }
      //===========================================================
        if($productivo==0){
          $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
        }elseif ($productivo==1) {
          //Produccion
          $URL_WS='https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL';
        }else{
          $URL_WS='';
        }
        if($productivo==0){
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }elseif ($productivo==1) {
          //Produccion
          $password= "@fuJXsS@";
          $usuario= "HERD890308UAA";
        }else{
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }
      //=======================================================
        
        $fp = fopen($ruta.'pfx.pem', "r");
        $clavePrivada = fread($fp, filesize($ruta.'pfx.pem'));
        fclose($fp);
        
        
        $rutaarchivos=$rutainterna.'/hulesyg/elementos/';
        $rutapass=$rutaarchivos.'passs.txt';
        $passwordClavePrivada = file_get_contents($rutapass);
        
        //$passwordClavePrivada='Daniel182 ';// solo para las pruebas
        
        $parametros = array(
            'usuario' => $usuario,
            'password' => $password,
            'rFCEmisor' => $rFCEmisor,
            'listaCFDI' => $datosCancelacion,
            'clavePrivada_Base64'=>$clavePrivada,
            'passwordClavePrivada'=> $passwordClavePrivada  );
        file_put_contents($rutalog.'log_'.$this->fechahoyL.'xxxxx_.txt', json_encode($parametros));
        $clienteSOAP = new SoapClient($URL_WS);

        $result = $clienteSOAP->CancelarCFDIConValidacion($parametros);

        if($result->CancelarCFDIConValidacionResult->OperacionExitosa) {
          $resultado = array(
                          'resultado'=>'correcto',
                          'Mensajeadicional'=>'Factura Cancelada',
                          'info'=>$result
                          );
          //Guardar en archivo  y la ruta en la bd del acuse de la cancelacion
                $fileacuse='acuseCancelacion_'.$FacturasId.'_'.$datosfactura->uuid.'.xml';
                $ruta = $rutaf .$fileacuse;
                //file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
                file_put_contents($ruta,$result->CancelarCFDIConValidacionResult->XMLAcuse);

                $datosdecancelacion = array(
                                            'Estado'=>0,
                                            'rutaAcuseCancelacion '=>$fileacuse,
                                            'fechacancelacion'=>$this->fechahoyc
                                          );
                $this->ModeloCatalogos->updateCatalogo('f_facturas',$datosdecancelacion,array('FacturasId'=>$FacturasId));
          
        }else{
          $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->CancelarCFDIConValidacionResult->MensajeError,
                          'MensajeError'=>$result->CancelarCFDIConValidacionResult->MensajeErrorDetallado,
                          'Mensajeadicional'=>'PORFAVOR CANCELAR MAS TARDE',
                          'info'=>$result
                          );
          file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
        }
        
        echo json_encode($resultado);
    }*/

    function cancelarCfdi(){
      $rutainterna= $_SERVER['DOCUMENT_ROOT'].'/zaurora';//rutalocal
      //$rutainterna= $_SERVER['DOCUMENT_ROOT'];//rutaserver
      $ruta=$rutainterna.'/zaurora_sat/temporalsat/';
      $rutaf=$rutainterna.'/zaurora_sat/facturas/';
      $rutalog=$rutainterna.'/zaurora_sat/facturaslog/';

      $productivo=1;//0 demo 1 producccion
      $datas = $this->input->post();
      $facturas=$datas['facturas'];
      $motivo=$datas['motivo'];
      $uuidrelacionado=$datas['uuidrelacionado'];

      unset($datas['facturas']);
      unset($datas['motivo']);
      unset($datas['uuidrelacionado']);

      $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
      $datosconfiguracion=$datosconfiguracion->result();
      $datosconfiguracion=$datosconfiguracion[0];
      $rFCEmisor=$datosconfiguracion->Rfc;
      $passwordClavePrivada=$datosconfiguracion->paswordkey;

      $datosCancelacion=array();
      $DATAf = json_decode($facturas);
      for ($i=0;$i<count($DATAf);$i++) {
        $datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$DATAf[$i]->FacturasIds));
        $datosfactura=$datosfactura->result();
        $datosfactura=$datosfactura[0];
        $FacturasId=$datosfactura->FacturasId;
        $datosCancelacion[] = array(
                                  'RFCReceptor'=>$datosfactura->Rfc,
                                  'Total'=>round($datosfactura->total, 2),
                                  'UUID'=>$datosfactura->uuid,
                                  'Motivo'=>$motivo,
                                  'FolioSustitucion'=>$uuidrelacionado

                                );
      }
      //===========================================================
        if($productivo==0){
          $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
        }elseif ($productivo==1) {
          //Produccion
          $URL_WS='https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL';
        }else{
          $URL_WS='';
        }
        if($productivo==0){
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }elseif ($productivo==1) {
          //Produccion
          $password= "@fuJXsS@";
          $usuario= "HERD890308UAA";
        }else{
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }
      //=======================================================
        
        $fp = fopen($ruta.'pfx.pem', "r");
        $clavePrivada = fread($fp, filesize($ruta.'pfx.pem'));
        fclose($fp);
        
        
        $rutaarchivos=$rutainterna.'/zaurora_sat/elementos/';
        $rutapass=$rutaarchivos.'passs.txt';
        $passwordClavePrivada = file_get_contents($rutapass);
        
        //$passwordClavePrivada='Daniel182 ';// solo para las pruebas
        
        $parametros = array(
            'usuario' => $usuario,
            'password' => $password,
            'rFCEmisor' => $rFCEmisor,
            'listaCFDI' => $datosCancelacion,
            'clavePrivada_Base64'=>$clavePrivada,
            'passwordClavePrivada'=> $passwordClavePrivada  );
        file_put_contents($rutalog.'log_'.$this->fechahoyL.'xxxxx_.txt', json_encode($parametros));
        $clienteSOAP = new SoapClient($URL_WS);

        $result = $clienteSOAP->CancelarCFDI($parametros);

        $fileacuse1='log_cancelcion2_'.$FacturasId.'_'.$datosfactura->uuid.'.xml';
        $ruta1 = $rutalog .$fileacuse1;

        file_put_contents($ruta1,json_encode($result));

        if($result->CancelarCFDIResult->OperacionExitosa) {
          $resultado = array(
                          'resultado'=>'correcto',
                          'Mensajeadicional'=>'Factura Cancelada',
                          'info'=>$result
                          );
          //Guardar en archivo  y la ruta en la bd del acuse de la cancelacion
                $fileacuse='acuseCancelacion_'.$FacturasId.'_'.$datosfactura->uuid.'.xml';
                $ruta = $rutaf .$fileacuse;
                //file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
                file_put_contents($ruta,$result->CancelarCFDIResult->XMLAcuse);

                $datosdecancelacion = array(
                                            'Estado'=>0,
                                            'rutaAcuseCancelacion '=>$fileacuse,
                                            'fechacancelacion'=>$this->fechahoyc
                                          );
                $this->ModeloCatalogos->updateCatalogo('f_facturas',$datosdecancelacion,array('FacturasId'=>$FacturasId));
          
        }else{
          $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->CancelarCFDIResult->MensajeError,
                          'MensajeError'=>$result->CancelarCFDIResult->MensajeErrorDetallado,
                          'Mensajeadicional'=>'PORFAVOR CANCELAR MAS TARDE',
                          'info'=>$result
                          );
          file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
        }
        
        echo json_encode($resultado);
    }

  private function checkPorcentajePuntos($FCompra)
  {
    $porcentajePuntos = 5;

    $dataPuntos = $this->ModeloPuntos->getDataConfigPuntos();
    //log_message("error", "dataPuntos: " . json_encode($dataPuntos));

    if ($dataPuntos) {
      $fechaIniConfig = (new DateTime($dataPuntos->fechaIni))->setTime(0, 0, 0);
      $fechaFinConfig = (new DateTime($dataPuntos->fechaFin))->setTime(0, 0, 0);
      $fechaComprobar = (new DateTime($FCompra))->setTime(0, 0, 0);

      log_message("error", "Fecha de Compra: " . $fechaComprobar->format('Y-m-d'));

      if ($dataPuntos->checkFecha == 1) {
        log_message("error", "Rango 1 desde: " . $fechaIniConfig->format('Y-m-d'));
        if ($fechaComprobar >= $fechaIniConfig) {
          log_message("error", "La fecha " . $fechaComprobar->format('Y-m-d') . " está en el rango 1.");
          $porcentajePuntos = $dataPuntos->porcentaje;
        }
      } else {
        log_message("error", "Rango 0 desde: " . $fechaIniConfig->format('Y-m-d') . " hasta: " . $fechaFinConfig->format('Y-m-d'));
        if ($fechaComprobar >= $fechaIniConfig && $fechaComprobar <= $fechaFinConfig) {
          log_message("error", "La fecha " . $fechaComprobar->format('Y-m-d') . " está dentro del rango 0.");
          $porcentajePuntos = $dataPuntos->porcentaje;
        }
      }
    }

    //Revision de porcentajes------------------------------
    //log_message('error','Porcentaje1: '. $porcentajePuntos);
    $porcentajePuntos = $porcentajePuntos > 100 ? 100 : $porcentajePuntos;
    $porcentajePuntos = $porcentajePuntos < 0 ? 0 : $porcentajePuntos;
    //log_message('error','Porcentaje2: '. $porcentajePuntos);
    //Revision de porcentajes------------------------------

    $porcentajePuntos = $porcentajePuntos / 100;
    log_message("error", "Porcentaje Puntos: " . json_encode($porcentajePuntos));
    return $porcentajePuntos;
  }

  private function checkPorcentajePuntosCliente($idCliente)
  {
    $porcentajePuntos = 0;

    $dataPuntosC = $this->ModeloPuntos->getDataConfigPuntosCliente($idCliente);
    log_message("error", "dataPuntosC: " . json_encode($dataPuntosC));

    if ($dataPuntosC) {
      $porcentajePuntos = $dataPuntosC->porcentaje;

      //log_message('error','Porcentaje1: '. $porcentajePuntos);
      $porcentajePuntos = $porcentajePuntos > 100 ? 100 : $porcentajePuntos;
      $porcentajePuntos = $porcentajePuntos < 0 ? 0 : $porcentajePuntos;
      //log_message('error','Porcentaje2: '. $porcentajePuntos);
    }

    $porcentajePuntos = $porcentajePuntos / 100;
    log_message("error", "Porcentaje Puntos: " . json_encode($porcentajePuntos));
    return $porcentajePuntos;
  }

  public function getPuntosCliente()
  {
    $id = $this->input->post('id');

    $this->checkVigenciaPuntos($id);

    //$total = $this->ModeloPuntos->getTotalPuntos($id);
    $data = $this->ModeloGeneral->getselectwheren('clientes',array('ClientesId'=>$id,'activo'=>1));
    $datos = array();
    
    if($data){
      foreach ($data as $key) {
        $datos = array('cliente' => $key->Nom,'puntos' => $key->puntos) ;
      }
    }

    echo json_encode($datos);
  }

  private function getPuntosActuales($id)
  {
    //$id = $this->input->post('id');
    $total = $this->ModeloPuntos->getTotalPuntos($id);
    return $total;
  }


  private function checkVigenciaPuntos($id_cliente){
    $anio = $this->ModeloPuntos->getAnioCliente($id_cliente);
    
    log_message('error','anioC: '. $this->current_anio);
    log_message('error','anioN: '. $anio);

    log_message('error','AUX 1: '.(int)$anio);

    if((int)$anio == 2024){
      log_message('error','AUX IF: ');
      $this->ModeloPuntos->auxPuntosY2024($id_cliente);
    }


    if ((int)$this->current_anio - (int)$anio >= 1) {
      $this->ModeloPuntos->setAnioYPuntosCliente($id_cliente, $this->current_anio);

      $this->ModeloPuntos->reviewNegativePoints($id_cliente);

    }
  }

}