<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ReporteMovs extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloReportes');
    }

	public function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('Reportes/movimientos');
        $this->load->view('templates/footer');
        $this->load->view('Reportes/jsmovs');
	}

    public function getData_sessions() {

        $params = $this->input->post();
        $paramss=json_encode($params);

        $sesiones = $this->ModeloReportes->listadoMovs($params);
        $totalRecords=$this->ModeloReportes->filasMovs();

        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $sesiones->result(),
            "query"           =>$this->db->last_query()   
        ); 
        echo json_encode($json_data);
  }

    function movsBusqueda(){
        $txtInicio = $this->input->post('txtInicio');
        $txtFin = $this->input->post('txtFin');

        $params = $this->input->post();
        $paramss=json_encode($params);
        //log_message('error', '....'.$paramss);
        $movs = $this->ModeloReportes->movimientosFechas($params);
        $totalRecords=$this->ModeloReportes->filasMovsFecha($txtInicio,$txtFin);

        $json_data = array(
            "draw"            => intval($params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $movs->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function registrosLogAgenda(){
        $params = $this->input->get();
        log_message('error', 'start. '.$params["start"]);
        $eventos = $this->ModeloReportes->getReportes($params);
        echo json_encode($eventos);
    }

    public function detalleLog(){
        $id = $this->input->post("id");
        $result = $this->ModeloReportes->getDetalleLog($id);
        echo json_encode($result);
    }

}
