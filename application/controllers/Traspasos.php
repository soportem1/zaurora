<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Traspasos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloTraspasos');
        $this->load->model('ModeloGeneral');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d  H:i:s');
        $logueo = $this->session->userdata();
        $this->personal=$logueo['idpersonal_tz'];
    }
	public function index(){       
    $data["sucursal"] = $this->ModeloGeneral->getselectwhere('sucursal','estatus',1);
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('traspasos/traspasos',$data);
    $this->load->view('templates/footer');
    $this->load->view('traspasos/traspasosjs');
	}
  public function getData_traspasos() {

        $params = $this->input->post();
        $paramss=json_encode($params);
        //log_message('error', '....'.$paramss);
        $traspasos = $this->ModeloTraspasos->listado($params);

        $totalRecords=$this->ModeloTraspasos->filas();

        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $traspasos->result(),
            "query"           =>$this->db->last_query()   
        );
        
        echo json_encode($json_data);
  }
  function cstock(){
      $producto = $this->input->post('producto');
      $sucursal = $this->input->post('sucursal');
      $where = array('idsucursal'=>$sucursal,'idproducto'=>$producto);
      $resultado= $this->ModeloGeneral->getselectwheren('productos_sucursales',$where);
        $stock=0;
      foreach ($resultado as $item) {
        $stock=$item->existencia;
      }
      echo $stock;
  }
  function traspasar(){
      $producto = $this->input->post('producto');
      $sucursals = $this->input->post('sucursals');
      $sucursale = $this->input->post('sucursale');
      $cantidad = $this->input->post('cantidad');

      $data_tr['productoId'] =$producto;
      $data_tr['suc_salida'] =$sucursals;
      $data_tr['suc_entrada'] =$sucursale;
      $data_tr['cantidad'] =$cantidad;
      $data_tr['personalId'] =$this->personal;
      $data_tr['reg'] =$this->fechahoy;
      $this->ModeloGeneral->tabla_inserta('traspasos',$data_tr);
      $wheres = array('idsucursal' => $sucursals,'idproducto'=>$producto);
      $this->ModeloGeneral->updateCatalogostock('existencia','-',$cantidad,'productos_sucursales',$wheres);

      $wheres = array('idsucursal' => $sucursale,'idproducto'=>$producto);
      $this->ModeloGeneral->updateCatalogostock('existencia','+',$cantidad,'productos_sucursales',$wheres);
  }
}
