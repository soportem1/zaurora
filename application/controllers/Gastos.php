<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gastos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloGeneral');
        $this->load->model('ModeloGastos');
    }
	public function index(){
        $pages=10; //Número de registros mostrados por páginas
        $this->load->library('pagination'); //Cargamos la librería de paginación
        $config['base_url'] = base_url().'Gastos/view/'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
        $config['total_rows'] = $this->ModeloGastos->filas();//calcula el número de filas
        $config['per_page'] = $pages; //Número de registros mostrados por páginas  
        $config['num_links'] = 20; //Número de links mostrados en la paginación
        $config['first_link'] = 'Primera';//primer link
        $config['last_link'] = 'Última';//último link
        $config["uri_segment"] = 3;//el segmento de la paginación
        $config['next_link'] = 'Siguiente';//siguiente link
        $config['prev_link'] = 'Anterior';//anterior link
        $this->pagination->initialize($config); //inicializamos la paginación 
        $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["gastos"] = $this->ModeloGastos->total_paginados($pagex,$config['per_page']);

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('gastos/gastos');
        $this->load->view('templates/footer');
        $this->load->view('gastos/jsgastos');
	}

    public function getData_gastos() {

        $params = $this->input->post();
        //$paramss=json_encode($params);
        //log_message('error', '....'.$paramss);
        $gastos = $this->ModeloGastos->listado($params);

        $totalRecords=$this->ModeloGastos->listado_total($params);

        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $gastos->result(),
            "query"           =>$this->db->last_query()   
        );
        
        echo json_encode($json_data);
  }

    public function Gastoadd(){
            $where_e = array('status'=>1);
            $data['gastos']=$this->ModeloGeneral->getselectwheren('gastos',$where_e);
            $data['menuse']=$this->ModeloPersonal->getAllmenu();
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('gastos/gastoadd',$data);
            $this->load->view('templates/footer');
            $this->load->view('gastos/jsgastos');
    }  
     
    public function insertarGasto(){
        $concepto = $this->input->post('concepto');
        $monto = $this->input->post('monto');
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_sucursal= $this->session->userdata('idsucursal_tz');
        $contenido=array(
            'concepto' => $concepto,
            'monto' => $monto,
            'fecha' => $date,
            'id_usuario' => $this->session->userdata('usuarioid_tz'), //se actualiza a id de usuario, no es de personal, la consulta del listado hace join a usuario
            //'id_sucursal' => $_SESSION['idsucursal_tz'],
            'id_sucursal' => $id_sucursal,
            'status' => 1
        );

        if($id>0) {
            $this->ModeloGastos->updateGasto($id,$contenido);
            $idres = $id;
            $tipo_mod = "modifica";
        }else{
            $idres=$this->ModeloGastos->insertarGasto($contenido);
            $tipo_mod = "inserta";
        }
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d h:i:s \G\M\T');
        $array = array("id_reg"=>$idres,
                        "tabla"=>'gastos',
                        "modificacion"=>$tipo_mod,
                        "campo_ant"=>'',
                        "id_producto"=>'0',
                        "id_usuario"=>$this->session->userdata('usuarioid_tz'),
                        "id_sucursal"=>$this->session->userdata('idsucursal_tz'),
                        'fecha'=>$date
        );
        $this->ModeloGeneral->log_movs('log_cambios',$array);
       
    }
     public function deleteGasto(){
        $id = $this->input->post('id');
        $this->ModeloGastos->eliminarGasto($id);
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d h:i:s \G\M\T');
        $array = array("id_reg"=>$id,
                        "tabla"=>'gastos',
                        "modificacion"=>'elimina',
                        "campo_ant"=>'',
                        "id_producto"=>'0',
                        "id_usuario"=>$this->session->userdata('usuarioid_tz'),
                        "id_sucursal"=>$this->session->userdata('idsucursal_tz'),
                        'fecha'=>$date
        );
        $this->ModeloGeneral->log_movs('log_cambios',$array); 
    }

    function buscarGastoDet(){
        $buscar = $this->input->post('buscar');
        $resultado=$this->ModeloGastos->gastosallsearch($buscar);
        foreach ($resultado->result() as $item){ ?>
            <tr id="trcli_<?php echo $item->id; ?>">
                <td><?php echo $item->concepto; ?></td>
                <td>$ <?php echo $item->monto; ?></td>
                <td><?php echo $item->fecha; ?></td>
                <td><?php echo $item->Usuario; ?></td>
                <td><?php echo $item->nombre; ?></td>
                <td>
                    <div class="btn-group mr-1 mb-1">
                        <button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>
                          <button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <span class="sr-only">Toggle Dropdown</span>
                          </button>
                          <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                              <a class="dropdown-item" onclick="gastodelete(<?php echo $item->id; ?>);"href="#">Eliminar</a>
                          </div>
                    </div>
                </td>
            </tr>
        <?php }
    }

    
}
