<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personal extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Personal/ModeloPersonal');
        $this->load->model('Usuarios/ModeloUsuarios');
        
        $this->load->model('ModeloGeneral');
    }
	public function index(){
            //$data['personal']=$this->ModeloPersonal->getpersonal();
            //carga de vistas
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            //$this->load->view('Personal/Personal',$data);
            $data['personal']=$this->ModeloPersonal->getpersonal();

            $this->load->view('Personal/Personal',$data);
            $this->load->view('templates/footer');
            $this->load->view('Personal/jspersonal');

	}
    /**
     * Retorna vista para agregar personal 
     */

    public function Personaladd(){
            //carga de vistas
            //$data['estadosp']=$this->ModeloPersonal->estados();
            $where_e = array('activo'=>1);
            $data['estadosp']=$this->ModeloGeneral->getselectwheren('estado',$where_e);
            $where_s = array('estatus'=>1);
            $data['sucursal']=$this->ModeloGeneral->getselectwheren('sucursal',$where_s);
            $data['menuse']=$this->ModeloPersonal->getAllmenu();
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('Personal/Personaladd',$data);
            $this->load->view('templates/footer');
            $this->load->view('Personal/jspersonal');
    }  
     
    public function addpersonal(){
        $id = $this->input->post('id');
        $nom = $this->input->post('nom');
        $ape = $this->input->post('ape');
        $fnaci = $this->input->post('fnaci');
        $sexo = $this->input->post('sex');
        $domic = $this->input->post('domic');
        $ciudad = $this->input->post('ciudad');
        $estado = $this->input->post('estado');
        $copo = $this->input->post('copo');
        $telcasa = $this->input->post('telcasa');
        $telcel = $this->input->post('telcel');
        $email = $this->input->post('email');
        $turno = $this->input->post('turno');
        $fechain = $this->input->post('fechain');
        $fechaba = $this->input->post('fechaba');
        $sueldo = $this->input->post('sueldo');
        $dias_vaca = $this->input->post('dias_vaca');

        $num_checador = $this->input->post('num_checador');
        $salario_dia = $this->input->post('salario_dia');
        $salario_hora = $this->input->post('salario_hora');
        $hora_extra = $this->input->post('hora_extra');
        $hora_doble = $this->input->post('hora_doble');
        $hora_dia_feriado = $this->input->post('hora_dia_feriado');
        $comision_venta = $this->input->post('comision_venta');
        $almacen = $this->input->post('almacen');

        $idsucursal=$this->input->post('idsucursal');
        $usuario = $this->input->post('usuario');
        $pass = $this->input->post('pass');
        log_message('error','$salario_hora: '.$salario_hora);
        if ($id>0) {
            $this->ModeloPersonal->personalupdate($id,$nom,$ape,$fnaci,$sexo,$domic,$ciudad,$estado,$copo,$telcasa,$telcel,$email,$turno,$fechain,$fechaba,$sueldo,$idsucursal,$num_checador,$salario_dia,$salario_hora,$hora_extra,$hora_doble,$hora_dia_feriado,$comision_venta,$dias_vaca,$almacen); 
            $this->ModeloUsuarios->usuariosupdate($usuario,$pass,$id);
            $tipo_mod = "modifica";
            $id_reg = $id;
            echo $id;  
        }else{
            $idd=$this->ModeloPersonal->personalnuevo($nom,$ape,$fnaci,$sexo,$domic,$ciudad,$estado,$copo,$telcasa,$telcel,$email,$turno,$fechain,$fechaba,$sueldo,$idsucursal,$num_checador,$salario_dia,$salario_hora,$hora_extra,$hora_doble,$hora_dia_feriado,$comision_venta,$dias_vaca,$almacen);
            $this->ModeloUsuarios->usuariosinsert($usuario,$pass,$idd);
            $tipo_mod = "insertar";
            $id_reg = $idd;
            echo $idd;
            
        }
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d h:i:s \G\M\T');
        $array = array("id_reg"=>$id_reg,
                        "tabla"=>'personal',
                        "modificacion"=>$tipo_mod,
                        "campo_ant"=>'',
                        "id_producto"=>'0',
                        "id_usuario"=>$this->session->userdata('usuarioid_tz'),
                        "id_sucursal"=>$this->session->userdata('idsucursal_tz'),
                        'fecha'=>$date
        );
        $aa=$this->ModeloGeneral->log_movs('log_cambios',$array);
    }
    public function deletepersonal(){
        $id = $this->input->post('id');
        $this->ModeloPersonal->personaldelete($id); 
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d h:i:s \G\M\T');
        $array = array("id_reg"=>$id,
                        "tabla"=>'personal',
                        "modificacion"=>'elimina',
                        "campo_ant"=>'',
                        "id_producto"=>'0',
                        "id_usuario"=>$this->session->userdata('usuarioid_tz'),
                        "id_sucursal"=>$this->session->userdata('idsucursal_tz'),
                        'fecha'=>$date
        );
        $this->ModeloGeneral->log_movs('log_cambios',$array);
    }
    public function addmenuss(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {
            $usu =  $DATA[$i]->usu;
            $menu= $DATA[$i]->menu;
            echo $this->ModeloUsuarios->addmenus($usu,$menu);
        }
    }   
    
}
