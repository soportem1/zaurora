<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Corte_caja extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        //$this->load->model('Personal/ModeloPersonal');
        //$this->load->model('Usuarios/ModeloUsuarios');
        $this->load->model('ModeloVentas');
        $this->load->model('ModeloGeneral');
    }
	public function index(){
            //$data['personal']=$this->ModeloPersonal->getpersonal();
            //carga de vistas
            $where_s = array('estatus'=>1);
            $data['sucursal']=$this->ModeloGeneral->getselectwheren('sucursal',$where_s);
            $where_so = array('estatus'=>1);
            $data['socios']=$this->ModeloGeneral->getselectwheren('departamento',$where_so);
            $data['cajeros']=$this->ModeloGeneral->getselectwheren('personal',$where_so);
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            //$this->load->view('Personal/Personal',$data);

            $this->load->view('corte/corte',$data);
            $this->load->view('templates/footer');
            $this->load->view('corte/jscorte');

	}
    function corte(){
        $inicio = $this->input->post('fecha1');
        $fin = $this->input->post('fecha2');
        $sucursal = $this->input->post('sucursal');
        $socios = $this->input->post('socios');
        $cajero = $this->input->post('cajeros');
        $prf = $this->session->userdata("perfilid_tz");
        $almacen = $this->session->userdata("almacen");
        $idpersonal = $this->session->userdata("idpersonal_tz");

        if($prf==1 || $idpersonal==17){
            $resultadoc=$this->ModeloVentas->corte($inicio,$fin,$sucursal,$socios,$cajero);
            $totefecCP=0;
            $tottarCP=0;
            $totalCanP=0;
            $totalCPG = 0;
            $resultadocparcial=$this->ModeloVentas->corteCParcial($inicio,$fin,$sucursal,$socios); //obtener las ventas canceladas parcial
            foreach ($resultadocparcial->result() as $fila) { 
                if($fila->id_detalle_venta>0){
                    $resDetCanP=$this->ModeloVentas->detaVentaCP($fila->id_detalle_venta);//obtener prods cant y $ de las canceladas parcial
                    foreach ($resDetCanP->result() as $row) { 
                        $totalCanP = $totalCanP+$row->totVPC;
                        $totefecCP = $totefecCP+$row->efectivo;
                        $tottarCP = $tottarCP+$row->pagotarjeta;
                    }
                }
                //$totefecCP=$totefecCP+$fila->efectivo;
                //$tottarCP=$tottarCP+$fila->pagotarjeta;
            }
            //$totalCanP = floatval($totalCanP);
            number_format($totalCanP,2,'.',',');
            /*foreach ($resDetCanP->result() as $fila) { 
                $totVPC= $fila->totVPC; //obtener el total de cancel parcial
            }*/

            /*$resultotparcial=$this->ModeloVentas->corteTotalCanceladoP($inicio,$fin,$sucursal,$socios); //obtener las ventas canceladas parc $$
            foreach ($resultotparcial->result() as $fila) { 
                $totalCPG = $fila->totalCGP;
                //number_format($totalCPG,2,'.',',');
            }*/
        }

        if($prf==1 || $idpersonal==17){
            $resultadocv=$this->ModeloVentas->cortev($inicio,$fin,$sucursal,$socios);//corte por vendedor en prods
            $resultadocvp=$this->ModeloVentas->cortevp($inicio,$fin,$sucursal,$socios); //corte por vendedor en global
            $resultadogastos=$this->ModeloVentas->cortegastos($inicio,$fin,$sucursal); 

            $resultadotodos=$this->ModeloVentas->cortets($inicio,$fin,$sucursal,$socios); //corte socios
            $resultadocajero=$this->ModeloVentas->cortetc($inicio,$fin,$cajero); //ventas por cajero
            $resultadocancela=$this->ModeloVentas->cortecancel($inicio,$fin,$sucursal,$socios); //ventas canceladas
        }

        $productomayor=$this->ModeloVentas->cortevmm($inicio,$fin,$sucursal,$socios,1);
        $productomenor=$this->ModeloVentas->cortevmm($inicio,$fin,$sucursal,$socios,0);

        if($prf==1 || $idpersonal==17){
            $resultefec=$this->ModeloVentas->corteEfectivo($inicio,$fin,$sucursal,$socios);
            $resultarj=$this->ModeloVentas->corteTarjeta($inicio,$fin,$sucursal,$socios);
            $resultmixto=$this->ModeloVentas->cortemixtos($inicio,$fin,$sucursal,$socios); //total de pagos mixtos
            $resulegrecaj=$this->ModeloVentas->corteEgresosCajero($inicio,$fin,$cajero);

            $resultpuntos=$this->ModeloVentas->cortePuntos($inicio,$fin,$sucursal,$socios);
            //log_message('error','Puntos: '.json_encode($resultpuntos->result()));
        }
        
        $suma_bono=0;
        $total_bono=0;
        $total_descuento=0;
        //$resultadocs=$this->ModeloVentas->cortesum($inicio,$fin);
        if($prf==1 || $idpersonal==17){
            $table="<table class='table table-striped table-bordered table-hover' id='sample_2'>
                        <thead>
                            <tr>
                                <th>No. venta</th>
                                <th>Cajero</th>
                                <th>Cliente</th>
                                <th>Fecha</th>
                                <th>Subtotal</th>
                                <th>Descuento</th>
                                <th>Total</th>
                                <th>Efectivo</th>
                                <th>Tarjeta</th>
                                <th>Puntos</th>
                            </tr>
                        </thead>
                        <tbody>";
            $rowventas=0;
            $total=0;
            $subtotal=0;
            $descuento=0;
            $efectivo=0;
            $tarjetas=0;
            $mixtos=0;
            $subgastos=0;
            $efectivoCancel=0;

            $totalPuntos=0;

            $resultadoefectcancel=$this->ModeloVentas->corteTotalEfectCancel($inicio,$fin,$sucursal,$socios);
            foreach ($resultadoefectcancel->result() as $fila) { 
                $efectivoCancel=$fila->totalvd;
            }

            foreach ($resultefec->result() as $fila) { 
                $efectivo = $efectivo+$fila->efectivo;
                /*$tarjetas=$tarjetas+$fila->pagotarjeta;*/
            }
            foreach ($resultarj->result() as $fila) { 
                /*$efectivo = $efectivo+$fila->totalvd;*/
                $tarjetas=$tarjetas+$fila->pagotarjeta;
            }
            foreach ($resultmixto->result() as $fila) { 
                $mixtos=$mixtos+$fila->totalvd;
            }

            foreach ($resultpuntos->result() as $fila) { 
                $totalPuntos = $totalPuntos+$fila->pagopuntos;
            }
            

            $total_credito=0;
            $total_credito_pagos=0;
            $getventas=$this->ModeloVentas->getventas_credito($inicio,$fin,$sucursal); //corte por metodo crédito
            foreach ($getventas->result() as $item){
                $total_credito = $item->monto_total;
            }

            $getventas_pagos=$this->ModeloVentas->getventas_credito_pagos($inicio,$fin,$sucursal); //corte por metodo crédito pagos
            foreach ($getventas_pagos->result() as $item){
                $total_credito_pagos = $item->monto;
            }

            //echo "mixtos: ".$mixtos;
            foreach ($resultadoc->result() as $fila) { 
                $table .= "<tr>
                                <td>".$fila->id_venta."</td>
                                <td>".$fila->vendedor."</td>
                                <td>".$fila->Nom."</td>
                                <td>".$fila->reg."</td>
                                <td>$ ".number_format($fila->totalvd,2,'.',',')."</td>
                                <td>$ ".number_format($fila->descuentocant,2,'.',',')."</td>
                                <td>$ ".number_format($fila->monto_total,2,'.',',')."</td>
                                <td>$ ".number_format($fila->efectivo,2,'.',',')."</td>
                                <td>$ ".number_format($fila->pagotarjeta,2,'.',',')."</td>
                                <td>$ ".number_format($fila->pagopuntos,2,'.',',')."</td>
                            </tr>";
                            $rowventas++;
                $total=$total+$fila->totalvd;
                $subtotal=$subtotal+$fila->totalvd;
                $subtotal=$subtotal-$fila->desctotal;
                //var_dump($fila->totalvd);
                //var_dump($fila->desctotal);
                //$subtotal=$subtotal-$fila->pagopuntos;
                /*$efectivo = $efectivo+$fila->totalvd;
                $tarjetas=$tarjetas+$fila->pagotarjeta;*/
            }
            //$efectivo = $efectivo - $efectivoCancel;

            $table.="</tbody> </table>";


            $table_c="<table class='table table-striped table-bordered table-hover'>
                        <thead>
                            <tr>
                                <th>No. venta</th>
                                <th>Cajero</th>
                                <th>Cliente</th>
                                <th>Fecha</th>
                                <th>Subtotal</th>
                                <th>Descuento</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>";
                $resultadocredito=$this->ModeloVentas->corte_credito($inicio,$fin,$sucursal,$socios,$cajero);
                foreach ($resultadocredito->result() as $x) { 
                $table_c .= "<tr>
                                <td>".$x->id_venta."</td>
                                <td>".$x->vendedor."</td>
                                <td>".$x->Nom."</td>
                                <td>".$x->reg."</td>
                                <td>$ ".number_format($x->totalvd,2,'.',',')."</td>
                                <td>$ ".number_format($x->descuentocant,2,'.',',')."</td>
                                <td>$ ".number_format($x->monto_total,2,'.',',')."</td>
                            </tr>";
                }

            $table_c.="</tbody></table>";

            $table_c_p='<h5>Pagos recibidos</h5><table class="table table-striped responsive" id="data-tables" style="width: 100%">
            <thead>
              <tr>
                <th>Folio</th>
                <th>Referencia</th>
                <th>Fecha</th>
                <th>Monto</th>
                <th>Método</th>
              </tr>
            </thead>
            <tbody>';
                $result_pagos=$this->ModeloVentas->corte_credito_pagos($inicio,$fin,$sucursal);
                foreach ($result_pagos->result() as $x){
                    $met='';
                    if($x->metodo==1){
                        $met='Efectivo';
                    }else if($x->metodo==2){
                        $met='Tarjeta de crédito';
                    }else if($x->metodo==3){
                        $met='Tarjeta de débito';
                    }
                    $table_c_p.='<tr ><td>'.$x->id_venta.'</td><td>'.date('d/m/Y',strtotime($x->fecha)).'</td><td>'.$x->referencia.'</td><td>$'.number_format($x->monto, 2, '.', ',').'</td><td>'.$met.'</td></tr>';
                }
                
            $table_c_p.='</tbody></table>';
            
            
            $tabla_bono='<table class="table table-striped responsive" id="tables_bono" style="width: 100%">
            <thead>
              <tr>
                <th>Tipo</th>
                <th>Fecha</th>
                <th>Cantidad</th>
                <th>Motivo</th>
              </tr>
            </thead>
            <tbody>';
                $result_bono=$this->ModeloVentas->corte_bonos($inicio,$fin);
                foreach ($result_bono->result() as $k){

                    if($k->cant_bono>0){
                        $tipo="Bono";
                        $motivo=$k->motivo_bono;
                        $cantidad = $k->cant_bono;
                        $total_bono+=$k->cant_bono;
                    }else{
                        $tipo="Descuento";
                        $motivo=$k->motivo_desc;
                        $cantidad = $k->cant_desc;
                        $total_descuento+=$k->cant_desc;
                    }
                    if($cantidad>0){
                        $tabla_bono.='<tr>
                            <td>'.$tipo.'</td>
                            <td>'.date("d/m/Y", strtotime($k->fecha)).'</td>
                            <td>$'.number_format($cantidad,2,".",",").'</td>
                            <td>'.$motivo.'</td>
                        </tr>';
                    }
                    
                    $suma_bono+=$cantidad;
                }
                
            $tabla_bono.='</tbody></table>';

            $table2="<table class='table table-striped table-bordered table-hover' id='sample_2'>
                        <thead>
                            <tr>
                                <th>Turno</th>
                                <th>Apertura</th>
                                <th>Cierre</th>
                                <!--<th>Utilidad</th>-->
                            </tr>
                        </thead>
                        <tbody>";
            $respuestaturno=$this->ModeloVentas->consultarturnoname($inicio,$fin);
            $obedt=0;
            foreach ($respuestaturno->result() as $fila) { 
                if ($fila->fechacierre=='0000-00-00') {
                    $horac =date('H:i:s');
                    $fechac =date('Y-m-d');
                }else{
                    $horac =$fila->horac;
                    $fechac =$fila->fechacierre;
                }
                $fecha =$fila->fecha;
                $horaa =$fila->horaa; 
                $totalventas=$this->ModeloVentas->consultartotalturno($fecha,$horaa,$horac,$fechac);
                $totalpreciocompra=$this->ModeloVentas->consultartotalturno2($fecha,$horaa,$horac,$fechac);
                $obed =$totalventas-$totalpreciocompra;
                $table2 .= "<tr>
                                <td>".$fila->nombre."</td>
                                <td>".$fila->horaa."</td>
                                <td>".$fila->horac."</td>
                                <!--<td>".number_format($obed,2,'.',',')."</td>-->
                            </tr>";
                $obedt=$obedt+$obed;
            }
            $table2.="</tbody> </table>";
            //==================================================
                $table3="<table class='table table-striped table-bordered table-hover' id='sample_3'>
                            <thead>
                                <tr>
                                    <th>No. venta</th>
                                    <th>Cajero</th>
                                    <th>Vendedor</th>
                                    <th>Producto</th>
                                    <th>Cantidad</th>
                                    <th>Precio</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>";
                
                foreach ($resultadocv->result() as $fila) { 
                    $cal_desc=$fila->precio*$fila->descuento;
                    $sub_vend=$fila->cantidad*$fila->precio;
                    $desc_vend=$cal_desc*$fila->cantidad;
                    $tot_vend=$fila->precio-$cal_desc;
                    $table3 .= "<tr>
                                    <td>".$fila->id_venta."</td>
                                    <td>".$fila->cajero."</td>
                                    <td>".$fila->vendedor."</td>
                                    <td>".$fila->producto."</td>
                                    <td>".$fila->cantidad."</td>
                                    <td>$ ".number_format($fila->precio-$cal_desc,2,'.',',')."</td>
                                    <td>$ ".number_format($fila->cantidad*$tot_vend,2,'.',',')."</td>
                                    
                                </tr>";
                }
                $table3.="</tbody> </table>";
            //==================================================
            //==================================================
               $table4="<table class='table table-striped table-bordered table-hover' id='sample_4'>
                            <thead>
                                <tr>
                                    <th>Vendedor</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>";
                
                foreach ($resultadocvp->result() as $fila) { 
                    $table4 .= "<tr>
                                    <td>".$fila->vendedor."</td>
                                    <td>$ ".number_format($fila->total-$fila->desctotal,2,'.',',')."</td>
                                    
                                </tr>";
                }
                $table4.="</tbody> </table>";
        }
        //==================================================
        //==================================================
            $table5="<table class='table table-striped table-bordered table-hover' id='sample_5'>
                        <thead>
                            <tr>
                                <th>Producto</th>
                                <th>Cantidad</th>
                            </tr>
                        </thead>
                        <tbody>";
            
            foreach ($productomayor->result() as $fila) { 
                $table5 .= "<tr>
                                <td>".$fila->producto."</td>
                                
                                <td>".number_format($fila->cantidad,2,'.',',')."</td>
                                
                            </tr>";
            }
            $table5.="</tbody> </table>";
        //==================================================
        //==================================================
            $table6="<table class='table table-striped table-bordered table-hover' id='sample_6'>
                        <thead>
                            <tr>
                                <th>Producto</th>
                                <th>Cantidad</th>
                            </tr>
                        </thead>
                        <tbody>";
            
            foreach ($productomenor->result() as $fila) { 
                $table6 .= "<tr>
                                <td>".$fila->producto."</td>
                                
                                <td>".number_format($fila->cantidad,2,'.',',')."</td>
                                
                            </tr>";
            }
            $table6.="</tbody> </table>";
        //==================================================
        //==================================================
        if($almacen==1){
            $resultadocv=$this->ModeloVentas->cortev($inicio,$fin,$sucursal,$socios);//corte por vendedor en prods
            //==================================================
                $table3="<table class='table table-striped table-bordered table-hover' id='sample_3'>
                            <thead>
                                <tr>
                                    <th>No. venta</th>
                                    <th>Cajero</th>
                                    <th>Vendedor</th>
                                    <th>Producto</th>
                                    <th>Cantidad</th>
                                </tr>
                            </thead>
                            <tbody>";
                
                foreach ($resultadocv->result() as $fila) { 
                    $cal_desc=$fila->precio*$fila->descuento;
                    $sub_vend=$fila->cantidad*$fila->precio;
                    $desc_vend=$cal_desc*$fila->cantidad;
                    $tot_vend=$fila->precio-$cal_desc;
                    $table3 .= "<tr>
                                    <td>".$fila->id_venta."</td>
                                    <td>".$fila->cajero."</td>
                                    <td>".$fila->vendedor."</td>
                                    <td>".$fila->producto."</td>
                                    <td>".$fila->cantidad."</td>                                    
                                </tr>";
                }
                $table3.="</tbody> </table>";
            //==================================================
        }
        if($prf==1 || $idpersonal==17){
            //reporte de ventas por socios    
            $table7="<table class='table table-striped table-bordered table-hover' id='sample_7'> 
                        <thead>
                            <tr>
                                <th>Sucursal</th>
                                <th>Socio</th>
                                <!--<th>Cajero</th>-->
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>";
            foreach ($resultadotodos->result() as $filas) { 
                $totalSocio = $filas->total-$filas->desctotal;
                $totalSocio = number_format($totalSocio,2,'.',',');
                //$tot = $totalCanP + $totalSocio;
                $table7 .= "<tr>
                                <td>".$filas->sucursal."</td>
                                <td>".$filas->depto."</td>
                                <td>$ ".$totalSocio."</td>
                            </tr>";
            }

            $table7.="</tbody> </table>";
            //==================================================
            $table8="<table class='table table-striped table-bordered table-hover' id='sample_8'>
                        <thead>
                            <tr>
                                <th>Cajero</th>
                                <th>Sucursal</th>
                                <!--<th>Vendedor</th>-->
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>";
            //$rowventas=0;
            //$total=0;
            foreach ($resultadocajero->result() as $fila) { 
                $total = $fila->total-$fila->desctotal;
                $table8 .= "<tr>
                                <td>".$fila->cajero."</td>
                                <td>".$fila->sucursal."</td>
                                <!--<td>".$fila->vendedor."</td>-->
                                <td>$ ".number_format($total,2,'.',',')."</td>
                            </tr>";
                            //$rowventas++;
            }
            $table8.="</tbody> </table>";
            //================================================== ventas canceladas
            $table9="<table class='table table-striped table-bordered table-hover' id='sample_9'> 
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Cajero</th>
                                <th>Fecha</th>
                                <th>Producto</th>
                                <th>Cantidad</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>";
            foreach ($resultadocancela->result() as $fila) { 
                $table9.= "<tr>
                                <td>".$fila->id_venta."</td>
                                <td>".$fila->cajero."</td>
                                <td>".$fila->hcancelacion."</td>
                                <td>".$fila->prod."</td>
                                <td>".$fila->cant."</td>
                                <td>$ ".number_format($fila->total,2,'.',',')."</td>
                            </tr>";
            }
            $table9.="</tbody> </table>";
            //==================================================
            $table10="<table class='table table-striped table-bordered table-hover' id='sample_10'>
                        <thead>
                            <tr>
                                <th>Sucursal</th>
                                <th>Egreso Total</th>
                            </tr>
                        </thead>
                        <tbody>";
            foreach ($resultadogastos->result() as $fila) { 
                $table10.= "<tr>
                                <td>".$fila->sucursal."</td>
                                <td>$ ".number_format($fila->total,2,'.',',')."</td>
                            </tr>";
                $subgastos = $fila->total;
            }
            $table10.="</tbody> </table>";
            //==================================================
            //==================================================
            $table11="<table class='table table-striped table-bordered table-hover' id='sample_11'>
                        <thead>
                            <tr>
                                <th>Cajero</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>";
            //$rowventas=0;
            //$total=0;
            foreach ($resulegrecaj->result() as $fila) { 
                $table11 .= "<tr>
                                <td>".$fila->cajero."</td>
                                <td>$ ".number_format($fila->total,2,'.',',')."</td>
                            </tr>";
                            //$rowventas++;
            }
            $table11.="</tbody> </table>";


            $metodos = array('','Efectivo','Tarjeta de credito','Tarjeta de débito','Pago mixto','Puntos Aurora','Crédito');
            $table20="<table class='table table-striped table-bordered table-hover' id='sample_20'>
                        <thead>
                            <tr>
                                <th>Fecha de venta</th>
                                <th>Folio</th>
                                <th>Vendedor</th>
                                <th>Sucursal</th>
                                <th>Metodo de pago</th>
                                <th>% Puntos</th>
                                <th>Monto venta</th>
                                <th>Puntos venta</th>
                                <th>Puntos utilizados</th>
                                <th>Detalle</th>
                            </tr>
                        </thead>
                        <tbody>";
            //$rowventas=0;
            //$total=0;
            foreach ($resultpuntos->result() as $fila) { 
                $table20 .= "<tr>
                                <td>".$fila->reg."</td>
                                <td>".$fila->id_venta."</td>
                                <td>".$fila->vendedor."</td>
                                <td>".$fila->sucursal."</td>
                                <td>".$metodos[$fila->metodo]."</td>
                                <td>".$fila->porcentaje_puntos."</td>
                                <td>$ ".number_format($fila->monto_total,2,'.',',')."</td>
                                <td>$ ".number_format($fila->puntos_total,2,'.',',')."</td>
                                <td>$ ".number_format($fila->pagopuntos,2,'.',',')."</td>
                                <td>
                                    <button class='btn btn-raised gradient-blackberry white sidebar-shadow' onclick='ticket(".$fila->id_venta.")' title='Ticket' data-toggle='tooltip' data-placement='top'>
                                        <i class='fa fa-book'></i>
                                    </button>
                                </td>
                                
                            </tr>";
            }
            $table20.="</tbody> </table>";

        }

        if($prf==1 || $idpersonal==17){
            $total = round($total,2);
            $subtotal = round($subtotal,2);
            $subgastos = round($subgastos,2);
            $totalCanP = round($totalCanP,2);


            

            $total_credito = number_format($total_credito,2,'.',',');
            $total_credito_pagos = number_format($total_credito_pagos,2,'.',',');

            $total_bono = number_format($total_bono,2,'.',',');
            $total_descuento = number_format($total_descuento,2,'.',',');
            //$tfin=$efectivo-$totalCanP;
            //$tfin = round($tfin,2);

            //$subtotal=$tfin+$tarjetas;

            //$subgastos=number_format($subgastos,2,'.',',');
            //$subtotal=number_format($subtotal,2,'.',',');
            $obedt=number_format($obedt,2,'.',',');
            $efectivo=number_format($efectivo,2,'.',',');
            $tarjetas=number_format($tarjetas,2,'.',',');
            $mixtos=number_format($mixtos,2,'.',',');
            $totalPuntos=number_format($totalPuntos,2,'.',',');

            //$tfin=number_format($tfin,2,'.',',');
            if($subgastos>0 || $subgastos!=null || $subgastos!=""){
                $totGral = $subtotal - $subgastos;
                //$totGral = round($totGral,2);
                $subtotal = $subtotal - $totalPuntos;
                $subtotal=number_format($subtotal,2,'.',',');

                $totGral = $totGral - $totalPuntos;
                $totGral = $totGral - $suma_bono;
                $totGral = number_format($totGral,2,'.',',');
            }
            else{
                $totGral = $subtotal - $subgastos;

                $subtotal = $subtotal - $totalPuntos;
                $subtotal=number_format($subtotal,2,'.',',');
                //$totGral = $subtotal;

                $totGral = $totGral - $totalPuntos;
                $totGral = $totGral - $suma_bono;
                $totGral = number_format($totGral,2,'.',',');
            }
            //$totGral = number_format($totGral,2,'.',',');
        }

        if($prf==1 || $idpersonal==17){
            $array = array("tabla"=>$table,
                        "tabla2"=>$table2,
                        "tabla3"=>$table3,
                        "tabla4"=>$table4,
                        "tabla5"=>$table5,
                        "tabla6"=>$table6,
                        "tabla7"=>$table7,
                        "tabla8"=>$table8,
                        "tabla9"=>$table9,
                        "tabla10"=>$table10,
                        "tabla11"=>$table11,
                        "tabla20"=>$table20,
                        "dTotal"=>"".$totGral."",
                        "totalventas"=>$rowventas,
                        "totalefectivo"=>$efectivo,
                        "totalmixto"=>$mixtos,
                        "totalegresos"=>number_format($subgastos,2,'.',','),
                        "totaltarjetas"=>$tarjetas,
                        "totalPuntos"=>$totalPuntos,
                        "dImpuestos"=>0,
                        "totalutilidad"=>"".$obedt."",
                        "descuento"=>$descuento,
                        "dSubtotal"=>"".$subtotal."",
                        "tabla_credito"=>$table_c,
                        "total_credito"=>$total_credito,
                        "total_credito_pagos"=>$total_credito_pagos,
                        "tabla_credito_tabla"=>$table_c_p,
                        "tabla_bono"=>$tabla_bono,
                        "totalbonos"=>$total_bono,
                        "totaldescuentos"=>$total_descuento,
                        

                    );
                    log_message('error','subtotal: '.$subtotal);
        }if($almacen==1){
            $array = array(
                        "tabla3"=>$table3,
                        "tabla5"=>$table5,
                        "tabla6"=>$table6
                    );
        }else if($prf!=1 && $idpersonal!=17 && $almacen==0){
            $array = array(
                        "tabla5"=>$table5,
                        "tabla6"=>$table6
                    );  
        }
        echo json_encode($array);
        //log_message('error','ARRAY: '.json_encode($array));
    }
    

       
}
