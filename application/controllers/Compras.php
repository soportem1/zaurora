<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Compras extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloProveedor');
        $this->load->model('ModeloProductos');
        $this->load->model('ModeloVentas');
        $this->load->model('ModeloGeneral');

        $logueo = $this->session->userdata();
        if (isset($logueo['perfilid_tz'])) {
            $perfilid = $logueo['perfilid_tz'];
            $this->sucursal = $logueo['idsucursal_tz'];
            $this->idpersonal = $this->session->userdata('idpersonal_tz');
            if ($this->sucursal == 0) {
                $this->sucursalid = 1;
            } else {
                $this->sucursalid = $logueo['idsucursal_tz'];
            }
            $this->personal = $logueo['idpersonal_tz'];
        } else {
            $perfilid = 0;
        }/*
                $permiso=$this->ModeloCatalogos->getviewpermiso($perfilid,19);// 13 es el id del menu
                if ($permiso==0) {
                    redirect('/Sistema');
                }*/
    }
	public function index(){
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('compras/compras');
        $this->load->view('templates/footer');
        $this->load->view('compras/jscompras');
	}
    public function searchpro(){
        $usu = $this->input->get('search');
        $results=$this->ModeloProveedor->proveedorallsearch($usu);
        echo json_encode($results->result());
    }
    
    
    public function addproducto(){
        $cant = $this->input->post('cant');
        $prod = $this->input->post('prod');
        $prec = $this->input->post('prec');
        $personalv=$this->ModeloProductos->getproducto($prod);
        foreach ($personalv->result() as $item){
              $id = $item->productoid;
              $codigo = $item->codigo;
              $nombre = $item->nombre;
              $precio = $item->preciocompra;
        }
        $array = array("id"=>$id,
                        "codigo"=>$codigo,
                        "cant"=>$cant,
                        "producto"=>$nombre,
                        "precio"=>$prec
                    );
            echo json_encode($array);
    }
    function ingresarcompra(){
        $uss = $this->input->post('uss');
        $prov = $this->input->post('prov');
        $personal = $this->personal;
        $total = $this->input->post('total');
        $id=$this->ModeloVentas->ingresarcompra($uss,$prov,$personal,$total);
        echo $id;
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d h:i:s \G\M\T');
        $array = array("id_reg"=>$id,
                        "tabla"=>'compras',
                        "modificacion"=>'insertar',
                        "campo_ant"=>'',
                        "id_producto"=>'0',
                        "id_usuario"=>$this->session->userdata('usuarioid_tz'),
                        "id_sucursal"=>$this->session->userdata('idsucursal_tz'),
                        'fecha'=>$date
        );

        //log_message('error','array-> '.json_encode($array));

        if (!$this->db->conn_id) {
            log_message('error', 'No se pudo conectar a la base de datos.');
            return;
        }
        
        $this->ModeloGeneral->log_movs('log_cambios',$array);

        /*
        if (!$this->ModeloGeneral->log_movs('log_cambios', $array)) {
            log_message('error', 'Error al registrar el log de movimientos.');
        }
        */
    }
    
    function ingresarcomprapro(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $idcompra = $DATA[$i]->idcompra;
            $producto = $DATA[$i]->producto;
            $cantidad = $DATA[$i]->cantidad;
            $cantInicial = $DATA[$i]->cantIni;
            $precio = $DATA[$i]->precio;
            $this->ModeloVentas->ingresarcomprad($idcompra,$producto,$cantidad,$precio,$cantInicial,$this->session->userdata('idsucursal_tz'));
        }
    }


    function checkCantproducto(){
        $id = $this->input->post('id');
        //$sucursal = $this->input->post('sucursal');
        log_message('error','id '.$id);
        $cant = $this->ModeloVentas->checkStock($id,$this->sucursalid);
        echo $cant;
    }
}