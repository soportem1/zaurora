<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Pagos extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloGeneral');
        
        $logueo = $this->session->userdata();
        if (isset($logueo['perfilid_tz'])) {
            $perfilid=$logueo['perfilid_tz'];
            $this->sucursal=$logueo['idsucursal_tz'];
            $this->idpersonal=$logueo['idpersonal_tz'];
            if ($this->sucursal==0) {
                $this->sucursalid=1;
            }else{
                $this->sucursalid=$logueo['idsucursal_tz'];
            }
            $this->personal=$logueo['idpersonal_tz'];
        }else{
            $perfilid=0;
        }
        date_default_timezone_set('America/Mexico_City');
        $this->date = date('Y-m-d h:i:s');
    }
	public function index(){
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('pagos/boleta_pagos');
        $this->load->view('templates/footer');
        $this->load->view('pagos/boleta_pagosjs');   
	}

    function searchPersonal(){
        $cli = $this->input->get('search');
        $results=$this->ModeloGeneral->searchPersonal($cli);
        //log_message('error','Search:  '.json_encode($results->result()));
        echo json_encode($results->result());
    }

    public function getVentas(){
        $id_emp = $this->input->post('id_emp');
        $ini = $this->input->post('ini');
        $fin = $this->input->post('fin');
        $results=$this->ModeloGeneral->getVentasVendedor($id_emp,$ini,$fin);
        $results2=$this->ModeloGeneral->getDescsVentasVendedor($id_emp,$ini,$fin);
        echo json_encode(array("data"=>$results,"descs"=>$results2));
    }

    public function generarBoleta($id_emp,$i,$f,$dif,$hrs,$min){
        $results["det"]=$this->ModeloGeneral->getVentasVendedor2($id_emp,$i,$f);
        $results["desc"]=$this->ModeloGeneral->getDescsVentasVendedor($id_emp,$i,$f);
        $results["vende"]=$this->ModeloGeneral->getDatosVende($id_emp,$i,$f);
        $results["bon_desc"]=$this->ModeloGeneral->getBonosDescs($id_emp,$i,$f);
        $results["desc_nl"]=$this->ModeloGeneral->getNolaborales($id_emp,$i,$f);
        $results["i"]=$i;
        $results["f"]=$f;
        $results["dif"]=$dif;
        $results["hrsLaboradas"]=$hrs;
        $results["minLaborados"]=$min;
        $this->load->view('Reportes/boleta_pagos',$results);
    }

    public function detallesVentas(){
        $id_emp = $this->input->post('id_emp');
        $ini = $this->input->post('ini');
        $fin = $this->input->post('fin');
        $results=$this->ModeloGeneral->detallesVentas($id_emp,$ini,$fin);
        $html='<table class="table table-striped responsive" id="tabla_det_prods" style="width: 100%">
                <thead>
                    <tr>
                        <th width="50%">Producto</th>
                        <th width="15%">Cantidad</th>
                        <th width="15%">Precio</th>
                        <th width="20%">Total</th>
                    </tr>
                </thead>
                <tbody>';
        foreach ($results as $k) {
            $html.='<tr>
                    <th width="50%">'.$k->nombre.'</th>
                    <th width="15%">'.$k->cantidad.'</th>
                    <th width="15%">$'.number_format($k->precio,2,".",",").'</th>
                    <th width="20%">$'.number_format($k->cantidad*$k->precio,2,".",",").'</th>
                </tr>';
        }
        $html.='</tbody>
            </table>';

        echo $html;
    }

    /* *********************************************** */
    public function BonosDescuentos(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('pagos/bonos_descuentos');
        $this->load->view('templates/footer');
        $this->load->view('pagos/bonos_descuentosjs');   
    }

    public function insertarBonoDesc(){
        $data = $this->input->post();
        $idres=$this->ModeloGeneral->tabla_inserta("bonos_descuentos",$data);
        
        $array = array("id_reg"=>$idres,
                        "tabla"=>'Bonos Descuentos',
                        "modificacion"=>"inserta",
                        "campo_ant"=>'',
                        "id_producto"=>'0',
                        "id_usuario"=>$this->session->userdata('usuarioid_tz'),
                        "id_sucursal"=>$this->session->userdata('idsucursal_tz'),
                        'fecha'=>$this->date
        );
        $this->ModeloGeneral->log_movs('log_cambios',$array);
    }

    public function getTotalBD(){
        $id_emp = $this->input->post('id_emp');
        $ini = $this->input->post('ini');
        $fin = $this->input->post('fin');
        $results=$this->ModeloGeneral->getTotalBonosDescs($id_emp,$ini,$fin);
        $resultsnl=$this->ModeloGeneral->getTotalNolaborales($id_emp,$ini,$fin);
        echo json_encode(array("results"=>$results,"resultsnl"=>$resultsnl));
    }

    public function detallesExtras(){
        $id_emp = $this->input->post('id_emp');
        $ini = $this->input->post('ini');
        $fin = $this->input->post('fin');
        $tipo = $this->input->post('tipo');
        $results=$this->ModeloGeneral->getBonosDescs($id_emp,$ini,$fin);
        $html='<table class="table table-striped responsive" id="tabla_det_bds" style="width: 100%">
                <thead>
                    <tr>
                        <th width="25%">Fecha</th>
                        <th width="25%">Cantidad</th>
                        <th width="50%">Motivo</th>
                    </tr>
                </thead>
                <tbody>';
        $motivo="";
        $cantidad="";
        foreach ($results as $k) {
            if($tipo==1){
                $motivo=$k->motivo_bono;
                $cantidad = $k->cant_bono;
            }else{
                $motivo=$k->motivo_desc;
                $cantidad = $k->cant_desc;
            }
            if($cantidad>0){
                $html.='<tr>
                    <th width="25%">'.date("d/m/Y", strtotime($k->fecha)).'</th>
                    <th width="25%">$'.number_format($cantidad,2,".",",").'</th>
                    <th width="50%">'.$motivo.'</th>
                </tr>';
            }
        }
        $html.='</tbody>
            </table>';

        echo $html;
    }

    public function detallesExtrasNoLaboral(){
        $id_emp = $this->input->post('id_emp');
        $ini = $this->input->post('ini');
        $fin = $this->input->post('fin');
        $tipo = $this->input->post('tipo');
        $results=$this->ModeloGeneral->getNolaborales($id_emp,$ini,$fin);
        $html='<table class="table table-striped responsive" id="tabla_det_nl" style="width: 100%">
                <thead>
                    <tr>
                        <th width="20%">Inicio</th>
                        <th width="20%">Fin</th>
                        <th width="15%">Cantidad</th>
                        <th width="15%">Tipo</th>
                        <th width="30%">Observaciones</th>
                    </tr>
                </thead>
                <tbody>';
        $tipo="";
        foreach ($results as $k) {
            if($k->tipo=="3"){
                $tipo="Día enfermo";
            }else if($k->tipo=="4"){
                $tipo="Día descuento";
            }else if($k->tipo=="5"){
                $tipo="Día suspendido";
            }
            if($k->tot_descuento>0){
                $html.='<tr>
                    <th width="20%">'.date("d/m/Y", strtotime($k->inicio)).'</th>
                    <th width="20%">'.date("d/m/Y", strtotime($k->fin)).'</th>
                    <th width="15%">$'.number_format($k->tot_descuento,2,".",",").'</th>
                    <th width="15%">'.$tipo.'</th>
                    <th width="30%">'.$k->observaciones.'</th>
                </tr>';
            }
        }
        $html.='</tbody>
            </table>';

        echo $html;
    }

    /*public function detallesBonos(){
        $id_emp = $this->input->post('id_emp');
        $results=$this->ModeloGeneral->getBonosDescs($id_emp,0,0);
        $html='<table class="table table-striped responsive" id="tabla_det_bds" style="width: 100%">
                <thead>
                    <tr>
                        <th></th>
                        <th colspan="2">BONOS</th>
                        <th colspan="2">DESCUENTOS</th>
                    </tr>
                    <tr>
                        <th width="15%">Fecha</th>
                        <th width="15%">Cantidad</th>
                        <th width="25%">Motivo</th>
                        <th width="15%">Cantidad</th>
                        <th width="25%">Motivo</th>
                    </tr>
                </thead>
                <tbody>';
        
        foreach ($results as $k){
            $html.='<tr>
                <th width="15%">'.date("d/m/Y", strtotime($k->fecha)).'</th>
                <th width="15%">$'.number_format($k->cant_bono,2,".",",").'</th>
                <th width="25%">'.$k->motivo_bono.'</th>
                <th width="15%">$'.number_format($k->cant_desc,2,".",",").'</th>
                <th width="25%">'.$k->motivo_desc.'</th>
            </tr>';
        }
        $html.='</tbody>
            </table>';

        echo $html;
    }*/

    public function detallesBonos(){
        $id_emp = $this->input->post('id_emp');
        $results=$this->ModeloGeneral->getBonosDescs($id_emp,0,0);
        $html='<table class="table table-striped responsive" id="tabla_det_bds" style="width: 100%">
                <thead>
                    <tr>
                        <th width="15%">Tipo</th>
                        <th width="20%">Fecha</th>
                        <th width="25%">Cantidad</th>
                        <th width="40%">Motivo</th>
                    </tr>
                </thead>
                <tbody>';
        $motivo="";
        $cantidad=""; $tipo="";
        foreach ($results as $k) {
            if($k->cant_bono>0){
                $tipo="Bono";
                $motivo=$k->motivo_bono;
                $cantidad = $k->cant_bono;
            }else{
                $tipo="Descuento";
                $motivo=$k->motivo_desc;
                $cantidad = $k->cant_desc;
            }
            if($cantidad>0){
                $html.='<tr>
                    <th width="15%">'.$tipo.'</th>
                    <th width="20%">'.date("d/m/Y", strtotime($k->fecha)).'</th>
                    <th width="25%">$'.number_format($cantidad,2,".",",").'</th>
                    <th width="40%">'.$motivo.'</th>
                </tr>';
            }
        }
        $html.='</tbody>
            </table>';

        echo $html;
    }

    /* *********************************************** */

    public function getHorasLaboradas(){
        $idPersonal = $this->input->post('id_emp');
        $fechaIni = $this->input->post('ini');
        $fechaFin = $this->input->post('fin');
        $results = $this->ModeloGeneral->getHorasLaboradas($idPersonal,$fechaIni,$fechaFin);
        log_message('error','Results: '.json_encode($results));
        echo json_encode($results);
    }

    public function confirmarPagos(){
        $idPersonal = $this->input->post('id_emp');
        $fechaIni = $this->input->post('ini');
        $fechaFin = $this->input->post('fin');
        $salarioHora = $this->input->post('salarioHora');
        $horasTrabajadas = $this->input->post('horasTrabajadas');
        $pagoTotal = $this->input->post('pagoTotal');

        $existPago = $this->ModeloGeneral->checkPagos($idPersonal, $fechaIni, $fechaFin);
        //log_message('error','Exist: '.$existPago);
        if($existPago == 0){
            $array = array('empleadoId'=>$idPersonal,'fechaIni'=>$fechaIni,'fechaFin'=>$fechaFin,'pagoHora'=>$salarioHora,'cantidadHoras'=>$horasTrabajadas,'pagoTotal'=>$pagoTotal,'usuarioId'=>$this->idpersonal ,'reg'=>$this->date);
            $id = $this->ModeloGeneral->tabla_inserta('bitacora_pago',$array);
        }else{
            $id = 'E';
        }

        //log_message('error','Insert ID: '.json_encode($id));
        echo json_encode($id);
    }

}

