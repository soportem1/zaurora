<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('excel');
        $this->load->model('ModeloProductos');
        $this->load->model('ModeloGeneral');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d');
    } 
	public function index(){       
    $data["departamento"] = $this->ModeloGeneral->getselectwhere('departamento','estatus',1);
    $data["sucursal"] = $this->ModeloGeneral->getselectwhere('sucursal','estatus',1);
    //$where_so = array('estatus'=>1'and cuantos==1');
    $data["sucursaldesc"] = $this->ModeloGeneral->getselectwhere('productos_sucursales','cuantos',1);

    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    //$this->load->view('Personal/Personal',$data);
    $this->load->view('productos/productos',$data);
    $this->load->view('templates/footer');
    $this->load->view('productos/jsproductol');
	}
  public function getData_productos() {

        $params = $this->input->post();
        $paramss=json_encode($params);
        $tipoProd = $this->input->post('tipoProd');
        $productos = $this->ModeloProductos->listado($params);
        $totalRecords=$this->ModeloProductos->total_prods($params);

        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $productos->result(),
            "query"           =>$this->db->last_query()   
        );
        
        echo json_encode($json_data);
  }
  
  public function productosadd(){
        //$data["categorias"] = $this->ModeloProductos->categorias();
        $data["departamento"] = $this->ModeloGeneral->getselectwhere('departamento','estatus',1);
        $data["sucursal"] = $this->ModeloGeneral->getselectwhere('sucursal','estatus',1);
        $data['fecharegistro']= $this->fechahoy;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('productos/productoadd',$data);
        $this->load->view('templates/footer');
        $this->load->view('productos/jsproducto');
  }
  function productoadd(){
        $datos = $this->input->post();
        $id =$datos['productoid'];
        $codigo=$datos['pcodigo'];
        $codigo = ltrim($codigo, '0');
        $datos['codigo'] = $codigo;
        $datos['nombre'] = $datos['productoname'];
        $datos['mayoreo'] = $datos['preciomayoreo'];
        $datos['canmayoreo'] = $datos['cpmayoreo'];
        /*
        $id = $this->input->post('id');
        $cod = $this->input->post('cod');
        $pfiscal = $this->input->post('pfiscal');
        $nom = $this->input->post('nom');
        $des = $this->input->post('des');
        $catego = $this->input->post('catego');
        $stock = $this->input->post('stock');
        $preciocompra = $this->input->post('preciocompra');
        $porcentaje = $this->input->post('porcentaje');
        $precioventa = $this->input->post('precioventa');
        $pmmayoreo = $this->input->post('pmmayoreo');
        $cpmmayoreo = $this->input->post('cpmmayoreo');
        $pmayoreo = $this->input->post('pmayoreo');
        $cpmayoreo = $this->input->post('cpmayoreo');
        */
        $tipo_mod = '';
        unset($datos['idproducto']);
        unset($datos['pcodigo']);
        unset($datos['productoname']);
        unset($datos['preciomayoreo']);
        unset($datos['cpmayoreo']);
        if ($id>0) {
            $result = $this->ModeloGeneral->updateCatalogo($datos,'productoid',$id,'productos');
            $tipo_mod = 'insertar';
            /*
            $this->ModeloProductos->productosupdate($id,$cod,$pfiscal,$nom,$des,$catego,$stock,$preciocompra,$porcentaje,$precioventa,$pmmayoreo,$cpmmayoreo,$pmayoreo,$cpmayoreo);
            echo $id;
            */
        }else{
            $result = $this->ModeloGeneral->tabla_inserta('productos',$datos);
            $id=$result;
            $tipo_mod = 'modifica';
            /*
            $idd=$this->ModeloProductos->productosinsert($cod,$pfiscal,$nom,$des,$catego,$stock,$preciocompra,$porcentaje,$precioventa,$pmmayoreo,$cpmmayoreo,$pmayoreo,$cpmayoreo);
            echo $idd;
            */
        }
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d h:i:s \G\M\T');
        $array = array("id_reg"=>$result,
                        "tabla"=>'productos',
                        "modificacion"=>$tipo_mod,
                        "campo_ant"=>'',
                        "id_producto"=>$id,
                        "id_usuario"=>$this->session->userdata('usuarioid_tz'),
                        "id_sucursal"=>$this->session->userdata('idsucursal_tz'),
                        'fecha'=>$date
        );
        $this->ModeloGeneral->log_movs('log_cambios',$array);
        echo $result;
  }
  function productos_sucursales(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);

        for ($i=0;$i<count($DATA);$i++) { 
            $idproductosucursal = $DATA[$i]->idproductosucursal; 
            $idsucursal = $DATA[$i]->idsucursal;
            $idproducto = $DATA[$i]->idproducto;
            $precio_venta = $DATA[$i]->precio_venta;
            $mayoreo = $DATA[$i]->mayoreo;
            $cuantos = $DATA[$i]->cuantos;
            $existencia = $DATA[$i]->existencia;
            $minimo = $DATA[$i]->minimo;
            $data['idsucursal']=$idsucursal;
            $data['idproducto']=$idproducto;
            $data['precio_venta']=$precio_venta;
            $data['mayoreo']=$mayoreo;
            $data['cuantos']=$cuantos;
            $data['existencia']=$existencia;
            $data['minimo']=$minimo;

            $stock = $this->ModeloProductos->getStockProducto($idproducto, $idsucursal);

            log_message('error','STOCKs: '.json_encode($stock));

            log_message('error','Existencia: '.$existencia);
            
            if($idproductosucursal==0){
                $res=$this->ModeloGeneral->tabla_inserta('productos_sucursales',$data);
                $tipo_mod = 'insertar';
            }else{
                $res=$this->ModeloGeneral->updateCatalogo($data,'idproductosucursal',$idproductosucursal,'productos_sucursales');
                $tipo_mod = 'modifica';


                //--------------------
                if($stock != $existencia){
                    $dataEdicion = array(
                        "id_producto" => $idproducto,
                        //"id_usuario" => $this->session->userdata('usuarioid_tz'),
                        "id_personal" => $this->session->userdata('idpersonal_tz'),
                        "id_sucursal" => $idsucursal,
                        "cantInicial" => $stock,
                        "cantidad" => $existencia,
                        "tipo" => 1,
                        "reg" => date('Y-m-d G:i:s')
                    );
            
                    $this->ModeloGeneral->log_eliminacion($dataEdicion);
                }
                //--------------------
            
            }
        }

        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d h:i:s \G\M\T');
        $array = array("id_reg"=>$res,
                        "tabla"=>'productos_sucursales',
                        "modificacion"=>$tipo_mod,
                        "campo_ant"=>'',
                        "id_producto"=>$idproducto,
                        "id_usuario"=>$this->session->userdata('usuarioid_tz'),
                        "id_sucursal"=>$this->session->userdata('idsucursal_tz'),
                        'fecha'=>$date
        );
        $this->ModeloGeneral->log_movs('log_cambios',$array);
  }
    /*
    function imgpro(){
        $idpro = $this->input->post('idpro');
        

        $upload_folder ='public/img/productos';
        $nombre_archivo = $_FILES['img']['name'];
        $tipo_archivo = $_FILES['img']['type'];
        $tamano_archivo = $_FILES['img']['size'];
        $tmp_archivo = $_FILES['img']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $tipo_archivo1 = explode('/', $tipo_archivo); 
        $tipoactivo=$tipo_archivo1[1];
        if ($tipoactivo!='png') {
            $tipo_archivo1 = explode('+', $tipoactivo); 
            $tipoactivo=$tipo_archivo1[0];
        }
        $archivador = $upload_folder . '/'.$fecha.'pro.'.$tipoactivo;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
            $resimg=$this->ModeloProductos->imgpro($archivador,$idpro);
            $return = Array('ok'=>TRUE,'img'=>$tipoactivo);
        }
        echo json_encode($return);
    }
    */
  public function deleteproductos(){
      $id = $this->input->post('id');

      $stocks = $this->ModeloProductos->getStocksProducto($id);

      $this->ModeloProductos->productosdelete($id);
      date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d h:i:s \G\M\T');
        $array = array("id_reg"=>$id,
                        "tabla"=>'productos',
                        "modificacion"=>'elimina',
                        "campo_ant"=>'activo',
                        "id_producto"=>$id,
                        "id_usuario"=>$this->session->userdata('usuarioid_tz'),
                        "id_sucursal"=>$this->session->userdata('idsucursal_tz'),
                        'fecha'=>$date
        );
        $this->ModeloGeneral->log_movs('log_cambios',$array);

        //--------------------
        foreach ($stocks as $s) {
            if($s->existencia > 0){
                $dataEliminacion = array(
                    "id_producto" => $id,
                    //"id_usuario" => $this->session->userdata('usuarioid_tz'),
                    "id_personal" => $this->session->userdata('idpersonal_tz'),
                    "id_sucursal" => $s->idsucursal,
                    "cantInicial" => $s->existencia,
                    "cantidad" => $s->existencia,
                    "tipo" => 0,
                    "reg" => date('Y-m-d G:i:s')
                );
        
                $this->ModeloGeneral->log_eliminacion($dataEliminacion);
            }
        }
        //--------------------
  }

  public function delete_select(){
      $id = $this->input->post('productos');
      
      /*foreach ($id as $value){
        $this->ModeloProductos->productosdelete($id); 
      }*/
      $DATAa = json_decode($id);
      for ($i = 0; $i < count($DATAa); $i++) {
        log_message('error','DATAa: '.json_encode($DATAa[$i]));
          $prodselect=$DATAa[$i]->prodselect; 
          $stocks = $this->ModeloProductos->getStocksProducto($prodselect);
          $this->ModeloProductos->productosdelete($prodselect);
          date_default_timezone_set('America/Mexico_City');
          $date = date('Y-m-d h:i:s \G\M\T');
          $array = array("id_reg"=>$prodselect,
                          "tabla"=>'productos',
                          "modificacion"=>'elimina multiple',
                          "campo_ant"=>'',
                          "id_producto"=>$prodselect,
                          "id_usuario"=>$this->session->userdata('usuarioid_tz'),
                          "id_sucursal"=>$this->session->userdata('idsucursal_tz'),
                          'fecha'=>$date
          );
          $this->ModeloGeneral->log_movs('log_cambios',$array);  
          

        //--------------------
            foreach ($stocks as $s) {
                if($s->existencia > 0){
                    $dataEliminacion = array(
                        "id_producto" => $prodselect,
                        //"id_usuario" => $this->session->userdata('usuarioid_tz'),
                        "id_personal" => $this->session->userdata('idpersonal_tz'),
                        "id_sucursal" => $s->idsucursal,
                        "cantInicial" => $s->existencia,
                        "cantidad" => $s->existencia,
                        "tipo" => 0,
                        "reg" => date('Y-m-d G:i:s')
                    );
            
                    $this->ModeloGeneral->log_eliminacion($dataEliminacion);
                }
            }
        //--------------------
      }
  }

    function buscarpro(){
        $buscar = $this->input->post('buscar');
        $sucu = $this->input->post('buscars');
        $depa = $this->input->post('buscard');

        $resultado=$this->ModeloProductos->productoallsearch($sucu,$depa,$buscar);
        foreach ($resultado->result() as $item){ ?>
            <tr id="trpro_<?php echo $item->productoid; ?>">
              <td></td>
              <td>
                  <div class="btn-group mr-1 mb-1">
                  <button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>
                  <button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                      <a class="dropdown-item" href="<?php echo base_url();?>Productos/productosadd?id=<?php echo $item->productoid; ?>">Editar</a>
                      <a class="dropdown-item" onclick="modal_sucursales(<?php echo $item->productoid; ?>)">Sucursales</a>
                      <a class="dropdown-item" onclick="etiquetas(<?php echo $item->productoid; ?>,'<?php echo $item->codigo; ?>','<?php echo $item->nombre; ?>');"href="#">Etiquetas</a>
                      <a class="dropdown-item" onclick="productodelete(<?php echo $item->productoid; ?>);"href="#">Eliminar</a>
                  </div>
              </div>
              </td>
              <td><?php echo $item->productoid; ?></td>
              <td><?php echo $item->codigo; ?></td>
              <td><?php echo $item->nombre; ?></td>
              <td><?php echo $item->suc_1; ?></td>
              <td><?php echo $item->pv_1 ?></td>
              <td><?php echo $item->exi_1; ?></td>
              <td><?php echo $item->suc_2; ?></td>
              <td><?php echo $item->pv_2; ?></td>
              <td><?php echo $item->exi_2; ?></td>
              <td><?php echo $item->suc_3 ?></td>
              <td><?php echo $item->pv_3; ?></td>
              <td><?php echo $item->exi_3; ?></td>
              <td><?php echo $item->depa; ?></td>
              <td><?php echo $item->fecharegistro; ?></td>              
            </tr>
        <?php }
    }

    function buscarproStock(){
        $tipostock = $this->input->post('tipostock');
        $sucurs = $this->input->post('sucurs');

        $params = $this->input->post();
        $paramss=json_encode($params);
        //log_message('error', '....'.$paramss);
        $productos = $this->ModeloProductos->productoallsearchStock($params);
        $totalRecords=$this->ModeloProductos->filasStock($tipostock,$sucurs);

        $json_data = array(
            "draw"            => intval($params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $productos->result(),
            "query"           =>$this->db->last_query()   
        );
        
        echo json_encode($json_data);

        /*echo '<table class="table table-striped responsive" id="data-tables" style="width: 100%">
                                      <thead>
                                        <tr>
                                          <th><input type="checkbox" id="chckHead" /></th>
                                          <th>Acciones</th>
                                          
                                          <th>#</th>
                                          <th>Codigo</th>
                                          <th>Descripción</th>
                                          <th>Sucursal</th>
                                          <th>Precio venta</th>
                                          <th>Stock</th>                                       
                                          <th>Departamento</th>
                                          <th>Fecha de Registro</th>
                                        </tr>
                                      </thead>
                                      <tbody id="tbodyresultadospro">';

        $resultado=$this->ModeloProductos->productoallsearchStock($sucurs,$tipostock);
        foreach ($resultado->result() as $item){ ?>
          
            <tr id="trpro_<?php echo $item->productoid; ?>">
              <td></td>
              <td>
                  <div class="btn-group mr-1 mb-1">
                  <button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>
                  <button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                      <a class="dropdown-item" href="<?php echo base_url();?>Productos/productosadd?id=<?php echo $item->productoid; ?>">Editar</a>
                      <a class="dropdown-item" onclick="modal_sucursales(<?php echo $item->productoid; ?>)">Sucursales</a>
                      <a class="dropdown-item" onclick="etiquetas(<?php echo $item->productoid; ?>,'<?php echo $item->codigo; ?>','<?php echo $item->nombre; ?>');"href="#">Etiquetas</a>
                      <a class="dropdown-item" onclick="productodelete(<?php echo $item->productoid; ?>);"href="#">Eliminar</a>
                  </div>
              </div>
              </td>
              <td><?php echo $item->productoid; ?></td>
              <td><?php echo $item->codigo; ?></td>
              <td><?php echo $item->nombre; ?></td>
              <td><?php echo $item->suc_1; ?></td>
              <td><?php echo $item->pv_1 ?></td>
              <td><?php echo $item->exi_1; ?></td>
              <td><?php echo $item->depa; ?></td>
              <td><?php echo $item->fecharegistro; ?></td>              
            </tr>
        <?php }
         echo '</tbody>
          </table>';*/ 

     } 
     
    function tablasucursal($id)
    {
        $resul = $this->ModeloProductos->tablasucursal($id);
        $html = "";
        foreach ($resul->result() as $item) {
        $html .="<tr>";
        $html .="<td>".$item->idproductosucursal."</td>";
        $html .="<td>".$item->nombre."</td>";
        $html .="<td>$ ".number_format($item->precio_venta,2,'.',',')."</td>";
        $html .="<td>$ ".number_format($item->mayoreo,2,'.',',')."</td>";
        $html .="<td>".$item->cuantos."</td>";
        $html .="<td>".$item->existencia."</td>";
        $html .="<td></td>";
        $html .="<td>".$item->minimo."</td>";
        $html .="</tr>";
        }
        echo  $html;
    }

    function upload() {
        //       echo  $archivo = $_FILES['excel']['name'];
       //Path of files were you want to upload on localhost (C:/xampp/htdocs/ProjectName/uploads/excel/)     
        $configUpload['upload_path'] = FCPATH.'fileExcel/';
        $configUpload['allowed_types'] = 'xls|xlsx|csv';
        $configUpload['max_size'] = '5000';
        $this->load->library('upload', $configUpload);
        $this->upload->do_upload('excel');  
        $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
        $file_name = $upload_data['file_name']; //uploded file name
        $extension=$upload_data['file_ext'];    // uploded file extension

        //$objReader =PHPExcel_IOFactory::createReader('Excel5');     //For excel 2003 
        $objReader= PHPExcel_IOFactory::createReader('Excel2007'); // For excel 2007     
          //Set to read only
        $objReader->setReadDataOnly(true);          
        //Load excel file
        $objPHPExcel=$objReader->load(FCPATH.'fileExcel/'.$file_name);      
        $totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel         
        $objWorksheet=$objPHPExcel->setActiveSheetIndex(0); 
        //Cambia  de activo a inactivo los registro existentes
        $this->ModeloProductos->aupdatetabla();               
          //loop from first data untill last data}
        for($i=6;$i<=$totalrows;$i++)
          {
              $codigo= $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();           
              $nombre= $objWorksheet->getCellByColumnAndRow(1,$i)->getValue(); //Excel Column 1
              $preciocompra= $objWorksheet->getCellByColumnAndRow(2,$i)->getValue(); //Excel Column 2
              $gananciaefectivo=$objWorksheet->getCellByColumnAndRow(3,$i)->getValue(); //Excel Column 3
              $porcentaje=$objWorksheet->getCellByColumnAndRow(4,$i)->getValue(); //Excel Column 4
              $precioventa=$objWorksheet->getCellByColumnAndRow(5,$i)->getValue(); //Excel Column 5
              $mayoreo=$objWorksheet->getCellByColumnAndRow(6,$i)->getValue(); //Excel Column 6
              $canmayoreo=$objWorksheet->getCellByColumnAndRow(7,$i)->getValue(); //Excel Column 7
              $fecharegistro=$objWorksheet->getCellByColumnAndRow(8,$i)->getValue()+1; //Excel Column 8
              $date_formated = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($fecharegistro));
              $iddepartamento=$objWorksheet->getCellByColumnAndRow(9,$i)->getValue(); //Excel Column 9
              $tipo_ganancia=$objWorksheet->getCellByColumnAndRow(10,$i)->getValue(); //Excel Column 10
              // Sucursales precio_venta  mayoreo cuantos existencia  minimo
              $precio_venta1=$objWorksheet->getCellByColumnAndRow(12,$i)->getValue();//Excel Column 13
              $mayoreo1=$objWorksheet->getCellByColumnAndRow(13,$i)->getValue();//Excel Column 14
              $cuantos1=$objWorksheet->getCellByColumnAndRow(14,$i)->getValue();//Excel Column 15
              $existencia1=$objWorksheet->getCellByColumnAndRow(15,$i)->getValue();//Excel Column 16
              $minimo1=$objWorksheet->getCellByColumnAndRow(16,$i)->getValue();//Excel Column 17

              $precio_venta2=$objWorksheet->getCellByColumnAndRow(18,$i)->getValue();//Excel Column 13
              $mayoreo2=$objWorksheet->getCellByColumnAndRow(19,$i)->getValue();//Excel Column 14
              $cuantos2=$objWorksheet->getCellByColumnAndRow(20,$i)->getValue();//Excel Column 15
              $existencia2=$objWorksheet->getCellByColumnAndRow(21,$i)->getValue();//Excel Column 16
              $minimo2=$objWorksheet->getCellByColumnAndRow(22,$i)->getValue();//Excel Column 17

              $precio_venta3=$objWorksheet->getCellByColumnAndRow(24,$i)->getValue();//Excel Column 13
              $mayoreo3=$objWorksheet->getCellByColumnAndRow(25,$i)->getValue();//Excel Column 14
              $cuantos3=$objWorksheet->getCellByColumnAndRow(26,$i)->getValue();//Excel Column 15
              $existencia3=$objWorksheet->getCellByColumnAndRow(27,$i)->getValue();//Excel Column 16
              $minimo3=$objWorksheet->getCellByColumnAndRow(28,$i)->getValue();//Excel Column 17

              if ($nombre!='') {
                
              $idp=$this->ModeloProductos->Addproductos($codigo, $nombre, $preciocompra, $gananciaefectivo, $porcentaje, $precioventa, $mayoreo , $canmayoreo, $date_formated, $iddepartamento, $tipo_ganancia); 
              //log_message('error','p----'.$idp);
     
              //$sucursal=$this->ModeloProductos->getSucursales();
              //foreach ($sucursal->result() as $item) {
                      $this->ModeloProductos->Addsucursales(1, $idp, $precio_venta1, $mayoreo1, $cuantos1, $existencia1, $minimo1);  
                      $this->ModeloProductos->Addsucursales(2, $idp, $precio_venta2, $mayoreo2, $cuantos2, $existencia2, $minimo2);  
                      $this->ModeloProductos->Addsucursales(3, $idp, $precio_venta3, $mayoreo3, $cuantos3, $existencia3, $minimo3);            
              //}
                    }
            }
             unlink('fileExcel/'.$file_name); //File Deleted After uploading in database .           
             //redirect(base_url() . "put link were you want to redirect");
             $output = [];
         echo json_encode($output);
    }
    function excel(){
        $producto = $this->ModeloProductos->getProducto_excel();
        $sucursal2 = $this->ModeloProductos->getProducto_excel2();
        $sucursal3 = $this->ModeloProductos->getProducto_excel3();
        $data['producto']=$producto;
        $data['sucursal2']=$sucursal2;
        $data['sucursal3']=$sucursal3;
        $data['fecha']=$this->fechahoy;
        $this->load->view('productos/excel',$data);
    }
    function validarcode(){
      $codigo = $this->input->post('codigo');
        $where = array('codigo' => $codigo,'activo'=>1 );
        $result=$this->ModeloGeneral->getselectwheren('productos',$where);
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }

}
/*
SELECT p.productoid,p.codigo,s.idsucursal, su.idsucursal, suc.idsucursal FROM productos AS p 
INNER JOIN productos_sucursales AS s ON s.idproducto = p.productoid
INNER JOIN productos_sucursales AS su ON s.idproducto = p.productoid
INNER JOIN productos_sucursales AS suc ON s.idproducto = p.productoid
WHERE p.activo=1 AND s.idsucursal = 1 AND su.idsucursal=2 AND suc.idsucursal=3 GROUP BY p.productoid

SELECT p.nombre, p.preciocompra,  p.gananciaefectivo, p.porcentaje, p.precioventa,  p.mayoreo,  p.canmayoreo, p.fecharegistro,  p.iddepartamento, p.tipo_ganancia, s.precio_venta,  s.mayoreo,  s.cuantos,  s.existencia, s.minimo, su.precio_venta,  su.mayoreo, su.cuantos, su.existencia,  su.minimo, suc.precio_venta,  suc.mayoreo,  suc.cuantos,  suc.existencia, suc.minimo FROM productos AS p 
INNER JOIN productos_sucursales AS s ON s.idproducto = p.productoid
INNER JOIN productos_sucursales AS su ON s.idproducto = p.productoid
INNER JOIN productos_sucursales AS suc ON s.idproducto = p.productoid
WHERE p.activo=1 AND s.idsucursal = 1 AND su.idsucursal=2 AND suc.idsucursal=3 GROUP BY p.productoid

SELECT p.codigo, p.nombre, p.preciocompra,  p.gananciaefectivo, p.porcentaje, p.precioventa,  p.mayoreo,  p.canmayoreo,p.fecharegistro, p.iddepartamento, p.tipo_ganancia, s.precio_venta,  s.mayoreo,  s.cuantos,  s.existencia, s.minimo FROM productos AS p 
INNER JOIN productos_sucursales AS s ON s.idproducto = p.productoid
INNER JOIN productos_sucursales AS su ON s.idproducto = p.productoid
INNER JOIN productos_sucursales AS suc ON s.idproducto = p.productoid
WHERE p.activo=1 AND s.idsucursal = 1 AND su.idsucursal=2 AND suc.idsucursal=3 GROUP BY p.productoid, s.idproductosucursal
*/
