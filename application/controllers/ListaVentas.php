<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ListaVentas extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloVentas');
        $this->load->model('ModeloGeneral');
        $this->load->model('ModeloPuntos');
        $this->load->model('ModeloProductos');
        //==============================================================
        date_default_timezone_set("America/Mexico_City");
        $this->current_anio = date('Y');
        //==============================================================

        $logueo = $this->session->userdata();
        if (isset($logueo['perfilid_tz'])) {
            $perfilid = $logueo['perfilid_tz'];
            $this->sucursal = $logueo['idsucursal_tz'];
            if ($this->sucursal == 0) {
                $this->sucursalid = 1;
            } else {
                $this->sucursalid = $logueo['idsucursal_tz'];
            }
            $this->personal = $logueo['idpersonal_tz'];
            $this->user = $logueo['id_user'];
        } else {
            $perfilid = 0;
            redirect('/Sistema');
        }/*
                $permiso=$this->ModeloCatalogos->getviewpermiso($perfilid,19);// 13 es el id del menu
                if ($permiso==0) {
                    redirect('/Sistema');
                }*/
        //===================================================
    }
    public function index()
    {
        /*
        $pages = 10; //Número de registros mostrados por páginas
        $this->load->library('pagination'); //Cargamos la librería de paginación
        $config['base_url'] = base_url() . 'ListaVentas/view/'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
        $config['total_rows'] = $this->ModeloVentas->filas(); //calcula el número de filas
        $config['per_page'] = $pages; //Número de registros mostrados por páginas  
        $config['num_links'] = 3; //Número de links mostrados en la paginación
        $config['first_link'] = 'Primera'; //primer link
        $config['last_link'] = 'Última'; //último link
        $config["uri_segment"] = 3; //el segmento de la paginación
        $config['next_link'] = 'Siguiente'; //siguiente link
        $config['prev_link'] = 'Anterior'; //anterior link
        $this->pagination->initialize($config); //inicializamos la paginación 
        $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["ventas"] = $this->ModeloVentas->total_paginados($pagex, $config['per_page']);
*/
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        //$this->load->view('Personal/Personal',$data);
        //$this->load->view('ventas/listaventas', $data);
        $this->load->view('ventas/listaventas');
        $this->load->view('templates/footer');
        $this->load->view('ventas/jslistaventas');
    }

    function get_table()
    {
        //log_message('error', 'PruebaA:' . 1114.6);
        //log_message('error', 'PruebaD:' . $this->custom_round(1114.6));
        $params = $this->input->post();

        $getdata = $this->ModeloVentas->get_data($params);
        $totaldata = $this->ModeloVentas->total_data($params);

        $json_data = array(
            "draw"            => intval($params['draw']),
            "recordsTotal"    => intval($totaldata),
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           => $this->db->last_query()
        );
        echo json_encode($json_data);
    }

    function cancelarventa()
    {
        $id = $this->input->post('id');
        $aux = $this->cancelarPuntos($id, 0);

        log_message('error','AUX: '.$aux);
        if($aux=="ERROR"){
            echo "ERROR";
            return;
        }

        $ventas_aux = $this->ModeloVentas->getVentaCancelPar($id);
        log_message('error', 'RESULT: ' . json_encode($ventas_aux));

        $this->ModeloVentas->cancelarventa($id, $this->user);
        $this->ModeloVentas->cancelaParcial2($id); //cancela la venta detalle status=0 

        $resultado = $this->ModeloVentas->ventadetalles($id);
        //log_message('error', 'RESULT: ' . json_encode($id));
        //log_message('error', 'RESULT: ' . json_encode($resultado->result()));
        
        foreach ($resultado->result() as $item) {
            $this->ModeloVentas->regresarpro2($item->id_producto, $item->cantidad, $item->sucursalid);
        }

        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d h:i:s \G\M\T');
        $array = array(
            "id_reg" => $id,
            "tabla" => 'ventas',
            "modificacion" => 'cancelado total',
            "campo_ant" => '',
            "id_producto" => '0',
            "id_usuario" => $this->session->userdata('usuarioid_tz'),
            "id_sucursal" => $this->session->userdata('idsucursal_tz'),
            'fecha' => $date
        );
        $this->ModeloGeneral->log_movs('log_cambios', $array);


        foreach ($ventas_aux as $vd) {
            //--------------------
            $stock = $this->ModeloProductos->getStockProducto($vd->id_producto, $vd->sucursalid);//$this->session->userdata('idsucursal_tz')
            $this->ModeloGeneral->kardex_add_devolucion($vd->id_detalle_venta, $this->session->userdata('idpersonal_tz'), $stock, date('Y-m-d G:i:s'));
            //--------------------
        }
        

    }
    function buscarvent()
    {
        $buscar = $this->input->post('buscar');
        $resultado = $this->ModeloVentas->ventassearch($buscar);
        foreach ($resultado->result() as $item) { ?>
            <tr id="trven_<?php echo $item->id_venta; ?>">
                <td><?php echo $item->id_venta; ?></td>
                <td><?php echo $item->reg; ?></td>
                <td><?php echo $item->vendedor; ?></td>
                <td><?php echo $item->sucursal; ?></td>
                <td><?php echo $item->monto_total; ?></td>
                <td><?php echo $item->cliente; ?></td>
                <td><?php if ($item->cancelado == 1) {
                        echo '<span class="badge badge-danger">Cancelado</span>';
                    }
                    if ($item->cancelado == 2) {
                        echo '<span class="badge badge-danger">Cancelado Parcial</span>';
                    } ?></td>
                <td>
                    <?php if ($item->cancelado == 0) { ?>
                        <button class="btn btn-raised gradient-blackberry white sidebar-shadow" onclick="facturar(<?php echo $item->id_venta; ?>)" title="Facturar Venta" data-toggle="tooltip" data-placement="top"> Facturar
                        </button>
                    <?php } ?>
                </td>
                <td>
                    <button class="btn btn-raised gradient-blackberry white sidebar-shadow" onclick="ticket(<?php echo $item->id_venta; ?>)" title="Ticket" data-toggle="tooltip" data-placement="top">
                        <i class="fa fa-book"></i>
                    </button>
                    <button class="btn btn-raised gradient-flickr white sidebar-shadow" onclick="cancelar(<?php echo $item->id_venta; ?>,<?php echo $item->monto_total; ?>)" title="Cancelar" data-toggle="tooltip" data-placement="top" <?php if ($item->cancelado == 1) {
                                                                                                                                                                                                                                                echo 'disabled';
                                                                                                                                                                                                                                            } ?>>
                        <i class="fa fa-times"></i>
                    </button>
                </td>
            </tr>
<?php }
    }

    public function consultaDetalleVenta($id)
    {
        $data = $this->ModeloVentas->detallesVentas($id);
        //log_message('error','DATA: '.json_encode($data->result()));
        echo "<table id='tabla_temp_detventa' class='table table-bordered table-striped table-hover js-basic-example dataTable'>
                 <thead>
                   <tr>
                   <th class=''>Codigo</th>
                   <th class=''>Cantidad</th>
                   <th class=''>Producto</th>
                   <th class=''>Precio</th>
                   <th class=''>Cancelar</th>
                   </tr>
                 </thead>
                 <tbody>";
        foreach ($data->result() as $row) {
            echo "<tr>";
            echo "<td class=''>" . $row->codigo . "</td>";
            echo "<td class=''>" . $row->cantidad . "</td>";
            echo "<td class=''>" . $row->nombre . "</td>";
            echo "<td class=''>" . $row->precio . "</td>";
            if ($row->status_vd == 1) {
                echo "<td class=''><button class='btn btn-raised gradient-flickr white sidebar-shadow' onclick='cancelaParcial($row->id_detalle_venta)'> Cancelar</button></td>";
            } else {
                echo "<td class='btn btn-raised gradient-flickr white sidebar-shadow'>Cancelado</td>";
            }
            echo "</tr>";
        }
        echo "</tbody>
            </table>";
    }

    public function cancelaParcial($id_vd, $id_v)
    {
        $aux = $this->cancelarPuntos($id_v, $id_vd);

        log_message('error','AUX: '.$aux);
        if($aux=="ERROR"){
            echo "ERROR";
            return;
        }

        $this->ModeloVentas->cancelaParcial($id_vd); //cancela la venta detalle status=0 ok 
        $this->ModeloVentas->cancelarventa2($id_v, $this->personal); //cancela venta parcial ok 

        $obt = $this->ModeloVentas->detallesVentasId($id_vd);
        //log_message('error', 'RESULT: ' . json_encode($obt->result()));
        foreach ($obt->result() as $row) {
            $cant = $row->cantidad;
            $sucursalid = $row->sucursalid;
            $id_producto = $row->id_producto;
        }


        //$this->session->userdata('idsucursal_tz'),
        $stock = $this->ModeloProductos->getStockProducto($id_producto, $sucursalid );//$this->session->userdata('idsucursal_tz')
        log_message('error', 'STOCK: ' . json_encode($stock));
        $this->ModeloGeneral->kardex_add_devolucion($id_vd,$this->session->userdata('idpersonal_tz'), $stock, date('Y-m-d G:i:s'));
        //--------------------

        $this->ModeloVentas->update_stock($id_producto, $cant, $sucursalid); // modifica el inventario, regresa prod cancelado a stock 
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d h:i:s \G\M\T');
        $array = array(
            "id_reg" => $id_v,
            "tabla" => 'ventas',
            "modificacion" => 'cancelado parcial',
            "campo_ant" => '',
            "id_producto" => '0',
            "id_usuario" => $this->session->userdata('usuarioid_tz'),
            "id_sucursal" => $this->session->userdata('idsucursal_tz'),
            'fecha' => $date
        );
        $this->ModeloGeneral->log_movs('log_cambios', $array);
    }


    private function cancelarPuntos($id_venta, $id_venta_detalle = 0)
    {
        if ($id_venta_detalle == 0) {

            //Condicional-----------
            $getGastoPuntosVenta = $this->ModeloPuntos->getPuntosGastadosVenta($id_venta);
            log_message('error','GastoPuntosVentas: '.json_encode($getGastoPuntosVenta));
            if($getGastoPuntosVenta > 0){
                return "ERROR";
            }
            //Condicional------------

            //Completa
            $dataG = $this->ModeloPuntos->getDataVentaGeneral($id_venta);
            $datosG = $dataG->result();
            log_message('error', 'DATOS G:' . json_encode($datosG));
            $sumaPrecios = 0;

            $anioReg = 0;

            foreach ($datosG as $key) {
                //$descuento = $key->precio * $key->descuento;

                log_message('error', 'METODO: MIXTO');
                $puntos = ($key->precio * $key->cantidad);
                $sumaPrecios +=  $puntos;

                log_message('error', 'puntos1::: '.$puntos);
                log_message('error', 'precio - %: ' . $this->custom_round(($key->precio - $key->precio * $key->descuento) * $key->cantidad));
                
                log_message('error', ' ');
                $anioReg = date('Y', strtotime($key->reg));

                
            }

            log_message('error', 'S:' . $sumaPrecios);
            log_message('error', 'S Round:' . $this->custom_round($sumaPrecios));

            $puntos = ($this->custom_round($sumaPrecios) - ($this->custom_round($sumaPrecios) * $key->descuento));

            log_message('error', 'P:' . $puntos);
            log_message('error', 'P Round:' . $this->custom_round($puntos));

            $puntos = ($this->custom_round($puntos) - $key->pagopuntos) * ($key->porcentaje_puntos / 100);
            log_message('error', 'P2:' . $puntos);

            log_message('error', 'Prueba:' . $this->custom_round(1114.6));

            
            log_message('error', 'P2 Round:' . $this->custom_round($puntos));

            log_message('error', 'Puntos G:' . $puntos);
            log_message('error', 'Montos total:' . $key->monto_total);
            log_message('error', 'Suma puntos:' . $puntos);
            log_message('error', 'Puntos PAGO:' . $key->pagopuntos);


            /*------------------------------------------------------------------------------ */
            //$this->ModeloPuntos->subPuntosCliente($key->id_cliente, $puntos);   
            //$this->ModeloPuntos->subPuntosVenta($key->id_venta, $puntos);

            
            if ((int)$this->current_anio - (int)$anioReg == 0){

                $this->ModeloPuntos->subPuntosCliente($key->id_cliente, $puntos);
                $this->ModeloPuntos->subPuntosVenta($id_venta, $puntos);

                $this->ModeloPuntos->subPuntosClienteAnioAct($key->id_cliente, $puntos);
                log_message('error','Año Actual');
                
            }else if((int)$this->current_anio - (int)$anioReg == 1){

                $this->ModeloPuntos->subPuntosCliente($key->id_cliente, $puntos);
                $this->ModeloPuntos->subPuntosVenta($id_venta, $puntos);

                $this->ModeloPuntos->subPuntosClienteAnioAnt($key->id_cliente, $puntos);
                log_message('error','Año Anterior');
            }

            /*---------------------------------------------------------------------------- */


            if($key->metodo == 4 || $key->metodo == 5){

                //$this->ModeloPuntos->subPuntosUsados($key->id_venta, $key->pagopuntos);//Ventas pagopuntos - 
                //$this->ModeloPuntos->addPuntosCliente($key->id_cliente, $key->pagopuntos);


                if ((int)$this->current_anio - (int)$anioReg == 0){

                    $this->ModeloPuntos->subPuntosUsados($key->id_venta, $key->pagopuntos);//Ventas pagopuntos - 
                    $this->ModeloPuntos->addPuntosCliente($key->id_cliente, $key->pagopuntos);
    
                    $this->ModeloPuntos->subPuntosClienteAnioAct($key->id_cliente, $puntos);
                    $this->ModeloPuntos->addPuntosClienteAnioAct($key->id_cliente, $puntos);
                    log_message('error','Año Actual+');
                    
                }else if((int)$this->current_anio - (int)$anioReg == 1){
    
                    $this->ModeloPuntos->subPuntosUsados($key->id_venta, $key->pagopuntos);//Ventas pagopuntos - 
                    $this->ModeloPuntos->addPuntosCliente($key->id_cliente, $key->pagopuntos);
    
                    $this->ModeloPuntos->subPuntosClienteAnioAnt($key->id_cliente, $puntos);
                    $this->ModeloPuntos->addPuntosClienteAnioAnt($key->id_cliente, $puntos);
                    log_message('error','Año Anterior+');
                }

                
            }

        } else {
            //PARCIAL

            //Condicional-----------
            $getGastoPuntosVenta = $this->ModeloPuntos->getPuntosGastadosVenta($id_venta);
            log_message('error','GastoPuntosVentas: '.json_encode($getGastoPuntosVenta));
            if($getGastoPuntosVenta > 0){
                return "ERROR";
            }
            //Condicional------------

            $dataP = $this->ModeloPuntos->getDataVentaParticular($id_venta, $id_venta_detalle);
            $datosP = $dataP->result();

            log_message('error', 'DATOS P:' . json_encode($datosP));
            //2024-12-20 13:17:32

            foreach ($datosP as $key) {
                $puntos = 0;
                $descuento = $key->precio * $key->descuento;
                //log_message('error', 'ID_VENTA:' . $key->id_venta);


                if ($key->metodo == 4) {
                    $total = $key->monto_total;

                    $dinero = $total - $key->pagopuntos;
                    $valorCancelados = $this->ModeloPuntos->getSumaPrecios($id_venta);
                    $valorCanceladosActivo = $valorCancelados + ($key->precio * $key->cantidad);

                    $totalParcial = $this->ModeloPuntos->checkTotalParcial($key->id_venta); //Por descuento
                    $montoPasado = $totalParcial - $this->custom_round($totalParcial * $key->descuento);
                    $montoActual = $totalParcial - ($key->precio * $key->cantidad);
                    $montoActual = $montoActual - $this->custom_round($montoActual * $key->descuento);

                    //log_message('error', 'Valor cancelados: ' . $valorCancelados);
                    //log_message('error', 'Valor cancelados y actual: ' . $valorCanceladosActivo);

                    //log_message('error', 'Dinero::: ' . $dinero);
                    //log_message('error', 'Cancelado::: ' . $key->cancelado);
                    //log_message('error', 'Total:::' . $total);
                    //log_message('error', 'Monto Actual:::' . $montoActual);
                    //log_message('error', 'Monto Anterior:::' . $montoPasado);
                    //log_message('error', 'PagoPunto:::' . $key->pagopuntos);

                    //log_message('error', 'Subtotal::: ' . ($key->subtotal));
                    //log_message('error', 'Precio::: ' . ($key->precio));
                    //log_message('error', 'Valor cancelados::: ' . $valorCancelados);
                    //log_message('error', 'Valor cancelados y actual::: ' . $valorCanceladosActivo);

                    if ($valorCanceladosActivo > $dinero) {
                        log_message('error', 'IF--->');

                        $totalAnterior = $montoPasado;
                        $montoNuevo = $montoActual;

                        $diferenciaPuntos = $key->pagopuntos - $montoNuevo;

                        if ($montoNuevo < $key->pagopuntos) {

                            $diferenciaMontos = $totalAnterior - $montoNuevo;
                            //log_message('error', 'diferenciaMontos:::* N/A');

                            $var = number_format($key->puntos_total, 2);
                            //log_message('error', 'VAR: ' . $var);

                            $this->ModeloPuntos->subPuntosCliente($key->id_cliente, $var);
                            $this->ModeloPuntos->setPuntosVenta($id_venta, 0);

                            //log_message('error', 'diferenciaPuntos::: ' . $diferenciaPuntos);

                            $this->ModeloPuntos->subPuntosUsados($key->id_venta, $diferenciaPuntos);
                            $this->ModeloPuntos->addPuntosCliente($key->id_cliente, $diferenciaPuntos);

                            return;

                        } else {
                            $diferenciaMontos = $totalAnterior - $montoNuevo;
                            //log_message('error', 'diferenciaMontos:::*' . $diferenciaMontos);

                            $puntos = $diferenciaMontos * ($key->porcentaje_puntos / 100);
                            //log_message('error', 'Puntos:::* ' . $puntos);
                        }

                        //log_message('error', 'Puntos:::*** ' . $puntos);


                        $this->ModeloPuntos->subPuntosCliente($key->id_cliente, $puntos);
                        $this->ModeloPuntos->subPuntosVenta($id_venta, $puntos);

                        return;
                    } else {
                        //log_message('error', 'ELSE--->');

                        $totalAnterior = $montoPasado;
                        $montoNuevo = $montoActual;

                        $diferenciaMontos = $totalAnterior - $montoNuevo;
                        //log_message('error', 'diferenciaMontos:::*' . $diferenciaMontos);

                        $puntos = $diferenciaMontos * ($key->porcentaje_puntos / 100);
                        //log_message('error', 'Puntos:::* ' . $puntos);

                        $this->ModeloPuntos->subPuntosCliente($key->id_cliente, $puntos);
                        $this->ModeloPuntos->subPuntosVenta($id_venta, $puntos);
                        return;
                    }
                }


                if($key->metodo == 5){
                    $puntos = ($puntos * ($key->porcentaje_puntos / 100)) * $key->cantidad;
                    //log_message('error', 'Puntos P:' . $puntos);

                    $totalParcial = $this->ModeloPuntos->checkTotalParcial($key->id_venta); //Por descuento
                    $montoPasado = $totalParcial - $this->custom_round($totalParcial * $key->descuento);
                    $montoActual = $totalParcial - ($key->precio * $key->cantidad);
                    $montoActual = $montoActual - $this->custom_round($montoActual * $key->descuento);

                    $diferenciaMontos = $montoPasado - $montoActual;

                    //log_message('error', 'Monto Actual:::' . $montoActual);
                    //log_message('error', 'Monto Anterior:::' . $montoPasado);
                    //log_message('error', 'Diferencia:::' . $diferenciaMontos);
                    
                    $this->ModeloPuntos->subPuntosUsados($key->id_venta, $diferenciaMontos);
                    $this->ModeloPuntos->addPuntosCliente($key->id_cliente, $diferenciaMontos);

                    return;
                }

                //------------------------------------------------------------------
                $totalParcial = $this->ModeloPuntos->checkTotalParcial($key->id_venta); //Por descuento
                $montoPasado = $totalParcial - $this->custom_round($totalParcial * $key->descuento);
                $montoActual = $totalParcial - ($key->precio * $key->cantidad);
                $montoActual = $montoActual - $this->custom_round($montoActual * $key->descuento);

                $diferenciaMontos = $montoPasado - $montoActual;
                //log_message('error', 'diferenciaMontos:::*' . $diferenciaMontos);

                $puntos = $diferenciaMontos * ($key->porcentaje_puntos / 100);
                log_message('error', 'Puntos: ' . $puntos);

                //log_message('error', 'Total Parcial:' . $totalParcial);
                //log_message('error', 'Monto Actual:' . $montoActual);
                //log_message('error', 'Monto Anterior:' . $montoPasado);
                //log_message('error', 'Diferencia:' . $diferenciaMontos);

/*------------------------------------------------------------- */

            //$this->ModeloPuntos->subPuntosCliente($key->id_cliente, $puntos);   
            //$this->ModeloPuntos->subPuntosVenta($key->id_venta, $puntos);

                $anioReg = date('Y', strtotime($key->reg));
                if ((int)$this->current_anio - (int)$anioReg == 0){

                    $this->ModeloPuntos->subPuntosCliente($key->id_cliente, $puntos);
                    $this->ModeloPuntos->subPuntosVenta($id_venta, $puntos);

                    $this->ModeloPuntos->subPuntosClienteAnioAct($key->id_cliente, $puntos);
                    log_message('error','Año Actual');

                }else if((int)$this->current_anio - (int)$anioReg == 1){

                    $this->ModeloPuntos->subPuntosCliente($key->id_cliente, $puntos);
                    $this->ModeloPuntos->subPuntosVenta($id_venta, $puntos);

                    $this->ModeloPuntos->subPuntosClienteAnioAnt($key->id_cliente, $puntos);
                    log_message('error','Año Anterior');
                }


/*------------------------------------------------------------- */

                /*
                if ($key->pagopuntos > 0) {
                    $this->ModeloPuntos->addPuntosCliente($key->id_cliente, $difPuntos);
                }
                */
            }
        }
    }

    private function custom_round($number)
    {
        //log_message('error', 'CUSTOM ROUND----------');
        //log_message('error', 'Number:' . $number);
        $int_part = floor($number); // Parte entera del número
        $decimal_part = $number - $int_part; // Parte decimal del número

        //log_message('error', 'Unidad:' . $int_part );
        //log_message('error', 'Decimal:' . $decimal_part);

        $tolerance = 0.00001; // Pequeña tolerancia para comparación

        if ($decimal_part >= (0.6 - $tolerance)) {
            //log_message('error', 'T:' . ceil($number));
            return ceil($number); // Redondea hacia arriba
        } else {
            //log_message('error', 'F:' . floor($number));
            return floor($number); // Redondea hacia abajo
        }
    }

    public function detalles_credito()
    {   
        $id = $this->input->post('id');
        $monto_total = '';
        $getventas=$this->ModeloVentas->getventasVendedor($id);
        foreach ($getventas->result() as $item){
            $monto_total = $item->monto_total;
        }
        $restante=0;
        $getresta=$this->ModeloVentas->get_restante_venta($id);
        foreach ($getresta->result() as $x){
            $restante = $x->monto;
        }
        
        $restante_total=$monto_total-$restante;

        $html='<div class="row">
          <div class="col-md-12">
            <h5>Total de venta: $'.number_format($monto_total, 2, '.', ',').'</h5>
            <h5>Saldo restante: $'.number_format($restante_total, 2, '.', ',').'</h5>
          </div> 
        </div>';
        if($restante_total!=0){
        $html.='<div class=row">
            <div class="col-md-3">
              <div class="form-group">
                  <label class=" control-label">Método:</label>
                  <select class="form-control" id="mpago">
                        <option value="0">Selecciona una opción</option>
                        <option value="1">Efectivo</option>
                        <option value="2">Tarjeta de crédito</option>
                        <option value="3">Tarjeta de débito</option>
                  </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                  <label class=" control-label">Referencia:</label>
                  <input type="text" class="form-control" id="referencia_p">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                  <label class="control-label">Fecha:</label>
                  <input type="date" class="form-control" id="fecha_p">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                  <label class="control-label">Monto:</label>
                  <input type="number" class="form-control" id="monto_p">
              </div>
            </div>
            <div class="col-md-1">
              <div class="form-group">
                  <label class="control-label" style="color:white">-</label>
                  <a class="btn btn-raised gradient-green-tea white sidebar-shadow" onclick="agregar_pago_credito('.$id.')"><i class="fa fa-plus" aria-hidden="true"></i></a>
              </div>
            </div>
        </div>';
        }
        echo $html;

    }

    function add_pagos_credito()
    {   
        $id_venta = $this->input->post('id_venta');
        $metodo = $this->input->post('metodo');
        $referencia = $this->input->post('referencia');
        $fecha = $this->input->post('fecha');
        $monto = $this->input->post('monto');
        $monto_total = 0;
        $getventas=$this->ModeloVentas->getventasVendedor($id_venta);
        foreach ($getventas->result() as $item){
            $monto_total = $item->monto_total;
        }

        $restante=0;
        $getresta=$this->ModeloVentas->get_restante_venta($id_venta);
        foreach ($getresta->result() as $x){
            $restante = $x->monto;
        }

        $restante_total=$monto_total-$restante;

        $validar=0;
        if($restante_total>=$monto){
            $validar=0;
            $data = array('id_venta'=>$id_venta,'metodo'=>$metodo,'referencia'=>$referencia,'fecha'=>$fecha,'monto'=>$monto);
            $this->ModeloGeneral->tabla_inserta('venta_pagos_credito',$data);
        }else{
            $validar=1;
        }
        echo $validar; 
    }

    public function detalles_tabla_credito()
    {   
        $id = $this->input->post('id');
        $html='<h4>Historial de pagos</h4><table class="table table-striped responsive" id="data-tables" style="width: 100%">
            <thead>
              <tr>
                <th>Método</th>
                <th>Referencia</th>
                <th>Fecha</th>
                <th>Monto</th>
                <th></th>
              </tr>
            </thead>
            <tbody>';
                $where = array('id_venta'=>$id,'activo'=>1);
                $result=$this->ModeloGeneral->getselectwheren('venta_pagos_credito',$where);
                foreach ($result as $x){
                    $met='';
                    if($x->metodo==1){
                        $met='Efectivo';
                    }else if($x->metodo==2){
                        $met='Tarjeta de crédito';
                    }else if($x->metodo==3){
                        $met='Tarjeta de débito';
                    }
                    $html.='<tr class="row_p'.$x->id.'" ><td>'.$met.'</td><td>'.$x->referencia.'</td><td>'.date('d/m/Y',strtotime($x->fecha)).'</td><td>$'.number_format($x->monto, 2, '.', ',').'</td><td><button class="btn btn-raised gradient-flickr white sidebar-shadow" onclick="eliminar_pago('.$x->id.')" title="Eliminar" data-toggle="tooltip" data-placement="top"> <i class="fa fa-times"></i>                  </button></td></tr>';
                }
                
            $html.='</tbody>
          </table>';

        echo $html;
    }

    function delete_pago()
    {   
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $result=$this->ModeloGeneral->updateCatalogo($data,'id',$id,'venta_pagos_credito');
    }
}
