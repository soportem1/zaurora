<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Facturaslis extends CI_Controller {
    public function __construct()     {
        parent::__construct();
        $this->load->model('ModeloGeneral');
        $this->load->model('ModeloSession');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Modelofacturas');
        $this->load->model('ModeloVentas');
        $this->load->model('ModeloClientes');
        $this->load->helper('url');
  
        date_default_timezone_set("America/Mexico_City");
        $this->fechahoy=date('Y-m-d H:i:s');
        //==============================================================
        $logueo = $this->session->userdata();
        if (isset($logueo['perfilid_tz'])) {
            $perfilid=$logueo['perfilid_tz'];
            $this->sucursal=$logueo['idsucursal_tz'];
            if ($this->sucursal==0) {
                $this->sucursalid=1;
            }else{
                $this->sucursalid=$logueo['idsucursal_tz'];
            }
            $this->personal=$logueo['idpersonal_tz'];
            /*$permiso=$this->ModeloCatalogos->getviewpermiso($this->personal,21);// cambiar por el de factura lista
            if ($permiso==0) {
                redirect('/Sistema');
            }*/
        }else{
            $perfilid=0;
            //redirect('/Sistema');
        }
    
    }
    function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('facturaciones/Facturaslist');
        $this->load->view('templates/footer');
        $this->load->view('facturaciones/Facturaslistjs');
    }

    public function searchcli(){
        $usu = $this->input->get('search');
        $results=$this->ModeloClientes->clientesallsearch($usu);
        echo json_encode($results->result());
    }

    public function getlistfacturas() {
        $params = $this->input->post();
        $getdata = $this->Modelofacturas->getfacturas($params);
        $totaldata= $this->Modelofacturas->total_facturas($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function solicitarpermiso(){
        $pass = $this->input->post('pass');
        $permiso=0;
        $permisos=$this->ModeloSession->permisoadmin();
        foreach ($permisos as $item) {
            $verificar = password_verify($pass,$item->contrasena);
            if ($verificar) {
                $permiso=1;
            }
        }
        echo $permiso;
    }

    function add(){
        $data['cfdi']=$this->ModeloGeneral->genSelect('f_uso_cfdi'); 
        $data['metodo']=$this->ModeloGeneral->genSelect('f_metodopago'); 
        $data['forma']=$this->ModeloGeneral->genSelect('f_formapago'); 
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('facturaciones/Facturasadd',$data);
        $this->load->view('templates/footer');
        $this->load->view('facturaciones/Facturasaddjs');
    }
    function obtenerrfc(){
        $params = $this->input->post();
        $idcliente = $params['id'];
        $result=$this->ModeloGeneral->getselectwheren('cliente_has_datos_fiscales',array('idCliente'=>$idcliente,'activo'=>1));
        $html='';
        foreach ($result->result() as $item) {
            $html.='<option value="'.$item->id.'">'.$item->rfc.'</option>';
        }
        echo $html;
    }

    function complemento($idfactura){
        $data['cfdi']=$this->ModeloGeneral->genSelect('f_uso_cfdi'); 
        $data['metodo']=$this->ModeloGeneral->genSelect('f_metodopago'); 
        $data['forma']=$this->ModeloGeneral->genSelect('f_formapago'); 
        $data['Folio']=$idfactura;
        $idsucursal=$this->sucursalid;
        $datosconfiguracion=$this->ModeloGeneral->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $datosconfiguracion=$datosconfiguracion;
        $datosconfiguracion=$datosconfiguracion[0];
        $data['rFCEmisor']=$datosconfiguracion->Rfc;
        $data['Nombre']=$datosconfiguracion->Nombre;
        $data['Regimen']=$datosconfiguracion->Regimen;
        $data['LugarExpedicion']=$datosconfiguracion->CodigoPostal;

        $datosfactura=$this->ModeloGeneral->getselectwheren('f_facturas',array('FacturasId'=>$idfactura));
        $datosfactura=$datosfactura;
        $datosfactura=$datosfactura[0];
        $data['razonsocialreceptor']=$datosfactura->Nombre;
        $data['rfcreceptor']=$datosfactura->Rfc;
        $data['Fecha']=date('Y-m-d').'T'.date('H:i:s');
        $data['uuid']=$datosfactura->uuid;
        $data['Folio']=$datosfactura->Folio;
        $data['serie']=$datosfactura->serie;
        $data['FacturasId']=$datosfactura->FacturasId;
        $data['Fechafa']=date('Y-m-d',strtotime($datosfactura->fechatimbre)).'T'.date('H:i:s',strtotime($datosfactura->fechatimbre));
        $data['formapago']=$datosfactura->MetodoPago;
        
        $saldo=$this->Modelofacturas->saldocomplemento($idfactura);
        $datoscopnum=$this->Modelofacturas->saldocomplementonum($idfactura);

        $data['copnum']=$datoscopnum+1;
        //log_message('error', 'validar saldo: '.$datosfactura->total.'-'.$saldo);
        $data['saldoanterior']=$datosfactura->total-$saldo;
        $data['cliente']=$datosfactura->Clientes_ClientesId;
        $data['MetodoDePagoDR']=$datosfactura->FormaPago;

        /*$datoscop=$this->ModeloGeneral->getselectwheren('f_complementopago',array('FacturasId'=>$idfactura));
        $saldo=0; $datoscopnum=0;
        foreach ($datoscop as $item) {
            $saldo=$saldo+$item->ImpPagado;
            $datoscopnum++;
        }
        $data['copnum']=$datoscopnum+1;
        $data['saldoanterior']=$datosfactura->total-$saldo;*/

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('facturaciones/complementodd',$data);
        $this->load->view('templates/footer');
        $this->load->view('facturaciones/complementojs');
    }
    function listasdecomplementos(){
        $factura=$this->input->post('facturas');
        //$resultado=$this->ModeloGeneral->getselectwheren('f_complementopago',array('FacturasId'=>$factura,'Estado'=>1));
        $resultado=$this->ModeloCatalogos->complementofacturas($factura);
        $html='<table class="responsive-table display no-footer dataTable" id="tableliscomplementos" width="100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Fecha</th>
                        <th>Numero de parcialidad</th>
                        <th>Monto</th>
                        <th>Archivo</th>
                    </tr>
                </thead>
                <tbody>';
        foreach ($resultado->result() as $item) {
            $html.='<tr>
                        <td>'.$item->complementoId.'</td>
                        <td>'.$item->FechaPago.'</td>
                        <td>'.$item->NumParcialidad.'</td>
                        <td>'.$item->ImpPagado.'</td>
                        <td>
                            <a class="b-btn b-btn-primary tooltipped" href="'.base_url().$item->rutaXml.'" target="_blank" data-position="top" data-delay="50" data-tooltip="XML" download="" data-tooltip-id="54ee1647-5011-f5dd-45f2-7aa729169609"><i class="fa fa-file-code-o fa-2x"></i></a>
                            <a class="b-btn b-btn-primary tooltipped" href="'.base_url().'Facturaslis/complementodoc/'.$item->complementoId.'" target="_blank" data-position="top" data-delay="50" data-tooltip="XML"  data-tooltip-id="54ee1647-5011-f5dd-45f2-7aa729169609">
                                <i class="fa fa-file-pdf-o fa-2x"></i>
                                </a>
                        </td>
                    </tr>';
        }
        $html.='</tbody></table>';
        echo $html;
    }
    
    public function generarfacturas($FacturasId,$file){
        //$file 1 factura 2xml 3 completo 4 detalle
        $data["Nombrerasonsocial"]='Nombrerasonsocial';
        $data['rrfc']='';
        $data['rdireccion']='direccion conocida';
        $data['regimenf']='601 General de personas morales';
        $logotipo = base_url().'public/img/ops.png';

        $data["logotipo"]=$logotipo;
        $factura = $this->ModeloGeneral->getselectwheren('f_facturas',array('FacturasId'=>$FacturasId));
        foreach ($factura as $item) {
            $MetodoPago=$item->MetodoPago;
            $FormaPago=$item->FormaPago;
            $observaciones=$item->observaciones;
            $Folio=$item->Folio;
            $isr=$item->isr;
            $uuid=$item->uuid;
            $nocertificadosat=$item->nocertificadosat;
            $certificado=$item->nocertificado;
            $fechatimbre=$item->fechatimbre;
            $uso_cfdi=$item->uso_cfdi;
            $cliente=$item->Nombre;
            $clirfc=$item->Rfc;
            $clidireccion=$item->Direccion;
            $data["cp"] = $item->Cp; 
            $data["Lote"] = $item->Lote; 
            $numproveedor=$item->numproveedor;
            $numordencompra=$item->numordencompra;
            $moneda=$item->moneda;
            $subtotal  = $item->subtotal;
            $iva = $item->iva;
            $total = $item->total;
            $ivaretenido = $item->ivaretenido;
            $cedular = $item->cedular;
            $selloemisor = $item->sellocadena;
            $sellosat = $item->sellosat;
            $cadenaoriginal = $item->cadenaoriginal;
            $tarjeta=$item->tarjeta;
            $tipoComprobante=$item->TipoComprobante;

            $Caducidad=$item->Caducidad;
            $Lote=$item->Lote;
        }
        $facturadetalles = $this->ModeloVentas->facturadetalle($FacturasId);

      //=============
          $data['FormaPago']=$MetodoPago;
          //log_message('error', 'FormaPago c: '.$MetodoPago);
          $data['FormaPagol']=$MetodoPago;
          //$data['FormaPagol']='vv';
        //=================
          $data['MetodoPago']=$FormaPago;
          $data['MetodoPagol']=$FormaPago;
          //$data['MetodoPagol']='cc';
        //=================
          $data['observaciones']=$observaciones;
          $data['Folio']=$Folio;
        //=================
          $data["isr"]=$isr;
        //=================
          $data['folio_fiscal']=$uuid;
          $data['nocertificadosat']=$nocertificadosat;
          $data['certificado']=$certificado;
          $data['fechatimbre']=$fechatimbre;
          //$data['cfdi']=$this->gt_uso_cfdi($uso_cfdi);
          $data['cfdi']=$uso_cfdi;
          $data['cliente']=$cliente;
          $data['clirfc']=$clirfc;
          $data['clidireccion']=$clidireccion;
          $data['numproveedor']=$numproveedor;
          $data['numordencompra']=$numordencompra;
          $data['moneda']=$moneda;
          $data['subtotal']=$subtotal;
          $data['iva']=$iva;

          /*$data['isr']=$isr;
          $data['ivaretenido']=$ivaretenido;
          $data['cincoalmillarval']=$cincoalmillarval;
          $data['outsourcing']=$outsourcing;*/

          $data['total']=$total;
          $data['ivaretenido']=$ivaretenido;
          $data['cedular']=$cedular;
          $data['selloemisor']=$selloemisor;
          $data['sellosat']=$sellosat;
          $data['cadenaoriginal']=$cadenaoriginal;
          $data['tarjeta']=$tarjeta;
          $data['facturadetalles']=$facturadetalles->result();
          $data['tipoComprobante']=$tipoComprobante;

        $data['idfactura']=$FacturasId;
        $data['Caducidad']      =   $Caducidad;
        $data['Lote']           =   $Lote;
        $this->load->view('Reportes/factura',$data); 
    }
    function complementodoc($id){
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $datosconfiguracion=$datosconfiguracion->row();
        if($datosconfiguracion->logotipo!=''){
          $logotipo = base_url().'public/img/'.$datosconfiguracion->logotipo;
        }else{
          $logotipo = base_url().'public/img/ops.png';
        }
        $data["logotipo"]=$logotipo;

        $data['rfcemisor']=$datosconfiguracion->Rfc;
        $data['nombreemisor']=$datosconfiguracion->Nombre;
        $data['rdireccion']=$datosconfiguracion->Calle.' '.$datosconfiguracion->Municipio.' '.$datosconfiguracion->Estado.' '.$datosconfiguracion->PaisExpedicion.' C.P. '.$datosconfiguracion->CodigoPostal;
        $data['regimenf']=$this->gt_regimenfiscal($datosconfiguracion->Regimen);
        //==========================================
            $data['idcomplemento']=$id;
            $resultscp=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$id));
            $data['resultscpd']=$this->ModeloCatalogos->getselectwheren('f_complementopago_documento',array('complementoId'=>$id));
            $resultscp=$resultscp->row();
            $FacturasId=$resultscp->FacturasId;
            $data['uuid']=$resultscp->uuid;
            $data['nocertificadosat']=$resultscp->nocertificadosat;
            $data['LugarExpedicion']=$resultscp->LugarExpedicion;
            $data['fechatimbre']=$resultscp->fechatimbre;
            $data['fechapago']=$resultscp->FechaPago;
            $data['monto']=$resultscp->Monto;
            $data['IdDocumento']=$resultscp->IdDocumento;
            //$data['Folio']=$resultscp->Folio;
            $data['ImpSaldoAnt']=$resultscp->ImpSaldoAnt;
            $data['NumParcialidad']=$resultscp->NumParcialidad;
            $data['ImpSaldoInsoluto']=$resultscp->ImpSaldoInsoluto;
            $data['Sello']=$resultscp->Sello;
            $data['sellosat']=$resultscp->sellosat;
            $data['cadenaoriginal']=$resultscp->cadenaoriginal;
            $data['nocertificadosat']=$resultscp->nocertificadosat;
            $data['rfcreceptor']=$resultscp->R_rfc;
            $data['nombrereceptorr']=$resultscp->R_nombre;
            $data['NumOperacion']=$resultscp->NumOperacion;
            $docrelac = $this->Modelofacturas->documentorelacionado($id);
            $data['docrelac']=$docrelac->result();
        //====================================================================
            $resultsfp=$this->ModeloCatalogos->getselectwheren('f_formapago',array('id'=>$resultscp->FormaDePagoP));
            $resultsfp=$resultsfp->row();
            $data['formapago']=$resultsfp->formapago_text;
        //==========================================
            //$resultsfc=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$FacturasId));
            //$resultsfc=$resultsfc->row();
            /*
            if ($resultsfc->moneda=='pesos') {
                $data['moneda']='Peso Mexicano';
            }else{
                $data['moneda']='Dolar';
            }
            */
        //====================================================================
            //$resultsmp=$this->ModeloCatalogos->getselectwheren('f_metodopago',array('metodopago'=>$resultsfc->FormaPago));
            //$resultsmp=$resultsmp->row();
            //$data['metodopago']=$resultsmp->metodopago_text;
        //==========================================
        $this->load->view('reportes/complemento',$data);
    }
    function gt_regimenfiscal($text){
          if($text=="601"){ 
            $textl='601 General de Ley Personas Morales';

          }elseif($text=="603"){ 
            $textl='603 Personas Morales con Fines no Lucrativos';

          }elseif($text=="605"){ 
            $textl='605 Sueldos y Salarios e Ingresos Asimilados a Salarios';

          }elseif($text=="606"){ 
            $textl='606 Arrendamiento';

          }elseif($text=="607"){ 
            $textl='607 Régimen de Enajenación o Adquisición de Bienes';

          }elseif($text=="608"){ 
            $textl='608 Demás ingresos';

          }elseif($text=="609"){ 
            $textl='609 Consolidación';

          }elseif($text=="610"){ 
            $textl='610 Residentes en el Extranjero sin Establecimiento Permanente en México';

          }elseif($text=="611"){ 
            $textl='611 Ingresos por Dividendos (socios y accionistas)';

          }elseif($text=="612"){ 
            $textl='612 Personas Físicas con Actividades Empresariales y Profesionales';

          }elseif($text=="614"){ 
            $textl='614 Ingresos por intereses';

          }elseif($text=="615"){ 
            $textl='615 Régimen de los ingresos por obtención de premios';

          }elseif($text=="616"){ 
            $textl='616 Sin obligaciones fiscales';

          }elseif($text=="620"){ 
            $textl='620 Sociedades Cooperativas de Producción que optan por diferir sus ingresos';

          }elseif($text=="621"){ 
            $textl='621 Incorporación Fiscal';

          }elseif($text=="622"){ 
            $textl='622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras';

          }elseif($text=="623"){ 
            $textl='623 Opcional para Grupos de Sociedades';

          }elseif($text=="624"){ 
            $textl='624 Coordinados';

          }elseif($text=="628"){ 
            $textl='628 Hidrocarburos';

          }elseif($text=="629"){ 
            $textl='629 De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales';

          }elseif($text=="630"){ 
            $textl='630 Enajenación de acciones en bolsa de valores';
          }else{
            $textl='';
          }
          return $textl;
    }
    /**************COMPLEMENTOS********************** */
    function obtenerfacturas(){
        $datos = $this->input->post();
        $cli=$datos['cli'];
        $fact=$datos['fact'];
        $rowsfacturas=$this->ModeloCatalogos->getselectwheren('f_facturas',array('Estado'=>1,'Clientes_ClientesId'=>$cli,'Folio !='=>$fact));
        $html='<table id="facturascli" class="table" width="100%">
                <thead>
                    <tr>
                        <th>Folio</th>
                        <th>IdDocumento(uuid)</th>
                        <th>Monto</th>
                        <th>Fecha Timbre</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
        ';
        foreach ($rowsfacturas->result() as $item) {
            $html.='<tr>
                        <td>'.$item->Folio.'</td>
                        <td class="facturasuuid">'.$item->uuid.'</td>
                        <td>'.$item->total.'</td>
                        <td>'.$item->fechatimbre.'</td>
                        <td>
                            <a class="btn-bmz waves-effect waves-light cyan" 
                                onclick="adddoc('.$item->FacturasId.')"
                                >Agregar</a></td>
                    </tr>';
        }
        $html.='</tbody></table>';
        echo $html;
    }

    function documentoadd(){
        $datos = $this->input->post();
        $idfactura=$datos['factura'];
        $datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$idfactura));
        $datosfactura=$datosfactura->result();
        $datosfactura=$datosfactura[0];

        $datoscop=$this->ModeloCatalogos->getselectwheren('f_complementopago_documento',array('FacturasId'=>$idfactura,'Estado'=>1));
        $datoscopnum=$datoscop->num_rows();
        $saldo=0;
        foreach ($datoscop->result() as $item) {
            $saldo=$saldo+$item->ImpPagado;
        }
        $NumParcialidad=$datoscopnum+1;
        $array = array(
            'idfactura'=>$idfactura,
            'MetodoDePagoDR'=>$datosfactura->FormaPago,
            'IdDocumento'=>$datosfactura->uuid,
            'serie'=>$datosfactura->serie,
            'folio'=>$datosfactura->Folio,
            'NumParcialidad'=>$NumParcialidad,
            'ImpSaldoAnt'=>$datosfactura->total-$saldo
        );
        echo json_encode($array);
    }

    function addcomplemento(){
      $data = $this->input->post();
        $arraydoc=$data['arraydocumento'];
        unset($data['arraydocumento']);
        //$idfactura=$data['idfactura'];
        //==================================================================
          //$datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$idfactura));
          //$datosfactura=$datosfactura->result();
          //$datosfactura=$datosfactura[0];
        //==================================================================
          //$idsucursal=$this->idsucursal;
          $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
          $datosconfiguracion=$datosconfiguracion->result();
          $datosconfiguracion=$datosconfiguracion[0];
        //==================================================================

        //$Sello=$datosfactura->sellosat;
        //$Sello=$datosfactura->sellocadena;
        
        $datacp['Folio']=$data['Folio'];
        $datacp['Fecha']=$data['Fecha'];
        $datacp['Sello']='';
        //$datacp['NoCertificado']=$datosfactura->nocertificado;
        //$datacp['Certificado']=$datosfactura->certificado;
        $datacp['Certificado']='';
        $datacp['LugarExpedicion']=$data['LugarExpedicion'];
        $datacp['E_rfc']=$datosconfiguracion->Rfc;
        $datacp['E_nombre']=$datosconfiguracion->Nombre;
        $datacp['E_regimenfiscal']=$datosconfiguracion->Regimen;
        $datacp['R_rfc']=$data['rfcreceptor'];
        $datacp['R_nombre']=$data['razonsocialreceptor'];
        $datacp['R_regimenfiscal']='';
        $datacp['FechaPago']=$data['Fechatimbre'];
        $datacp['FormaDePagoP']=$data['FormaDePagoP'];
        $datacp['MonedaP']=$data['ModedaP'];
        $datacp['Monto']=$data['Monto'];
        $datacp['NumOperacion']=str_replace(' ', '', $data['NumOperacion']);
        $datacp['CtaBeneficiario']=$data['CtaBeneficiario'];
        
        $datacp['Serie']=$data['Serie'];//xxx
        $datacp['Foliod']=$data['Folio'];
        $datacp['MonedaDR']=$data['ModedaP'];
    
        $datacp['UsoCFDI']='';
        $idcomplemento=$this->ModeloGeneral->tabla_inserta('f_complementopago',$datacp);
        $DATAdoc = json_decode($arraydoc);
        for ($j=0;$j<count($DATAdoc);$j++){
          $datacdoc['complementoId']=$idcomplemento;
          $datacdoc['facturasId']=$DATAdoc[$j]->idfactura;
          $datacdoc['IdDocumento']=$DATAdoc[$j]->IdDocumento;
          $datacdoc['serie']=$DATAdoc[$j]->serie;
          $datacdoc['folio']=$DATAdoc[$j]->folio;
          $datacdoc['NumParcialidad']=$DATAdoc[$j]->NumParcialidad;
          $datacdoc['ImpSaldoAnt']=$DATAdoc[$j]->ImpSaldoAnt;
          $datacdoc['ImpPagado']=$DATAdoc[$j]->ImpPagado;
          $datacdoc['ImpSaldoInsoluto']=$DATAdoc[$j]->ImpSaldoInsoluto;
          $datacdoc['MetodoDePagoDR']=$DATAdoc[$j]->MetodoDePagoDR;
          $this->ModeloGeneral->tabla_inserta('f_complementopago_documento',$datacdoc);
        }
        $respuesta=$this->procesarcomplemento($idcomplemento);
        echo json_encode($respuesta);
    }

    function retimbrarcomplemento(){
      $complemento = $this->input->post('complemento');
      $respuesta=$this->procesarcomplemento($complemento);
        echo json_encode($respuesta);
    }

    // FALTA ACTUALIZAR A VERSION 4.0 - COMPLEMENTOS
    function procesarcomplemento($idcomplemento){
        //========================================================================
        $rutainterna= $_SERVER['DOCUMENT_ROOT'].'/zaurora';//rutalocal
        //$rutainterna= $_SERVER['DOCUMENT_ROOT'];//rutaserver
        $ruta=$rutainterna.'/zaurora_sat/temporalsat/';
        $rutaf=$rutainterna.'/zaurora_sat/facturas/';
        $rutalog=$rutainterna.'/zaurora_sat/facturaslog/';
        $datosFactura = array(
          'carpeta'=>'zaurora_sat',
          'pwskey'=>'',
          'archivo_key'=>'',
          'archivo_cer'=>'',
          'factura_id'=>0
        );
        $numeroCertificado=file_get_contents(base_url() . $datosFactura['carpeta'] . '/temporalsat/Serial.txt');
        //fclose($archivo);
        $numeroCertificado=  str_replace("serial=", "", $numeroCertificado);//quitar letras
        $temporal=  str_split($numeroCertificado);//Pasar todos los digitos a un array
        $numeroCertificado="";
        $i=0;  
        foreach ($temporal as $value) {
            if(($i%2))
                $numeroCertificado .= $value;
          
            $i++;
        }
        
        $numeroCertificado = str_replace('\n','',$numeroCertificado);
        $numeroCertificado = trim($numeroCertificado);

        $certificado=file_get_contents(base_url() . $datosFactura['carpeta'] . '/temporalsat/certificate.pem');
        //fclose($archivo);
        $certificado=  str_replace("-----BEGIN CERTIFICATE-----", "", $certificado);
        $certificado=  str_replace("-----END CERTIFICATE-----", "", $certificado);
        
        $certificado=  str_replace("\n", "", $certificado);
        $certificado=  str_replace(" ", "", $certificado);
        $certificado=  str_replace("\n", "", $certificado);
        $certificado= preg_replace("[\n|\r|\n\r]", '', $certificado);
        $certificado= trim($certificado);
      //========================================================================================
        $xml=$this->generaxmlcomplemento('',$certificado,$numeroCertificado,$idcomplemento);
        log_message('error', 'se genera xml2: '.$xml);
        $str = trim($this->generaCadenaOriginalReal($xml,$datosFactura['carpeta']));    
        
        $cadena=$str;
      //===========================================================================================
        $fp = fopen(base_url() . $datosFactura['carpeta'] . '/temporalsat/llaveprivada.pem', "r");  
        $priv_key = fread($fp, 8192); 
        fclose($fp); 
        $pkeyid = openssl_get_privatekey($priv_key);
        //openssl_sign($cadena, $sig, $pkeyid);
        openssl_sign($cadena, $sig, $pkeyid,'sha256');
        openssl_free_key($pkeyid);
        $sello = base64_encode($sig);
      //===============accessos al webservice===================================================

        $productivo=0;//0 demo 1 productivo
        if($productivo==0){
          $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
        }elseif ($productivo==1) {
          //Produccion
          $URL_WS='https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL';
        }else{
          $URL_WS='';
        }
        if($productivo==0){
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }elseif ($productivo==1) {
          //Produccion
          $password= "@fuJXsS@";
          $usuario= "HERD890308UAA";
        }else{
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }
        $idsucursal=$this->idsucursal;
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$idsucursal));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];
        $rFCEmisor=$datosconfiguracion->Rfc;
        $passwordClavePrivada=$datosconfiguracion->paswordkey;

        $clienteSOAP = new SoapClient($URL_WS);

        //==================================================================
          $datoscomplemento=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$idcomplemento));
          $datoscomplemento=$datoscomplemento->result();
          $datoscomplemento=$datoscomplemento[0];
        //==================================================================
          
          $xmlcomplemento=$this->generaxmlcomplemento($sello,$certificado,$numeroCertificado,$idcomplemento);
          //log_message('error', 'se genera xml3: '.$xmlcomplemento);
            file_put_contents('zaurora_sat/facturas/preview_complemento_'.$idcomplemento.'.xml',$xmlcomplemento);
            
            $referencia=$datoscomplemento->E_rfc.'_'.$idcomplemento;
        //======================================================================================
            
          $result = $clienteSOAP->TimbrarCFDI(array(
                    'usuario' => $usuario,
                    'password' => $password,
                    'cadenaXML' => $xmlcomplemento,
                    'referencia' => $referencia));
          file_put_contents($rutalog.'log_complemento'.$this->fechahoyL.'_.txt', json_encode($result));
          if($result->TimbrarCFDIResult->CodigoRespuesta > 0 ) {                       
            
            $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->TimbrarCFDIResult->CodigoRespuesta,
                          'MensajeError'=>$result->TimbrarCFDIResult->MensajeError,
                          'Mensajeadicional'=>'PORFAVOR RETIMBRAR MAS TARDE',
                          'idcomplemento'=>$idcomplemento,
                          'info'=>$result,
                          'info2'=>$result->TimbrarCFDIResult->MensajeErrorDetallado
                          );
            
            $this->ModeloCatalogos->updateCatalogo('f_complementopago',array('Estado'=>2),array('complementoId'=>$idcomplemento));   
            return $resultado;
        } else {
            try {
                $xmlCompleto=utf8_decode($result->TimbrarCFDIResult->XMLResultado);//Contiene XML
                //Guardar en archivo     y la ruta en la bd
                $ruta = $datosFactura['carpeta'] . '/facturas/complemento_'.$idcomplemento.'.xml';
                file_put_contents($ruta,"\xEF\xBB\xBF".$xmlCompleto);
                $this->ModeloCatalogos->updateCatalogo('f_complementopago',array('rutaXml'=>$ruta),array('complementoId'=>$idcomplemento));
                $sxe = new SimpleXMLElement($xmlCompleto);
                $ns = $sxe->getNamespaces(true);
                $sxe->registerXPathNamespace('c', $ns['cfdi']);
                $sxe->registerXPathNamespace('t', $ns['tfd']);
                $uuid = '';
                foreach ($sxe->xpath('//t:TimbreFiscalDigital') as $tfd) {          
                 
                    //Actualizamos el Folio Fiscal UUID
                    //$uuid = $tfd['UUID'];
                    $updatedatossat=array(
                      'uuid'=>$tfd['UUID'],
                      'Sello'=>$tfd['SelloCFD'],
                      'sellosat'=>$tfd['SelloSAT'],
                      'NoCertificado'=>$tfd['NoCertificado'],
                      'nocertificadosat'=>$tfd['NoCertificadoSAT'],
                      'fechatimbre'=>date('Y-m-d G:i:s'),
                      'cadenaoriginal'=>$cadena,
                      'Estado'=>1
                    );
                    $this->ModeloCatalogos->updateCatalogo('f_complementopago',$updatedatossat,array('complementoId'=>$idcomplemento)); 
                } 
                //$this->ModeloCatalogos->updateCatalogo('f_facturas',array('Estado'=>1),array('FacturasId'=>$datosFactura['factura_id']));
                $resultado = array(
                          'resultado'=>'correcto',
                          'Mensajeadicional'=>'Complemento Generado',
                          'idcomplemento'=>$idcomplemento
                          );
            return $resultado;
           }  catch (Exception $e) {
              $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->TimbrarCFDIResult->CodigoRespuesta,
                          'MensajeError'=>$result->TimbrarCFDIResult->MensajeError,
                          'Mensajeadicional'=>'PORFAVOR RETIMBRAR MAS TARDE',
                          'idcomplemento'=>$idcomplemento,
                          'info'=>$result,
                          'info2'=>$result->TimbrarCFDIResult->MensajeErrorDetallado
                          );
            $this->ModeloCatalogos->updateCatalogo('f_complementopago',array('Estado'=>2),array('complementoId'=>$idcomplemento));
            return $resultado;
           }
        }
        //echo json_encode($result);  
        return $resultado;
    }
    function generaxmlcomplemento($sello,$certificado,$numeroCertificado,$idcomplemento){
        
          $datoscomplemento=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$idcomplemento));
          $datoscomplemento=$datoscomplemento->result();
          $datoscomplemento=$datoscomplemento[0];
          $caract   = array('&','ñ','Ñ','°','á','é','í','ó','ú','Á','É','Í','Ó','Ú','  ',' ');
          $caract2  = array('','n','N','','a','e','i','o','u','A','E','I','O','U',' ','');
          $R_nombre=strval(str_replace($caract, $caract2, $datoscomplemento->R_nombre));
            $f_relacion=$datoscomplemento->f_relacion;
            $f_r_tipo=$datoscomplemento->f_r_tipo;
            $f_r_uuid=$datoscomplemento->f_r_uuid;
        //==================================================================
            $datoscliente=$this->ModeloCatalogos->getselectwheren('clientes',array('rfcdf'=>$datoscomplemento->R_rfc,'activo'=>1));
            $datoscliente=$datoscliente->result();
            $datoscliente=$datoscliente[0];
        //==================================================================
          $NumOperacion=str_replace(' ', '', $datoscomplemento->NumOperacion);

          $xmlcomplemento='<?xml version="1.0" encoding="utf-8"?>';
          $xmlcomplemento.='
            <cfdi:Comprobante xmlns:pago20="http://www.sat.gob.mx/Pagos20" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:cfdi="http://www.sat.gob.mx/cfd/4" xsi:schemaLocation="http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd http://www.sat.gob.mx/Pagos20 http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos20.xsd" Version="4.0" Exportacion="01"
              Folio="'.$datoscomplemento->Folio.'" Fecha="'.date('Y-m-d',strtotime($datoscomplemento->Fecha)).'T'.date('H:i:s',strtotime($datoscomplemento->Fecha)).'" Sello="'.$sello.'" NoCertificado="'.$numeroCertificado.'" Certificado="'.$certificado.'" SubTotal="'.$datoscomplemento->SubTotal.'" Moneda="'.$datoscomplemento->Moneda.'" Total="'.$datoscomplemento->Total.'" TipoDeComprobante="'.$datoscomplemento->TipoDeComprobante.'" LugarExpedicion="'.$datoscomplemento->LugarExpedicion.'">';
              if($f_relacion==1){
                    $xmlcomplemento .='<cfdi:CfdiRelacionados TipoRelacion="'.$f_r_tipo.'">
                                <cfdi:CfdiRelacionado UUID="'.$f_r_uuid.'"/>
                            </cfdi:CfdiRelacionados>';
                }$xmlcomplemento .='
            <cfdi:Emisor Rfc="'.$datoscomplemento->E_rfc.'" Nombre="'.$datoscomplemento->E_nombre.'" RegimenFiscal="'.$datoscomplemento->E_regimenfiscal.'"/>
            <cfdi:Receptor Rfc="'.$datoscomplemento->R_rfc.'" Nombre="'.$R_nombre.'"  DomicilioFiscalReceptor="'.$datoscliente->cp_fiscal.'" RegimenFiscalReceptor="'.$datoscliente->RegimenFiscalReceptor.'" UsoCFDI="CP01"/>
            <cfdi:Conceptos>
              <cfdi:Concepto ClaveUnidad="'.$datoscomplemento->ClaveUnidad.'" ClaveProdServ="'.$datoscomplemento->ClaveProdServ.'" Cantidad="'.$datoscomplemento->Cantidad.'" Descripcion="'.$datoscomplemento->Descripcion.'" ValorUnitario="'.$datoscomplemento->ValorUnitario.'" Importe="'.$datoscomplemento->Importe.'" ObjetoImp="01"/>
            </cfdi:Conceptos>
            <cfdi:Complemento>
            <pago20:Pagos Version="2.0">
                <pago20:Totales MontoTotalPagos="'.$datoscomplemento->Monto.'" />
                <pago20:Pago FechaPago="'.date('Y-m-d',strtotime($datoscomplemento->FechaPago)).'T'.date('H:i:s',strtotime($datoscomplemento->FechaPago)).'" FormaDePagoP="'.$datoscomplemento->FormaDePagoP.'" MonedaP="MXN" Monto="'.$datoscomplemento->Monto.'" TipoCambioP="1"  ';
            $xmlcomplemento.=' NumOperacion="'.$NumOperacion.'" '; 
            //$xmlcomplemento.=' CtaBeneficiario="9680096800" ';
            $xmlcomplemento.=' >';
            $dcompdoc=$this->ModeloCatalogos->getselectwheren('f_complementopago_documento',array('complementoId'=>$idcomplemento));
            foreach ($dcompdoc->result() as $itemdoc) {
             $xmlcomplemento.='<pago20:DoctoRelacionado IdDocumento="'.$itemdoc->IdDocumento.'" Serie="'.$itemdoc->serie.'" Folio="'.$itemdoc->folio.'" MonedaDR="'.$datoscomplemento->MonedaDR.'" NumParcialidad="'.$itemdoc->NumParcialidad.'" ImpSaldoAnt="'.$itemdoc->ImpSaldoAnt.'" ImpPagado="'.$itemdoc->ImpPagado.'" ImpSaldoInsoluto="'.$itemdoc->ImpSaldoInsoluto.'" ObjetoImpDR="01" EquivalenciaDR="1"/>';
            }
            
            $xmlcomplemento.='</pago20:Pago>
            </pago20:Pagos>
            </cfdi:Complemento>
            </cfdi:Comprobante>';
            //log_message('error', 'se genera xml: '.$xmlcomplemento);
      return $xmlcomplemento;
    }
    function cancelarCfdicomplemento(){
      $rutainterna= $_SERVER['DOCUMENT_ROOT'].'/zaurora';//rutalocal
      //$rutainterna= $_SERVER['DOCUMENT_ROOT'];//rutaserver
      $ruta=$rutainterna.'/zaurora_sat/temporalsat/';
      $rutaf=$rutainterna.'/zaurora_sat/facturas/';
      $rutalog=$rutainterna.'/zaurora_sat/facturaslog/';

      $productivo=1;//0 demo 1 producccion
      $datas = $this->input->post();
      $facturas=$datas['facturas'];
      $motivo=$datas['motivo'];
      $uuidrelacionado=$datas['uuidrelacionado'];
      
      unset($datas['facturas']);
      unset($datas['motivo']);
      unset($datas['uuidrelacionado']);

      $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
      $datosconfiguracion=$datosconfiguracion->result();
      $datosconfiguracion=$datosconfiguracion[0];
      $rFCEmisor=$datosconfiguracion->Rfc;
      $passwordClavePrivada=$datosconfiguracion->paswordkey;

      $datosCancelacion=array();
      //$DATAf = json_decode($facturas);
      //for ($i=0;$i<count($DATAf);$i++) {
        $datosfactura=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$facturas));
        $datosfactura=$datosfactura->result();
        $datosfactura=$datosfactura[0];
        $FacturasId=$datosfactura->FacturasId;
        $datosCancelacion[] = array(
                                  'RFCReceptor'=>$datosfactura->R_rfc,
                                  'Total'=>0,
                                  'UUID'=>$datosfactura->uuid,
                                  'Motivo'=>$motivo,
                                  'FolioSustitucion'=>$uuidrelacionado

                                );
        //$this->liberarfacturas($DATAf[$i]->FacturasIds);
      //}
      //===========================================================
        if($productivo==0){
          $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
        }elseif ($productivo==1) {
          //Produccion
          $URL_WS='https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL';
        }else{
          $URL_WS='';
        }
        if($productivo==0){
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }elseif ($productivo==1) {
          //Produccion
          $password= "@fuJXsS@";
          $usuario= "HERD890308UAA";
        }else{
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }
      //=======================================================
        
        $fp = fopen($ruta.'pfx.pem', "r");
        $clavePrivada = fread($fp, filesize($ruta.'pfx.pem'));
        fclose($fp);
        
        
        $rutaarchivos=$rutainterna.'/zaurora_sat/elementos/';
        $rutapass=$rutaarchivos.'passs.txt';
        $passwordClavePrivada = file_get_contents($rutapass);
        
        //$passwordClavePrivada='Daniel182 ';// solo para las pruebas
        
        $parametros = array(
            'usuario' => $usuario,
            'password' => $password,
            'rFCEmisor' => $rFCEmisor,
            'listaCFDI' => $datosCancelacion,
            'clavePrivada_Base64'=>$clavePrivada,
            'passwordClavePrivada'=> $passwordClavePrivada  );
        file_put_contents($rutalog.'log_'.$this->fechahoyL.'xxxxx_.txt', json_encode($parametros));
        $clienteSOAP = new SoapClient($URL_WS);

        $result = $clienteSOAP->CancelarCFDI($parametros);

        if($result->CancelarCFDIResult->OperacionExitosa) {
          $resultado = array(
                          'resultado'=>'correcto',
                          'Mensajeadicional'=>'Factura Cancelada',
                          'info'=>$result
                          );
          //Guardar en archivo  y la ruta en la bd del acuse de la cancelacion
                $fileacuse='acuseCancelacion_'.$facturas.'_'.$datosfactura->uuid.'.xml';
                $ruta = $rutaf .$fileacuse;
                //file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
                file_put_contents($ruta,$result->CancelarCFDIResult->XMLAcuse);

                $datosdecancelacion = array(
                                            'Estado'=>0,
                                            'rutaAcuseCancelacion '=>$fileacuse,
                                            'fechacancelacion'=>$this->fechahoyc
                                          );
                $this->ModeloCatalogos->updateCatalogo('f_complementopago',$datosdecancelacion,array('complementoId'=>$facturas));
          
        }else{
          $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->CancelarCFDIResult->MensajeError,
                          'MensajeError'=>$result->CancelarCFDIResult->MensajeErrorDetallado,
                          'Mensajeadicional'=>'PORFAVOR CANCELAR MAS TARDE',
                          'info'=>$result
                          );
          file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
        }
        echo json_encode($resultado);
    }
    public function generaCadenaOriginalReal($xml,$carpeta) {  
        error_reporting(0); 
        $paso = new DOMDocument();
        //log_message('error', 'generaCadenaOriginalReal xml: '.$xml);
        $paso->loadXML($xml);
        $xsl = new DOMDocument();            
        //$file= base_url().$carpeta."/xslt33/cadenaoriginal_3_3.xslt";
        $file= base_url().$carpeta."/xslt40/cadenaoriginal_4_0.xslt";
        $xsl->load($file);
        $proc = new XSLTProcessor();
        // activar php_xsl.dll
        $proc->importStyleSheet($xsl); 
        $cadena_original = $proc->transformToXML($paso);
        //echo '<br><br>' . $cadena_original . '<br><br>';
        $str = mb_convert_encoding($cadena_original, 'UTF-8');                      
        //GUARDAR CADENA ORIGINAL EN ARCHIVO  
        $sello = utf8_decode($str);
        $sello = str_replace('#13','',$sello);
        $sello = str_replace('#10','',$sello);
        //$fp = fopen(DIR_FILES . $carpeta . '/temporalsat/CadenaOriginal.txt', 'w');//Cadena original 
        /*
        $fp = fopen(base_url() . $carpeta . '/temporalsat/CadenaOriginal.txt', 'w');//Cadena original 
        fwrite($fp, $sello);
        fclose($fp);
        */
        file_put_contents( $carpeta . '/temporalsat/CadenaOriginal.txt',$sello);
        return $sello;    
    }

    
}
?>
