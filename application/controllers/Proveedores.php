<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proveedores extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloProveedor');
        $this->load->model('ModeloGeneral');
    }
	public function index(){
            $pages=10; //Número de registros mostrados por páginas
            $this->load->library('pagination'); //Cargamos la librería de paginación
            $config['base_url'] = base_url().'Proveedores/view/'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
            $config['total_rows'] = $this->ModeloProveedor->filas();//calcula el número de filas
            $config['per_page'] = $pages; //Número de registros mostrados por páginas  
            $config['num_links'] = 20; //Número de links mostrados en la paginación
            $config['first_link'] = 'Primera';//primer link
            $config['last_link'] = 'Última';//último link
            $config["uri_segment"] = 3;//el segmento de la paginación
            $config['next_link'] = 'Siguiente';//siguiente link
            $config['prev_link'] = 'Anterior';//anterior link
            $this->pagination->initialize($config); //inicializamos la paginación 
            $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["preveedor"] = $this->ModeloProveedor->total_paginados($pagex,$config['per_page']);
            
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            //$this->load->view('Personal/Personal',$data);
            $this->load->view('proveedores/proveedores',$data);
            $this->load->view('templates/footer');
            $this->load->view('proveedores/jsproveedores');
	}
    public function proveedoradd(){
        $data["Estados"] = $this->ModeloProveedor->estados();
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('proveedores/proveedoradd',$data);
        $this->load->view('templates/footer');
        $this->load->view('proveedores/jsproveedores');
    }
    function preoveedoraddd(){
        $id = $this->input->post('id');
        $razonSocial = $this->input->post('razonSocial');
        $domicilio = $this->input->post('domicilio');
        $ciudad = $this->input->post('ciudad');
        $cp = $this->input->post('cp');
        $estado = $this->input->post('estado');
        $contacto = $this->input->post('contacto');
        $email = $this->input->post('email');
        $rfc = $this->input->post('rfc');
        $telefonoLocal = $this->input->post('telefonoLocal');
        $telefonoCelular = $this->input->post('telefonoCelular');
        $fax = $this->input->post('fax');
        $obser = $this->input->post('obser');

        if ($id>0) {
            $this->ModeloProveedor->proveedorupdate($id,$razonSocial,$domicilio,$ciudad,$cp,$estado,$contacto,$email,$rfc,$telefonoLocal,$telefonoCelular,$fax,$obser);
            $tipo_mod = "modifica";
            $idresp = $id;
        }else{
            $idresp = $this->ModeloProveedor->proveedorinsert($razonSocial,$domicilio,$ciudad,$cp,$estado,$contacto,$email,$rfc,$telefonoLocal,$telefonoCelular,$fax,$obser);
            $tipo_mod = "insertar";
        }
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d h:i:s \G\M\T');
        $array = array("id_reg"=>$idresp,
                        "tabla"=>'proveedores',
                        "modificacion"=>$tipo_mod,
                        "campo_ant"=>'',
                        "id_producto"=>'0',
                        "id_usuario"=>$this->session->userdata('usuarioid_tz'),
                        "id_sucursal"=>$this->session->userdata('idsucursal_tz'),
                        'fecha'=>$date
        );
        $this->ModeloGeneral->log_movs('log_cambios',$array);
        redirect('/Proveedores');
    }
    
    public function deleteproveedor(){
        $id = $this->input->post('id');
        $this->ModeloProveedor->deleteproveedors($id); 
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d h:i:s \G\M\T');
        $array = array("id_reg"=>$id,
                        "tabla"=>'proveedores',
                        "modificacion"=>'elimina',
                        "campo_ant"=>'',
                        "id_producto"=>'0',
                        "id_usuario"=>$this->session->userdata('usuarioid_tz'),
                        "id_sucursal"=>$this->session->userdata('idsucursal_tz'),
                        'fecha'=>$date
        );
        $this->ModeloGeneral->log_movs('log_cambios',$array);
    }
    function buscarprov(){
        $buscar = $this->input->post('buscar');
        $resultado=$this->ModeloProveedor->proveedorallsearch($buscar);
        foreach ($resultado->result() as $item){ ?>
            <tr id="trcli_<?php echo $item->id_proveedor; ?>">
                <td><?php echo $item->razon_social; ?></td>
                <td><?php echo $item->domicilio; ?></td>
                <td><?php echo $item->ciudad; ?></td>
    
                <td><?php echo $item->telefono_local; ?></td>
                <td><?php echo $item->telefono_celular; ?></td>
                <td><?php echo $item->email_contacto; ?></td>
                <td><?php echo $item->rfc; ?></td>
                <td>
                    <div class="btn-group mr-1 mb-1">
                        <button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>
                          <button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <span class="sr-only">Toggle Dropdown</span>
                          </button>
                          <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                              <a class="dropdown-item" href="<?php echo base_url();?>Proveedores/proveedoradd?id=<?php echo $item->id_proveedor; ?>">Editar</a>
                              <a class="dropdown-item" onclick="proveedordelete(<?php echo $item->id_proveedor; ?>);"href="#">Eliminar</a>
                          </div>
                    </div>
                </td>
            </tr>
        <?php }
    }
    
    

       
    
}
