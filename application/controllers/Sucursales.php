<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sucursales extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloGeneral');
        $this->load->model('ModeloSucursal');
        $this->load->model('ModeloPersonal');
    }
	public function index(){
        $data['sucursales'] = $this->ModeloSucursal->verSucursales();
        $data['empleados'] = $this->ModeloSucursal->getEmpleado();
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('sucursales/sucursales',$data);
        $this->load->view('templates/footer');
        $this->load->view('sucursales/jssucursales');
	}

    public function sucursalAdd($id=0){
        $data['idsucursal']=$id;
        $data['sucursales'] = $this->ModeloSucursal->verSucursales();
        $data['empleados'] = $this->ModeloSucursal->getEmpleado();
        $data['menuse']=$this->ModeloPersonal->getAllmenu();
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('sucursales/sucursaladd',$data);
        $this->load->view('templates/footer');
        $this->load->view('sucursales/jssucursales');
    }  

    public function datatable_records(){
        $sucursales = $this->ModeloSucursal->get_table_active();
        $json_data = array("data" => $sucursales);
        echo json_encode($json_data);
    }
     
    public function insertar(){
        //$id = $this->input->post('idsucursal');
        $data=$this->input->post();
        $id = $data['idsucursal'];
        unset($data['idsucursal']);
        //log_message('error', 'idsucursal: '.$id);
        if ($id>0) {
            //$this->ModeloSucursal->update($id,$data);
            $this->ModeloSucursal->updateCatalogo($data,'idsucursal',$id,'sucursal');
             $tipo_mod = "modifica";
             $id_resp = $id;
        }else{
             $id_resp = $this->ModeloSucursal->insertar($data);
             $tipo_mod = "insertar";
        }
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d h:i:s \G\M\T');
        $array = array("id_reg"=>$id_resp,
                        "tabla"=>'sucursales',
                        "modificacion"=>$tipo_mod,
                        "campo_ant"=>'',
                        "id_producto"=>'0',
                        "id_usuario"=>$this->session->userdata('usuarioid_tz'),
                        "id_sucursal"=>$this->session->userdata('idsucursal_tz'),
                        'fecha'=>$date
        );
        $this->ModeloGeneral->log_movs('log_cambios',$array);
       
    }
     public function deleteSucursal(){
        $id = $this->input->post('id');
        $this->ModeloSucursal->eliminar($id);
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d h:i:s \G\M\T');
        $array = array("id_reg"=>$id,
                        "tabla"=>'sucursales',
                        "modificacion"=>'elimina',
                        "campo_ant"=>'',
                        "id_producto"=>'0',
                        "id_usuario"=>$this->session->userdata('usuarioid_tz'),
                        "id_sucursal"=>$this->session->userdata('idsucursal_tz'),
                        'fecha'=>$date
        );
        $this->ModeloGeneral->log_movs('log_cambios',$array);
    }
    
}
