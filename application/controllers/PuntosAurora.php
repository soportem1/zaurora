<?php
defined('BASEPATH') or exit('No direct script access allowed');

  class PuntosAurora extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloPuntos');
    $this->load->model('ModeloCatalogos');

    date_default_timezone_set("America/Mexico_City");

    $this->fechahoy = date('Y-m-d G:i:s');
    $this->fecha_reciente = date('Y-m-d');
    //$this->fechahoy = date('Y-m-d');
    $this->fechahoyc = date('Y-m-d H:i:s');
    $this->fechahoyL = date('Y-m-d_H_i_s');
    $this->current_anio = date('Y');

    //======================  ========================================
    $logueo = $this->session->userdata();
    if (isset($logueo['perfilid_tz'])) {
      $perfilid = $logueo['perfilid_tz'];
      $this->sucursal = $logueo['idsucursal_tz'];
      $this->idpersonal = $this->session->userdata('idpersonal_tz');
      if ($this->sucursal == 0) {
        $this->sucursalid = 1;
      } else {
        $this->sucursalid = $logueo['idsucursal_tz'];
      }
      $this->personal = $logueo['idpersonal_tz'];
    } else {
      $perfilid = 0;
    }
    /*
    $permiso=$this->ModeloCatalogos->getviewpermiso($perfilid,19);// 13 es el id del menu
    if ($permiso==0) {
        redirect('/Sistema');
    }
    */
    //===================================================
  }


  public function index()
  {
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('consultaPuntos/index');
    $this->load->view('templates/footer');
    $this->load->view('consultaPuntos/index_js');
  }

  public function searchcli()
  {
    $usu = $this->input->get('search');
    $results = $this->ModeloClientes->clientesallsearch($usu);
    echo json_encode($results->result());
  }

  public function get_list()
  {
    $fechaLimite = new DateTime('2024-01-01');
    $id = $this->input->post('id');
    $FInicio = $this->input->post('fInicio');
    $fFinal = $this->input->post('fFinal');
    $idClienteGeneral = 45;

    log_message("error", "------------------------");
    //Actualizar puntos
    if($id != $idClienteGeneral){
      log_message("error", "ACTUALIZAR PUNTOS");
      $this->checkVigenciaPuntos($id);
    }
    log_message("error", "------------------------");


    $result1 = $this->ModeloPuntos->getDataPuntos($id, $FInicio, $fFinal);
    log_message("error", "RESULT: " . json_encode($result1));

    foreach ($result1 as $r) {
      $date = new DateTime($r->reg);
      $puntos = 0;
      $total = $r->monto_total;
      $pagoPuntos = $r->pagopuntos;

      $porcentajePuntos = $this->checkPorcentajePuntos($r->reg);
      $porcentajePuntosCliente = $this->checkPorcentajePuntosCliente($id);
      

      if($r->en_monedero == 0){
        if ($date > $fechaLimite) {
          $puntos = $total;
          $porcentajeP = $porcentajePuntos * 100;
          //$puntos = $puntos * 0.05;          
          //$puntos = $total * $porcentajePuntos;

          if($id != $idClienteGeneral){
            if($porcentajePuntosCliente > 0){
              $porcentajeP = $porcentajePuntosCliente * 100;
              $puntos = ($total - $pagoPuntos ) * $porcentajePuntosCliente;
            }else{
              $porcentajeP = $porcentajePuntos * 100;
              $puntos = ($total - $pagoPuntos ) * $porcentajePuntos;
            }
          }else{
            $porcentajeP = 0;
            $puntos = 0;
          }

          //$puntos = ($total - $pagoPuntos ) * $porcentajePuntos;
          log_message("error", "PUNTOS MONEDERO IDCliente: " . json_encode($id));
          log_message("error", "PUNTOS MONEDERO IDVenta: " . json_encode($r->id_venta));
          log_message("error", "PUNTOS MONEDERO Puntos: " . json_encode($puntos));
          log_message("error", "PUNTOS MONEDERO PorcentajePuntos: " . json_encode($porcentajeP));

          $this->checkPuntosPasados($id, $r->id_venta, $puntos, $porcentajeP);
  
        }
      }
    }


    $result2 = $this->ModeloPuntos->getDataPuntos($id, $FInicio, $fFinal);
    //log_message("error", "RESULT: " . json_encode($result2));
    $html = '';
    foreach ($result2 as $r) {
      $date = new DateTime($r->reg);
      $fecha = $date->format('d-m-Y H:i:s');
      $total = $r->monto_total;
      $metodo = array(" ","Efectivo","Tarjeta de crédito","Tarjeta de débito","Pago mixto","Puntos Aurora");

      if ($r->cancelado > 0) {
        $total = $this->ModeloPuntos->checkTotalParcial($r->id_venta); //Por descuento
        $total = $total - $this->custom_round($total * $r->descuento);
      }

      $html .= '<tr>
                  <td>' . $fecha . '</td>
                  <td>' . $r->id_venta . '</td>
                  <td>' . $r->vendedor . '</td>
                  <td>' . $r->sucursal . '</td>
                  <td>' . $metodo[$r->metodo] . '</td>
                  <td>' . $r->porcentaje_puntos . '% </td>
                  <td>$ ' . number_format($total, 2, ".", ",") . '</td>
                  <td>$ ' . number_format($r->puntos_total, 2, ".", ",") . '</td>
                  <td>$ ' . number_format($r->pagopuntos, 2, ".", ",") . '</td>
                  <td> 
                    <button class="btn btn-raised gradient-blackberry white sidebar-shadow" onclick="ticket(' . $r->id_venta . ')" title="Ticket" data-toggle="tooltip" data-placement="top">
                      <i class="fa fa-book"></i>
                    </button>
                    <input id="subtotal_' . $r->id_venta . '" type="hidden" value="' . number_format($total, 2, ".", ",") . '">
                    <input id="puntos_' . $r->id_venta . '" type="hidden" value="' . number_format($r->puntos_total, 2, ".", ",") . '">
                  </td>
                </tr>';
    }

    echo $html;
    
  }

  public function getTotalPuntos()
  {
    $id = $this->input->post('id');
    $total = $this->ModeloPuntos->getTotalPuntos($id);

    echo $total;
  }

  private function checkPuntosPasados($id_cliente, $id_venta, $puntos, $porcentajePuntos)
  {
    $minFecha = '2024-01-01'; //Enero 2024

    $points = $this->ModeloPuntos->getPuntosVenta($id_cliente, $id_venta, $minFecha);
    log_message("error", "POINTS: " . $points);

    if (floatval($points) === floatval(0)) {
      log_message("error", "---Se cumple condicion para puntos antiguos---");
      $this->ModeloCatalogos->updateCatalogo('ventas', array('puntos_total' => $puntos, 'porcentaje_puntos' => $porcentajePuntos, 'en_monedero'=>'1'), array('id_venta' => $id_venta, 'id_cliente' => $id_cliente));

      $this->ModeloPuntos->addPuntosCliente($id_cliente, $puntos); //puntos en clientes

    }
  }

  private function checkPorcentajePuntos($FCompra)
  {
    $porcentajePuntos = 5;

    $dataPuntos = $this->ModeloPuntos->getDataConfigPuntos();
    //log_message("error", "dataPuntos: " . json_encode($dataPuntos));

    if ($dataPuntos) {
      $fechaIniConfig = (new DateTime($dataPuntos->fechaIni))->setTime(0, 0, 0);
      $fechaFinConfig = (new DateTime($dataPuntos->fechaFin))->setTime(0, 0, 0);
      $fechaComprobar = (new DateTime($FCompra))->setTime(0, 0, 0);

      //log_message("error", "Fecha de Compra: " . $fechaComprobar->format('Y-m-d'));

      if ($dataPuntos->checkFecha == 1) {
        //log_message("error", "Rango 1 desde: " . $fechaIniConfig->format('Y-m-d'));
        if ($fechaComprobar >= $fechaIniConfig) {
          //log_message("error", "La fecha " . $fechaComprobar->format('Y-m-d') . " está en el rango 1.");
          $porcentajePuntos = $dataPuntos->porcentaje;
        }
      } else {
        //log_message("error", "Rango 0 desde: " . $fechaIniConfig->format('Y-m-d') . " hasta: " . $fechaFinConfig->format('Y-m-d'));
        if ($fechaComprobar >= $fechaIniConfig && $fechaComprobar <= $fechaFinConfig) {
          //log_message("error", "La fecha " . $fechaComprobar->format('Y-m-d') . " está dentro del rango 0.");
          $porcentajePuntos = $dataPuntos->porcentaje;
        }
      }
    }

    //Revision de porcentajes------------------------------
    $porcentajePuntos = $porcentajePuntos > 100 ? 100 : $porcentajePuntos;
    $porcentajePuntos = $porcentajePuntos < 0 ? 0 : $porcentajePuntos;
    //Revision de porcentajes------------------------------

    $porcentajePuntos = $porcentajePuntos / 100;
    return $porcentajePuntos;
  }


  private function checkPorcentajePuntosCliente($idCliente)
  {
    $porcentajePuntos = 0;

    $dataPuntosC = $this->ModeloPuntos->getDataConfigPuntosCliente($idCliente);
    log_message("error", "dataPuntosC: " . json_encode($dataPuntosC));

    if ($dataPuntosC) {
      $porcentajePuntos = $dataPuntosC->porcentaje;

      $porcentajePuntos = $porcentajePuntos > 100 ? 100 : $porcentajePuntos;
      $porcentajePuntos = $porcentajePuntos < 0 ? 0 : $porcentajePuntos;
    }

    $porcentajePuntos = $porcentajePuntos / 100;
    log_message("error", "Porcentaje Puntos: " . json_encode($porcentajePuntos));
    return $porcentajePuntos;
  }


  private function custom_round($number)
    {
        //log_message('error', 'CUSTOM ROUND----------');
        //log_message('error', 'Number:' . $number);
        $int_part = floor($number); // Parte entera del número
        $decimal_part = $number - $int_part; // Parte decimal del número

        //log_message('error', 'Unidad:' . $int_part );
        //log_message('error', 'Decimal:' . $decimal_part);

        $tolerance = 0.00001; // Pequeña tolerancia para comparación

        if ($decimal_part >= (0.6 - $tolerance)) {
            //log_message('error', 'T:' . ceil($number));
            return ceil($number); // Redondea hacia arriba
        } else {
            //log_message('error', 'F:' . floor($number));
            return floor($number); // Redondea hacia abajo
        }
    }


  private function checkVigenciaPuntos($id_cliente){
    $anio = $this->ModeloPuntos->getAnioCliente($id_cliente);
    
    log_message('error','anioC: '. $this->current_anio);
    log_message('error','anioN: '. $anio);

    if((int)$anio <= 2024){//Auxiliar para los puntos ya existentes refrente a el año 2024.
      log_message('error','AUX IF: ');
      $this->ModeloPuntos->auxPuntosY2024($id_cliente);
    }


    if ((int)$this->current_anio - (int)$anio >= 1) { //Si el año en base es menor al actual
      $this->ModeloPuntos->setAnioYPuntosCliente($id_cliente, $this->current_anio);
      $this->ModeloPuntos->reviewNegativePoints($id_cliente);

    }
  }

}
