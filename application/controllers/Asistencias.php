<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Asistencias extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloGeneral');
        date_default_timezone_set('America/Mexico_City');
        $this->date = date('Y-m-d h:i:s');
        $this->fecha = date('Y-m-d');
        $this->hora = date('H:i:s');
    }

    public function index()
    {
        $data['fecha'] = $this->fecha;
        $data['hora'] = $this->hora;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('asistencias/index', $data);
        $this->load->view('templates/footer');
        $this->load->view('asistencias/indexjs');
    }

    public function insertar()
    {
        $datos = $this->input->post();
        $id = $datos['id'];
        unset($datos['id']);
        $id_reg = 0;
        
        if ($id > 0) {
            $datosUp['reg_out'] = $this->date;
            $datosUp['check_out'] = 1;
            $datosUp['salida'] = $datos['salida'];
            $this->ModeloGeneral->updateCatalogo($datosUp, 'id', $id, 'asistencias');
            $id_reg = $id;
        } else {
            $datos['reg_in'] = $this->date;
            $id_reg = $this->ModeloGeneral->tabla_inserta('asistencias', $datos);
        }

        echo $id_reg;
    }

    public function getList_asistencias()
    {
        $id_emp = $this->input->post('id_emp');
        log_message('error','ID: '.$id_emp);
        $asistencias = $this->ModeloGeneral->getDataAsistencias($id_emp);
        $json_data = array("data" => $asistencias);
        echo json_encode($json_data);
    }

    function searchPersonal()
    {
        $cli = $this->input->get('search');
        $results = $this->ModeloGeneral->searchPersonal($cli);
        echo json_encode($results->result());
    }

    function checkEntradas()
    {
        $id =  $this->input->post('id');
        $exists = $this->ModeloGeneral->checkEntradas($id);
        if ($exists == "1"){
            echo "1";
        }else{
            echo "0";
        }
    }

    function checkInOutToday(){
        $id =  $this->input->post('id');
        $inOut = $this->ModeloGeneral->checkInOutToday($id,$this->fecha);

        if ($inOut != null){
            echo $inOut->check_out;
        }else{
            echo 0;
        }
    }

    function exportExcel($fIni, $fFin, $idP){
        $params["fIni"] = $fIni;
        $params["fFin"] = $fFin;
        $params["idP"] = $idP;

        $data["list"] = $this->ModeloGeneral->getAsistenciaLista($params);

        $this->load->view('asistencias/listaExcel',$data);
    }
}
