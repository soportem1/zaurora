<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Etiquetas extends CI_Controller {
	function __construct(){
        parent::__construct();
        //$this->load->model('Solicitud');
        $this->load->model('ModeloProductos');
        $this->load->model('ModeloGeneral');
        $logueo = $this->session->userdata();
        if (isset($logueo['perfilid_tz'])) {
            $perfilid=$logueo['perfilid_tz'];
            $this->sucursal=$logueo['idsucursal_tz'];
            $this->idpersonal=$logueo['idpersonal_tz'];
            if ($this->sucursal==0) {
                $this->sucursalid=1;
            }else{
                $this->sucursalid=$logueo['idsucursal_tz'];
            }
            $this->personal=$logueo['idpersonal_tz'];
            
        }else{
            $perfilid=0;
        }
    }
	public function index(){
		$id=$this->input->get('id');
		$data['getventasd']=$this->ModeloProductos->getproducto($id);
		$where_suc = array('idproducto' => $id,'idsucursal'=>$this->sucursalid);
        $data['datossuc']=$this->ModeloGeneral->getselectwheren('productos_sucursales',$where_suc);
        $data['idpersonal']=$this->idpersonal;
		$this->load->view('Reportes/etiqueta',$data);
	        
	}
}