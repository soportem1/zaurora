<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vacaciones extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloGeneral');
        date_default_timezone_set('America/Mexico_City');
        $this->date = date('Y-m-d h:i:s');
    }

	public function index(){
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('vacaciones/vacaciones');
        $this->load->view('templates/footer');
        $this->load->view('vacaciones/vacacionesjs');
	}

    public function insertarVacaciones(){
        $data = $this->input->post();
        $data["reg"]=$this->date;
        $idres=$this->ModeloGeneral->tabla_inserta("vacaciones_empleado",$data);
 
        $array = array("id_reg"=>$idres,
                        "tabla"=>'Vacaciones',
                        "modificacion"=>"inserta",
                        "campo_ant"=>'',
                        "id_producto"=>'0',
                        "id_usuario"=>$this->session->userdata('usuarioid_tz'),
                        "id_sucursal"=>$this->session->userdata('idsucursal_tz'),
                        'fecha'=>$this->date
        );
        $this->ModeloGeneral->log_movs('log_cambios',$array);
    }

    public function getData_vacaciones() {
        $id_emp = $this->input->post("id_emp");
        $vacaciones = $this->ModeloGeneral->getDataVacaciones($id_emp);
        $json_data = array("data" => $vacaciones);
        echo json_encode($json_data);
    }

    /* **************************************** */
    public function config(){
        $data['c']=$this->ModeloCatalogos->getselectwhereRow("config_descuentos");
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('config/descuentos',$data);
        $this->load->view('templates/footer');
        $this->load->view('config/descuentosjs');
    }

    public function insertarConfigDesc(){
        $data = $this->input->post();
        if($data["id"]==0){
            $idres=$this->ModeloGeneral->tabla_inserta("config_descuentos",$data);
        }else{
            $idres=$this->ModeloGeneral->updateCatalogon($data,array("id"=>$data["id"]),"config_descuentos");
        }
        
        $array = array("id_reg"=>$idres,
                        "tabla"=>'Config Descuentos',
                        "modificacion"=>"inserta",
                        "campo_ant"=>'',
                        "id_producto"=>'0',
                        "id_usuario"=>$this->session->userdata('usuarioid_tz'),
                        "id_sucursal"=>$this->session->userdata('idsucursal_tz'),
                        'fecha'=>$this->date
        );
        $this->ModeloGeneral->log_movs('log_cambios',$array);
    }
    /* ***************************************** */

    public function getConfigDescs() {
        $config = $this->ModeloCatalogos->getselectwhereRow("config_descuentos");
        echo json_encode($config);
    }

}