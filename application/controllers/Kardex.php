<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kardex extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloGeneral');
        $this->load->model('ModeloKardex');
        date_default_timezone_set('America/Mexico_City');
        $this->date = date('Y-m-d h:i:s');
        $this->fecha = date('Y-m-d');
        $this->hora = date('H:i:s');
    }

    public function index()
    {
        $data['sucursales'] = $this->ModeloGeneral->getselectwheren('sucursal',array('estatus'=>1));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('kardex/index',$data);
        $this->load->view('templates/footer');
        $this->load->view('kardex/indexjs');
    }

    public function get_list()
    {
        $params = $this->input->post();
        log_message("error", "PARAMS: " . json_encode($params));
        $getdata = $this->ModeloKardex->get_result($params);
        log_message('error','Query:'.$this->db->last_query());
        $totaldata = $this->ModeloKardex->total_result($params);

        $json_data = array(
            "draw"            => intval($params['draw']),
            "recordsTotal"    => intval($totaldata),
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata,
            "query"           => $this->db->last_query()
        );
        echo json_encode($json_data);
    }


    public function searchPersonal()
    {
        $usu = $this->input->get('search');
        $results = $this->ModeloKardex->personalAllSearch($usu);
        //log_message('error',"RESULTS: ".json_encode($results->result()));
        echo json_encode($results->result());
    }


    /*
    function exportExcel($fIni, $fFin, $idP){
        $params["fIni"] = $fIni;
        $params["fFin"] = $fFin;
        $params["idP"] = $idP;

        $data["list"] = $this->ModeloGeneral->getAsistenciaLista($params);

        $this->load->view('asistencias/listaExcel',$data);
    }
*/
}
