<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reporte extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloReportes');
    }

	public function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('Reportes/sesiones');
        $this->load->view('templates/footer');
        $this->load->view('Reportes/jssession');
	}

    public function getData_sessions() {

        $params = $this->input->post();
        $paramss=json_encode($params);

        $sesiones = $this->ModeloReportes->listado($params);
        $totalRecords=$this->ModeloReportes->filas();

        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $sesiones->result(),
            "query"           =>$this->db->last_query()   
        ); 
        echo json_encode($json_data);
  }

}
