<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Departamentos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloGeneral');
    }
	public function index(){
            /*
            $pages=10; //Número de registros mostrados por páginas
            $this->load->library('pagination'); //Cargamos la librería de paginación
            $config['base_url'] = base_url().'Proveedores/view/'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
            $config['total_rows'] = $this->ModeloProveedor->filas();//calcula el número de filas
            $config['per_page'] = $pages; //Número de registros mostrados por páginas  
            $config['num_links'] = 20; //Número de links mostrados en la paginación
            $config['first_link'] = 'Primera';//primer link
            $config['last_link'] = 'Última';//último link
            $config["uri_segment"] = 3;//el segmento de la paginación
            $config['next_link'] = 'Siguiente';//siguiente link
            $config['prev_link'] = 'Anterior';//anterior link
            $this->pagination->initialize($config); //inicializamos la paginación 
            $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["preveedor"] = $this->ModeloProveedor->total_paginados($pagex,$config['per_page']);
            */
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('catalogos/departamento');
            $this->load->view('templates/footer');
            $this->load->view('catalogos/departamentojs');
            //$this->load->view('/');
	}
    function depaadd(){

    }
    
    public function deletedepartamento(){
        
    }
    public function getDepartamentos() {
        $clientes = $this->ModeloGeneral->getselectwhere('departamento','estatus',1);
        $json_data = array("data" => $clientes);
        echo json_encode($json_data);
    }
    function add(){
        $iddepartamento = $this->input->post('iddepartamento');
        $nombre = $this->input->post('nombre');
        $data = array('nombre'=>$nombre);
        if ($iddepartamento>0) {
            $this->ModeloGeneral->updateCatalogo($data,'iddepartamento',$iddepartamento,'departamento');
            $tipo_mod = "modifica";
            $idresp = $iddepartamento;
        }else{
            $idresp = $this->ModeloGeneral->tabla_inserta('departamento',$data);
            $tipo_mod = "insertar";
        }
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d h:i:s \G\M\T');
        $array = array("id_reg"=>$idresp,
                        "tabla"=>'socios',
                        "modificacion"=>$tipo_mod,
                        "campo_ant"=>'',
                        "id_producto"=>'0',
                        "id_usuario"=>$this->session->userdata('usuarioid_tz'),
                        "id_sucursal"=>$this->session->userdata('idsucursal_tz'),
                        'fecha'=>$date
        );
        $this->ModeloGeneral->log_movs('log_cambios',$array);
    }
    function delete(){
        $iddepartamento = $this->input->post('iddepartamento');
        $data = array('estatus'=>0);
        $this->ModeloGeneral->updateCatalogo($data,'iddepartamento',$iddepartamento,'departamento');
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d h:i:s \G\M\T');
        $array = array("id_reg"=>$iddepartamento,
                        "tabla"=>'socios',
                        "modificacion"=>'elimina',
                        "campo_ant"=>'',
                        "id_producto"=>'0',
                        "id_usuario"=>$this->session->userdata('usuarioid_tz'),
                        "id_sucursal"=>$this->session->userdata('idsucursal_tz'),
                        'fecha'=>$date
        );
        $this->ModeloGeneral->log_movs('log_cambios',$array);
    }
    
    

       
    
}
