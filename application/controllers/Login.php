<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('ModeloSession');
        $this->load->helper('url');
    }
	public function index()
	{
            $this->load->view('login/header');
            $this->load->view('login/index');
           
            $this->load->view('login/footer');
            $this->load->view('login/pages-logintpl');
            
	}   
    public function session(){
        $usu = $this->input->post('usu');
        $pass = $this->input->post('passw');
        $respuesta = $this->ModeloSession->login($usu,$pass);
        //$respuesta;
        
        if($respuesta=='1'){ //no reconoce esta validacion en el servidor
            $this->insertLog();
        }
    }  

    public function verificaPass(){
        $usu = $this->input->post('usuario');
        $pass = $this->input->post('passAut');
        $respuesta = $this->ModeloSession->verificaPassDesc($usu,$pass);
        //$respuesta;
    }   

    public function insertLog() {
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d h:i:s \G\M\T');

        $data2 = array(
            'id_usuario' => $this->session->userdata('usuarioid_tz'),
            'fecha_ini' => $date,
            'fecha_fin'=>'',
            'id_sucursal'=> $this->session->userdata('idsucursal_tz'),
            'status' => 1
        );
        $this->ModeloSession->insertLogData($data2);
    }

    public function exitlogin() {
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d h:i:s \G\M\T');
        $this->ModeloSession->cerrarLogSess($this->session->userdata('usuarioid_tz'),$date);
        $this->load->view('login/exit');
    }


}
