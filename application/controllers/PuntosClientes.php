<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PuntosClientes extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloGeneral');
        $this->load->model('ModeloPuntos');
        date_default_timezone_set('America/Mexico_City');
        $this->date = date('Y-m-d h:i:s');
        $this->fecha = date('Y-m-d');
        $this->hora = date('H:i:s');
        $this->year = date('Y');
        $this->current_anio = date('Y');
    }

    public function index()
    {
        $data['fecha'] = $this->fecha;
        $data['hora'] = $this->hora;
        $this->load->view('puntosClientes/headerConsulta');
        //$this->load->view('templates/navbar');
        $this->load->view('puntosClientes/index', $data);
        $this->load->view('templates/footer');
        $this->load->view('puntosClientes/indexjs');
    }

    public function consulta_puntos($tel)
    {
        $puntos = 0;
        $url = '';

        
        log_message("error", "------------------------");

        $resultData = $this->ModeloPuntos->getDataCliente($tel);
        log_message('error','ResultD: '.json_encode($resultData));

        //Actualizar puntos
        log_message("error", "ACTUALIZAR PUNTOS");
        $this->checkVigenciaPuntos($resultData->ClientesId);
        log_message("error", "------------------------");
        

        $result = $this->ModeloPuntos->getDataCliente($tel);
        log_message('error','Result: '.json_encode($result));
        
        if($result){
            //log_message('error','Existe! '. $result->puntos);
            $url = 'consulta';
            if($result->anio_puntos >= $this->year){
                $puntos = $result->puntos;
            }
        }else{
            $url = 'not_found';
        }
        
            $data['puntos'] = number_format($puntos,2,".",",");
            $data['phone'] = $tel;
            $data['year'] = $this->year;

            $this->load->view('puntosClientes/headerConsulta');
            //$this->load->view('templates/navbar');
            $this->load->view('puntosClientes/'.$url, $data);
            $this->load->view('templates/footer');
            $this->load->view('puntosClientes/indexjs');
    }

    function searchCliente()
    {
        $key = 0;
        $tel = $this->input->post('num_tel');
        //log_message('error','Num_tel: '.json_encode($tel));
        $result = $this->ModeloPuntos->existsCliente($tel);
        //log_message('error','Result: '.json_encode($result));
        

        if($result){
            //log_message('error','Existe! ');
            $key = 1;
        }else{
            //log_message('error','NO!');
        }

        echo json_encode($key);
    }


    private function checkVigenciaPuntos($id_cliente)
    {
        $anio = $this->ModeloPuntos->getAnioCliente($id_cliente);

        log_message('error', 'anioC: ' . $this->current_anio);
        log_message('error', 'anioN: ' . $anio);

        if ((int)$anio <= 2024) { //Auxiliar para los puntos ya existentes refrente a el año 2024.
            log_message('error', 'AUX IF: ');
            $this->ModeloPuntos->auxPuntosY2024($id_cliente);
        }


        if ((int)$this->current_anio - (int)$anio >= 1) { //Si el año en base es menor al actual
            $this->ModeloPuntos->setAnioYPuntosCliente($id_cliente, $this->current_anio);
            $this->ModeloPuntos->reviewNegativePoints($id_cliente);
        }
    }

}
