<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ConfigPuntos extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloSucursal');

        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        //==============================================================
        $logueo = $this->session->userdata();
        if (isset($logueo['perfilid_tz'])) {
            $perfilid = $logueo['perfilid_tz'];
            $this->sucursal = $logueo['idsucursal_tz'];
            $this->idpersonal = $this->session->userdata('idpersonal_tz');
            if ($this->sucursal == 0) {
                $this->sucursalid = 1;
            } else {
                $this->sucursalid = $logueo['idsucursal_tz'];
            }
            $this->personal = $logueo['idpersonal_tz'];
        } else {
            $perfilid = 0;
        }
    }

    public function index()
    {
        $dataConf = $this->ModeloCatalogos->getDataConfigPuntos();
        //log_message('error','DATA: '.json_encode($dataConf));
        
        $data['porcentaje'] = 5;
        $data['checkFecha'] = 0;
        $data['fInicio'] = '';
        $data['fFinal'] = '';

        if($dataConf){
            $data['porcentaje'] = $dataConf->porcentaje;
            $data['checkFecha'] = $dataConf->checkFecha;
            $data['fInicio'] = $dataConf->fechaIni;
            $data['fFinal'] = $dataConf->fechaFin;
        }

        if ($data['porcentaje'] < 0 || $data['porcentaje'] > 100) {
            $data['porcentaje'] = 5;
        }

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('config/puntos', $data);
        $this->load->view('templates/footer');
        $this->load->view('config/jsPuntos');
    }

    function updateConfigPuntos()
    {
        $porcentaje = $this->input->post('porcentaje');
        $sinFinal = $this->input->post('sinFinal');
        $fInicio = $this->input->post('fInicio');
        $fFinal = $this->input->post('fFinal');
        $personal = $this->idpersonal;
        $reg = $this->fechahoy;

        if ($sinFinal) {
            $fFinal = '';
        }

        $data = array(
            'porcentaje'=>$porcentaje,
            'checkFecha'=>$sinFinal,
            'fechaIni'=>$fInicio,
            'fechaFin'=>$fFinal,
            'id_personal'=>$personal,
            'reg'=>$reg
        );

        $id = $this->ModeloCatalogos->configPuntosUpdate($data);
        echo $id;
    }
}
