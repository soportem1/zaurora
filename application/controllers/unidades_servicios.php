<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class unidades_servicios extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloSession');
        $this->load->model('ModeloUnidadesservicios');
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal_tz');
            $permiso=$this->ModeloSession->getviewpermiso($this->idpersonal,23);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Sistema');
            }
        }else{
            redirect('/Sistema');
        }
    }
	public function index(){
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('config/unidades_servicios');
        $this->load->view('templates/footer');
        $this->load->view('config/unidades_serviciosjs');
	}
    public function getlistservicios() {
        $params = $this->input->post();
        $getdata = $this->ModeloUnidadesservicios->getservicios($params);
        $totaldata= $this->ModeloUnidadesservicios->total_servicios($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getlistunidades() {
        $params = $this->input->post();
        $getdata = $this->ModeloUnidadesservicios->getunidades($params);
        $totaldata= $this->ModeloUnidadesservicios->total_unidades($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function adservicio(){
        $params = $this->input->post();
        $this->ModeloCatalogos->updateCatalogo('f_servicios',array('activo'=>$params['activo']),array('ServiciosId'=>$params['ServiciosId']));
    }
    function adunidad(){
        $params = $this->input->post();
        $this->ModeloCatalogos->updateCatalogo('f_unidades',array('activo'=>$params['activo']),array('UnidadId'=>$params['UnidadId']));
    }
    
    

}