<div class="row">
  <div class="col-md-12">
    <h2>Compras</h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title">Listado de Compras</h4>
          </div>
          <div class="card-body">
              <div class="card-block">
                  <!--------//////////////-------->
                  <div class="col-md-3">
                    <input type="date" id="productoscin" class="form-control">
                  </div>
                  <div class="col-md-3">
                    <input type="date" id="productoscfin" class="form-control">
                  </div>
                  <div class="col-md-3">
                    <a class="btn btn-raised gradient-purple-bliss white" id="buscarlproducto">Consultar</a>
                  </div>
                  <table class="table table-striped" id="data-tables">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Fecha</th>
                        <th>Producto</th>
                        <th>Proveedor</th>
                        <th>Cantidad</th>
                        <th>Precio</th>
                      </tr>
                    </thead>
                    <tbody class="tbody_lcompras">
                      <?php foreach ($lcompras->result() as $item){ ?>
                       <tr id="trcli_<?php echo $item->id_detalle_compra; ?>">
                          <td><?php echo $item->id_detalle_compra; ?></td>
                          <td><?php echo $item->reg; ?></td>
                          <td><?php echo $item->producto; ?></td>
                          <td><?php echo $item->razon_social; ?></td>
                          <td><?php echo $item->cantidad; ?></td>
                          <td><?php echo $item->precio_compra; ?></td>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                  <div class="col-md-12">
                    <div class="col-md-9">
                    </div>
                    <div class="col-md-3">
                      <?php echo $this->pagination->create_links() ?>
                    </div>
                  </div>
          <!--------//////////////-------->
              </div>
          </div>
      </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#buscarlproducto').click(function(){
        params = {};
        params.fechain = $('#productoscin').val();
        params.fechafin = $('#productoscfin').val();
        $.ajax({
            type:'POST',
            url: 'Listacompras/consultar',
            data: params,
            async: false,
            success:function(data){
              $('.tbody_lcompras').html('');
              $('.tbody_lcompras').html(data);
            }
        });
    });
});
</script>