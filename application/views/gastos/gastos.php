            <div class="row">
                <div class="col-md-12">
                  <h2>Gastos</h2>
                </div>
                
                <div class="col-md-12">
                  <div class="col-md-11"></div>
                  <div class="col-md-1">
                    <a href="<?php echo base_url(); ?>Gastos/Gastoadd" class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow"><i class="fa fa-plus"></i></span> Nuevo</a>
                  </div>
                </div>
                
                
              </div>
              <!--Statistics cards Ends-->

              <!--Line with Area Chart 1 Starts-->
              <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Listado de Gastos</h4>
                        </div>
                        <div class="col-md-12">
                          <div class="col-md-8">
                            
                          </div>
                          <div class="col-md-3">
                              <!--<form role="search" class="navbar-form navbar-right mt-1">
                                <div class="position-relative has-icon-right">
                                  <input type="text" placeholder="Buscar" id="buscargasto" class="form-control round" oninput="buscarGasto();">
                                  <div class="form-control-position"><i class="ft-search"></i></div>
                                </div>
                              </form>-->
                          </div>
                        </div>
                        <div class="card-body">
                            <div class="card-block">
                                <!--------//////////////-------->
                                <table class="table table-striped" id="data-tables" style="width: 100%">
                                      <thead>
                                        <tr>
                                          <th>Concepto</th>
                                          <th>Monto</th>
                                          <th>Sucursal</th>
                                          <th>Fecha</th>
                                          <th>Usuario</th>
                                          <!--<th>Empleado</th>-->
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody id="tbodyresultadosgastos">

                                        <?php /*foreach ($gastos->result() as $item){ ?>
                                         <tr id="trcli_<?php echo $item->id; ?>">
                                                  <td><?php echo $item->concepto; ?></td>
                                                  <td>$ <?php echo $item->monto; ?></td>
                                                  <td><?php echo $item->fecha; ?></td>
                                                  <td><?php echo $item->Usuario; ?></td>
                                                  <td><?php echo $item->sucursal; ?></td>
                                                  <td>
                                                      <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>
                                                      <button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                          <span class="sr-only">Toggle Dropdown</span>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                          <a class="dropdown-item" onclick="gastodelete(<?php echo $item->id; ?>);"href="#">Eliminar</a>
                                                      </div>
                                                  </div>                                                    
                                                  </td>
                                             
                                                </tr>
                                          
                                        <?php }*/ ?>
                                            
                                      </tbody>
                                    </table>
                                    <table class="table table-striped" id="data-tables2" style="display: none; width: 100%">
                                      <thead>
                                        <tr>
                                          <th>Concepto</th>
                                          <th>Monto</th>
                                          <th>Fecha</th>
                                          <th>Usuario</th>
                                          <th>Sucursal</th>
                                        </tr>
                                      </thead>
                                      <tbody id="tbodyresultadosgastos2">
                                    </tbody>
                                    </table>
                                    <div class="col-md-12">
                                      <div class="col-md-7">
                                        
                                      </div>
                                      <div class="col-md-5">
  
                                      
                                    </div>
                                    
                                    
                        <!--------//////////////-------->
                            </div>
                        </div>
                    </div>
                </div>
              </div>
<!------------------------------------------------>
