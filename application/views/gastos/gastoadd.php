<?php 
if (isset($_GET['id'])) {
    $id=$_GET['id'];
    $gastosv=$this->ModeloGastos->getGasto($id);
    foreach ($gastosv->result() as $item){
      $label='Editar Gasto';
      $id = $item->id;
      $monto = $item->monto;
      $concepto = $item->concepto;
      $fecha = $item->fecha;
      $id_usuario = $item->id_usuario;
      $id_sucursal = $item->id_sucursal;
  }
}else{
  $label='Nuevo Gasto';
  $id = 0;
  $monto = '';
  $concepto = '';
  $fecha = '';
  $id_usuario = '';
  $id_sucursal = ''; 
} 
?>
<style type="text/css">
  .vd_red{
    font-weight: bold;
    color: red;
  }
  .vd_green{
    color: #009688; 
  }
  .controls{
    margin-bottom: 10px;
  }
</style>
<input type="hidden" name="id" id="id" value="<?php echo $id;?>">
<div class="row">
  <div class="col-md-12">
    <h2>Gasto</h2>
  </div>
</div>
              <!--Statistics cards Ends-->
              <!--Line with Area Chart 1 Starts-->
              <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title"><?php echo $label;?></h4>
                        </div>
                        <div class="card-body">
                            <div class="card-block form-horizontal">
                                <!--------//////////////-------->
                                <div class="row">
                                  <form method="post"  role="form" id="formgasto" class="form-horizontal">
                                    <div class="col-md-12">
                                      <div class="col-md-12">
                                        <h3>Datos generales</h3>
                                        <hr />
                                      </div>
                                      <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Concepto:</label>
                                            <div class="col-sm-8 controls">
                                              <input type="text" name="concepto" class="form-control" id="concepto" value="<?php echo $concepto;?>">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Monto:</label>
                                            <div class="col-sm-8 controls">
                                              <input type="text" name="monto" id="monto" class="form-control" value="<?php echo $monto;?>">
                                          </div>
                                        </div>
                                      </div>
                                    </div> 
                                  </form>
                                  <div class="col-md-12">
                                    <button class="btn btn-warning white shadow-z-1-hover" id="savegasto">Guardar</button>
                                  </div>
                                </div>

                                <!--------//////////////-------->
                            </div>
                        </div>
                    </div>
                </div>
              </div>