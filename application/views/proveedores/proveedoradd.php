<?php 
if (isset($_GET['id'])) {
    $id=$_GET['id'];
    $proveedorv=$this->ModeloProveedor->getproveedor($id);
    foreach ($proveedorv->result() as $item){
      $label='Editar Proveedor';
      $id = $item->id_proveedor;
      $razon_social = $item->razon_social;
      $domicilio = $item->domicilio;
      $ciudad = $item->ciudad;
      $cp = $item->cp;
      $id_estado = $item->id_estado;
      $telefono_local = $item->telefono_local;
      $telefono_celular = $item->telefono_celular;
      $contacto = $item->contacto;
      $email_contacto = $item->email_contacto;
      $rfc = $item->rfc;
      $fax = $item->fax;
      $obser = $item->obser;
  }
}else{
  $label='Nuevo Proveedor';
  $id = 0;
  $razon_social = '';
  $domicilio = '';
  $ciudad = '';
  $cp = '';
  $id_estado = '';
  $telefono_local = '';
  $telefono_celular = '';
  $contacto = '';
  $email_contacto = '';
  $rfc = '';
  $fax = '';
  $obser = '';   
} 
?>
<style type="text/css">
  .vd_red{
    font-weight: bold;
    color: red;
  }
  .vd_green{
    color: #009688; 
  }
  .controls{
    margin-bottom: 10px;
  }
</style>
<input type="hidden" name="proveedorid" id="proveedorid" value="<?php echo $id;?>">
<div class="row">
  <div class="col-md-12">
    <h2>Proveedor</h2>
  </div>
</div>
              <!--Statistics cards Ends-->
              <!--Line with Area Chart 1 Starts-->
              <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title"><?php echo $label;?></h4>
                        </div>
                        <div class="card-body">
                            <div class="card-block form-horizontal">
                                <!--------//////////////-------->
                                <div class="row">
                                  <form method="post"  role="form" id="formproveedor" class="form-horizontal">
                                    <div class="col-md-12">
                                      <div class="col-md-12">
                                        <h3>Datos generales</h3>
                                        <hr />
                                      </div>
                                      <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Raz&oacute;n social:</label>
                                            <div class="col-sm-8 controls">
                                              <input type="text" name="txtRazonSocial" class="form-control" id="txtRazonSocial" value="<?php echo $razon_social;?>">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Domicilio:</label>
                                            <div class="col-sm-8 controls">
                                              <input type="text" name="txtDomicilio" id="txtDomicilio" class="form-control" value="<?php echo $domicilio;?>">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <label class="col-sm-3 control-label">Ciudad:</label>
                                          <div class="col-sm-3 controls">
                                              <input type="text" id="txtCiudad" name="txtCiudad" class="form-control" value="<?php echo $ciudad;?>" />
                                          </div>
                                          <label class="col-sm-2 control-label">Estado:</label>
                                          <div class="col-sm-3 controls">
                                            <select id="cmbEstado" name="cmbEstado"class="form-control">
                                              <?php foreach($Estados->result() as $fila): ?>
                                              <option value="<?php echo $fila->EstadoId; ?>" <?php if ($fila->EstadoId==$id_estado) { echo 'selected';}?> ><?php echo $fila->Nombre; ?></option>
                                              <?php endforeach; ?>
                                            </select>
                                         </div>
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <label class="col-sm-3 control-label">C&oacute;digo postal:</label>
                                          <div class="col-sm-3 controls">
                                              <input type="text" id="txtCP" name="txtCP" value="<?php echo $cp;?>" class="form-control"/>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <label class="col-sm-3 control-label">Observaciones:</label>
                                          <div class="col-sm-3 controls">
                                            <textarea id="txtobservacion" name="txtobservacion" class="form-control"><?php echo $obser;?></textarea>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                        <h3>Datos de contacto</h3>
                                        <hr />
                                      </div>
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <label class="col-sm-3 control-label">Nombre del contacto:</label>
                                          <div class="col-sm-8 controls">
                                          <input type="text" name="txtContacto" id="txtContacto" value="<?php echo $contacto;?>"class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <label class="col-sm-3 control-label">E-mail:</label>
                                          <div class="col-sm-3 controls">
                                            <input type="text" id="txtEmail" name="txtEmail" value="<?php echo $email_contacto;?>" class="form-control"/>
                                          </div>
                                          <label class="col-sm-2 control-label">RFC:</label>
                                          <div class="col-sm-3 controls">
                                            <input type="text" id="txtRFC" name="txtRFC" value="<?php echo $rfc;?>" class="form-control"/>
                                         </div>
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <label class="col-sm-3 control-label">Telefono Local:</label>
                                          <div class="col-sm-3 controls">
                                            <input type="text" id="txtTL" name="txtTL" value="<?php echo $telefono_local;?>" class="form-control"/>
                                          </div>
                                          <label class="col-sm-2 control-label">Telefono celular:</label>
                                          <div class="col-sm-3 controls">
                                            <input type="text" id="txtTC" name="txtTC" value="<?php echo $telefono_celular;?>" class="form-control"/>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <label class="col-sm-3 control-label">Fax:</label>
                                          <div class="col-sm-3 controls">
                                            <input type="text" id="txtFax" name="txtFax" value="<?php echo $fax;?>" class="form-control"/>
                                          </div>
                                        </div>
                                      </div>
                                    </div> 
                                  </form>
                                  <div class="col-md-12">
                                    <button class="btn btn-warning white shadow-z-1-hover" id="saveprov">Guardar</button>
                                  </div>
                                </div>





                                
                                <!--------//////////////-------->
                            </div>
                        </div>
                    </div>
                </div>
              </div>