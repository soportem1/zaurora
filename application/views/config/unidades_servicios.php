<style type="text/css">
    .card-header{
        border-bottom: 3px solid #f5f7fa !important;
    }
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/print.css" media="print">
<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/print.css" media="print">-->
<div class="row">
    <div class="col-md-12">
      <h2>Unidades / servicios</h2>
    </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
    <div class="col-sm-12">
        <div id="accordionWrap1" role="tablist" aria-multiselectable="true">
            <div class="card collapse-icon accordion-icon-rotate" >
                <div id="heading11" class="card-header">
                    <a data-toggle="collapse" data-parent="#accordionWrap1" href="#accordion11" aria-expanded="false" aria-controls="accordion11" class="card-title lead collapsed">Servicios</a>
                </div>
                <div id="accordion11" role="tabpanel" aria-labelledby="heading11" class="collapse" style="">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="tabla_servicio"  class="table display" cellspacing="0" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>Clave</th>
                                                <th>Concepto</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                     </div>
                </div>
                <div id="heading12" class="card-header">
                    <a data-toggle="collapse" data-parent="#accordionWrap1" href="#accordion12" aria-expanded="false" aria-controls="accordion12" class="card-title lead collapsed">Unidades</a>
                </div>
                <div id="accordion12" role="tabpanel" aria-labelledby="heading12" class="collapse" aria-expanded="false">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="tabla_unidades"  class="table display" cellspacing="0" style="width: 100%">
                                        <thead>
                                            <tr>
                                              <th>Clave</th>
                                              <th>Concepto</th>
                                              <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
          
            </div>
        </div>
    </div>
</div>

