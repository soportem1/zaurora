<?php
/*
  foreach ($configticket->result() as $item){
    $id_ticket = $item->id_ticket;
    $titulo = $item->titulo;
    $mensaje = $item->mensaje;
    $mensaje2 = $item->mensaje2;
    $fuente = $item->fuente;
    $tamano = $item->tamano;
    $margensup = $item->margensup;
    $id_sucursal = $item->id_sucursal;
  } 
  */
?>
<div class="row">
  <div class="col-md-12">
    <h2>Puntos Aurora</h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
    <div class="card">

      <form method="post" role="form" id="formConfig">
        <div class="card-header">
          <h4 class="card-title">Configuración</h4>
        </div>
        <div class="card-body">
          <div class="card-block">
            <!--------//////////////-------->
            
            <div class="row">
              <div class="col-md-12 form-group">

                <label class="control-label col-md-1">Porcentaje:</label>
                <div class="col-md-3">
                  <input type="number" id="porcentaje" name="porcentaje" class="form-control date-picker" value="<?php echo $porcentaje; ?>">
                </div>

                <div class="col-md-2">
                  <div class="checkbox-list">
                    <label>Sin fecha final: &ensp; <input type="checkbox" id="chkFecha" name="checkFecha" <?php echo $checkFecha == 1 ? "checked" : "" ; ?> value="1"></label>
                  </div>
                </div>

              </div>

              <div class="col-md-12 form-group">
                <label class="control-label col-md-1">Desde:</label>
                <div class="col-md-3">
                  <?php
                    $today = date('Y-m-d');
                    echo '<input id="fechaIni" name="fechaIni" class="form-control date-picker"  size="16" type="date" value="'.$fInicio.'" />';
                  ?>
                </div>

                <label class="control-label col-md-1">Hasta:</label>
                <div class="col-md-3">
                  <?php
                    $today = date('Y-m-d');
                    echo '<input id="fechaFin" name="fechaFin" class="form-control date-picker"  size="16" type="date" value="'.$fFinal.'" '.($checkFecha == 1 ? "disabled":"").' />';
                  ?>
                </div>

              </div>
            </div>
            <!--------//////////////-------->
          </div>
        </div>
      </form>
      <div class="col-md-12 form-group">
        <br>
        <button class="btn btn-raised gradient-purple-bliss white" id="guardar"><i class="icon-ok"></i> Guardar</button>
      </div>
    </div>
  </div>

</div>