<div class="row">
    <div class="col-md-12">
      <h2>Descuentos por ausencia </h2>
    </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Configuración: Ingresar % de descuento de acuerdo al día </h4>
            </div>
            <div class="card-body">
                <div class="card-block">
                    <!--------//////////////-------->
                    <div class="row">
                        <form method="post"  role="form" id="formconfig">
                            <input type="hidden" name="id" value="<?php if(isset($c)) echo $c->id; else echo '0' ?>">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">% Día enfermo:</label>
                                    <div class="col-sm-6 controls">
                                      <input type="text" id="dia_enfermo" name="dia_enfermo" value="<?php if(isset($c)) echo $c->dia_enfermo;?>" class="form-control" placeholder="% de descuento por día enfermo" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <br>
                                    <label class="col-sm-2 control-label">% Día descuento:</label>
                                    <div class="col-sm-6 controls">
                                      <input type="text" id="dia_desc" name="dia_desc" value="<?php if(isset($c)) echo $c->dia_desc;?>" class="form-control" placeholder="% de descuento por día descontado"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <br>
                                    <label class="col-sm-2 control-label">% Día suspendido:</label>
                                    <div class="col-sm-6 controls">
                                      <input type="text" id="dia_suspende" name="dia_suspende" value="<?php if(isset($c)) echo $c->dia_suspende;?>" class="form-control" placeholder="% de descuento por día enfermo" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                              <br>
                              <br>
                              <button type="submit" class="btn btn-raised gradient-purple-bliss white" id="guardar" ><i class="fa fa-save"></i> Guardar</button>
                            </div>
                        </form>
                        
                    </div>
                    
                    <!--------//////////////-------->
                </div>
            </div>
        </div>
    </div>
</div>
