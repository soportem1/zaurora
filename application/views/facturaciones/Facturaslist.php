<style type="text/css">
  table td{
    font-size: 12px;
    padding: 6px 7px;
  }
  .btns-factura{
    min-width: 147px;
  }
  .iframepdf{
    text-align: center;
  }
  .iframepdf_doc{
    min-height: 239px;
    border: 0px;
  }
</style>
<div class="row">
  <div class="col-md-12">
    <h2>Facturación</h2>
  </div>
  
  <div class="col-md-12">
    <div class="col-md-11"></div>
  </div>
  
  
</div>
<!--Statistics cards Ends-->

<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
          <h4 class="card-title">Listado de Facturas</h4>
      </div>
      <div class="col-md-12">
        <div class="col-md-1">
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label for="">Cliente</label> 
            <select id="idcliente" class="form-control" onchange="loadtable()">
            </select>
          </div>  
        </div> 
        <div class="col-md-3">
          <label>Fecha Factura:</label>
          <input id="fecha_fact" class="form-control date-picker" type="date" onchange="loadtable()" />
        </div>
        <!--<a class="btn btn-raised gradient-back-to-earth white sidebar-shadow" href="<?php echo base_url();?>Facturaslis/add">Nuevo</a>-->
        <div class="col-md-3"></div>
        <div class="col-md-1">
          <label style="color: transparent;">cancelar</label>
          <a class=" pull-right btn btn-danger shadow-z-1 white" onclick="cancelarfacturas()">Cancelar</a><br>
        </div>
      </div>
      <div class="col-md-12">
        <br>
      </div>
      <div class="card-body">
        <div class="card-block">
          <table id="tabla_facturacion" name="tabla_facturacion_cliente" class="table table-striped" style="width: 100%">
            <thead>
              <tr>
                <th></th>
                <th>Folio</th>
                <th>Cliente</th>
                <th>RFC</th>
                <th>Monto</th>
                <th>Estatus</th><!--sin timbrar timbrada cancelada-->
                <th>Fecha</th>
                <th>Emitió</th>
                <th></th>
                
              </tr>
            </thead>
            <tbody>
            </tbody>
      </table>    
        </div>
      </div>
  </div>
</div>
</div>

<!--<div id="modalcomplementos" class="modal" >
  <div class="modal-content" style="padding: 5px">
    <div class="row">
      <div class="col s12">
        <a class="waves-effect green btn-bmz" onclick="addcomplemento()" >Nuevo</a>
      </div>
    </div>
    <div class="row">
      <div class="col s12 listadocomplementos">
      </div>
    </div>
  </div>  
  <div class="modal-footer">
    <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
  </div>
</div>-->

<div class="modal fade" id="modalcomplementos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">COMPLEMENTOS DE PAGO</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col s12">
            <a class="btn btn-success" onclick="addcomplemento()" >Nuevo</a>
          </div>
        </div>
        <div class="row">
          <div class="col s12 listadocomplementos">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalenvio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel-2">Envio Email</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="form_envio">
        <div class="row">
          <div class="col-md-2">
            <label>Correo</label>
          </div>
          <div class="col-md-10 form-group">
            <input type="email" name="f_email" id="f_email" class="form-control" required>
            <input type="hidden" name="f_id" id="f_id" class="form-control" required>
          </div>
        </div>
        </form>
        <div class="row">
          <div class="col-md-6 iframepdf">
            <iframe src=""></iframe>
          </div>
          <div class="col-md-6 iframexml">
            
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn gradient_nepal2" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success" onclick="enviocorreo()">Enviar</button>
      </div>
    </div>
  </div>
</div>