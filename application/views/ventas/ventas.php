<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugin/sweealert/sweetalert.css">
<div class="row">
    <div class="col-md-12">
      <h2>Ventas </h2>
    </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Ventas de mostrador</h4>
            <h3> Cajero: <?php echo $_SESSION['usuario_tz']; ?> </h3>
        </div>
        <div class="card-body">
            <div class="card-block">
                <!--------//////////////-------->
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <input type="checkbox" name="checkimprimir" id="checkimprimir" checked> <label for="checkimprimir"> Imprimir Ticket</label>
                        </div>
                        <div class="col-md-7">
                            <input type="checkbox" name="factura" id="factura" > <label for="factura"> ¿Se Factura? </label>
                        </div>
                        <div class="col-md-3">
                            <button class="btn btn-raised gradient-green-tea white sidebar-shadow btn_modal_gastos" title="GASTOS" ><i class="fa fa-book"></i></button>
                            <a href="<?php echo base_url(); ?>Ticket" class="btn btn-raised gradient-green-tea white sidebar-shadow button_reimprimir" target="_blank" title="TICKET"><i class="fa fa-print"></i></a>
                            <button class="btn btn-raised gradient-green-tea white sidebar-shadow modalchecador" title="CHECADOR">Checador</button>
                        </div>
                        <div class="col-md-9">
                          <div class="form-group">
                            <label class="col-md-2 control-label">Código:</label>
                            <div class=" col-md-3">
                              <input type="text" class="form-control" id="codigo" aria-describedby="button-addon2">
                            </div>
                            <div class="col-md-1">
                              <a href="javascript:void(0)" type="hidden"></a>
                            </div>
                            <div class="col-md-2">
                              <a href="#" class="btn btn-warning white" id="generacodigo"><i class="fa fa-barcode" aria-hidden="true"></i> Generar</a>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Vendedor:</label>
                                <div class=" col-md-10">
                                    <select class="form-control" id="vvendedores" name="vvendedores">
                                        <?php  
                                            foreach ($vendedores as $item){ 
                                            echo '<option value="'.$item->personalId.'">'.$item->nombre.' '.$item->apellidos.'</option>';
                                            } 
                                        ?>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <br>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Cliente:</label>
                                <div class=" col-md-8">
                                    <select class="form-control" id="vcliente" name="vcliente">
                                        <?php /*foreach ($clientedefault->result() as $item){ 
                                            echo '<option value="'.$item->ClientesId.'">'.$item->Nom.'</option>';
                                        } */
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <a href="<?php echo base_url()?>Clientes/clientesadd" class="btn btn-warning white"><i class="fa fa-user" aria-hidden="true"></i> Nuevo Cliente</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <br>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class=" col-md-4">
                
                                </div>
                                <label class="col-md-3 control-label"><i class="fa fa-search"></i> Producto/Código:</label>
                                <div class=" col-md-5">
                                    <select class="form-control" id="vproducto" name="vproducto"></select>
                                </div>
                                <div class="col-md-12">
                                    <br>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Cantidad:</label>
                                <div class=" col-md-2">
                                    <input type="number" name="vcantidad" id="vcantidad" value="1" class="form-control" min="0">
                                </div>
                                <label class="col-md-3 control-label"><i class="fa fa-barcode"></i> Código:</label>
                                <div class=" col-md-5">
                                    <input type="text" class="form-control" id="vproducto2" placeholder="Lector de Código">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <br>
                        </div>
                        <div class="col-md-12">
                            <table class="table table-hover" id="productosv">
                                <thead>
                                    <tr>
                                        <th>Clave</th>
                                        <th>Cantidad</th>
                                        <th>Producto</th>
                                        <th>Vendedor</th>
                                        <th>Precio</th>
                                        <th>Total</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="class_productos">
                                    
                                </tbody>
                                </table>
                            
                        </div>
                        <div class="col-md-12">
                            <a href="#" class="btn btn-raised gradient-green-tea white sidebar-shadow" onclick="addproducto()">Agregar producto</a>
                            <a href="#" class="btn btn-raised gradient-blackberry white sidebar-shadow" onclick="limpiar()">Limpiar</a>
                            
                        </div>  
                    </div>
                    <div class="col-md-3">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Método de pago:</label>
                                <select class="form-control" id="mpago" name="mpago" onchange="fillOutCard()">
                                    <option value="1">Efectivo</option>
                                    <option value="2">Tarjeta de crédito</option>
                                    <option value="3">Tarjeta de débito</option>
                                    <option value="4">Pago mixto</option>
                                    <option value="5">Puntos Aurora</option>
                                    <option value="6">Crédito</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Descuento:</label>
                                <select class="form-control" id="mdescuento" name="mdescuento" onchange="calculartotal()">
                                    <option value="0">0 %</option>
                                    <option value="0.05">5 %</option>
                                    <option value="0.07">7 %</option>
                                    <option value="0.15">15 %</option>
                                    <option value="0.2">20 %</option>
                                    <option value="0.3">30 %</option>
                                </select>
                                <input type="hidden" name="cantdescuento" id="cantdescuento" readonly>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                            <div class="form-group" id="cont_rest" style="display: none;">
                                <label class="control-label">Restante: <b><span id="rest">($00.00)</span></b></label>
                                <hr>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Ingreso Efectivo:</label>
                                <input type="number" name="vingreso" id="vingreso" value="0" class="form-control" oninput="ingreso()">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Ingreso Tarjeta:</label>
                                <input type="number" name="vingresot" id="vingresot" value="0" class="form-control" oninput="ingreso()" readonly>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Ingreso Puntos: <b><span id="labelPuntos"></span></b> </label>
                                <input type="number" name="vingresop" id="vingresop" value="0" class="form-control" oninput="ingreso()" readonly>
                                <input type="hidden" id="currentP" class="form-control" value="0" readonly>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Total:</label>
                                <input type="number" name="vtotal" id="vtotal" class="form-control" readonly>
                                <input type="hidden" name="vsbtotal" id="vsbtotal" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Cambio:</label>
                                <input type="number" name="vcambio" id="vcambio" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <a href="#" class="btn btn-raised gradient-flickr white sidebar-shadow">Cancelar</a>
                            <a href="#" class="btn btn-raised gradient-green-tea white sidebar-shadow" id="ingresaventa">Ingresar venta</a>
                        </div>
                    </div>
                    
                    
                </div>
        <!--------//////////////-------->
            </div>
        </div>
    </div>
</div>
</div>
<style type="text/css">
    #iframereporte{
        background: white;
    }
    iframe{
        height: 500px;
        border:0;
        width: 100%;
    }
</style>
<div class="modal fade text-left" id="iframeri" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Ticket</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--<div class="modal-body">-->
            <div id="iframereporte"></div>
            <!--</div>-->
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="modalchecador" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Checador de precios</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
               <div class="row">
                   <label class="col-md-2">Código</label>
                   <div class="col-md-10">
                       <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-barcode"></i> </span>
                            <input type="text" class="form-control" id="inputchecador" >
                        </div>
                   </div>
               </div>
               <div class="row">
                   <div class="col-md-12 infochecador">
                       
                   </div>
               </div> 
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="modalPass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Autorización de Movimiento</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
               <div class="row">
                   <label class="col-md-2">Password</label>
                   <div class="col-md-10">
                       <div class="input-group">
                            <span class="input-group-addon" id="ppp"><i class="fa fa-key"></i> </span>
                            <input type="text" name="passAD" class="" id="passAD" value="">
                        </div>
                   </div>
               </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="envPassAD" class="btn grey btn-outline-secondary">Aceptar</button>
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="modalturno" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="false" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Nuevo Turno</h4>
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>-->
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Abrir Turno:</label>
                            <div class="col-sm-8 controls">
                                <input type="number"class="input-border-btm form-control" id="cantidadt" name="cantidadt" style="text-transform:uppercase;"  placeholder="$"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"> Nombre del Turno</label>
                            <div class="col-sm-8 controls">
                                <input type="text" class="input-border-btm form-control" id="nombredelturno" name="nombredelturno" style="text-transform:uppercase;" > 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal" id="btnabrirt">Abrir turno</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="modalgastos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Gastos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Concepto:</label>
                            <div class="col-sm-8 controls">
                              <input type="text" name="concepto" class="form-control" id="concepto" value="">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Monto:</label>
                            <div class="col-sm-8 controls">
                              <input type="number" name="monto" id="monto" class="form-control" value="">
                          </div>
                        </div>
                      </div>
                </div>
               

            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn grey btn-outline-secondary guardargasto" data-dismiss="modal">Agregar Gasto</button>
            </div>
        </div>
    </div>
</div>
<?php if ($sturno=='cerrado') { ?>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#modalturno').modal();
        });
    </script>
 <?php } ?>
