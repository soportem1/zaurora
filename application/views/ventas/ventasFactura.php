<style type="text/css">
  .switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #061437;
}

input:focus + .slider {
  box-shadow: 0 0 1px #061437;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
<!--<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.2.1/dist/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">-->

<div class="row">
    <div class="col-md-12">
      <h2>Facturar </h2>
    </div>
</div>
<input type="hidden" id="mesactual" value="<?php echo date('m');?>"> 
<input type="hidden" id="anioactual" value="<?php echo date('Y');?>"> 
<input type="hidden" id="id_venta" value="<?php echo $id_venta; ?>">
<div class="row">
  <!-- FORMULARIO  <?php ?>-->
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Datos de Factura</h4>
        </div>
        <div class="card-body">
          <div class="card-block form-horizontal">
            <div class="row"> 
              <form id="validateSubmitForm" role="form" method="post" autocomplete="off" required> 
               
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="">Cliente</label> <br>
                    <select id="idcliente" name="idcliente" class="form-control" value="<?php echo $fact->id_cliente;?>">
                      <?php if($fact->id_cliente!="") echo "<option value=".$fact->id_cliente.">".$fact->cliente_name."</option>" ?>
                    </select>
                  </div>  
                </div> 
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="">RFC</label> <!--Llenar -->
                    <input class="form-control" id="rfc" name="rfc" type="text" value="<?php echo $fact->rfcdf;?>" onchange="verficartiporfc()" required>
                  </div> 
                </div>
                <div class="agregardatospublicogeneral">
                  
                </div>
               
                <div class="col-md-6">
                  <div class="form-group">
                    <i class="fa fa-usd"></i>
                    <label class="active">Forma de pago</label>
                    <select class="form-control" id="FormaPago" name="FormaPago">
                      <option <?php if($fact->metodo=="PUE") echo "selected"; ?> value="PUE">Pago en una sola exhibición</option>
                      <option <?php if($fact->metodo=="PPD") echo "selected"; ?> value="PPD">Pago en parcialidades o diferido</option>
                    </select>
                  </div> 
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <i class="fa fa-usd"></i>
                    <label class="active">Método de pago</label>
                    <select class="form-control" id="MetodoPago" name="MetodoPago">
                      <option <?php if($fact->metodo==1) echo "selected"; ?> value="01">Efectivo</option>
                      <option value="02">ChequeNominativo</option>
                      <option value="03">Transferencia Electrónica Fondos</option>
                      <option <?php if($fact->metodo==2) echo "selected"; ?> value="04">Tarjetas De Crédito</option>
                      <option value="05">Monedero Electrónico</option>
                      <option value="06">Dinero Electrónico</option>
                      <option value="07">Tarjetas digitales</option>
                      <option value="08">Vales De Despensa</option>
                      <option <?php if($fact->metodo==3) echo "selected"; ?> value="28">Tarjeta de Debito</option>
                      <option value="29">Tarjeta Servicio</option>
                      <option value="99">Otros</option>
                      <option value="12">Dación Pago</option>
                      <option value="13">Pago Subrogación</option>
                      <option value="14">Pago Consignación</option>

                      <option value="15">Condonación</option>
                      <option value="17">Compensación</option>
                      <option value="23">Novación</option>
                      <option value="24">Confusión</option>
                      <option value="25">Remisión Deuda</option>
                      <option value="26">Prescripción o Caducidad</option>
                      <option value="27">Satisfacción Acreedor</option>
                      <option value="30">Aplicación Anticipos</option>
                      <option value="99">Por Definir</option>
                    </select>
                  </div>
                </div>
 
                <div class="col-md-6">
                  <i class="fa fa-calendar"></i>
                  <label for="">Fecha Aplicación</label>
                  <input class="form-control" id="fecha_aplica" name="fecha_aplica" type="date" value="<?php echo date("Y-m-d"); ?>">
                </div> 
                <div class="col-md-6">
                  <div class="form-group">
                    <i class="fa fa-map-marker"></i>
                    <label for="">Lugar Expedición</label>
                    <input class="form-control" name="lugar_aplica" id="lugar_aplica" type="text" value="Tlaxcala">
                  </div>
                </div> 
                <div class="col-md-6">
                  <div class="form-group">
                    <i class="fa fa-usd"></i> <label for=""> Tipo Moneda</label>
                    <select id="moneda" name="moneda" class="form-control" required>
                      <option value="pesos">Pesos</option>
                      <option value="USD">Dólares Norteamericanos</option>
                       <option value="EUR">Euro</option>
                    </select>
                  </div>
                </div> 
                <div class="col-md-6">
                  <div class="form-group">
                    <i class="fa fa-file"></i> <label> Uso CFDI</label>
                    <select id="uso_cfdi" name="uso_cfdi" class="form-control" required>
                      <option value="" disabled selected>Selecciona una opción</option>
                      <?php foreach ($cfdi as $key) { ?>
                        <option value="<?php echo $key->uso_cfdi; ?>" ><?php echo $key->uso_cfdi_text; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="CondicionesDePago">Condición de Pago</label>
                    <input type="text" name="CondicionesDePago" id="CondicionesDePago" class="form-control" onpaste="return false;" required value="Pago de contado en una exhibición">
                  </div>
                </div>
                <div class="col-md-12">
                  <hr>
                  <h4 class="card-title">Datos adicionales</h4>
                  <br>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label"># Proveedor</label>
                      <input type="text" name="numproveedor" id="numproveedor" value="" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label"># Orden de compra</label>
                      <input type="text" name="numordencompra" id="numordencompra" value="" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Lote</label>
                      <input type="text" name="lote" id="lote" value="" class="form-control">
                    </div>
                  </div>
                </div>
                
                <div class="col-md-12">
                  <label> Factura Relacionada</label>
                  <label class="switch">
                    <input title="Relacionar Factura" data-toggle="tooltip" type="checkbox" id="facturarelacionada">
                    <span class="slider round"></span>
                  </label>

                  <div class="form-group divfacturarelacionada" style="display:none;">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Tipo Relacion</label>
                        <select class="form-control" id="TipoRelacion">
                          <!--
                          <option value="01">01 Nota de crédito de los documentos relacionados</option>
                          <option value="02">02 Nota de débito de los documentos relacionados</option>
                          <option value="03">03 Devolución de mercancía sobre facturas o traslados previos</option>
                          -->
                          <option value="04">04 Sustitución de los CFDI previos</option>
                          <!--
                          <option value="05">05 Traslados de mercancias facturados previamente</option>
                          <option value="06">06 Factura generada por los traslados previos</option>
                          <option value="07">07 CFDI por aplicación de anticipo</option>
                          -->
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Folio Fiscal</label>
                        <input type="text" id="uuid_r" class="form-control" placeholder="111AAA1A-1AA1-1A11-11A1-11A1AA111A11">
                      </div>
                    </div>
                  </div>
                </div>

              </form>

              <!-- inicio -->
              <div class="col-sm-12">
                <div class="col-sm-12">
                  <!--<div class="card-block">-->
                    <table id="table_conceptos" class="table table-responsive table-striped" style="width: 100%">
                      <thead>
                        <tr>
                          <th width="5%">Cantidad</th>
                          <th width="15%">Unidad SAT</th>
                          <th width="15%">Concepto SAT</th>
                          <th width="25%">Descripción</th>
                          <th width="10%">Precio</th>
                          <th width="10%">Descuento</th>
                          <th width="10%">Monto</th>
                          <th width="10%">iva</th>
                        </tr>
                        <tr>
                          <th >
                            <input type="number" name="scantidad" id="scantidad" class="form-control" value="1" onchange="calculartotal()">
                          </th>
                          <th >
                            <select name="ssunidadsat" id="sunidadsat" class="sunidadsat form-control span12 form-control browser-default chosen-select"></select>
                          </th>
                          <th >
                            <select name="sconseptosat" id="sconseptosat" class="sconseptosat form-control span12 form-control browser-default chosen-select"></select>
                          </th>
                          <th >
                            <input type="text" name="sdescripcion" id="sdescripcion" class="form-control">
                          </th>
                          <th >
                            <input type="number" name="sprecio" id="sprecio" class="form-control" value="0" onchange="calculartotal()">
                          </th>
                          <th>
                            <input type="number" name="descuento" id="descuento" data-diva="0" class="form-control"
                            value="0" style="" onchange="calculardescuento()" readonly>
                          </th>
                          <th class="montototal" style="text-align: center;"></th>
                          <th >
                            <!--<input title="Aplica IVA" type="checkbox" class="filled-in switch" id="aplicariva" checked="checked">
                            <label for="aplicariva"></label>-->
                            <label class="switch">
                              <input title="Aplica IVA" data-toggle="tooltip" type="checkbox" id="aplicariva" checked="checked">
                              <span class="slider round"></span>
                            </label>
                          </th>
                          <th>
                            <button type="button" class="btn btn-info" data-toggle="tooltip" style="border-radius: 25px;background-color: rgb(6,20,55)" title="Agregar Concepto" onclick="agregarconcepto()"><i class="fa fa-plus"></i></button>
                          </th>
                        </tr>
                      </thead>
                      <tbody class="addcobrar">
                        <?php if(isset($det_venta)){
                          foreach ($det_venta->result() as $i) {
                            $sub=$i->cantidad*$i->precio;

                            echo '
                            <tr class="rowcobrar_'.$i->id_detalle_venta.'">
                              <td>
                                <input type="number" name="scantidad" id="scantidad" class="form-control" value="'.$i->cantidad.'" style="background: transparent !important; border: 0px !important;" readonly>
                              </th>
                              <th >
                                <select name="ssunidadsat" id="unidadsat" class="form-control span12 form-control browser-default chosen-select sunidadsat usat_'.$i->id_detalle_venta.'"></select>
                              </th>
                              <th >
                                <select  name="sconseptosat" id="conseptosat" class="form-control span12 form-control browser-default chosen-select sconseptosat csat_'.$i->id_detalle_venta.'"></select>
                              </th>
                              <th >
                                <input type="text" name="sdescripcion" id="descripcion" class="form-control" value="'.$i->nombre.'" style="background: transparent !important; border: 0px !important;">
                              </th>
                              <th>
                                <input type="number" name="sprecio" id="precio" class="form-control" value="'.$i->precio.'" style="background: transparent !important; border: 0px !important;" readonly onchange="calculartotal()">
                              </th>
                              <th>
                                <input type="number" name="descuento" id="descuento" data-diva="0" class="form-control cdescuento cdescuento_'.$i->id_detalle_venta.'"
                                value="0" style="" onchange="calculardescuento('.$i->id_detalle_venta.')" readonly>
                              </th>
                              <th>
                                <input type="number" name="subtotal" id="subtotal" class="form-control csubtotal" value="'.$i->precio.'" style="background: transparent !important; border: 0px !important;" readonly>
                              </th>
                              <th>
                                <!--<input type="number" id="tiva" value="'.$sub*0.16.'" class="form-control siva siva_'.$i->id_detalle_venta.'" style="background: transparent !important; border: 0px !important;" readonly>-->
                                <input type="number" id="tiva" value="0" class="form-control siva" style="background: transparent !important; border: 0px !important;" readonly>
                              </th>
                              <th>
                               <!--<input title="Aplica IVA" type="checkbox" class="filled-in chk_pi_'.$i->id_detalle_venta.'" id="aplicariva" onclick="calculartotales()">
                                <label for="aplicariva"></label>-->
                                <label class="switch">
                                  <input data-toggle="tooltip" title="Aplica IVA" type="checkbox" class="filled-in chk_pi_'.$i->id_detalle_venta.'" id="aplicariva" onclick="calculartotales()" checked>
                                  <span class="slider round"></span>
                                </label>
                              </th>
                            </tr>';
                          }
                        } ?>
                        
                      </tbody>
                    </table>
                  </div>
                <!--</div>-->
              </div>
              
                <div class="col-sm-7">
                  
                </div>
                <div class="col-sm-5">
                  <div class="row">
                    <div class="col s5" style="text-align: right;">
                      Subtotal
                    </div>
                    <div class="col s7">
                      <span style="float: left;">$</span>
                      <input type="" name="Subtotal" id="Subtotal" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col s5" style="text-align: right;">
                      Descuento
                    </div>
                    <div class="col s7">
                      <span style="float: left;">$</span>
                      <input type="" name="descuentof" id="descuentof" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                    </div>
                  </div> 
 
                  <div class="row">
                    <div class="col s5" style="text-align: right;">
                      I.V.A
                    </div>
                    <div class="col s7">
                      <span style="float: left;">$</span>
                      <input type="" name="iva" id="iva" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                    </div>
                  </div>
                  <!--<div class="row">
                      <div class="col s7" style="text-align: right;" onclick="calculartotales_set(1000)">
                        <input type="checkbox" id="risr" />
                        <label for="risr">10 % Retencion I.S.R.</label>
                      </div>
                      <div class="col s5">
                        <span style="float: left;">$</span>
                        <input type="" name="isr" id="isr" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                      </div>
                  </div> 
                  <div class="row">
                    <div class="col s7" style="text-align: right;" onclick="calculartotales_set(1000)">
                      <input type="checkbox" id="riva" />
                      <label for="riva">10.67 % Retencion I.V.A.</label>
                    </div>
                    <div class="col s5">
                      <span style="float: left;">$</span>
                      <input type="" name="ivaretenido" id="ivaretenido" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col s7" style="text-align: right;" onclick="calculartotales_set(1000)">
                      <input type="checkbox" id="5almillar" />
                      <label for="5almillar" >5 al millar.</label>
                    </div>
                    <div class="col s5">
                      <span style="float: left;">$</span>
                      <input type="" name="5almillarval" id="5almillarval" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col s7" style="text-align: right;" onclick="calculartotales_set(1000)">
                      <input type="checkbox" id="aplicaout" />
                      <label for="aplicaout" >6% Retención servicios de personal.</label>
                    </div>
                    <div class="col s5">
                      <span style="float: left;">$</span>
                      <input type="" name="outsourcing" id="outsourcing" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                    </div>
                  </div>  -->
 
                  <div class="row">
                    <div class="col s5" style="text-align: right;">
                      Total
                    </div>
                    <div class="col s7">
                      <span style="float: left;">$</span>
                      <input type="" name="total" id="total" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                    </div>
                  </div> 
                
                  <!-- fin -- 
                  <div class="row">
                    <div class="botonfinalizar"></div>
                    <div class="col-md-9">
                    </div>
                    <div class="col-md-3 guadarremove">
                      <button class="btn cyan waves-effect waves-light right" onclick="verPrefactura()" type="button">Ver Prefactura <i class="fa fa-arrow-right"></i>
                      </button>
                    </div>
                  </div>-->
                </div>
                <div class="col-md-12">
                  <div class="col-md-5">
                  </div>
                  <div class="col-md-3">
                    <button class="btn cyan waves-effect waves-light registrofac" type="button">Facturar <i class="fa fa-file-pdf-o"></i>
                    </button>
                  </div>
                </div>
              

            </div>
          </div>
        </div>
      </div>
    </div>
  
</div>
<script type="text/javascript">
  $(document).ready(function($) {
    setTimeout(function(){ 
        $('#rfc').change();
    }, 1000);  
  });
</script>