<div class="row">
  <div class="col-md-12">
    <h2>Lista ventas</h2>
  </div>
</div>
<!--Statistics cards Ends-->

<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Listado de ventas</h4>
      </div>
      <div class="col-md-2">
        <select class="form-control" id="ventas_credito" onchange="loadTableTipo()">
          <option value="0">Todas las ventas</option>
          <option value="6">Ventas a crédito</option>
        </select>  
      </div>
      <div class="col-md-12">
        <!--
        <div class="col-md-8">

        </div>
        <div class="col-md-3">
          <form role="search" class="navbar-form navbar-right mt-1">
            <div class="position-relative has-icon-right">
              <input type="text" placeholder="Buscar" id="buscarvent" class="form-control round" oninput="buscarventa()">
              <div class="form-control-position"><i class="ft-search"></i></div>
            </div>
          </form>
        </div>
        -->
      </div>
      <div class="card-body">
        <div class="card-block">
          <!--------//////////////-------->
          <table class="table table-striped responsive" id="data-tables" style="width: 100%">
            <thead>
              <tr>
                <th>Folio</th>
                <th>Fecha</th>
                <th>Vendedor</th>
                <th>Sucursal</th>
                <th>Monto</th>
                <th>Cliente</th>
                <th>Estatus</th>
                <th></th>
                <th></th>
              </tr>
            </thead>
            <tbody id="tbodyresultadosvent">

              <?php /* foreach ($ventas->result() as $item) { ?>
                <tr id="trven_<?php echo $item->id_venta; ?>">
                  <td><?php echo $item->id_venta; ?></td>
                  <td><?php echo $item->reg; ?></td>
                  <td><?php echo $item->vendedor; ?></td>
                  <td><?php echo $item->sucursal; ?></td>
                  <td><?php echo $item->monto_total; ?></td>
                  <td><?php echo $item->cliente; ?></td>
                  <td><?php if ($item->cancelado == 1) {
                        echo '<span class="badge badge-danger">Cancelado</span>';
                      }
                      if ($item->cancelado == 2) {
                        echo '<span class="badge badge-danger">Cancelado Parcial</span>';
                      } ?>
                  </td>
                  <td>
                    <?php if ($item->cancelado == 0) { ?>
                      <button class="btn btn-raised gradient-blackberry white sidebar-shadow" onclick="facturar(<?php echo $item->id_venta; ?>)" title="Facturar Venta" data-toggle="tooltip" data-placement="top"> Facturar
                      </button>
                    <?php } ?>
                  </td>
                  <td>
                    <button class="btn btn-raised gradient-blackberry white sidebar-shadow" onclick="ticket(<?php echo $item->id_venta; ?>)" title="Ticket" data-toggle="tooltip" data-placement="top">
                      <i class="fa fa-book"></i>
                    </button>
                    <button class="btn btn-raised gradient-flickr white sidebar-shadow" onclick="cancelar(<?php echo $item->id_venta; ?>,<?php echo $item->monto_total; ?>)" title="Cancelar" data-toggle="tooltip" data-placement="top" <?php if ($item->cancelado == 1) {
                                                                                                                                                                                                                                            echo 'disabled';
                                                                                                                                                                                                                                          } ?>>
                      <i class="fa fa-times"></i>
                    </button>
                  </td>
                </tr>

              <?php } */ ?>

            </tbody>
          </table>
          <!--
          <table class="table table-striped responsive" id="data-tables2" style="display: none;width: 100%">
            <thead>
              <tr>
                <th>Folio</th>
                <th>Fecha</th>
                <th>Vendedor</th>
                <th>Sucursal</th>
                <th>Monto</th>
                <th>Cliente</th>
                <th>Estatus</th>
                <th></th>
              </tr>
            </thead>
            <tbody id="tbodyresultadosvent2">
            </tbody>

          </table>
          <div class="col-md-12">
            <div class="col-md-7">

            </div>
            <div class="col-md-5">
              <?php /* echo $this->pagination->create_links() */?>
            </div>

          </div>
          -->


          <!--------//////////////-------->
        </div>
      </div>
    </div>
  </div>
</div>
<!------------------------------------------------>
<style type="text/css">
  #iframereporte {
    background: white;
  }

  iframe {
    height: 500px;
    border: 0;
    width: 100%;
  }
</style>
<div class="modal fade text-left" id="iframeri" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Ticket</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!--<div class="modal-body">-->
      <div id="iframereporte"></div>
      <!--</div>-->
      <div class="modal-footer">
        <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade text-left" id="cancelar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmaci&oacute;n</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Articulos especificos para cancelar del ticket #<span id="NoTicket2"></span>: <br>
        <div id="detVen"></div>
        <input type="hidden" id="hddIdVenta">
        <div class="col text-center">
          <button type="button" id="sicancelar" class="btn btn-danger">Cancelar Todo</button>
        </div>
      </div>

      <div class="modal-footer">

        <button type="button" id="confCancel" class="btn btn-raised gradient-purple-bliss white" data-dismiss="modal">Aceptar</button>
        <button type="button" class="btn btn-raised gradient-ibiza-sunset white" onclick="recarga();" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade text-left" id="pagocredito" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Detalles de crédito</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="txt_credito"></div>
        <div class="txt_credito_tabla"></div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-raised gradient-ibiza-sunset white" onclick="recarga();" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade text-left" id="modaldeletepago" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Confirmación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
                  
                  <div class="col-md-12">
                      <h3>¿Confirma que desea eliminar?</h3>
                  </div>
              </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal" onclick="delete_pago()">Guardar</button>
            </div>
        </div>
    </div>
</div>