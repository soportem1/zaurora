<div class="row">
    <div class="col-md-12">
      
    </div>
    
    <div class="col-xl-3 col-lg-6 col-md-6 col-12">
        <div class="card gradient-blackberry">
            <div class="card-body">
                <div class="card-block pt-2 pb-0">
                    <div class="media">
                        <div class="media-body white text-left">
                            <h3 class="font-large-1 mb-0"><?php echo number_format($total_rows,0,'.',',');?></h3>
                            <span>Productos registrados</span>
                        </div>
                        <div class="media-right white text-right">
                            <i class="icon-pie-chart font-large-1"></i>
                        </div>
                    </div>
                </div>
                <div id="Widget-line-chart" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2">                   
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-6 col-md-6 col-12">
        <div class="card gradient-ibiza-sunset">
            <div class="card-body">
                <div class="card-block pt-2 pb-0">
                    <div class="media">
                        <div class="media-body white text-left">
                            <h3 class="font-large-1 mb-0"><?php echo number_format($totalexistencia,0,'.',',');?></h3>
                            <span>Productos en existencia</span>
                        </div>
                        <div class="media-right white text-right">
                            <i class="icon-bulb font-large-1"></i>
                        </div>
                    </div>
                </div>
                <div id="Widget-line-chart1" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2">                  
                </div>

            </div>
        </div>
    </div>
    <?php if($this->session->userdata("almacen")!=1){ ?>
        <div class="col-xl-3 col-lg-6 col-md-6 col-12">
            <div class="card gradient-green-tea">
                <div class="card-body">
                    <div class="card-block pt-2 pb-0">
                        <div class="media">
                            <div class="media-body white text-left">
                                <h3 class="font-large-1 mb-0">$ <?php echo number_format($totalproductopreciocompra,2,'.',',');?></h3>
                                <span>Total de Precios unitarios</span>
                            </div>
                            <div class="media-right white text-right">
                                <i class="icon-graph font-large-1"></i>
                            </div>
                        </div>
                    </div>
                    <div id="Widget-line-chart2" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2">              
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-md-6 col-12">
            <div class="card gradient-pomegranate">
                <div class="card-body">
                    <div class="card-block pt-2 pb-0">
                        <div class="media">
                            <div class="media-body white text-left">
                                <h3 class="font-large-1 mb-0">$ <?php echo number_format($totalproductoporpreciocompra,2,'.',',');?></h3>
                                <span>Total de Precios</span>
                            </div>
                            <div class="media-right white text-right">
                                <i class="icon-wallet font-large-1"></i>
                            </div>
                        </div>
                    </div>
                    <div id="Widget-line-chart3" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2">                  
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
              <!--Statistics cards Ends-->

<!--Line with Area Chart 1 Starts-->
<div class="row">
    <div class="col-sm-12">
        <!--<div class="card">
            <div class="card-header">
                <h4 class="card-title">PRODUCTS SALES</h4>
            </div>
            <div class="card-body">
                <div class="card-block">
                    
                </div>
            </div>
        </div>-->
    </div>
</div>
<div class="modal fade text-left" id="modalturno" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="false" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Nuevo Turno</h4>
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>-->
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Abrir Turno:</label>
                            <div class="col-sm-8 controls">
                                <input type="number"class="input-border-btm form-control" id="cantidadt" name="cantidadt" style="text-transform:uppercase;"  placeholder="$"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"> Nombre del Turno</label>
                            <div class="col-sm-8 controls">
                                <input type="text" class="input-border-btm form-control" id="nombredelturno" name="nombredelturno" style="text-transform:uppercase;" > 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal" id="btnabrirt">Abrir turno</button>
            </div>
        </div>
    </div>
</div>
<?php if ($sturno=='cerrado') { ?>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#modalturno').modal();
        });
    </script>
 <?php } ?>