<?php
if (isset($_GET['id'])) {
  $id = $_GET['id'];
  $clientesv = $this->ModeloClientes->getcliente($id);

  foreach ($clientesv->result() as $item) {
    $label = 'Editar Cliente';
    $id = $item->ClientesId;
    $Nom = $item->Nom;
    $porcentaje = $item->porcentaje;
    $Calle = $item->Calle;
    $noExterior = $item->noExterior;
    $Colonia = $item->Colonia;
    $Localidad = $item->Localidad;
    $Municipio = $item->Municipio;
    $Estado = $item->Estado;
    $Pais = $item->Pais;
    $CodigoPostal = $item->CodigoPostal;
    $Correo = $item->Correo;
    $noInterior = $item->noInterior;
    $nombrec = $item->nombrec;
    $correoc = $item->correoc;
    $telefonoc = $item->telefonoc;
    $extencionc = $item->extencionc;
    $nextelc = $item->nextelc;
    $descripcionc = $item->descripcionc;

    $rfcdf = $item->rfcdf;
    $razon_social = $item->razon_social;
    $direccion_fiscal = $item->direccion_fiscal;
    $cp_fiscal = $item->cp_fiscal;
    $RegimenFiscalReceptor = $item->RegimenFiscalReceptor;
  }
} else {
  $label = 'Nuevo Cliente';
  $id = 0;
  $Nom = '';
  $porcentaje = '';
  $Calle = '';
  $noExterior = '';
  $Colonia = '';
  $Localidad = '';
  $Municipio = '';
  $Estado = '';
  $Pais = 'México';
  $CodigoPostal = '';
  $Correo = '';
  $noInterior = '';
  $nombrec = '';
  $correoc = '';
  $telefonoc = '';
  $extencionc = '';
  $nextelc = '';
  $descripcionc = '';

  $rfcdf = '';
  $razon_social = '';
  $direccion_fiscal = '';
  $cp_fiscal = '';
  $RegimenFiscalReceptor = 0;
}
if ($RegimenFiscalReceptor > 0) {
?>
  <script type="text/javascript">
    $(document).ready(function($) {
      $('#RegimenFiscalReceptor').val(<?php echo $RegimenFiscalReceptor; ?>);
    });
  </script>
<?php
}
?>
<style type="text/css">
  .vd_red {
    font-weight: bold;
    color: red;
  }

  .vd_green {
    color: #009688;
  }
</style>
<input type="hidden" name="clienteid" id="clienteid" value="<?php echo $id; ?>">
<div class="row">
  <div class="col-md-12">
    <h2>Cliente</h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title"><?php echo $label; ?></h4>
      </div>
      <div class="card-body">
        <div class="card-block form-horizontal">
          <!--------//////////////-------->
          <div class="row">
            <form method="post" role="form" id="formclientes">
              <div class="col-md-12">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-md-2 control-label">Nombre:</label>
                    <div class=" col-md-10 input-group">
                      <input type="text" class="form-control" id="nombrecli" name="nombrecli" value="<?php echo $Nom; ?>">
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-md-2 control-label">Teléfono / Celular:</label>
                    <div class=" col-md-10 input-group">
                      <input type="text" class="form-control" id="telefonocli" name="telefonocli" pattern="\d*" maxlength="12" value="<?php echo $telefonoc; ?>">
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-md-2 control-label">Porcentaje Puntos Aurora:</label>

                    <div class="col-md-2 input-group">
                      <input type="number" id="porcentaje" name="porcentaje" class="form-control date-picker" value="<?php echo $porcentaje ?>">
                    </div>
                  </div>
                </div>

                <div class="col-md-12">
                  <h3>Datos Fiscales</h3>
                  <hr />
                </div>
                <div class="col-md-12">
                  <div class="col-md-3">
                    <div class="form-group">
                      <label class=" control-label">RFC:</label>
                      <input type="text" class="form-control" id="rfcdf" name="rfcdf" value="<?php echo $rfcdf; ?>">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">

                      <label class=" control-label">Razón Social:</label>
                      <input type="text" class="form-control" id="razon_social" name="razon_social" value="<?php echo $razon_social; ?>">

                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">

                      <label class="control-label">C.P. Fiscal:</label>
                      <input type="text" class="form-control" id="cp_fiscal" name="cp_fiscal" value="<?php echo $cp_fiscal; ?>">

                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">

                      <label class="control-label">RegimenFiscalReceptor:</label>
                      <select id="RegimenFiscalReceptor" name="RegimenFiscalReceptor" class="form-control" required>';
                        <option value="601">601 General de Ley Personas Morales</option>
                        <option value="603">603 Personas Morales con Fines no Lucrativos</option>
                        <option value="605">605 Sueldos y Salarios e Ingresos Asimilados a Salarios</option>
                        <option value="606">606 Arrendamiento</option>
                        <option value="607">607 Régimen de Enajenación o Adquisición de Bienes</option>
                        <option value="608">608 Demás ingresos</option>
                        <option value="609">609 Consolidación</option>
                        <option value="610">610 Residentes en el Extranjero sin Establecimiento Permanente en México</option>
                        <option value="611">611 Ingresos por Dividendos (socios y accionistas)</option>
                        <option value="612">612 Personas Físicas con Actividades Empresariales y Profesionales</option>
                        <option value="614">614 Ingresos por intereses</option>
                        <option value="615">615 Régimen de los ingresos por obtención de premios</option>
                        <option value="616">616 Sin obligaciones fiscales</option>
                        <option value="620">620 Sociedades Cooperativas de Producción que optan por diferir sus ingresos</option>
                        <option value="621">621 Incorporación Fiscal</option>
                        <option value="622">622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras</option>
                        <option value="623">623 Opcional para Grupos de Sociedades</option>
                        <option value="624">624 Coordinados</option>
                        <option value="628">628 Hidrocarburos</option>
                        <option value="629">629 De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales</option>
                        <option value="630">630 Enajenación de acciones en bolsa de valores</option>
                      </select>

                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">

                      <label class="col-md-4 control-label">Dirección Fiscal:</label>
                      <input type="text" class="form-control" id="direccion_fiscal" name="direccion_fiscal" value="<?php echo $direccion_fiscal; ?>">

                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <br>
                </div>
                <div class="col-md-12">
                  <h3>Domicilio</h3>
                  <hr />
                </div>
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-3 control-label">Calle:</label>
                      <div class=" col-md-9 input-group">
                        <input type="text" class="form-control" id="callecli" name="callecli" value="<?php echo $Calle; ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label class="col-md-7 control-label">No Exterior:</label>
                      <div class=" col-md-5 input-group">
                        <input type="text" class="form-control" id="nexteriorcli" name="nexteriorcli" value="<?php echo $noExterior; ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label class="col-md-7 control-label">No Interior:</label>
                      <div class=" col-md-5 input-group">
                        <input type="text" class="form-control" id="ninteriorcli" name="ninteriorcli" value="<?php echo $noInterior; ?>">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-3 control-label">Colonia:</label>
                      <div class=" col-md-9 input-group">
                        <input type="text" class="form-control" id="coloniacli" name="coloniacli" value="<?php echo $Colonia; ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-3 control-label">Localidad:</label>
                      <div class=" col-md-9 input-group">
                        <input type="text" class="form-control" id="localidadcli" name="localidadcli" value="<?php echo $Localidad; ?>">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-3 control-label">Municipio:</label>
                      <div class=" col-md-9 input-group">
                        <input type="text" class="form-control" id="municipiocli" name="municipiocli" value="<?php echo $Municipio; ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-3 control-label">Código postal:</label>
                      <div class=" col-md-9 input-group">
                        <input type="text" class="form-control" id="codigopcli" name="codigopcli" value="<?php echo $CodigoPostal; ?>">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-3 control-label">Estado:</label>
                      <div class=" col-md-9 input-group">
                        <input type="text" class="form-control" id="estadocli" name="estadocli" value="<?php echo $Estado; ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-3 control-label">País:</label>
                      <div class=" col-md-9 input-group">
                        <input type="text" class="form-control" id="paiscli" name="localidadcli" value="<?php echo $Pais; ?>">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <h3>Contacto Directo</h3>
                  <hr />
                </div>
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-3 control-label">Nombre contacto:</label>
                      <div class=" col-md-9 input-group">
                        <input type="text" class="form-control" id="contactocli" name="contactocli" value="<?php echo $nombrec; ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-3 control-label">Correo:</label>
                      <div class=" col-md-9 input-group">
                        <input type="email" class="form-control" id="correocli" name="correocli" value="<?php echo $Correo; ?>">
                      </div>
                    </div>
                  </div>
                  <!--
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label class="col-md-3 control-label">Correo:</label>
                                              <div class=" col-md-9 input-group">
                                                <input type="text" class="form-control" id="correoccli" name="correoccli" value="<?php echo $correoc; ?>">
                                              </div>
                                            </div>
                                          </div>
                                          -->
                </div>
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-3 control-label">Extencios:</label>
                      <div class=" col-md-9 input-group">
                        <input type="text" class="form-control" id="extenciocli" name="extenciocli" value="<?php echo $extencionc; ?>">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-3 control-label">Nextel:</label>
                      <div class=" col-md-9 input-group">
                        <input type="text" class="form-control" id="nextelcli" name="nextelcli" value="<?php echo $nextelc; ?>">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-3 control-label">Descripción:</label>
                      <div class=" col-md-9 input-group">
                        <textarea class="form-control" id="descripcioncli" name="descripcioncli"><?php echo $descripcionc; ?></textarea>
                      </div>
                    </div>
                  </div>
                </div>
            </form>
            <div class="col-md-12">
              <a href="#" class="btn btn-warning white shadow-z-1-hover" id="savecl">Guardar</a>
            </div>





























          </div>


          <!--------//////////////-------->
        </div>
      </div>
    </div>
  </div>
</div>