<div class="row">
  <div class="col-md-12">
    <h2>Asistencias</h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Registros de Asistencia</h4>
      </div>
      <div class="card-body">
        <div class="card-block form-horizontal">
          <!--------//////////////-------->
          <div class="row">

            <form method="post" role="form" id="form_asistencias">

              <div class="col-md-9">
                <div class="form-group">
                  <input type="hidden" class="form-control col-md-2" id="id" name="id" value="0" readonly>
                  <input type="hidden" class="form-control col-md-2" id="existIn" value="0" readonly>

                  <label class="col-md-2 control-label">Empleado:</label>
                  <div class="col-md-10">
                    <select class="form-control" id="empleado" name="personal_id">
                    </select>
                  </div>

                </div>
              </div>

              <div class="col-md-3">
                <a class="btn btn-raised gradient-green-tea white shadow-z-1-hover" onclick="modal_export_excel()"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Exportar a Excel</a>
              </div>

              <div class="col-md-12"><br></div>

              <div class="col-md-9">
                <div class="form-group">
                  <label class="col-md-2 control-label">Fecha:</label>
                  <div class="col-md-4">
                    <input type="date" class="form-control" id="fecha" name="fecha" step="60" value="<?php echo $fecha; ?>" readonly>
                  </div>
                </div>
              </div>

              <div class="col-md-12"><br></div>

              <div class="col-md-9">
                <div class="form-group">
                  <label class="col-md-2 control-label">Hora de entrada:</label>
                  <div class="col-md-4">
                    <input type="time" class="form-control" id="entrada" name="entrada" step="60" value="<?php echo $hora; ?>" readonly>
                  </div>

                  <label class="col-md-2 control-label">Hora de salida:</label>
                  <div class="col-md-4">
                    <input type="time" class="form-control" id="salida" name="salida" value="" readonly>
                  </div>
                </div>
              </div>

            </form>

            <div class="col-md-12"><br></div>
            <div class="col-md-12">
              <br>
              <div class="col-md-10"></div>
              <div class="col-md-2" align="right">
                <button type="buton" class="btn btn-warning white shadow-z-1-hover" id="add"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button>
              </div>
            </div>


            <div class="col-md-12" id="cont_lista" style="display: none;">
              <div class="col-md-12 div_pagos">
                <div class="col-md-12">
                  <br>
                  <h3 class="card-title">Listado de registro de asistencia <i class="fa fa-clipboard" aria-hidden="true"></i></h3>
                  <br>
                </div>

                <div class="col-md-12">
                  <table class="table table-striped" id="tabla_asistencia" style="width: 100%">
                    <thead>
                      <tr>
                        <th>Empleado</th>
                        <th>Fecha</th>
                        <th>Hora entrada</th>
                        <th>Hora salida</th>
                        <th>Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                  <div class="col-md-12"><br></div>
                </div>
              </div>

            </div>
          </div>
          <!--------//////////////-------->
        </div>

      </div>
    </div>
  </div>
</div>


<!----------------->
<div class="modal fade" id="modal_export_excel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Ingresar un rango de fechas</h4>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <!---->
              <div class="card-body">
                <div class="mb-3 row">
                  <div class="col-md-4">
                    <input class="form-control" type="hidden" id="id_pers" value="0">
                  </div>
                </div>

                <div class="row">
                  <div class="col">
                    <div class="mb-3 row">
                      <label class="col-sm-2 control-label">Desde:</label>
                      <div class="col-sm-4">
                        <input type="date" class="form-control" id="f_ini">
                      </div>

                      <label class="col-sm-2 control-label">Hasta:</label>
                      <div class="col-sm-4">
                        <input type="date" class="form-control" id="f_fin">
                      </div>

                    </div>
                  </div>
                </div>
              </div>
              <!---->
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-raised gradient-green-tea white shadow-z-1-hover" onclick="export_excel()">Exportar Excel</button>
      </div>
    </div>
  </div>
</div>