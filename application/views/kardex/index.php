<div class="row">
  <div class="col-md-12">
    <h2>Kardex</h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Reporte de movimientos</h4>
      </div>
      <div class="card-body">
        <div class="card-block">
          <!--------//////////////-------->
          <div class="row input_busquedas">
            <div class="col-md-12">
              <div class="form-body">

                <div class="row">
                  <div class="col-md-12 form-group">

                    <label class="control-label col-md-1">Personal:</label>
                    <div class="col-md-3">
                      <select id="personal" class="form-control">
                      </select>
                    </div>
                    
                    <label class="control-label col-md-1">Sucursal:</label>
                    <div class="col-md-3">
                      <select id="sucursal" class="form-control">
                        <option value="0">Seleccione opción</option>
                        <?php foreach ($sucursales as $s) { ?>
                          <option value="<?php echo $s->idsucursal; ?>"><?php echo $s->nombre; ?></option>
                        <?php } ?>
                      </select>
                    </div>

                    <label class="control-label col-md-1">Movimiento:</label>
                    <div class="col-md-3">
                      <select id="tipo" class="form-control">
                        <option value="0">Seleccione opción</option>
                        <option value="1">Salida (por venta)</option>
                        <option value="2">Entrada (por compra)</option>
                        <option value="3">Devolución</option>
                        <option value="4">Eliminación</option>
                        <option value="5">Edición (ajuste)</option>
                      </select>
                    </div>

                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12 form-group">
                    <label class="control-label col-md-1">Desde:</label>
                    <div class="col-md-3">
                      <?php
                      $today = date('Y-m-d');
                      echo '<input id="fechaIni" class="form-control date-picker" max="' . $today . '" size="16" type="date" />';
                      ?>
                    </div>

                    <label class="control-label col-md-1">Hasta:</label>
                    <div class="col-md-3">
                      <?php
                      $today = date('Y-m-d');
                      echo '<input id="fechaFin" class="form-control date-picker" max="' . $today . '" size="16" type="date" />';
                      ?>
                    </div>

                    <div class="col-md-2">
                      <div class="checkbox-list">
                        <label>Fecha actual &ensp; <input type="checkbox" id="chkFecha" value="1"></label>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <a href="#" class="btn btn-raised gradient-purple-bliss white" onclick="load_table();">Buscar</a>
                    </div>

                  </div>
                </div>

                </div>
              </div>

            </div>
          </div>
        </div>


        <div class="col-sm-12 table">
          <table width="100%" id="data-tableK">
            <thead>
              <tr>
                <th>Fecha</th>
                <th>Hora</th>
                <th>Personal</th>
                <th>Código</th>
                <th>Descripción</th>
                <th>Sucursal</th>
                <th>Había</th>
                <th>Tipo</th>
                <th>Cantidad movida</th>
                <th>Hay</th>
              </tr>
            </thead>
            <tbody id="data-tbody">
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>