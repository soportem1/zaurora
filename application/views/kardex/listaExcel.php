<?php
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=lista_asistencia".date('Ymd Gis').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th scope="col">Empleado</th>
            <th scope="col">Fecha</th>
            <th scope="col">Hora entrada</th>
            <th scope="col">Hora salida</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($list as $l) {
            echo '
            <tr>
                <td scope="col">'.$l->empleado.'</td>
                <td scope="col">'.$l->fechaF.'</td>
                <td scope="col">'.$l->entradaF.'</td>
                <td scope="col">'.$l->salidaF.'</td>
            </tr>';
        }
        ?>
    </tbody>
</table>
