<div class="row">
  <div class="col-md-12">
    <h2>Monedero de puntos</h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Consulta de puntos</h4>
      </div>
      <div class="card-body">
        <div class="card-block">
          <!--------//////////////-------->
          <div class="row inputbusquedas">
            <div class="col-md-12">
              <div class="form-body">

              <!--
                <input type="hidden" id="prf" value="<?php echo $this->session->userdata("perfilid_tz"); ?>">
                <input type="hidden" id="idper" value="<?php echo $this->session->userdata("idpersonal_tz"); ?>">
              -->

                <div class="row">
                  <div class="col-md-12 form-group">

                    <label class="control-label col-md-1">Cliente:</label>
                    <div class="col-md-7">
                      <select id="cliente" class="form-control">
                      </select>
                    </div>

                    <label class="control-label col-md-2">Puntos actuales:</label>
                    <div class="col-md-1">
                      <input type="text" id="puntos" class="form-control date-picker" value="$ 00.00" readonly/>
                    </div>

                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12 form-group">
                    <label class="control-label col-md-1">Desde:</label>
                    <div class="col-md-3">
                      <?php
                          $today = date('Y-m-d');
                          echo '<input id="fechaIni" class="form-control date-picker" max="' . $today . '" size="16" type="date" />';
                      ?>
                      <!--
                      <input id="fechaIni" class="form-control date-picker" size="16" type="date" />
                      -->
                    </div>

                    <label class="control-label col-md-1">Hasta:</label>
                    <div class="col-md-3">
                      <?php
                          $today = date('Y-m-d');
                          echo '<input id="fechaFin" class="form-control date-picker" max="' . $today . '" size="16" type="date" />';
                      ?>
                      <!--
                      <input id="fechaFin" class="form-control date-picker" size="16" type="date" />
                      -->
                    </div>

                    <div class="col-md-2">
                      <div class="checkbox-list">
                        <label>Fecha actual &ensp; <input type="checkbox" id="chkFecha" value="1"></label>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <a href="#" class="btn btn-raised gradient-purple-bliss white" onclick="getTable();">Buscar</a>
                      <!--
                      <a id="btnImprimir" onclick="imprimir();"><button type="button" class="btn btn-raised gradient-purple-bliss white"><i class="fa fa-print"></i>
                          <div id="wait" style="display:none;width:69px;height:89px;border:1px solid black;position:absolute;top:50%;left:50%;padding:2px;"></div>
                        </button>
                      </a>
                      -->
                    </div>

                  </div>
                </div>

                <div id="wait" class="wait" style="width:89px;height:109px;border:0px solid black;position:absolute;top:50%;left:50%;padding:2px;">

                </div>
              </div>

            </div>
          </div>
        </div>


        <div class="col-sm-12 table">
          <table width="100%" id="data-tableM">
            <thead>
              <tr>
                <th>Fecha de venta</th>
                <th>Folio</th>
                <th>Vendedor</th>
                <th>Sucursal</th>
                <th>Método de pago</th>
                <th>% puntos</th>
                <th>Monto venta</th>
                <th>Puntos venta</th>
                <th>Puntos utilizados</th>
                <th>Detalle</th>
              </tr>
            </thead>
            <tbody id="data-tbody">
            </tbody>
            <tfoot>
              <tr>
                <td colspan="5"></td>
                <td><b>TOTAL DE VENTA</b></td>
                <td id="totalVenta">$ 0.00</td>
                <td colspan="3"></td>
              </tr>
              <tr>
                <td colspan="5"></td>
                <td><b>Total en puntos</b></td>
                <td id="totalPuntos">$ 0.00</td>
                <td colspan="3"></td>
              </tr>
            </tfoot>
          </table>
        </div>


        <!--------//////////////-------->
      </div>
    </div>
  </div>
</div>


<style type="text/css">
  #iframereporte {
    background: white;
  }

  iframe {
    height: 500px;
    border: 0;
    width: 100%;
  }
</style>

<div class="modal fade text-left" id="iframeri" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Ticket</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!--<div class="modal-body">-->
      <div id="iframereporte"></div>
      <!--</div>-->
      <div class="modal-footer">
        <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>