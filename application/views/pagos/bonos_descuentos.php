<style type="text/css">

.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #cfa520;
}

input:focus + .slider {
  box-shadow: 0 0 1px #cfa520;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
<div class="row">
  <div class="col-md-12">
    <h2>Bonos y Descuentos</h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title">Asignación de Registros</h4>
          </div>
          <div class="card-body">
              <div class="card-block form-horizontal">
                  <!--------//////////////-------->
                  <div class="row">
                    <form method="post"  role="form" id="formbonos">
                      
                      <div class="col-md-9">
                        <div class="form-group">
                          <label class="col-md-2 control-label">Empleado:</label>
                          <div class=" col-md-10">
                            <select class="form-control" id="empleado" name="id_empleado">
                            </select>
                          </div>
                        </div>
                      </div>                    
                      <div class="col-md-12"><br></div>
                      <div class="col-md-9">
                        <div class="form-group">
                          <label class="col-md-2 control-label">Fecha:</label>
                          <div class="col-md-4 input-group">
                            <input type="date" class="form-control" id="fecha" name="fecha">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12"><br></div>
                      <div class="col-md-12">
                        <h3>Datos</h3>
                        <hr style="border-color: #cfa520; border-width: 5px;" />
                      </div>
                      <div class="col-md-12">
                        <div class="col-md-3">
                          <label> Bono</label>
                          <label class="switch">
                            <input title="Ingreso de Bono" checked data-toggle="tooltip" type="radio" name="tipo_reg" id="bono">
                            <span class="slider round"></span>
                          </label>
                        </div>
                        <div class="col-md-3">
                          <label> Descuento</label>
                          <label class="switch">
                            <input title="Ingreso de Descuento" data-toggle="tooltip" type="radio" name="tipo_reg" id="descuento">
                            <span class="slider round"></span>
                          </label>
                        </div>
                        <div class="col-md-12 div_pagos bonos_">
                          <div class="col-md-12">
                            <br><h3 class="card-title">Bono <i class="fa fa-check-circle-o" aria-hidden="true"></i></h3><br>
                          </div>
                          <div class="col-md-2">
                            <label class="col-md-12">Cantidad:</label>
                          </div>
                          <div class="col-md-2">
                            <input type="number" id="cant_bono" name="cant_bono" class="form-control" placeholder="$">
                          </div>
                          <div class="col-md-2">
                            <label class="col-md-12">Motivo:</label>
                          </div>
                          <div class="col-md-6">
                            <input type="text" id="motivo_bono" name="motivo_bono" class="form-control">
                          </div>
                          <div class="col-md-1">
                            <br>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12"><br></div>
                      <div class="col-md-12">
                        <div class="col-md-12 div_pagos descuentos_">
                          <div class="col-md-12">
                            <br><h3 class="card-title">Descuento <i class="fa fa-minus-circle" aria-hidden="true"></i></h3><br>
                          </div>
                          <div class="col-md-2">
                            <label class="col-md-12">Cantidad:</label>
                          </div>
                          <div class="col-md-2">
                            <input type="number" id="cant_desc" name="cant_desc" class="form-control" placeholder="$">
                          </div>
                          <div class="col-md-2">
                            <label class="col-md-12">Motivo:</label>
                          </div>
                          <div class="col-md-6">
                            <input type="text" id="motivo_desc" name="motivo_desc" class="form-control">
                          </div>
                          <div class="col-md-1">
                            <br>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-12">
                        <br>
                        <div class="col-md-10"></div>
                        <div class="col-md-2" align="right">
                          <button type="submit" class="btn btn-warning white shadow-z-1-hover" id="save"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button>
                        </div>
                      </div> 
                    </form>
                    <div class="col-md-12" id="cont_lista" style="display: none;">
                      <div class="col-md-12 div_pagos">
                        <div class="col-md-12">
                          <br><h3 class="card-title">Listado de registros <i class="fa fa-usd " aria-hidden="true"></i></h3><br>
                        </div>

                        <div class="col-md-12" id="cont_tabla_bonos">

                          <div class="col-md-12"><br></div>
                        </div>
                      </div>

                    </div>                 
                  </div>
                  <!--------//////////////-------->
              </div>
              
          </div>
      </div>
  </div>
</div>

<div class="modal fade text-left" id="modal_detalles_prods" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="false" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Detalle de Productos</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
              <div class="cont_tabla">

              </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Aceptar</button>
      </div>
    </div>
  </div>
</div>