
<div class="row">
  <div class="col-md-12">
    <h2>Cálculo de Pagos</h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title">Boleta de Pagos</h4>
          </div>
          <div class="card-body">
              <div class="card-block form-horizontal">
                  <!--------//////////////-------->
                  <div class="row">
                    <form method="post"  role="form" id="form">
                      
                      <div class="col-md-9">
                        <div class="form-group">
                          <label class="col-md-2 control-label">Empleado:</label>
                          <div class=" col-md-10">
                            <select class="form-control" id="empleado" name="empleado">
                            </select>
                          </div>
                        </div>
                      </div>
                    
                      <div class="col-md-12"><br></div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="col-md-2 control-label">Desde:</label>
                          <div class=" col-md-3">
                            <input type="date" class="form-control" id="del" name="del">
                          </div>

                          <label class="col-md-1 control-label">Hasta:</label>
                          <div class=" col-md-3">
                            <input type="date" class="form-control" id="al" name="al">
                          </div>
                        </div>
                      </div>
                      
                      <div class="col-md-12">
                        <h3>Datos laborales</h3>
                        <hr style="border-color: #cfa520; border-width: 5px;" />
                      </div>
                      <div class="col-md-12">
                        <div class="col-md-6 div_pagos">
                          <div class="col-md-12">
                            <br>
                          </div>
                          <div class="col-md-5">
                            <label class="col-md-12">Trabajados:</label>
                          </div>
                          <div class="col-md-3">
                            <input type="text" id="cant_dias" readonly class="form-control">
                          </div>
                          <div class="col-md-4">
                            <label class="col-md-12">Días</label>
                          </div>
                          <div class="col-md-1">
                            <br>
                          </div>

                          <div class="col-md-5">
                            <label class="col-md-12">Salario diario:</label>
                          </div>
                          <div class="col-md-3">
                            <input type="text" id="salario_dia" readonly class="form-control">
                          </div>
                          <div class="col-md-4">
                            <label class="col-md-12">MNX</label>
                          </div>
                          <div class="col-md-1">
                            <br>
                          </div>


                          <div class="col-md-5">
                            <label class="col-md-12">Trabajadas:</label>
                          </div>
                          <div class="col-md-3">
                            <input type="hidden" id="cant_horas" readonly class="form-control">
                            <input type="text" id="cant_h_m" readonly class="form-control">
                          </div>
                          <div class="col-md-4">
                            <label class="col-md-12">Horas</label>
                          </div>
                          <div class="col-md-1">
                            <br>
                          </div>

                          <div style="display:none;" >
                            <div class="col-md-5">
                              <label class="col-md-12">Trabajados:</label>
                            </div>
                            <div class="col-md-3">
                              <input type="text" id="cant_min" readonly class="form-control">
                            </div>
                            <div class="col-md-4">
                              <label class="col-md-12">Minutos</label>
                            </div>
                            <div class="col-md-1">
                              <br>
                            </div>
                          </div>


                          <div class="col-md-5">
                            <label class="col-md-12">Salario Hora:</label>
                          </div>
                          <div class="col-md-3">
                            <input type="text" id="salario_hora" readonly class="form-control">
                          </div>
                          <div class="col-md-4">
                            <label class="col-md-12">MNX</label>
                          </div>
                          <div class="col-md-1">
                            <br>
                          </div>


                          <div style="display:none;" >
                            <div class="col-md-5">
                              <label class="col-md-12">Salario Minuto:</label>
                            </div>
                            <div class="col-md-3">
                              <input type="text" id="salario_minuto" readonly class="form-control">
                            </div>
                            <div class="col-md-4">
                              <label class="col-md-12">MNX</label>
                            </div>
                            <div class="col-md-1">
                              <br>
                            </div>
                          </div>


                          <div class="col-md-5">
                            <label class="col-md-12">Total de salario ganado:</label>
                          </div>
                          <div class="col-md-3">
                            <input type="text" id="total_salario" readonly class="form-control">
                          </div>
                          <div class="col-md-4">
                            <label class="col-md-12">MNX</label>
                          </div>
                          <div class="col-md-1">
                            <br>
                          </div>

                          <div class="col-md-12">
                            <br>
                          </div>
                        </div>

                        <div class="col-md-1"></div>

                        <div class="col-md-5 div_pagos">
                          <!-- VENTAS DATOS -->
                          <div class="col-md-12">
                            <div class="col-md-12">
                              <br>
                            </div>
                            <div class="col-md-5">
                              <label class="col-md-12">total :</label>
                            </div>
                            <div class="col-md-3">
                              <input type="text" id="cant_prods" readonly class="form-control">
                            </div>
                            <div class="col-md-4">
                              <label class="col-md-12"><button id="det_prods" type="button" class="btn" title="Ver Productos">PRODS</button></label>
                            </div>
                           

                            <div class="col-md-5">
                              <label class="col-md-12">Monto Total:</label>
                            </div>
                            <div class="col-md-3">
                              <input type="text" id="total_ventas" readonly class="form-control">
                            </div>
                            <div class="col-md-4">
                              <label class="col-md-12">MNX</label>
                            </div>
                            <div class="col-md-1">
                              <br>
                            </div>

                            <div class="col-md-5">
                              <label class="col-md-12">Comisión x venta:</label>
                            </div>
                            <div class="col-md-3">
                              <input type="text" id="comision" readonly class="form-control">
                            </div>
                            <div class="col-md-4">
                              <label class="col-md-12">%</label>
                            </div>
                            <div class="col-md-1">
                              <br>
                            </div>

                            <div class="col-md-5">
                              <label class="col-md-12">Total comisión:</label>
                            </div>
                            <div class="col-md-3">
                              <input type="text" id="total_comision" readonly class="form-control">
                            </div>
                            <div class="col-md-4">
                              <label class="col-md-12">mnx</label>
                            </div>
                            <div class="col-md-1">
                              <br>
                            </div>
                          </div>
                          <!-- -->
                        </div>

                      </div>

                      <!-- HORAS EXTRAS -->
                      <div class="col-md-12">
                        <div class="col-md-6 div_pagos extras">
                          <div class="col-md-12">
                            <br>
                          </div>
                          <div class="col-md-5">
                            <label class="col-md-12">total extras:</label>
                          </div>
                          <div class="col-md-3">
                            <input type="text" id="cant_extras" readonly class="form-control">
                          </div>
                          <div class="col-md-4">
                            <label class="col-md-12">Horas</label>
                          </div>
                          <div class="col-md-1">
                            <br>
                          </div>

                          <div class="col-md-5">
                            <label class="col-md-12">Costo x hora:</label>
                          </div>
                          <div class="col-md-3">
                            <input type="text" id="hora_extra" readonly class="form-control">
                          </div>
                          <div class="col-md-4">
                            <label class="col-md-12">MNX</label>
                          </div>
                          <div class="col-md-1">
                            <br>
                          </div>

                          <div class="col-md-5">
                            <label class="col-md-12">Total generado:</label>
                          </div>
                          <div class="col-md-3">
                            <input type="text" id="total_extra" readonly class="form-control">
                          </div>
                          <div class="col-md-4">
                            <label class="col-md-12">MNX</label>
                          </div>
                          <div class="col-md-1">
                            <br>
                          </div>
                        </div>
                        <div class="col-md-1"></div>

                        <!-- TOTAL FINAL -->
                        <div class="col-md-5 div_pagos extras">
                          <div class="col-md-12">
                            <br>
                          </div>

                          <div class="col-md-5">
                            <label class="col-md-12">bonos:</label>
                          </div>
                          <div class="col-md-3">
                            <input type="text" id="total_bonos" readonly class="form-control">
                          </div>
                          <div class="col-md-4">
                            <label class="col-md-12">mnx <button type="button" class="btn" id="ver_bonos"><i class="fa fa-eye" aria-hidden="true"></i> </button></label>
                          </div>
                        
                          <div class="col-md-5">
                            <label class="col-md-12">descuentos:</label>
                          </div>
                          <div class="col-md-3">
                            <input type="text" id="total_descs" readonly class="form-control">
                          </div>
                          <div class="col-md-4">
                            <label class="col-md-12">mnx <button type="button" class="btn" id="ver_descs"><i class="fa fa-eye" aria-hidden="true"></i> </button></label>
                          </div>

                          <div class="col-md-5">
                            <label class="col-md-12">no laborados:</label>
                          </div>
                          <div class="col-md-3">
                            <input type="text" id="total_no_laboral" readonly class="form-control">
                          </div>
                          <div class="col-md-4">
                            <label class="col-md-12">mnx <button type="button" class="btn" id="ver_descs_dias"><i class="fa fa-eye" aria-hidden="true"></i> </button></label>
                          </div>

                          <div class="col-md-5">
                            <label class="col-md-12">total a pagar:</label>
                          </div>
                          <div class="col-md-3">
                            <input type="text" id="total_pagar" readonly class="form-control">
                          </div>
                          <div class="col-md-4">
                            <label class="col-md-12">mnx</label>
                          </div>
                          <div class="col-md-1">
                            <br>
                          </div>
                          <div class="col-md-6">
                            <button type="button" class="btn btn-warning white shadow-z-1-hover" id="generar"><i class="fa fa-file-pdf-o " aria-hidden="true"></i> Generar boleta</button>
                          </div> 
                          <div class="col-md-6">
                            <button type="button" class="btn btn-warning white shadow-z-1-hover" id="confirmar"><i class="fa fa-dollar " aria-hidden="true"></i> Confirmar pago</button>
                          </div> 
                        </div>
                
                      </div>
                      <!-- -->

                      <!-- HORAS DOBLES -->
                      <div class="col-md-12">
                        <div class="col-md-6 div_pagos extras">
                          <div class="col-md-12">
                            <br>
                          </div>
                          <div class="col-md-5">
                            <label class="col-md-12">total dobles:</label>
                          </div>
                          <div class="col-md-3">
                            <input type="text" id="cant_dobles" readonly class="form-control">
                          </div>
                          <div class="col-md-4">
                            <label class="col-md-12">Horas</label>
                          </div>
                          <div class="col-md-1">
                            <br>
                          </div>

                          <div class="col-md-5">
                            <label class="col-md-12">Costo x hora:</label>
                          </div>
                          <div class="col-md-3">
                            <input type="text" id="hora_doble" readonly class="form-control">
                          </div>
                          <div class="col-md-4">
                            <label class="col-md-12">MNX</label>
                          </div>
                          <div class="col-md-1">
                            <br>
                          </div>

                          <div class="col-md-5">
                            <label class="col-md-12">Total generado:</label>
                          </div>
                          <div class="col-md-3">
                            <input type="text" id="total_doble" readonly class="form-control">
                          </div>
                          <div class="col-md-4">
                            <label class="col-md-12">MNX</label>
                          </div>
                          <div class="col-md-1">
                            <br>
                          </div>
                        </div>
                      </div>
                      <!-- -->

                      <!-- HORAS DOBLES -->
                      <div class="col-md-12">
                        <div class="col-md-6 div_pagos extras">
                          <div class="col-md-12">
                            <br>
                          </div>
                          <div class="col-md-5">
                            <label class="col-md-12">total día feriado:</label>
                          </div>
                          <div class="col-md-3">
                            <input type="text" id="cant_feriado" readonly class="form-control">
                          </div>
                          <div class="col-md-4">
                            <label class="col-md-12">Horas</label>
                          </div>
                          <div class="col-md-1">
                            <br>
                          </div>

                          <div class="col-md-5">
                            <label class="col-md-12">Costo x hora:</label>
                          </div>
                          <div class="col-md-3">
                            <input type="text" id="hora_feriado" readonly class="form-control">
                          </div>
                          <div class="col-md-4">
                            <label class="col-md-12">MNX</label>
                          </div>
                          <div class="col-md-1">
                            <br>
                          </div>

                          <div class="col-md-5">
                            <label class="col-md-12">Total generado:</label>
                          </div>
                          <div class="col-md-3">
                            <input type="text" id="total_feriado" readonly class="form-control">
                          </div>
                          <div class="col-md-4">
                            <label class="col-md-12">MNX</label>
                          </div>
                          <div class="col-md-1">
                            <br>
                          </div>
                        </div>
                      </div>
                      <!-- -->

                    </form>
                                       
                  </div>
                  <!--------//////////////-------->
              </div>
              
          </div>
      </div>
  </div>
</div>

<div class="modal fade text-left" id="modal_detalles_prods" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="false" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Detalle de Productos</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
              <div class="cont_tabla">

              </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade text-left" id="modal_bonos_descs" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="false" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Detalle de registros</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
              <div class="cont_tabla_bonos_descs">

              </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade text-left" id="modal_no_laboral" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="false" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Detalle de registros</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
              <div class="cont_tabla_no_laboral">

              </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Aceptar</button>
      </div>
    </div>
  </div>
</div>