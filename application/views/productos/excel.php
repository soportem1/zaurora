<?php

header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=reporte_aurora:_".$fecha.".xls");
?>
<table >
    <thead>
      <tr>    
        <th>Zapateria Aurora</th>             
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Fecha de reporte: <?php echo $fecha ?></td>
      </tr>
      <tr>
        <td>Productos</td>
      </tr>
    </tbody>
</table>

<table>
  <tr>
    <td>
          <table border="1px" >
              <thead>
                <tr>
                  <th colspan="11">Productos</th> 
                  <th></th> 
                  <th colspan="5">Matriz</th> 
                </tr>  
                <tr><!--16 column-->   
                  <th>codigo</th>             
                  <th>nombre</th>
                  <th>preciocompra</th>
                  <th>gananciaefectivo</th>
                  <th>porcentaje</th>
                  <th>precioventa</th>
                  <th>mayoreo</th>
                  <th>canmayoreo</th>
                  <th>fecharegistro</th>
                  <th>iddepartamento</th>
                  <th>tipo_ganancia</th>
                  <th></th>
                  <th>precio_venta</th>             
                  <th>mayoreo</th>
                  <th>cuantos</th>
                  <th>existencia</th>
                  <th>minimo</th>
                </tr>
              </thead>
              <tbody>
                 <?php foreach ($producto->result() as $item){ ?>
                 <tr>
                      <td><?php echo $item->codigo; ?></td>
                      <td><?php echo $item->nombre; ?></td>
                      <td><?php echo $item->preciocompra; ?></td>
                      <td><?php echo $item->gananciaefectivo; ?></td>
                      <td><?php echo $item->porcentaje ?></td>
                      <td><?php echo $item->precioventa ?></td>
                      <td><?php echo $item->mayoreo ?></td>
                      <td><?php echo $item->canmayoreo ?></td>
                      <td><?php echo $item->fecharegistro ?></td>
                      <td><?php echo $item->iddepartamento ?></td>
                      <td><?php echo $item->tipo_ganancia ?></td>
                      <td></td>
                      <td><?php echo $item->precio_s1 ?></td>
                      <td><?php echo $item->mayoreo_s1 ?></td>
                      <td><?php echo $item->cuantos_s1 ?></td>
                      <td><?php echo $item->existencia_s1 ?></td>
                      <td><?php echo $item->minimo_s1 ?></td>
                </tr>
                <?php } ?>
              </tbody>
          </table>
    </td>
    <td>
          <table border="1px" >
              <thead>
                <tr>
                  <th></th> 
                  <th colspan="5">Sucursal 2</th> 
                </tr> 
                <tr>  
                  <th></th>  
                  <th>precio_venta</th>             
                  <th>mayoreo</th>
                  <th>cuantos</th>
                  <th>existencia</th>
                  <th>minimo</th>
                </tr>
              </thead>
              <tbody>

                 <?php foreach ($sucursal2->result() as $item){ ?>
                 <tr>
                      <td></td>
                      <td><?php echo $item->precio_venta ?></td>
                      <td><?php echo $item->mayoreo ?></td>
                      <td><?php echo $item->cuantos ?></td>
                      <td><?php echo $item->existencia ?></td>
                      <td><?php echo $item->minimo ?></td>
                </tr>
                <?php } ?>
              </tbody>
          </table>
    </td>
    <td>
          <table border="1px" >
              <thead>
                <tr>
                  <th></th>
                  <th colspan="5">Sucursal 3</th> 
                </tr> 
                <tr> 
                  <th></th>   
                  <th>precio_venta</th>             
                  <th>mayoreo</th>
                  <th>cuantos</th>
                  <th>existencia</th>
                  <th>minimo</th>
                </tr>
              </thead>
              <tbody>

                 <?php foreach ($sucursal3->result() as $item){ ?>
                 <tr>
                      <td></td>
                      <td><?php echo $item->precio_venta ?></td>
                      <td><?php echo $item->mayoreo ?></td>
                      <td><?php echo $item->cuantos ?></td>
                      <td><?php echo $item->existencia ?></td>
                      <td><?php echo $item->minimo ?></td>
                </tr>
                <?php } ?>
              </tbody>
          </table>
    </td>
  </tr>
</table>
        