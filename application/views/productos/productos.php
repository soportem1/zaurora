<style type="text/css">
  .imgpro{
    width: 80px;
  }
  .imgpro:hover {
    transform: scale(2.6); 
    filter: drop-shadow(5px 9px 12px #444);
  }
  .border_t{
    border-top: 5px solid #ecd540;
    padding: 11px 0;
    margin-top: -27px;
  }
  #data-tables td{
    font-size: 14px;
  }
  .buttonsearch{
    position: absolute;
    right: 19px;top: 6px;
  }
  .printcolor{ background-color:#d0a41f9e; }
</style>
<div class="row">
    <div class="col-md-12">
        <h2>Producto</h2>
    </div>
    <?php if($this->session->userdata("almacen")!=1){ ?>
        <div class="col-md-12">
            <div class="col-md-2 navbar-right">
                <a class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow" onclick="modal_excel()"><i class=""></i> Importar Excel</a>
            </div>

            <div class="col-md-2">
                <a  href="<?php echo base_url(); ?>Productos/excel" class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow"><i class=""></i> Exportar a Excel</a>
            </div>

            <div class="col-md-3"></div>
            <div class="col-md-2">
                <a  href="#" id="checkselect" class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow"><i class=""></i> Eliminar Selección</a>
            </div>
            <div class="col-md-2">
                <a href="<?php echo base_url(); ?>Productos/productosadd" class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow"><i class="fa fa-plus"></i> Nuevo</a>
                <!--
                <button class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow" onclick="productosall()"><i class="fa fa-print"></i></button>
              -->
            </div>
        </div>
    <?php } ?>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Listado de productos</h4>
            </div>
            <div class="col-md-12 border_t">
          
            <div class="col-md-2">
              <div class="position-relative has-icon-right">
                    <select class="form-control" id="sucu" name="sucu" hidden="" onchange="buscarproductos()">
                      <option value="0">Selección Sucursal</option>
                      <?php foreach ($sucursal as $item ){?>
                      <option value="<?php echo $item->idsucursal ?>" ><?php echo $item->nombre; ?></option>
                      <?php } ?>
                    </select>  
              </div>     
            </div>
            <div class="col-md-2">
              <div class="position-relative has-icon-right">
                    <select class="form-control" id="depa" name="depa" hidden="" onchange="buscarproductod()">
                      <option value="0">Selección Socio</option>
                      <?php foreach ($departamento as $item ){?>
                      <option value="<?php echo $item->iddepartamento ?>" ><?php echo $item->nombre; ?></option>
                      <?php } ?>
                    </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="position-relative has-icon-right">
                    <select class="form-control" id="tipoProd" name="tipoProd" onchange="loadTableTipo()">
                      <option value="0">Selección Tipo:</option>
                      <option value="Producto Favorito">Producto Favorito</option>
                      <option value="Lista Negra">Lista Negra</option>
                    </select>  
              </div>     
            </div>
            <div class="col-md-3">
              <div class="position-relative has-icon-right">
                    <select class="form-control" id="sucurs" name="sucurs">
                      <option value="0" selected="" disabled="">Selección Sucursal</option>
                      <?php foreach ($sucursal as $item ){?>
                      <option value="<?php echo $item->idsucursal ?>" ><?php echo $item->nombre; ?></option>
                      <?php } ?>
                    </select>  
              </div>     
            </div>
            <div class="col-md-2">
              <div class="position-relative has-icon-right">
                    <select class="form-control" id="tipostock" disabled="" name="tipostock" onchange="buscarproductost()">
                      <option value="0">Selección Stock:</option>
                        <!--<option value="1">Máximo</option>-->
                        <option value="2">Minimo</option>
                    </select>
              </div>
            </div>
            <div class="col-md-3"></div> 
            </div>
            <div class="card-body">
                <div class="card-block table-prods" style="overflow: auto;">
                    <!--------//////////////-------->
                    <!--<table class="table table-striped" id="data-tables" style="width: 100%">-->
                    <table class="table table-striped responsive" id="data-tables" style="font-size: 12px; width: 100%">
                        <thead>
                            <tr>
                                <th><input disabled type="checkbox" id="chckHead" /></th>
                                <th>Acciones</th>                                
                                <th>Codigo</th>
                                <th>Descripción</th>
                                <th>Sucursal</th>                                  
                                <th>Precio venta</th>
                                  
                                <!--<th>mayoreo</th>
                                <th>cuantos</th>-->
                                <th>Stock</th>
                                <!--<th>Minimo</th>-->
                                <th>Sucursal</th>  
                                <th>Precio venta</th>
                                  
                                <!--th>mayoreo</th>
                                <th>cuantos</th>-->
                                <th>Stock</th>
                                <!--<th>Minimo</th>-->
                                <th>Sucursal</th>  
                                <th>Precio venta</th>
                                  
                                <!--<th>mayoreo</th>
                                <th>cuantos</th>-->
                                <th>Stock</th>
                                <!--<th>Minimo</th>-->
                                <th>Departamento</th>
                                <th>Fecha de Registro</th>
                                <th>#</th>
                                <th>Tipo</th>
                            </tr>
                        </thead>
                        <tbody id="tbodyresultadospro">    
                        </tbody>
                    </table>    
                </div>
            </div>
        </div>
    </div>
</div>

<!------------------------------------------------>
<style type="text/css">
  iframe{
        width: 100%;
        height: 300px;
        border: 0;
  }
</style>
<div class="modal fade text-left" id="modaletiquetas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Etiquetas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="col-md-6">
                <div class="col-md-12">
                  <p>Codigo de producto: <b><span id="ecodigo"></span></b> </p>
                </div>
                <div class="col-md-12">
                  <p>Producto: <b><span id="eproducto"></span></b></p>
                </div>
                <div class="col-md-12">
                  <label>Pares</label>
                  <textarea id="pares" class="form-control"></textarea>
                </div>
                <div class="col-md-12">
                  <label>Tipo</label>
                  <select class="form-control" id="tipo">
                    <option value="">Ninguno</option>
                    <option>Piel</option>
                    <option>Forro Piel</option>
                    <option>Piel/Clasico</option>
                    <option>Punta de piel</option>
                    <option>Luces</option>
                    <option>Cosido</option>
                    <option>Piel/Cosido</option>
                  </select>
                </div>
                
                
                

                

                <input type="hidden" name="idproetiqueta" id="idproetiqueta" readonly>
                <div class="col-md-12">
                  
                    <div class=" col-md-6">
                        <input type="number" name="numprint" id="numprint" class="form-control" min="1" value="1">
                    </div>
                    <div class=" col-md-5">
                      <a href="#" class="btn btn-raised gradient-purple-bliss white" id="imprimiretiqueta">Imprimir</a>
                    </div>
                  
                </div>

              </div>
              <div class="col-md-6" id="iframeetiqueta">
                
              </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="eliminacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmaci&oacute;n</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Desea <b>Eliminar</b> el producto ?
                      <input type="hidden" id="hddIdpro">
            </div>
            <div class="modal-footer">
                <button type="button" id="sieliminar" class="btn btn-raised gradient-purple-bliss white" data-dismiss="modal">Aceptar</button>
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="modalproductos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Productos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--<div class="modal-body">-->
              <div class="col-md-12" id="iframeproductos">
                
              </div>

           <!-- </div>-->
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="modalSucursales" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Sucursales</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <table class="table table-striped responsive" id="data-tables">
                                      <thead>
                                        <tr>
                                          <th>#</th>
                                          <th>Sucursal</th>
                                          <th>Precio venta</th>
                                          <th>Precio mayoreo</th>
                                          <th>A partir de cuentos pares</th>
                                          <th>Existencia</th>
                                          <th>Ventas</th>
                                          <th>Mínimo</th>
                                        </tr>
                                      </thead>
                                      <tbody id="tbodysucursal">
                                      </tbody>
                                </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-- Importr  a Excel -->

<div class="modal fade text-left" id="importar_excel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Importar Excel</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="col-md-12">
                <input type="file" class="form-control cargaarchivo exampleInputFile" name="excel" id="exampleInputFile">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
