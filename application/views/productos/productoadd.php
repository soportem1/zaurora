<?php
if (isset($_GET['id'])) {
  $id=$_GET['id'];
  $personalv=$this->ModeloProductos->getproducto($id);
  foreach ($personalv->result() as $item){
    $label='Editar Producto';
    $id = $item->productoid;
    $codigo = $item->codigo;
    $productofiscal = $item->productofiscal;
    $nombre = $item->nombre;
    $descripcion = $item->descripcion;
    $categoria = $item->categoria;
    $stock = $item->stock;
    $preciocompra = $item->preciocompra;
    $gananciaefectivo = $item->gananciaefectivo;
    $porcentaje = $item->porcentaje;
    $precioventa = $item->precioventa;
    $mediomayoreo = $item->mediomayoreo;
    $canmediomayoreo = $item->canmediomayoreo;
    $mayoreo = $item->mayoreo;
    $canmayoreo = $item->canmayoreo;
    $fecharegistro =$item->fecharegistro;
    $iddepartamento =$item->iddepartamento;
    $tipo_prod =$item->tipo_prod;
    $notas=$item->notas;
    $img = '<img src="'.base_url().''.$item->img.'">' ;
  }
}else{
  $label='Nuevo producto';
  $id=0;
  $productoid='';
  $codigo='';
  $productofiscal='';
  $nombre='';
  $descripcion='';
  $categoria='';
  $stock='';
  $preciocompra='';
  $gananciaefectivo='';
  $porcentaje=0;
  $precioventa='';
  $mediomayoreo='';
  $canmediomayoreo='';
  $mayoreo='';
  $canmayoreo='';
  $tipo_prod='';
  $notas="";
  //$fecharegistro='';
  $iddepartamento='';
  $img='';
}
?>
<style type="text/css">
  .vd_red{
    font-weight: bold;
    color: red;
    margin-bottom: 5px;
  }
  .vd_green{
    color: #009688;
  }
  input,select,textarea{
    margin-bottom: 15px;
  }
  .border_p{
    border-top: 5px solid #ecd540;
    padding: 11px 0;
    margin-top: 0px;
  }
  .border_t{
    border-top: 5px solid #ecd540;
    padding: 11px 0;
    margin-top: -27px;
  }
  .border_b{
    border-top: 1px solid #ecd540;
    padding: 11px 0;
    margin-top: 0px;
  }

</style>
<div class="row">
  <div class="col-md-12">
    <h2>Producto <i class="fa fa-barcode"></i></h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title"><?php echo $label;?></h4>
      </div>
      <div class="card-body">
        <div class="card-block form-horizontal">
          <!--------//////////////-------->
          <div class="row">
            <form method="post"  role="form" id="formproductos">
              <input type="hidden" name="productoid" id="productoid" value="<?php echo $id;?>">
              <div class="col-md-12 border_t">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-md-2 control-label">Codigo:</label>
                    <div class=" col-md-3">
                      <input type="text" class="form-control" name="pcodigo" id="pcodigo" value="<?php echo $codigo;?>" aria-describedby="button-addon2" autofocus>
                    </div>
                    <div class="col-md-1">
                      <a href="javascript:void(0)" type="hidden"></a>
                    </div>
                    <div class="col-md-2">
                      <a href="#" class="btn btn-warning white" id="generacodigo" >Generar</a>
                    </div>
                    <label class="col-md-2 control-label">Fecha de registro:</label>
                    <div class=" col-md-3">
                      <input type="date" class="form-control" name="fecharegistro" id="fecharegistro" value="<?php echo $fecharegistro;?>">
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-md-2 control-label">Descripción:</label>
                    <div class=" col-md-4 ">
                      <input type="text" class="form-control" id="productoname" name="productoname" value="<?php echo $nombre;?>">
                    </div>
                    <div class="col-md-1"></div>
                    <label class="col-md-2 control-label">Socios:</label>
                    <div class=" col-md-3 ">
                      <select class="form-control" id="iddepartamento" name="iddepartamento">
                        <?php foreach ($departamento as $item){ ?>
                        <option value="<?php echo $item->iddepartamento; ?>" <?php if ($item->iddepartamento==$iddepartamento) { echo 'selected';} ?>    ><?php echo $item->nombre; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
                <!--
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-md-2 control-label">Descripcion:</label>
                    <div class=" col-md-10 ">
                      <textarea class="form-control" id="pdescripcion" name="pdescripcion"><?php  $descripcion;?></textarea>
                    </div>
                  </div>
                </div>
                -->
                <!--
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-md-2 control-label">Stock:</label>
                    <div class=" col-md-10">
                      <input type="number" class="form-control" id="pstock" name="pstock" value="<?php  $stock;?>" placeholder="unidades">
                    </div>
                  </div>
                </div>
              </div>
              -->
              <!--
              <div class="col-md-5">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                  <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px; border: solid #a6a9ae 1px; border-radius: 12px;">
                    <?php  $img;?>
                  </div>
                  <div>
                    <span class="btn btn-warning white btn-file">
                      <span class="fileinput-new">Foto</span>
                      <span class="fileinput-exists">Cambiar</span>
                      <input type="file" name="imgpro" id="imgpro" data-allowed-file-extensions='["jpg", "png"]'>
                    </span>
                    <a href="#" class="btn btn-warning white fileinput-exists" data-dismiss="fileinput">Quitar</a>
                  </div>
                </div>
              </div>
              -->
              
              <!--
              <h3>Precios</h3>
              <hr />
              -->
              <div class="col-md-12">
                <div class="form-group">
                  <?php if($this->session->userdata("perfilid_tz")==1){ ?>
                    <label class="col-md-2 control-label">Precio Compra:</label>
                    <div class=" col-md-2">
                      <input type="number" name="preciocompra" id="preciocompra" class="form-control" value="<?php echo $preciocompra;?>" placeholder="">
                    </div>
                  <?php } ?>
                  <label class="col-md-2 control-label">Ganancia:</label>
                  <div class=" col-md-3">
                    <select class="form-control" id="tipo_ganancia" name="tipo_ganancia" onchange="ganancigana()">
                      <option value="1">Efectivo</option>
                      <option value="2">Porcentaje</option>
                    </select>
                  </div>
                  <div class="efectivo_v">
                    <div class=" col-md-2">
                      <input type="number" class="form-control" name="gananciaefectivo" id="gananciaefectivo"  aria-describedby="button-addon2" onchange="sumaventa();" value="<?php echo $gananciaefectivo;?>">
                    </div>
                  </div>
                  <div class="porcentaje_v" style="display: none;">
                    <div class=" col-md-1" style="width: 47px;">
                      <button class="btn btn-raised">%</button>
                    </div>
                    <div class=" col-md-2">
                      <input type="number" class="form-control" name="porcentaje" id="porcentaje"  aria-describedby="button-addon2" onchange="calcular();" value="<?php echo $porcentaje;?>">
                    </div>
                  </div>
                  
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-md-2 control-label">Precio venta:</label>
                  <div class=" col-md-2">
                    <input type="number" class="form-control" name="precioventa" id="precioventa" value="<?php echo $precioventa;?>" placeholder="">
                  </div>
                  <label class="col-md-2 control-label">Precio oferta:</label>
                  <div class=" col-md-2">
                    <input type="number" class="form-control" name="preciomayoreo" id="preciomayoreo" value="<?php echo $mayoreo;?>" placeholder="">
                  </div>
                  <label class="col-md-2 control-label">Cantidad:</label>
                  <div class=" col-md-2">
                    <input type="number" class="form-control" name="cpmayoreo" id="cpmayoreo" value="<?php echo $canmayoreo;?>" placeholder="a partir de #">
                  </div>
                </div>
                <label class="col-md-2 control-label">Tipo Producto:</label>
                <div class=" col-md-3 ">
                  <select class="form-control" id="tipo_prod" name="tipo_prod">
                    <option value="0">Elige:</option>
                    <option value="Producto Favorito"<?php if($tipo_prod=="Producto Favorito") { echo 'selected';} ?> >Producto Favorito</option>
                    <option value="Lista Negra"<?php if($tipo_prod=="Lista Negra") { echo 'selected';} ?> >Lista Negra</option>
                  </select>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-md-2 control-label">Notas:</label>
                  <div class=" col-md-12">
                    <textarea rows="6" class="form-control" name="notas" id="notas" value="<?php echo $notas;?>"><?php echo $notas;?></textarea>
                  </div>
                </div>
              </div>
              
              <h4 class="card-title">Precios</h4>
              <div class="col-md-12 border_p">
                <div class="form-group">
                  <label class="col-md-2 control-label">Precio venta:</label>
                  <div class=" col-md-2">
                    <input type="checkbox" id="test9"  onclick="checksucursal()">
                  </div>
                </div>  
              </div>

              <!--
              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-md-2 control-label">Precio medio mayoreo:</label>
                  <div class=" col-md-3">
                    <input type="number" class="form-control" name="preciommayoreo" id="preciommayoreo" placeholder="" value="<?php  $mediomayoreo;?>">
                  </div>
                  <div class=" col-md-2 ">
                    
                    <input type="number" class="form-control" name="cpmmayoreo" id="cpmmayoreo" value="<?php  $canmediomayoreo;?>"  placeholder="a partir de # de unidades">
                  </div>
                </div>
              </div>
              -->
              <!--
              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-md-2 control-label">Precio mayoreo:</label>
                  <div class=" col-md-3">
                    <input type="number" class="form-control" name="preciomayoreo" id="preciomayoreo" value="<?php  $mayoreo;?>" placeholder="">
                  </div>
                  <div class=" col-md-2">
                    <input type="number" class="form-control" name="cpmayoreo" id="cpmayoreo" value="<?php  $canmayoreo;?>" placeholder="a partir de # de unidades">
                  </div>
                </div>
              </div>
              -->
             </div>
          </form>
          <form method="post" class="col-md-12"  role="form" id="">
            <div class="">
              <div class="">
                <table id="tablesucursales" style="width: 100%;">
                  <tbody> 
                  <?php foreach ($sucursal as $item) {
                       $data = $this->ModeloGeneral->producto_existencia($id,$item->idsucursal);
                  ?>
                   
                    <tr><td>
                      
                        <input type="hidden" id="idproductosucursal" name="idproductosucursal" value="<?php echo $data['idproductosucursal']; ?>" disabled="">
                        <div class="border_b">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="col-md-2 control-label">Sucursal:</label>
                            <div class=" col-md-2">
                              <input type="hidden" id="idsucursal_s" name="idsucursal_s" value="<?php echo $item->idsucursal ?>" disabled="">
                              <input type="text" class="form-control" value="<?php echo $item->nombre?>" disabled="">
                            </div>
                            <label class="col-md-2 control-label">Precio venta:</label>
                            <div class=" col-md-2">
                              <input type="number" class="form-control precio_venta_s" name="precio_venta_s" id="precio_venta_s" value="<?php echo $data['precio_venta'];?>">
                            </div>
                            <label class="col-md-2 control-label">Precio oferta:</label>
                            <div class=" col-md-2">
                              <input type="number" class="form-control mayoreo_s" name="mayoreo_s" id="mayoreo_s" value="<?php echo $data['mayoreo'];?>">
                            </div>
                          </div> 
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">  
                            <label class="col-md-2 control-label">Aplica:</label>
                            <div class=" col-md-2">
                              <!--<input type="number" class="form-control cuantos_s" name="cuantos_s" id="cuantos_s" value="<?php echo $data['cuantos'];?>">-->
                              <input type="checkbox" class="form-control cuantos_s" name="cuantos_s" id="cuantos_s" value="0" <?php if($data['cuantos']==1){echo "checked";} ?>>
                            </div>
                            <label class="col-md-2 control-label">Existencia:</label>
                            <div class=" col-md-2">
                              <input type="number" class="form-control" name="existencia_s" id="existencia_s" value="<?php echo $data['existencia'];?>" placeholder="">
                            </div>
                            <label class="col-md-2 control-label" style="display: none;">Mínimo:</label>
                            <div class=" col-md-2" style="display: none;">
                              <input type="number" class="form-control" name="minimo_s" id="minimo_s" value="<?php echo $data['minimo'];?>" placeholder="">
                            </div>
                          </div>
                        </div>
                        </div>
                      </td> 
                    </tr>

                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </form>  
           
          <div class="col-md-12">
            <?php if($this->session->userdata("almacen")!="1"){ ?>
              <button class="btn btn-warning white shadow-z-1-hover"  type="submit"  id="savepr">Guardar</button>
            <?php } else{ ?>
              <a href="<?php echo base_url() ?>Productos" class="btn btn-primary white fileinput-exists"><i class="fa fa-chevron-left" aria-hidden="true"></i> Regresar</a>
            <?php } ?>
          </div>
        </div>
        <!--------//////////////-------->
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  <?php
    if($this->session->userdata("almacen")==1){
      echo '$("input,select,textarea").attr("disabled",true)';
    }
  ?>

  $("#formproductos").keypress(function(e) {
      if (e.which == 13) {
          return false;
      }
  });
$('#generacodigo').click(function(event) {
  var d = new Date();
  var dia=d.getDate();//1 31
  var dias=d.getDay();//0 6
  var mes = d.getMonth();//0 11
  var yy = d.getFullYear();//9999
  var hr = d.getHours();//0 24
  var min = d.getMinutes();//0 59
  var seg = d.getSeconds();//0 59
  var yyy = 18;
  var ram = Math.floor((Math.random() * 10) + 1);
  var codigo=seg+''+min+''+hr+''+yyy+''+ram+''+mes+''+dia+''+dias;
  //var condigo0=condigo.substr(0,13);
  //console.log(codigo);
  $('#pcodigo').val(codigo);
  });
});
</script>