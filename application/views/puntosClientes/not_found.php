<style>
  .background-image {
    height: 100vh;
    width: 100vw;
    background-size: cover;
    background-position: center bottom;
    background-repeat: no-repeat;
  }

  .footer {
    height: 5vh;
    margin-top: -5vh; 
    width: 100vw;
    z-index: 100 !important;
  }
</style>


<div class="d-flex background-image flex-row" style="background-image: url(<?php echo base_url(); ?>public/img/fondo_bg.png);">
  <div style="background: #ffffffd9; border-radius: 16px; margin: auto; box-shadow: 20px 25px #A78142;">
    <div class="text-center overflow-hidden py-2 px-3">
      <div class="row">
        <div class="col-md-12">
          <h1 style="color: #00319e;"><img style="margin-top: 4%;" src="<?php echo base_url(); ?>public/img/ops.png" width="280"></h1>
          <br>
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-12" align="center">
          <h1 style="color: black;">Ooops!</h1>
          <h4 style="color: black;">Ocurrió un error durante la consulta de puntos.</h4>
          <h4 style="color: black;">Por favor verifique su número e intente de nuevo.</h4>
          <br>
          <h5 style="color: black;">Agradecemos tu preferencia.</h5>
          <br>
        </div>
      </div>
      <div class="text-center">
        <button type="button" id="btn_return" class="btn btn-raised white shadow-big-navbar" style="background-image: linear-gradient(45deg, #ff992f, #d2dd24);" >Nueva consulta</button>
      </div>
    </div>
  </div>


</div>