<!DOCTYPE html>
<html lang="en" class="loading">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="author" content="Mangoo">
        <title>Aurora</title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>public/img/ops.png">
        <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>public/img/ops.png">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="default">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900|Montserrat:300,400,500,600,700,800,900" rel="stylesheet">
        <!-- BEGIN VENDOR CSS-->
        <!-- font icons-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/feather/style.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/simple-line-icons/style.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/perfect-scrollbar.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/prism.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/sweetalert2.min.css">
        <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/chartist.min.css">-->
        <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/tables/datatable/datatables.min.css">-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/toastr.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/chosen.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/balloon.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/pickadate/pickadate.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/formValidation.min3f0d.css?v2.2.0">
        <!-- END VENDOR CSS-->
        <!-- BEGIN APEX CSS-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/app.css">  
        <script src="<?php echo base_url(); ?>app-assets/vendors/js/core/jquery-3.2.1.min.js" type="text/javascript"></script>
        <!--<script src="<?php echo base_url(); ?>app-assets/vendors/js/datatable/datatables.min.js" type="text/javascript"></script>-->
        <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/theme.css">-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/select2.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/datatablex/datatables.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/datatablex/responsive.bootstrap4.min.css">
        <script src="<?php echo base_url(); ?>public/plugins/datatablex/jquery.datatables.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/plugins/datatablex/datatables.responsive.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/plugins/datatablex/datatables.bootstrap4.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/plugins/datatablex/responsive.bootstrap4.min.js" type="text/javascript"></script>
        <!--<link rel="stylesheet" type="text/css" src="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" src="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">-->
        

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/spectrum.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/fullcalendar.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/pickadate/pickadate.css">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">

        <!-- END APEX CSS-->
        <!-- BEGIN Page Level CSS-->
        <!-- END Page Level CSS-->
        <!-- BEGIN Custom CSS-->
        <!-- END Custom CSS-->
        <!--
        <input type="hidden" name="ssessius" id="ssessius" value="<?php echo $_SESSION['perfilid_tz'];?>" readonly>
        <input type="hidden" id="alm" value="<?php echo $this->session->userdata("almacen"); ?>">
        -->
        <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
    </head>
    <style type="text/css">
        .fc-event-container .fc-day-grid-event .fc-h-event .fc-event .fc-start .fc-end{
          background-color: #cfa520 !important;
          background: #cfa520 !important;
        }
    
        .fc-button{
            border-color: #cfa520 !important;
            background-color: #cfa520 !important;
        }

        .div_pagos{
            border-color: #cfa520 !important;
            border-width: 1px !important; 
            border-style: solid;
            border-radius: 3px !important;
        }

        .extras{
            margin-top: 6px;
        }
        .desp{
            margin-left: 2px;
        }
        .switch {
          position: relative;
          display: inline-block;
          width: 60px;
          height: 34px;
        }

        .switch input { 
          opacity: 0;
          width: 0;
          height: 0;
        }

        .slider {
          position: absolute;
          cursor: pointer;
          top: 0;
          left: 0;
          right: 0;
          bottom: 0;
          background-color: #ccc;
          -webkit-transition: .4s;
          transition: .4s;
        }

        .slider:before {
          position: absolute;
          content: "";
          height: 26px;
          width: 26px;
          left: 4px;
          bottom: 4px;
          background-color: white;
          -webkit-transition: .4s;
          transition: .4s;
        }

        input:checked + .slider {
          background-color: #cfa520;
        }

        input:focus + .slider {
          box-shadow: 0 0 1px #cfa520;
        }

        input:checked + .slider:before {
          -webkit-transform: translateX(26px);
          -ms-transform: translateX(26px);
          transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
          border-radius: 34px;
        }

        .slider.round:before {
          border-radius: 50%;
        }

    </style>
    <body data-col="2-columns" class=" 2-columns ">
        <!-- ////////////////////////////////////////////////////////////////////////////-->
            <div class="wrapper nav-collapsed menu-collapsed">