<style>
  .background-image {
    height: 100vh;
    width: 100vw;
    background-size: cover;
    background-position: center bottom;
    background-repeat: no-repeat;
  }

  .footer {
    height: 5vh;
    margin-top: -5vh;
    width: 100vw;
    z-index: 100 !important;
  }

  .vd_red {
    font-size: 1.2em;
    color: #e41818;
    margin-bottom: 1.8vh;
  }

</style>


<div class="d-flex background-image flex-row" style="background-image: url(<?php echo base_url(); ?>public/img/fondo_bg.png);">
  <div style="background: #ffffffd9; border-radius: 16px; margin: auto; box-shadow: 20px 25px #A78142;">
    <div class="text-center overflow-hidden py-2 px-3">
      <div class="row">
        <div class="col-md-12">
          <h1 style="color: #00319e;"><img style="margin-top: 4%;" src="<?php echo base_url(); ?>public/img/ops.png" width="280"></h1>
          <br>
          <!--<h3 style="color: black;">Consulta tus puntos Aurora ingresando el número de teléfono registrado.</h3>-->
          <h3 style="color: black;">Consulta los puntos acumulados por tus compras en Zapaterías Aurora.</h3>
          <h3 style="color: black;">Agradecemos tu preferencia.</h3>
          <br>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12" align="center">

          <form class="form-horizontal" id="form_data" action="#" role="form">
            <div class="col-md-2">
            </div>
            <div class="col-md-8" align="center">
              <div class="col-md-12" align="center">
                <div class="form-group input-group vd_input-wrapper">
                  <span class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </span>
                  <input type="number" placeholder="Teléfono" id="num_tel" name="num_tel" class="form-control" required oninput="this.value = this.value.slice(0, 10)">
                </div>
                <div class="text-center">
                  <button type="submit" id="btn_submit" class="btn btn-raised white shadow-big-navbar" style="background-image: linear-gradient(45deg, #ff992f, #d2dd24);">Consultar puntos</button>
                </div>
              </div>
            </div>
            <div class="col-md-2">
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>


</div>