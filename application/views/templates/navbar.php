<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->helper('url');
$sturno=$this->ModeloVentas->turnos();
if (!isset($_SESSION['idpersonal_tz'])) {
    $perfil=0;
}else{
    $perfil=$_SESSION['idpersonal_tz'];  
}
if(!isset($_SESSION['usuario_tz'])){
    ?>
    <script>
        document.location="<?php echo base_url(); ?>index.php/Login"
    </script>
    <?php 
}

$menu=$this->ModeloSession->menus($perfil);
?>
<!-- main menu-->
<!--.main-menu(class="#{menuColor} #{menuOpenType}", class=(menuShadow == true ? 'menu-shadow' : ''))-->
<div ddata-active-color="white" data-background-color="amarrillo" data-image="<?php echo base_url(); ?>public/img/fondo-bg.jpg" style="background: url(<?php echo base_url(); ?>public/img/fondo-bg.jpg);" class="app-sidebar">
<!-- main menu header-->
<!-- Sidebar Header starts-->
<div class="sidebar-header">
  <div class="logo clearfix" style="background: white;">
      <a href="<?php echo base_url(); ?>Inicio" class="logo-text float-left">
        <div class="logo-img">
        </div>
        <span class="text pull-left">
          <img src="<?php echo base_url(); ?>public/img/ops.png" style="width: 149px;" >
        </span>
      </a>
      <a id="sidebarToggle" href="javascript:;" class="nav-toggle d-none d-sm-none d-md-none d-lg-block" style="color: #d07731;"><i data-toggle="expanded" class="ft-toggle-right toggle-icon"></i></a><a id="sidebarClose" href="javascript:;" class="nav-close d-block d-md-block d-lg-none d-xl-none"><i class="ft-x"></i></a></div>
</div>
<!-- Sidebar Header Ends-->
<!-- / main menu header-->
<!-- main menu content-->
<div class="sidebar-content">
  <div class="nav-container">
    <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
          <li class="nav-item"><a href="#" data-target='#fnotas' data-toggle="modal"><i class="fa fa-pencil"></i><span data-i18n="" class="menu-title">Notas</span></li></a>
        <?php foreach ($menu->result() as $item){ ?>
          <li class="has-sub nav-item"><a href="#"><i class="<?php echo $item->Icon; ?>"></i><span data-i18n="" class="menu-title"><?php echo $item->Nombre; ?></span></a>
            <ul class="menu-content">
                <?php 
                    $perfil=$perfil;
                    $menu =  $item->MenuId;
                    $menusub = $this->ModeloSession->submenus($perfil,$menu);
                    foreach ($menusub->result() as $datos) { ?>
                        <li>
                            <a href="<?php echo base_url(); ?><?php echo $datos->Pagina; ?>" class="menu-item">
                                <i class="<?php echo $datos->Icon; ?>"></i>
                                <?php echo $datos->Nombre; ?>
                            </a>
                        </li>
                <?php } ?> 
            </ul>
          </li>
        <?php } ?>
    </ul>
  </div>
</div>
<!-- main menu content-->
<div class="sidebar-background"></div>
<!-- main menu footer-->
<!-- include includes/menu-footer-->
<!-- main menu footer-->
<?php 
$notas = $this->ModeloSession->notas();
foreach ($notas->result() as $notasf) { 
  $nmensaje=$notasf->mensaje;
  $nuser=$notasf->usuario;
  $nactualizado=$notasf->reg;
  
}?>
<div class="modal fade text-left" id="fnotas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Notas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12">
                  <textarea id="notass" data-rel="ckeditor" rows="3"><?php echo $nmensaje;?></textarea>
                </div>
                <div class="col-md-12">
                  <p><b>Actualizado: </b><?php echo $nuser;?> / <?php echo $nactualizado;?></p>
                </div>
              </div>
              <input type="type" id="notasedit2" readonly hidden value="<?php echo $_SESSION['usuario_tz']; ?>">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-raised gradient-green-tea white" data-dismiss="modal" id="updatenotas">Actualizar</button>
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
</div>
<!-- / main menu-->

<!-- Navbar (Header) Starts-->
      <nav class="navbar navbar-expand-lg navbar-light bg-faded" >
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" data-toggle="collapse" class="navbar-toggle d-lg-none float-left"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
          </div>
          <div class="navbar-container">
            <div id="navbarSupportedContent" class="collapse navbar-collapse">
              <ul class="navbar-nav">
                <li class="nav-item mr-2"><a id="navbar-fullscreen" href="javascript:;" class="nav-link apptogglefullscreen"><i class="ft-maximize font-medium-3 blue-grey darken-4"></i>
                    <p class="d-none">fullscreen</p></a></li>
                <!--<li class="dropdown nav-item"><a id="dropdownBasic3" href="#" data-toggle="dropdown" class="nav-link position-relative dropdown-toggle"><i class="ft-flag font-medium-3 blue-grey darken-4"></i><span class="selected-language d-none"></span></a>
                  <div class="dropdown-menu dropdown-menu-right"><a href="javascript:;" class="dropdown-item py-1"><img src="../app-assets/img/flags/us.png" class="langimg"/><span> English</span></a><a href="javascript:;" class="dropdown-item py-1"><img src="../app-assets/img/flags/es.png" class="langimg"/><span> Spanish</span></a><a href="javascript:;" class="dropdown-item py-1"><img src="../app-assets/img/flags/br.png" class="langimg"/><span> Portuguese</span></a><a href="javascript:;" class="dropdown-item"><img src="../app-assets/img/flags/de.png" class="langimg"/><span> French</span></a></div>
                </li>-->
                <li class="dropdown nav-item"><a id="dropdownBasic2" href="#" data-toggle="dropdown" class="nav-link position-relative dropdown-toggle"><i class="ft-bell font-medium-3 blue-grey darken-4"></i><span class="notification badge badge-pill <?php if($this->ModeloSession->alertproductos()>1){ echo 'badge-danger';} ?>"><?php echo $this->ModeloSession->alertproductos();?></span>
                    <p class="d-none">Notifications</p></a>
                  <div class="notification-dropdown dropdown-menu dropdown-menu-right">
                    <div class="noti-list">
                      <?php 
                          $resultado = $this->ModeloSession->alertproductosall();
                          foreach ($resultado->result() as $item){
                      ?>
                      <a class="dropdown-item noti-container py-3 border-bottom border-bottom-blue-grey border-bottom-lighten-4">
                        <i class="ft-bell danger float-left d-block font-large-1 mt-1 mr-2"></i>
                        <span class="noti-wrapper">
                          <span class="noti-title line-height-1 d-block text-bold-400 danger">Existencia baja (<?php echo $item->existencia; ?>)</span>
                          <span class="noti-text"><?php echo $item->producto; ?>/<?php echo $item->sucursal; ?></span>
                        </span>
                      </a>
                      <?php } ?>
                    </div>
                    <a class="noti-footer primary text-center d-block border-top border-top-blue-grey border-top-lighten-4 text-bold-400 py-1"><!--Read All Notifications--></a>
                  </div>
                </li>
                <li class="dropdown nav-item">
                  <a id="dropdownBasic3" href="#" data-toggle="dropdown" class="nav-link position-relative dropdown-toggle"><i class="ft-user font-medium-3 blue-grey darken-4"> <?php echo $_SESSION['usuario_tz'];?></i>
                    <p class="d-none">User Settings</p></a>
                      <div ngbdropdownmenu="" aria-labelledby="dropdownBasic3" class="dropdown-menu dropdown-menu-right">
                        
                        <a  class="dropdown-item" onclick="cerrarsession('<?php echo $sturno;?>')"><i class="ft-power mr-2"></i><span>Cerrar</span></a>
                  </div>
                </li>
                
              </ul>
            </div>
          </div>
        </div>
      </nav>
      <!-- Navbar (Header) Ends-->
      <div class="main-panel">
        <div class="main-content">
          <div class="content-wrapper">