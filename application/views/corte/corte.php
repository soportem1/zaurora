<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/print.css" media="print">
<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/print.css" media="print">-->
<div class="row">
    <div class="col-md-12">
      <h2>Corte de caja </h2>
    </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Corte de caja</h4>
        </div>
        <div class="card-body">
            <div class="card-block">
                <!--------//////////////-------->
                <div class="row inputbusquedas">
                    <div class="col-md-12">
                        <div class="form-body">
                            <div class="row">
                                <input type="hidden" id="prf" value="<?php echo $this->session->userdata("perfilid_tz"); ?>">
                                <input type="hidden" id="idper" value="<?php echo $this->session->userdata("idpersonal_tz"); ?>">
                                <div class="col-md-12 form-group">
                                    <label class="control-label col-md-1">Desde:</label>
                                    <div class="col-md-3">
                                        <input id="txtInicio" name="txtInicio" class="form-control date-picker" size="16" type="date" />
                                    </div>
                                    <label class="control-label col-md-1">Hasta:</label>
                                    <div class="col-md-3">
                                        <input id="txtFin" name="txtFin" class="form-control date-picker" size="16" type="date"/>
                                    </div>
                                    
                                    <div class="col-md-2">                      
                                        <div class="checkbox-list">
                                            <label><input type="checkbox" id="chkFecha" value="1"> Fecha actual </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="#" class="btn btn-raised gradient-purple-bliss white" id="btnBuscar">Buscar</a>
                                        <a id="btnImprimir" onclick="imprimir();"><button type="button" class="btn btn-raised gradient-purple-bliss white"  ><i class="fa fa-print"></i><div id="wait" style="display:none;width:69px;height:89px;border:1px solid black;position:absolute;top:50%;left:50%;padding:2px;"></div></button></a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label class="control-label col-md-1">Sucursal</label>
                                    <div class="col-md-3">
                                        <select id="sucursal" name="sucursal" class="form-control">
                                            <option value="0">Todos</option>
                                            <?php foreach ($sucursal as $item){ ?>
                                                <option value="<?php echo $item->idsucursal; ?>"  ><?php echo $item->nombre; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <label class="control-label col-md-1">Socios</label>
                                    <div class="col-md-3">
                                        <select id="socios" name="socios" class="form-control">
                                            <option value="0">Todos</option>
                                            <?php foreach ($socios as $item){ ?>
                                                <option value="<?php echo $item->iddepartamento; ?>"  ><?php echo $item->nombre; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <label class="control-label col-md-1">Cajero</label>
                                    <div class="col-md-3">
                                        <select id="cajeros" name="cajeros" class="form-control">
                                            <option value="0">Todos</option>
                                            <?php foreach ($cajeros as $item){ ?>
                                                <option value="<?php echo $item->personalId; ?>"  ><?php echo $item->nombre; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div id="wait" class="wait" style="width:89px;height:109px;border:0px solid black;position:absolute;top:50%;left:50%;padding:2px;">
                                    
                                </div>
                            </div> 
                            
                        </div>
                    </div>
                </div>
                

                <div class="card collapse-icon accordion-icon-rotate">
                    <?php if($this->session->userdata("perfilid_tz")==1 || $this->session->userdata("idpersonal_tz")==17){ ?>
                    <div id="headingCollapse11"  class="card-header">
                      <a data-toggle="collapse" href="#collapse11" aria-expanded="true" aria-controls="collapse11" class="card-title lead">Reporte General</a>
                    </div>
                    <div id="collapse11" role="tabpanel" aria-labelledby="headingCollapse11" class="collapse show">
                          <div class="card-body">
                            <div class="card-block">
                                <div class="row" id="imprimir">
                                    <div class="col-md-12" id="tbCorte">
                                        
                                    </div>
                                    <div class="col-md-12" id="tbCorte2">
                                        
                                    </div>

                                    <div class="col-md-12" id="tbCorte3">
                                        <h2>Reporte de Gastos</h2>
                                        
                                    </div>
                                    <div class="col-md-6">
                                        <p style="font-size: 20px">
                                            <span class="col-md-6 text-warning">Total de Efectivo:</span>
                                            <span class="col-md-4" >
                                                <span class="text-warning">$</span>
                                                <span id="totalefectivo">0</span>
                                            </span>
                                        </p>
                                        <p style="font-size: 20px">
                                            <span class="col-md-6 text-warning">Total de Tarjetas:</span>
                                            <span class="col-md-4" >
                                                <span class="text-warning">$</span>
                                                <span id="totaltarjetas">0</span>
                                            </span>
                                        </p>
                                        <!--
                                        <p style="font-size: 20px">
                                            <span class="col-md-6 text-warning">Total Mixtos:</span>
                                            <span class="col-md-4" >
                                                <span class="text-warning">$</span>
                                                <span id="totalmixto">0</span>
                                            </span>
                                        </p>
                                        -->
                                        <p style="font-size: 20px">
                                            <span class="col-md-6 text-warning">SUBTOTAL:</span>
                                            <span class="col-md-4" >
                                                <span class="text-warning">$</span>
                                                <span id="dSubtotal">0.00</span>
                                            </span>
                                        </p>
                                        <p style="font-size: 20px">
                                            <span class="col-md-6 text-warning">EGRESOS:</span>
                                            <span class="col-md-4" >
                                                <span class="text-warning">$</span>
                                                <span id="totalegresos">0.00</span>
                                            </span>
                                        </p>
                                        <p style="font-size: 20px">
                                            <span class="col-md-6 text-warning">Bonos:</span>
                                            <span class="col-md-4" >
                                                <span class="text-warning">$</span>
                                                <span id="total_bonos">0.00</span>
                                            </span>
                                        </p>
                                        <p style="font-size: 20px">
                                            <span class="col-md-6 text-warning">Descuentos:</span>
                                            <span class="col-md-4" >
                                                <span class="text-warning">$</span>
                                                <span id="total_descuentos">0.00</span>
                                            </span>
                                        </p>
                                        <p style="font-size: 20px">
                                            <span class="col-md-6 text-warning">Pagos a personal:</span>
                                            <span class="col-md-4" >
                                                <span class="text-warning">$</span>
                                                <span id="total_pagoPersonal">0.00</span>
                                            </span>
                                        </p>
                                        
                                        <p style="font-size: 20px">
                                            <span class="col-md-6 text-warning">TOTAL:</span>
                                            <span class="col-md-4" >
                                                <span class="text-warning">$</span>
                                                <span id="dTotal">0.00</span>
                                            </span>
                                        </p>
                                        <p style="font-size: 20px">
                                            <span class="col-md-6 text-warning">TOTAL A CRÉDITO:</span>
                                            <span class="col-md-4" >
                                                <span class="text-warning">$</span>
                                                <span id="dTotal_credito">0.00</span>
                                            </span>
                                        </p>
                                        <p style="font-size: 20px">
                                            <span class="col-md-6 text-warning">TOTAL PAGOS A CRÉDITO:</span>
                                            <span class="col-md-4" >
                                                <span class="text-warning">$</span>
                                                <span id="dTotal_credito_pagos">0.00</span>
                                            </span>
                                        </p>
                                        
                                        <p style="font-size: 20px">
                                            <span class="col-md-6 text-warning">Total de ventas:</span>
                                            <span class="col-md-4" >
                                                <span class="text-warning"> </span>
                                                <span id="rowventas">0</span>
                                            </span>
                                        </p>

                                        <br><br><br><br>

                                        <p style="font-size: 20px;" class="mt-5">
                                          <span class="col-md-6 text-warning">Total de puntos gastados:</span>
                                          <span class="col-md-4" >
                                            <span class="text-warning">$</span>
                                            <span id="totalPuntosG">0</span>
                                          </span>
                                        </p>

                                        <!--  
                                        <p style="font-size: 20px">
                                            <span class="col-md-6 text-warning">Total de utilidad:</span>
                                            <span class="col-md-4" >
                                                <span class="text-warning">$</span>
                                                <span id="totalutilidades">0</span>
                                            </span>
                                        </p>
                                    -->
                                    </div>
                                </div>
                            </div>
                          </div>
                    </div>
                    <div id="headingCollapse11_c"  class="card-header">
                      <a data-toggle="collapse" href="#collapse11_c" aria-expanded="false" aria-controls="collapse11_c" class="card-title lead collapsed"> Ventas a crédito</a>
                    </div>
                    <div id="collapse11_c" role="tabpanel" aria-labelledby="headingCollapse11_c" class="collapse">
                          <div class="card-body">
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-md-12" id="tbCorte_credito">
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" id="tbCorte_credito_pagos">
                                        
                                    </div>
                                </div>
                            </div>
                          </div>
                    </div>
                    <div id="headingCollapse11_bono"  class="card-header">
                      <a data-toggle="collapse" href="#collapse11_bono" aria-expanded="false" aria-controls="collapse11_bono" class="card-title lead collapsed"> Bonos y Descuentos</a>
                    </div>
                    <div id="collapse11_bono" role="tabpanel" aria-labelledby="headingCollapse11_bono" class="collapse">
                          <div class="card-body">
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-md-12" id="tbCorte_bono">
                                        
                                    </div>
                                </div>
                            </div>
                          </div>
                    </div>
                    <div id="headingCollapse12"  class="card-header">
                      <a data-toggle="collapse" href="#collapse12" aria-expanded="false" aria-controls="collapse12" class="card-title lead collapsed">Ventas Socios</a>
                    </div>
                    <div id="collapse12" role="tabpanel" aria-labelledby="headingCollapse12" class="collapse" aria-expanded="false">
                      <div class="card-body">
                        <div class="card-block">
                          <div class="row">
                              <div class="col-md-12 reportesocios">
                                  
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div id="headingCollapse15"  class="card-header">
                      <a data-toggle="collapse" href="#collapse15" aria-expanded="false" aria-controls="collapse15" class="card-title lead collapsed">Ventas de Cajero</a>
                    </div>
                    <div id="collapse15" role="tabpanel" aria-labelledby="headingCollapse15" class="collapse" aria-expanded="false">
                      <div class="card-body">
                        <div class="card-block">
                          <div class="row">
                              <div class="col-md-12 reportecajeros">
                                  
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>
                     <div id="headingCollapse17"  class="card-header">
                      <a data-toggle="collapse" href="#collapse17" aria-expanded="false" aria-controls="collapse17" class="card-title lead collapsed">Egresos de Cajero</a>
                    </div>
                    <div id="collapse17" role="tabpanel" aria-labelledby="headingCollapse15" class="collapse" aria-expanded="false">
                      <div class="card-body">
                        <div class="card-block">
                          <div class="row">
                              <div class="col-md-12 reporteagresoscajero">
                                  
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div id="headingCollapse16"  class="card-header">
                      <a data-toggle="collapse" href="#collapse16" aria-expanded="false" aria-controls="collapse16" class="card-title lead collapsed">Ventas Canceladas</a>
                    </div>
                    <div id="collapse16" role="tabpanel" aria-labelledby="headingCollapse16" class="collapse" aria-expanded="false">
                      <div class="card-body">
                        <div class="card-block">
                          <div class="row">
                              <div class="col-md-12 reportecanceladas">
                                  
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if($this->session->userdata("perfilid_tz")==1 || $this->session->userdata("idpersonal_tz")==17 || $this->session->userdata("almacen")==1){ ?>
                        <div id="headingCollapse13"  class="card-header">
                          <a data-toggle="collapse" href="#collapse13" aria-expanded="true" aria-controls="collapse13" class="card-title lead">Ventas de vendedores</a>
                        </div>
                        <div id="collapse13" role="tabpanel" aria-labelledby="headingCollapse13" class="collapse show" aria-expanded="true">
                          <div class="card-body">
                            <div class="card-block">
                              <div class="row">
                                  <div class="col-md-12 reportevendedores">
                                      
                                  </div>
                                  <div class="col-md-12 reportevendedoresg">
                                      
                                  </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    <?php } ?>

                    <?php if($this->session->userdata("perfilid_tz")==1 || $this->session->userdata("idpersonal_tz")==17 || $this->session->userdata("almacen")==1){ ?>
                        <div id="headingCollapse20"  class="card-header">
                          <a data-toggle="collapse" href="#collapse20" aria-expanded="true" aria-controls="collapse20" class="card-title lead">Ventas con puntos</a>
                        </div>
                        <div id="collapse20" role="tabpanel" aria-labelledby="headingCollapse20" class="collapse show" aria-expanded="true">
                          <div class="card-body">
                            <div class="card-block">
                              <div class="row">
                                  <div class="col-md-12 puntosventas">
                                      
                                  </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    <?php } ?>

                    <?php if($this->session->userdata("perfilid_tz")==1 || $this->session->userdata("idpersonal_tz")==17 || $this->session->userdata("almacen")==1){ ?>
                        <div id="headingCollapse21"  class="card-header">
                          <a data-toggle="collapse" href="#collapse21" aria-expanded="true" aria-controls="collapse21" class="card-title lead">Sueldos de personal</a>
                        </div>
                        <div id="collapse21" role="tabpanel" aria-labelledby="headingCollapse20" class="collapse show" aria-expanded="true">
                          <div class="card-body">
                            <div class="card-block">
                              <div class="row">
                                  <div class="col-md-12 sueldosPersonal">
                                      
                                  </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    <?php } ?>
                    
                    <div id="headingCollapse14"  class="card-header">
                      <a data-toggle="collapse" href="#collapse14" <?php if($this->session->userdata("perfilid_tz")==1 || $this->session->userdata("idpersonal_tz")==17){ echo 'aria-expanded="false"'; echo 'class="card-title lead collapsed"'; } else { echo 'aria-expanded="true"'; echo 'class="card-title lead"'; } ?> aria-controls="collapse14" >Mayor y menor vendido</a>
                    </div>
                    <div id="collapse14" role="tabpanel" aria-labelledby="headingCollapse14" <?php if($this->session->userdata("perfilid_tz")==1 || $this->session->userdata("idpersonal_tz")==17){ echo 'aria-expanded="false"'; echo 'class="collapse"'; } else { echo 'aria-expanded="true"'; echo 'class="collapse show"'; } ?> >
                      <div class="card-body">
                        <div class="card-block">
                          <div class="row">
                            <div class="col-md-6">
                                <div class="col-md-12" style="text-align: center;">Mayor Vendidos</div>
                                <div class="col-md-12 reportetablemayor"></div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-12" style="text-align: center;">Menor Vendidos</div>
                                <div class="col-md-12 reportetablemenor"></div>
                            </div>    
                              

                          </div>
                        </div>
                      </div>
                    </div>

                    
                </div>
               
                
                <!--------//////////////-------->
            </div>
        </div>
    </div>
</div>
</div>


<style type="text/css">
  #iframereporte {
    background: white;
  }

  iframe {
    height: 500px;
    border: 0;
    width: 100%;
  }
</style>

<div class="modal fade text-left" id="iframeri" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Ticket</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!--<div class="modal-body">-->
      <div id="iframereporte"></div>
      <!--</div>-->
      <div class="modal-footer">
        <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    function imprimir(){
      window.print();
    }
</script>