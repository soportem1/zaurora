
<div class="row">
  <div class="col-md-12">
    <h2>Permisos, Descuentos y Vacaciones</h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-header">
            <h4 class="card-title">Asignación de Registros</h4>
          </div>
          <div class="card-body">
              <div class="card-block form-horizontal">
                  <!--------//////////////-------->
                  <div class="row">
                    <form method="post"  role="form" id="form_vacaciones">
                      
                      <div class="col-md-9">
                        <div class="form-group">
                          <label class="col-md-2 control-label">Empleado:</label>
                          <div class=" col-md-9">
                            <select class="form-control" id="empleado" name="id_empleado">
                            </select>
                          </div>
                        </div>
                      </div>                    
                      <div class="col-md-12"><br></div>
                      <div class="col-md-9">
                        <div class="form-group">
                          <label class="col-md-2 control-label">Tipo:</label>
                          <div class=" col-md-9">
                            <select class="form-control" id="tipo" name="tipo">
                              <option value="">Elige una opción:</option>
                              <option value="1">Día libre</option>
                              <option value="2">Vacaciones</option>
                              <option value="3">Día enfermo</option>
                              <option value="4">Día descuento</option>
                              <option value="5">Día suspendido</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-9">
                        <div class="form-group">
                          <br>
                          <label class="col-md-2 control-label">Fecha inicio:</label>
                          <div class="col-md-3">
                            <input type="date" class="form-control" id="inicio" name="inicio" readonly>
                          </div>
                          <label class="col-md-2 control-label">Fecha fin:</label>
                          <div class="col-md-3">
                            <input type="date" class="form-control" id="fin" name="fin" readonly>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-12"><br></div>
                      <div class="col-md-12">
                        <h3>Datos Extras</h3>
                        <hr style="border-color: #cfa520; border-width: 5px;" />
                      </div>
                      <div class="col-md-12" id="cont_info_salario" style="display:none">
                        <div class="col-md-12 div_pagos">
                          <div class="col-md-12">
                            <br><h3 class="card-title"> <i class="fa fa-usd " aria-hidden="true"></i></h3><br>
                          </div>

                          <div class="col-md-2">
                            <label class="col-md-12">Salario por día:</label>
                          </div>
                          <div class="col-md-2">
                            <input type="text" id="salario_dia" class="form-control" readonly>
                          </div>
                          <div class="col-md-2">
                            <label class="col-md-12">Días a descontar:</label>
                          </div>
                          <div class="col-md-2">
                            <input type="text" id="dias_tot" class="form-control" readonly>
                          </div>
                          <div class="col-md-2">
                            <label class="col-md-12">Total a descontar:</label>
                          </div>
                          <div class="col-md-2">
                            <input type="text" id="tot_desc" name="tot_descuento" class="form-control" readonly>
                          </div>

                          <div class="col-md-1">
                            <br>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12"><br></div>

                      <div class="col-md-12">
                        <div class="col-md-12 div_pagos">
                          <div class="col-md-12">
                            <br><h3 class="card-title"> <i class="fa fa-briefcase " aria-hidden="true"></i></h3><br>
                          </div>

                          <div class="col-md-2">
                            <label class="col-md-12">Observaciones:</label>
                          </div>
                          <div class="col-md-10">
                            <textarea type="text" id="observaciones" name="observaciones" class="form-control"></textarea>
                          </div>
                          <div class="col-md-1">
                            <br>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12"><br></div>

                      <div class="col-md-12">
                        <br>
                        <div class="col-md-10"></div>
                        <div class="col-md-2" align="right">
                          <button type="submit" class="btn btn-warning white shadow-z-1-hover" id="save"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button>
                        </div>
                      </div> 
                    </form>
                    <div class="col-md-12" id="cont_lista" style="display: none;">
                      <div class="col-md-12 div_pagos">
                        <div class="col-md-12">
                          <br><h3 class="card-title">Listado de registro de vacaciones <i class="fa fa-briefcase " aria-hidden="true"></i></h3><br>
                        </div>

                        <div class="col-md-12">
                          <table class="table table-striped" id="tabla_vacaciones" style="width: 100%">
                            <thead>
                              <tr>
                                <th>Empleado</th>
                                <th>Tipo</th>
                                <th>Inicio</th>
                                <th>Fin</th>
                                <th>Observaciones</th>
                                <th>Registro</th>
                              </tr>
                            </thead>
                            <tbody>
                            </tbody>
                          </table>
                          <div class="col-md-12"><br></div>
                        </div>
                      </div>

                    </div>               
                  </div>
                  <!--------//////////////-------->
              </div>
              
          </div>
      </div>
  </div>
</div>
