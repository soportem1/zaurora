<div class="row">
                <div class="col-md-12">
                  <h2>Sucursales</h2>
                </div>
                
                <div class="col-md-12">
                  <div class="col-md-11"></div>
                  <div class="col-md-1">
                    <a href="<?php echo base_url(); ?>Sucursales/sucursalAdd" class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow"><i class="fa fa-plus"></i></span> Nuevo</a>
                  </div>
                </div>
                
                
              </div>
              <!--Statistics cards Ends-->

              <!--Line with Area Chart 1 Starts-->
              <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Listado de Sucursales</h4>
                        </div>
                        <div class="col-md-12">
                          <div class="col-md-8">
                            
                          </div>
                          <!--<div class="col-md-3">
                              <form role="search" class="navbar-form navbar-right mt-1">
                                <div class="position-relative has-icon-right">
                                  <input type="text" placeholder="Buscar" id="buscarcli" class="form-control round" oninput="buscarcliente()">
                                  <div class="form-control-position"><i class="ft-search"></i></div>
                                </div>
                              </form>
                          </div>-->
                        </div>
                        <div class="card-body">
                            <div class="card-block">
                                <!--------//////////////-------->
                                <table class="table table-striped" id="data-tables" style="width: 100%">
                                      <thead>
                                        <tr>
                                          <th>Nombre</th>
                                          <th>Teléfono</th>
                                          <th>Dirección</th>
                                          <th>Encargado</th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                    
                                      </tbody>
                                </table>
                                    
                                    
                        <!--------//////////////-------->
                            </div>
                        </div>
                    </div>
                </div>
              </div>
