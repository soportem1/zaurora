<?php 
if ($idsucursal>0) {
    //$idsucursal=$_GET['idsucursal'];
    $sucursalv=$this->ModeloSucursal->getSucursal($idsucursal);
    foreach ($sucursalv->result() as $item){
      $label='Editar Sucursal';
      $idsucursal = $item->idsucursal;
      $nombre = $item->nombre;
      $telefono = $item->telefono;
      $direccion = $item->direccion;
      $encargado = $item->encargado;
  }
}else{
  $label='Nueva Sucursal';
  $idsucursal = 0;
  $nombre = '';
  $telefono = '';
  $direccion = '';
  $encargado = 0; 
} 
?>
<style type="text/css">
  .vd_red{
    font-weight: bold;
    color: red;
  }
  .vd_green{
    color: #009688; 
  }
</style>

<div class="row">
  <div class="col-md-12">
    <h2>Sucursal</h2>
  </div>
</div>
              <!--Statistics cards Ends-->
              <!--Line with Area Chart 1 Starts-->
              <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title"><?php echo $label;?></h4>
                        </div>
                        <div class="card-body">
                            <div class="card-block form-horizontal">
                                <!--------//////////////-------->
                                <div class="row">
                                  <form method="post" role="form" id="formsucursales">
                                    <input type="hidden" name="idsucursal" id="idsucursal" value="<?php echo $idsucursal;?>">
                                    <div class="col-md-12">
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <label class="col-md-2 control-label">Nombre:</label>
                                          <div class=" col-md-10 input-group">
                                            <input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $nombre;?>">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <label class="col-md-2 control-label">Teléfono:</label>
                                          <div class=" col-md-10 input-group">
                                            <input type="text" class="form-control" id="telefono" name="telefono" value="<?php echo $telefono;?>">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <label class="col-md-2 control-label">Dirección:</label>
                                          <div class=" col-md-10 input-group">
                                            <input type="text" class="form-control" id="direccion" name="direccion" value="<?php echo $direccion;?>">
                                          </div>
                                        </div>
                                      </div>
                                      <label class="col-sm-2 control-label">Encargado:</label>
                                          <div class="col-sm-3 controls">
                                            <select id="encargado" name="encargado"class="form-control">
                                              <option value="">Elija un encargado:</option>
                                              <?php foreach($empleados->result() as $fila): ?>
                                              <option value="<?php echo $fila->UsuarioID; ?>"  <?php if($fila->UsuarioID==$encargado){ echo 'selected';} ?> ><?php echo $fila->Usuario; ?></option>
                                              <?php endforeach; ?>
                                            </select>
                                         </div>
                                         <div class="col-md-12">
                                            <button type="sumbit" class="btn btn-warning white shadow-z-1-hover"  id="savesuc">Guardar</button>
                                          </div>
                                  </form>
                                  

                                  
                                </div>
                                
                                
                                <!--------//////////////-------->
                            </div>
                        </div>
                    </div>
                </div>
              </div>