<style type="text/css">
  .select2-container{
    width: 100% !important;
  }
</style>
<div class="row">
  <div class="col-md-12">
    <h2>Producto</h2>
  </div>
  <div class="col-md-12">
    <div class="col-md-2">
      <button class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow newtraspaso">Nuevo traspaso</button>
      

      
    </div>
  </div>
</div>
<!--Statistics cards Ends-->

<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title">Listado de traspasos</h4>
          </div>
          <div class="col-md-12 border_t">
       
          <div class="col-md-3">
           

          </div>
          

          <div class="col-md-3"></div> 
          

          </div>
          <div class="card-body">
              <div class="card-block" style="overflow: auto;">
                  <!--------//////////////-------->
                  <table class="table table-striped" id="data-tables" style="width: 100%">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Producto</th>
                            <th>Sucursal salida</th>
                            <th>Sucursal increso</th>
                            <th>Cantidad</th>
                            <th>personal</th>
                            
                            <th></th>
                          </tr>
                        </thead>
                        <tbody id="tbodyresultadospro">

                          
                        </tbody>
                      </table>
                      

                  <!--------//////////////-------->
              </div>
          </div>
      </div>
  </div>
</div>
<div class="modal fade text-left" id="modaltraspaso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Traspaso</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body modalselect2">
              <div class="row">
                <label class="col-md-12">Buscar producto</label>
                <div class="col-md-12">
                  <select class="form-control" id="sproducto"></select>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <label class="col-md-12">Sucursal salida</label>
                  <div class="col-md-12">
                    <select class="form-control selectsucursalt" id="sucursal_salida" onchange="consultarstock(0)">
                      <option value="0">Seleccione...</option>
                      <?php foreach ($sucursal as $item ){?>
                        <option value="<?php echo $item->idsucursal ?>"  ><?php echo $item->nombre; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="col-md-12">
                    <input type="number" class="form-control" value="0" readonly id="stocks">
                  </div>

                </div>
                <div class="col-md-6">
                  <label class="col-md-12">Sucursal reseptora</label>
                  <div class="col-md-12">
                    <select class="form-control selectsucursalt" id="sucursal_entrada" onchange="consultarstock(1)">
                      <option value="0">Seleccione...</option>
                      <?php foreach ($sucursal as $item ){?>
                        <option value="<?php echo $item->idsucursal ?>"  ><?php echo $item->nombre; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="col-md-12">
                    <input type="number" class="form-control" value="0" readonly id="stocke">
                  </div>
                  <div class="col-md-12">
                    <label>Cantidad a traspasar</label>
                    <input type="number" class="form-control" id="cantidadtraspaso">
                  </div>
                </div>
              </div> 
              


            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-raised gradient-purple-bliss white aceptartraspaso">Traspasar</button>
              <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>