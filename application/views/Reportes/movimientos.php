<style type="text/css">
  .red_prod{ color:#FF0000; }
</style>
            <div class="row">
                <div class="col-md-12">
                  <h2>Log de Cambios</h2>
                </div>
                
                <div class="col-md-12">
                  <div class="col-md-11"></div>

                </div>
                
                
              </div>
              <!--Statistics cards Ends-->

              <!--Line with Area Chart 1 Starts-->
              <div class="row">
                <div class="col-sm-12">
                  <div class="card">
                    <div class="card-header">
                      <input type="hidden" id="fecha" value="<?php echo date("Y-m-d"); ?>">
                       <input type="hidden" id="startTime"/>
                        <input type="hidden" id="endTime"/>
                      <h4 class="card-title">Movimientos Realizados</h4>
                    </div>
                    <div class="card-block">
                      <h5><i class="ft-calendar"></i> Agenda de registros</h5><hr>
                      <div style="margin-left: 5%; width:90%; height: 50%" id='calendar'></div>
                    </div>
                    <div class="col-md-12">
                      <div class="col-md-8">
                        
                      </div>
                      <div class="col-md-3">
                      </div>
                      <!--<div class="col-md-12 form-group">
                        <label class="control-label col-md-1">Desde:</label>
                        <div class="col-md-3">
                            <input id="txtInicio" name="txtInicio" class="form-control date-picker" size="16" type="date" />
                        </div>
                        <label class="control-label col-md-1">Hasta:</label>
                        <div class="col-md-3">
                            <input id="txtFin" name="txtFin" class="form-control date-picker" size="16" type="date"/>
                        </div>
                        <div class="col-md-2">                      
                            <div class="checkbox-list">
                                <label><input type="checkbox" id="chkFecha" value="1"> Fecha actual </label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <a href="#" class="btn btn-raised gradient-purple-bliss white" id="btnBuscar">Buscar</a>
                        </div>
                      </div>-->
                    </div>
                    <!--<div class="card-body">
                      <div class="card-block table-movs" id="table-movs">
                        <table class="table table-striped movslog" id="data-tables" style="width: 100%">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th># Registro</th>
                              <th>Tabla</th>
                              <th>Modificación</th>
                              <th>Empleado</th>
                              <th>Fecha</th>
                              <th>Sucursal</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody id="tbodyresultadosess">
                          </tbody>
                        </table>
                        <div class="col-md-12">
                          <div class="col-md-7">
                            
                          </div>

                          
                        </div>
                      </div>
                    </div>-->
                  </div>
                </div>
              </div>
<!------------------------------------------------>

<!-- Modal to Event Details -->
<div id="calendarModal" class="modal fade">
  <div class="modal-dialog">
     <div class="modal-content">
       <div class="modal-header">
        <h4 class="modal-title">Detalle de movimiento</h4>
         <button type="button" class="close" data-dismiss="modal">×</button>
       </div>
       <div id="modalBody" class="modal-body">
          <h4 id="modalTitle" class="modal-title"></h4>
          <h4 id="modalSubTitle" class="modal-title"></h4>
          <h4 id="modalUser" class="modal-title"></h4>
          <div id="modalProd" style="margin-top:5px;"></div>
          <div id="modalWhen" style="margin-top:5px;"></div>
        </div>
        <input type="hidden" id="eventID"/>
        <div class="modal-footer">
         <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        </div>
     </div>
  </div>
</div>
<!--Modal-->