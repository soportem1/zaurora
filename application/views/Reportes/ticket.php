<?php

    //require_once('TCPDF/examples/tcpdf_include.php');
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');
    
    $cambio=$vcambio;
    $pagacon=$vpagacon;
    $direccion;
    $metodos = array('','Efectivo','Tarjeta de crédito','Tarjeta de débito','Pago mixto','Puntos Aurora','Crédito');
    foreach ($configticket->result() as $item){
      $GLOBALS['titulo'] = $item->titulo;
      $GLOBALS['mensaje'] = $item->mensaje;
      $GLOBALS['mensaje2'] = $item->mensaje2;
      $fuente = $item->fuente;
      $GLOBALS['tamano'] = $item->tamano;
      $GLOBALS['margensup'] = $item->margensup;
    }
    foreach ($getventas->result() as $item){
      $GLOBALS['idticket']= $item->id_venta;
      $id_personal = $item->id_personal;
      $id_cliente = $item->id_cliente;
      $sucursalid = $item->sucursalid;
      $metodo = $item->metodo;
      $GLOBALS['subtotal'] = $item->subtotal;
      $GLOBALS['descuento'] = $item->descuentocant;
      $GLOBALS['monto_total'] = $item->monto_total;
      $GLOBALS['cancelado'] = $item->cancelado;
      $GLOBALS['vendedor'] = $item->vendedor;
      $GLOBALS['cajero'] = $item->cajero;
      $GLOBALS['cliente'] = $item->cliente;
      $GLOBALS['metodo'] = $item->metodo;
      $GLOBALS['efectivo'] = $item->efectivo;
      $GLOBALS['tarjeta'] = $item->pagotarjeta;
      $GLOBALS['puntos'] = $item->pagopuntos;
      $reg = $item->reg;
    }
    //echo $sucursalid;
    $getsucs=$this->ModeloVentas->getsucs($sucursalid);
    foreach ($getsucs->result() as $item){
      $GLOBALS['direccion'] = $item->direccion;
    }
    $GLOBALS['fecha']= date("d-m-Y",strtotime($reg));
    $GLOBALS['hora']= date("G:i",strtotime($reg));
    if ($fuente==1) {
      $GLOBALS['tipofuente'] = "arial";
    }
    if ($fuente==2) {
      $GLOBALS['tipofuente'] = "Times New Roman";
    }
    if ($fuente==3) {
      $GLOBALS['tipofuente'] = "Open Sans";
    }
    if ($fuente==4) {
      $GLOBALS['tipofuente'] = "Calibri";
    }
//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      //$img_header = 'header.jpg';
      //$this->Image($img_header, 0, 0, 0, 197, '', '', '', false, 100, '', false, false, 0);
      //$this->Image($img_header, 0, 0, 210, '', 'JPG', '', false, false, 300, '', false, false, 0, false, false, false);
      $imglogo = base_url().'public/img/ops.png';
      $html = '
          <style>
            p{ 
              color:red;
              margin:0px;
              padding:0px;
            }
            h1{ 
              color:red;
              margin:50%;
              padding:50%;
              font-size: 13px;
            }
            imgCancelt{
                float: center;
                position: relative;
            }
          </style>
          <table width="100%" border="0" >
            <tr>
              <td colspan="3" align="center"><img src="'.$imglogo.'" width="140px" ></td>
            </tr>
            <tr>
              <th colspan="3" align="center" style="font-weight: bold; font-size: '.$GLOBALS['tamano'].'px;text-align: left; font-family: '.$GLOBALS['tipofuente'].';">'.$GLOBALS['titulo'].'</th>
            </tr>
            <tr>
                <th colspan="3" align="center" ></th>
            </tr>
            <tr>
                <th colspan="3" align="center" style="font-weight: bold;font-size: '.$GLOBALS['tamano'].'px; font-family: '.$GLOBALS['tipofuente'].';">'.$GLOBALS['direccion'].'</th>
            </tr>';
            
            $html .= '<tr>
                <th colspan="3" style="font-weight: bold;font-size: '.$GLOBALS['tamano'].'px; font-family: '.$GLOBALS['tipofuente'].';">Fecha: '.$GLOBALS['fecha'].'</th>
            </tr>
            <tr>
                <th colspan="3" style="font-weight: bold;font-size: '.$GLOBALS['tamano'].'px; ;font-family: '.$GLOBALS['tipofuente'].';">Hora: '.$GLOBALS['hora'].'</th>
            </tr>
            <tr>
                <th colspan="3" style="font-weight: bold;font-size: '.$GLOBALS['tamano'].'px; ;font-family: '.$GLOBALS['tipofuente'].';">No ticket: '.$GLOBALS['idticket'].'</th>
            </tr>
            <!--<tr>
                <th colspan="3" style="font-weight: bold;font-size: '.$GLOBALS['tamano'].'px; ;font-family: '.$GLOBALS['tipofuente'].';">Vendedor: '.$GLOBALS['vendedor'].'</th>
            </tr>-->
            <tr>
                <th colspan="3" style="font-weight: bold;font-size: '.$GLOBALS['tamano'].'px; ;font-family: '.$GLOBALS['tipofuente'].';">Cajero: '.$GLOBALS['cajero'].'</th>
            </tr>
            <tr>
                <th colspan="3" style="font-weight: bold;font-size: '.$GLOBALS['tamano'].'px; ;font-family: '.$GLOBALS['tipofuente'].';">Cliente: '.$GLOBALS['cliente'].'</th>
            </tr>';
            if($GLOBALS['cancelado']==1) $html.='<img class="imgCancelt" src="'.base_url().'public/img/canceladot.png">'; 
         $html .= '</table>';
        $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
      $html = ' 
      <table width="100%" border="0">
        <tr>
          <td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
        </tr>
      </table>';
      //$this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(80, 180), true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('agb');
$pdf->SetTitle('Ticket');
$pdf->SetSubject('Ticket');
$pdf->SetKeywords('Ticket');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('6', '80', '6');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 11);
// add a page
$pdf->AddPage();
$html0=' <style>
            h1{ 
              color:red;
              margin:1px;
              padding:1px;
              top: 1px;
              font-size:15px;
              position:absolute; z-index:3;
              right:50%; top:-25px;
            }
          </style>
          <table border="0" style="margin-top:0;margin-bottom:0;">
          <tr>
            <th style="font-size: 7px; font-family: '.$GLOBALS['tipofuente'].'px; font-weight: bold;" align="center" width="15%">Cant</th>
            <th style="font-size: 7px; font-family: '.$GLOBALS['tipofuente'].'px; font-weight: bold;" align="center" width="40%">Articulo</th>
            <th style="font-size: 7px; font-family: '.$GLOBALS['tipofuente'].'px; font-weight: bold;" align="center" width="22%">Precio</th>
            <th style="font-size: 7px; font-family: '.$GLOBALS['tipofuente'].'px; font-weight: bold;" align="center" width="23%">Vendedor</th>
          </tr>';
$descuento=0;
foreach ($getventasd->result() as $rowEmp){
  //$descuento=$descuento+$rowEmp->descuentocant;
  $tipoCancel='';
  if($rowEmp->status==0){
    $html0.='   <tr>
                    <th style="font-size: 7px; font-weight: bold; color: red" align="center">'.$rowEmp->cantidad.'</th>
                    <th style="font-size: 7px; font-weight: bold; color: red" align="center">'.$rowEmp->nombre.'</th>
                    <th style="font-size: 7px; font-weight: bold; color: red" align="center">'.$rowEmp->precio.'</th>
                    <th style="font-size: 7px; font-weight: bold; color: red" align="center">'.$rowEmp->vendedor.'</th>
                </tr>';
  } 
  else{
    $html0 .= '
              <tr>
                <th style="font-size: 7px; font-weight: bold;" align="center">'.$rowEmp->cantidad.'</th>
                <th style="font-size: 7px; font-weight: bold;" align="center">'.$rowEmp->nombre.'</th>
                <th style="font-size: 7px; font-weight: bold;" align="center">'.$rowEmp->precio.'</th>
                <th style="font-size: 7px; font-weight: bold;" align="center">'.$rowEmp->vendedor.'</th>
              </tr>';     
  }
}
$html0.='</table>';
 $pdf->writeHTML($html0, true, false, true, false, '');


$html = '<table border="0" style="margin-top:0;margin-bottom:0;">
                <tr>
                    <th style="font-size: 8px; font-weight: bold;" align="center"></th>
                    <th style="font-size: 8px; font-weight: bold;" align="rigth">SUBTOTAL</th>
                    <th style="font-size: 8px; font-weight: bold;" align="left">$ '.number_format($GLOBALS['subtotal'], 2, '.', ',').'</th>
                </tr>
                <tr>
                    <th style="font-size: 8px; font-weight: bold;" align="center"></th>
                    <th style="font-size: 8px; font-weight: bold;" align="rigth">DESCUENTO</th>
                    <!--<th style="font-size: 8px; font-weight: bold;" align="left">$ '.$descuento.'</th>-->
                    <th style="font-size: 8px; font-weight: bold;" align="left">$ '.number_format($GLOBALS['descuento'], 2, '.', ',').'</th>
                </tr>
                <tr>
                    <th style="font-size: 8px; font-weight: bold;" align="center"></th>
                    <th style="font-size: 8px; font-weight: bold;" align="rigth">TOTAL</th>
                    <th style="font-size: 8px; font-weight: bold;" align="left">$ '.number_format($GLOBALS['monto_total'], 2, '.', ',').'</th>
                </tr>';
                if($pagacon!=''){
                  $html .= '<tr>
                    <th style="font-size: 8px; font-weight: bold;" align="center"></th>
                    <th style="font-size: 8px; font-weight: bold;" align="rigth">PAGA CON</th>
                    <th style="font-size: 8px; font-weight: bold;" align="left">$ '.number_format($pagacon, 2, '.', ',').'</th>
                  </tr>';
                }
                if($cambio!=''){
                  $html .= '<tr>
                    <th style="font-size: 8px; font-weight: bold;" align="center"></th>
                    <th style="font-size: 8px; font-weight: bold;" align="rigth">CAMBIO</th>
                    <th style="font-size: 8px; font-weight: bold;" align="left">$ '.number_format($cambio, 2, '.', ',').'</th>
                  </tr>';
                }

                $html .= '
                  <tr><th align="center"></th></tr>
                  <tr>
                    <th style="font-size: 7px;" align="center"></th>
                    <th style="font-size: 7px; font-weight: bold;" align="rigth">Metodo de pago:</th>
                    <th style="font-size: 7px; font-weight: bold;" align="left">'.$metodos[$GLOBALS['metodo']].'</th>
                  </tr>';

                if($GLOBALS['metodo'] == 4){

                  if($GLOBALS['efectivo'] > 0){
                    $html .= '
                    <tr>
                      <th style="font-size: 7px;" align="center"></th>
                      <th style="font-size: 7px;" align="rigth">Efectivo:</th>
                      <th style="font-size: 7px;" align="left">$ '.number_format($GLOBALS['efectivo'], 2, '.', ',').'</th>
                    </tr>';
                  }
                  if($GLOBALS['tarjeta'] > 0){
                    $html .= '
                    <tr>
                      <th style="font-size: 7px;" align="center"></th>
                      <th style="font-size: 7px;" align="rigth">Tarjeta:</th>
                      <th style="font-size: 7px;" align="left">$ '.number_format($GLOBALS['tarjeta'], 2, '.', ',').'</th>
                    </tr>';
                  }
                  if($GLOBALS['puntos'] > 0){
                    $html .= '
                    <tr>
                      <th style="font-size: 7px;" align="center"></th>
                      <th style="font-size: 7px;" align="rigth">Puntos Aurora:</th>
                      <th style="font-size: 7px;" align="left">$ '.number_format($GLOBALS['puntos'], 2, '.', ',').'</th>
                    </tr>';
                  }
                }
                
                $html .= '<tr>
                      <th colspan="3"></th>
                    </tr>';

                if($GLOBALS['puntos'] > 0 || $GLOBALS['metodo'] == 4 || $GLOBALS['metodo'] == 5){
                  $html .= '<tr>
                    <th colspan="3" style="font-size: 7px; font-weight: bold;" align="Center">
                      No se permiten cancelaciones en ventas pagadas con puntos.
                    </th>
                  </tr>';
                }

                $html .= '<tr>
                  <th colspan="3" align="Center" style="font-weight: bold;font-size: '.$GLOBALS['tamano'].'px; font-family: '.$GLOBALS['tipofuente'].';">'.$GLOBALS['mensaje2'].'</th>
                </tr>
                </table>';
      $pdf->writeHTML($html, true, false, true, false, '');
$pdf->IncludeJS('print(true);');
$pdf->Output('Captura.pdf', 'I');

//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');

?>