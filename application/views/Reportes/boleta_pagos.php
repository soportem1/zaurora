<?php

    //require_once('TCPDF/examples/tcpdf_include.php');
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');

    $tot_prods=0;
    $tot_venta=0; $tot_ventaf=0; $tot_descvent=0;
    $tot_comision=0;
    $super_total=0;
    $salario_dia=0;
    $salario_hora=0;
    $comision=0;
    $tot_salario=0; $tot_ext=0; $tot_doble=0; $tot_feria=0; 
    $tot_bono=0; $tot_desc=0; $tot_desc_nl=0; 
    $GLOBALS["del"]=date("d/m/Y", strtotime($i));
    $GLOBALS["al"]=date("d/m/Y", strtotime($f));
    $GLOBALS["dif"]=$dif;

    $GLOBALS["hrsLaboradas"]=$hrsLaboradas;
    $GLOBALS["minLaborados"]=$minLaborados;
    $GLOBALS["hrsMinLaboradas"]=$hrsLaboradas.':'.$minLaborados;
    
    foreach ($det as $i){
      $tot_prods=$i->tot_prods;
      $tot_venta=$i->tot_venta;
    }
    log_message('error','tot_venta : '.$tot_venta);
    if(isset($desc)){
      $tot_descvent =$desc->tot_descs;
      log_message('error','tot_descvent : '.$tot_descvent);
    }

    $tot_ventaf=$tot_venta-$tot_descvent;

    foreach ($vende as $i){
      $comision=$i->comision_venta;
      $salario_dia=$i->salario_dia;
      $salario_hora=$i->salario_hora;
      $hora_extra=$i->hora_extra;
      $hora_doble=$i->hora_doble;
      $hora_dia_feriado=$i->hora_dia_feriado;
      $nombre=$i->nombre;
      $aps=$i->apellidos;
    }

    $tot_comision = $tot_ventaf * $comision;
    //$tot_salario = $dif * $salario_dia;
    //$tot_salario = $hrsLaboradas * $salario_hora;
    $tot_salario = ($hrsLaboradas * $salario_hora) + ($minLaborados * ($salario_hora/60));

    $super_total = $tot_comision + $tot_salario + $tot_ext + $tot_doble + $tot_feria;

    log_message('error','super_total : '.$super_total);
//=======================================================================================
class MYPDF extends TCPDF { 

  public function Header() {
    $logos = base_url()."public/img/ops.png";
    $html = '
        <table width="100%" border="0" cellpadding="4px">
          <tr>
            <td width="25%"><img src="'.$logos.'"></td>
            <td width="40%"></td>
            <td width="35%">
              <table border="1" cellpadding="4px">
                <tr>
                  <td>Fecha creación: </td>
                  <td>'.date("d/m/Y").'</td>
                </tr>
                <tr>
                  <td>Inicio: </td>
                  <td>'.$GLOBALS["del"].'</td>
                </tr>
                <tr>
                  <td>Fin: </td>
                  <td>'.$GLOBALS["al"].'</td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        ';
    $this->writeHTML($html, true, false, true, false, '');
  }
}
 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mangoo Software');
$pdf->SetTitle('Boleta de Pago');
$pdf->SetSubject('Pagos');


// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('9', '40', '9');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin('9');

// set auto page breaks
$pdf->SetAutoPageBreak(true, 10);//PDF_MARGIN_BOTTOM

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 8);
// add a page
$pdf->AddPage('P', 'A4');
  $html = '';
        //$pdf->writeHTML($html, true, false, true, false, '');
  
  $html = '<h1 align="center">BOLETA DE PAGO</h1><table width="100%" border="0">
              <tr>
                <td align="center" colspan="4"></td>
              </tr>
              <tr>
                <td align="center" colspan="4" style="font-size:14px"><strong>DATOS DEL EMPLEADO</strong></td>
              </tr>
            </table>
            <table width="100%" border="1">
              <thead>
                <tr>
                  <th align="center"><b>NOMBRE</b></th>
                  <th align="center"><b>SALARIO POR DÍA</b></th>
                  <th align="center"><b>DÍAS TRABAJADOS</b></th>
                  <th align="center"><b>SALARIO POR HORAS</b></th>
                  <th align="center"><b>HORAS TRABAJADAS</b></th>
                  <th align="center"><b>TOTAL GENERADO</b></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td align="center">'.$nombre.' '.$aps.'</td>
                  <td align="center">$'.$salario_dia.'</td>
                  <td align="center">'.$GLOBALS["dif"].'</td>
                  <td align="center">$'.$salario_hora.'</td>
                  <td align="center">'.$GLOBALS["hrsMinLaboradas"].'</td>
                  <td align="center">$'.number_format($tot_salario,2,".",",").'</td>
                </tr>
              </tbody>
            </table>
            <table width="100%" border="0">
              <tr>
                <th align="center"></th>
              </tr>
            </table>
            <table width="100%" border="1">
              <thead>
                <tr>
                  <td align="center" colspan="4" style="font-size:14px"><strong>DATOS DE HORAS EXTRAS</strong></td>
                </tr>
                <tr>
                  <th align="center"><b>NOMBRE</b></th>
                  <th align="center"><b>HORAS EXTRAS</b></th>
                  <th align="center"><b>COSTO POR HORA</b></th>
                  <th align="center"><b>TOTAL GENERADO</b></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td align="center">'.$nombre.' '.$aps.'</td>
                  <td align="center">0</td>
                  <td align="center">$'.$hora_extra.'</td>
                  <td align="center">$'.number_format($tot_ext,2,".",",").'</td>
                </tr>
              </tbody>
            </table>
            <table width="100%" border="0">
              <tr>
                <th align="center"></th>
              </tr>
            </table>
            <table width="100%" border="1">
              <thead>
                <tr>
                  <td align="center" colspan="4" style="font-size:14px; font-weight-bold;"><strong>DATOS DE HORAS DOBLES</strong></td>
                </tr>
                <tr>
                  <th align="center"><b>NOMBRE</b></th>
                  <th align="center"><b>HORAS DOBLES</b></th>
                  <th align="center"><b>COSTO POR HORA</b></th>
                  <th align="center"><b>TOTAL GENERADO</b></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td align="center">'.$nombre.' '.$aps.'</td>
                  <td align="center">0</td>
                  <td align="center">$'.$hora_doble.'</td>
                  <td align="center">$'.number_format($tot_doble,2,".",",").'</td>
                </tr>
              </tbody>
            </table>
            <table width="100%" border="0">
              <tr>
                <th align="center"></th>
              </tr>
            </table>
            <table width="100%" border="1">
              <thead>
                <tr>
                  <td align="center" colspan="4" style="font-size:14px; font-weight-bold;"><strong>DATOS DE DÍAS FERIADOS</strong></td>
                </tr>
                <tr>
                  <th align="center"><b>NOMBRE</b></th>
                  <th align="center"><b>HORAS</b></th>
                  <th align="center"><b>COSTO POR HORA</b></th>
                  <th align="center"><b>TOTAL GENERADO</b></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td align="center">'.$nombre.' '.$aps.'</td>
                  <td align="center">0</td>
                  <td align="center">$'.$hora_dia_feriado.'</td>
                  <td align="center">$'.number_format($tot_feria,2,".",",").'</td>
                </tr>
              </tbody>
            </table>
            <table width="100%" border="0">
              <tr>
                <th align="center"></th>
              </tr>
            </table>
            <table width="100%" border="1">
              <thead>
                <tr>
                  <td align="center" colspan="4" style="font-size:14px; font-weight-bold;"><strong>DATOS DE VENTAS </strong></td>
                </tr>
                <tr>
                  <th align="center"><b>TOTAL DE PRODS.</b></th>
                  <th align="center"><b>MONTO TOTAL</b></th>
                  <th align="center"><b>COMISIÓN POR VENTA</b></th>
                  <th align="center"><b>TOTAL GENERADO</b></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td align="center">'.$tot_prods.'</td>
                  <td align="center">$'.number_format($tot_ventaf,2,".",",").'</td>
                  <td align="center">'.$comision.' %</td>
                  <td align="center">$'.number_format($tot_comision,2,".",",").'</td>
                </tr>
              </tbody>
            </table>

            <table width="100%" border="0">
              <tr>
                <th align="center"></th>
              </tr>
            </table>
            <table width="100%" border="1">
              <thead>
                <tr>
                  <td align="center" colspan="3" style="font-size:14px; font-weight-bold;"><strong>DATOS DE BONOS </strong></td>
                </tr>
                <tr>
                  <th align="center" width="25%"><b>NOMBRE</b></th>
                  <th align="center" width="50%"><b>MOTIVO</b></th>
                  <th align="center" width="25%"><b>MONTO</b></th>
                </tr>
              </thead>
              <tbody>';
              foreach($bon_desc as $b){
                $tot_bono=$tot_bono+$b->cant_bono;
                if($b->cant_bono>0){
                  $html.='<tr>
                    <td align="center" width="25%">'.$nombre.' '.$aps.'</td>
                    <td align="center" width="50%">'.$b->motivo_bono.'</td>
                    <td align="center" width="25%">$'.number_format($b->cant_bono,2,".",",").'</td>
                  </tr>';
                }
              }
              $html.='</tbody>
            </table>

            <table width="100%" border="0">
              <tr>
                <th align="center"></th>
              </tr>
            </table>
            <table width="100%" border="1">
              <thead>
                <tr>
                  <td align="center" colspan="3" style="font-size:14px; font-weight-bold;"><strong>DATOS DE DESCUENTOS </strong></td>
                </tr>
                <tr>
                  <th align="center" width="25%"><b>NOMBRE</b></th>
                  <th align="center" width="50%"><b>MOTIVO</b></th>
                  <th align="center" width="25%"><b>MONTO</b></th>
                </tr>
              </thead>
              <tbody>';
              foreach($bon_desc as $d){
                $tot_desc=$tot_desc+$d->cant_desc;
                if($d->cant_desc>0){
                  $html.='<tr>
                    <td align="center" width="25%">'.$nombre.' '.$aps.'</td>
                    <td align="center" width="50%">'.$d->motivo_desc.'</td>
                    <td align="center" width="25%">$'.number_format($d->cant_desc,2,".",",").'</td>
                  </tr>';
                }
              }
              $html.='</tbody>
            </table>

            <table width="100%" border="0">
              <tr>
                <th align="center"></th>
              </tr>
            </table>
            <table width="100%" border="1">
              <thead>
                <tr>
                  <td align="center" colspan="3" style="font-size:14px; font-weight-bold;"><strong>DATOS DE DÍAS NO LABORADOS </strong></td>
                </tr>
                <tr>
                  <th align="center" width="15%"><b>INICIO</b></th>
                  <th align="center" width="15%"><b>FIN</b></th>
                  <th align="center" width="15%"><b>MONTO</b></th>
                  <th align="center" width="15%"><b>TIPO</b></th>
                  <th align="center" width="40%"><b>OBSERVACIONES</b></th>
                </tr>
              </thead>
              <tbody>';
              $tipo="";
              foreach($desc_nl as $d){
                $tot_desc_nl=$tot_desc_nl+$d->tot_descuento;
                if($d->tot_descuento>0){
                  if($d->tipo=="3"){
                    $tipo="Día enfermo";
                  }else if($d->tipo=="4"){
                    $tipo="Día descuento";
                  }else if($d->tipo=="5"){
                    $tipo="Día suspendido";
                  }
                  $html.='<tr>
                    <td align="center" width="15%">'.$d->inicio.'</td>
                    <td align="center" width="15%">'.$d->fin.'</td>
                    <td align="center" width="15%">$'.number_format($d->tot_descuento,2,".",",").'</td>
                    <td align="center" width="15%">'.$tipo.'</td>
                    <td align="center" width="40%">'.$d->observaciones.'</td>
                  </tr>';
                }
              }
              $html.='</tbody>
            </table>

            <table width="100%" border="0">
              <tr>
                <th align="center"></th>
              </tr>
            </table>
            <table width="100%" border="1" cellpadding="4">
              <thead>
                <tr>
                  <th align="rigth" colspan="4" width="60%"><b>TOTAL A PAGAR:</b></th>';
                  //log_message('error','tot_bono : '.$tot_bono);
                  $super_total=$super_total+$tot_bono;
                  $super_total2=$super_total-$tot_desc-$tot_desc_nl;
                  $html.='<td align="center" colspan="2" width="40%"><b>$'.number_format($super_total2,2,".",",").'</b></td>
                </tr>
              </thead>
            </table>

            <table width="100%" border="0">
              <tr>
                <th align="center" height="100px"></th>
              </tr>
            </table>
            <table width="100%" border="0">
              <thead>
                <tr>
                  <th align="center" width="100%">__________________________________________</th>
                </tr>
                <tr>
                  <th align="center" width="100%"><b>'.strtoupper($nombre).' '.strtoupper($aps).'</b></th>
                </tr>
                <tr>
                  <th align="center" width="100%"><b>RECIBÍ DE CONFORMIDAD</b></th>
                </tr>
              </thead>
            </table>
            ';
        $pdf->writeHTML($html, true, false, true, false, '');

$pdf->Output('Captura.pdf', 'I');
//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');

?>