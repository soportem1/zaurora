<?php

    //require_once('TCPDF/examples/tcpdf_include.php');
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');
    $imglogo = base_url().'public/img/ops.png';    
//=======================================================================================
 
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('agb');
$pdf->SetTitle('Productos');
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);



// set margins
$pdf->SetMargins('6', '6', '6');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 6);// margen del footer

$pdf->SetFont('dejavusans', '', 9);
// add a page

  $pdf->AddPage();
  $html = '<table width="100%" border="1" cellpadding="2">
              <tr>
                <th align="center">Productos registrados</th>
                <th align="center">Productos en existencia</th>
                <th align="center">Total de Precios unitarios</th>
                <th align="center">Total de Precios</th>
              </tr>
            <tr>
                <th align="center">'.$total_rows.'</th>
                <th align="center">'.$totalexistencia.'</th>
                <th align="center">'.number_format($totalproductopreciocompra,2,'.',',').'</th>
                <th align="center">'.number_format($totalproductoporpreciocompra,2,'.',',').'</th>
              </tr>
              </table>';



$pdf->writeHTML($html, true, false, true, false, '');
  $htmlp = '<table width="100%" border="1" cellpadding="2">
              <tr>
                <th align="center">Nombre</th>
                <th align="center">Descripcion</th>
                <th align="center">Categoria</th>
                <th align="center">Precio Venta</th>
                <th align="center">Excistencia</th>
              </tr>
            <tbody>';
foreach ($getventasd->result() as $item){
  if ($item->img=='') {
    $img='public/img/ops.png';
  }else{
    $img=$item->img;
  }
  //                <td><img src="'.base_url().''.$img.'" class="imgpro" ></td>

  $htmlp .= '
              <tr>
                <td>'.$item->nombre.'</td>
                <td>'.$item->descripcion.'</td>
                <td>'.$item->categoria.'</td>
                <td align="center">'.$item->precioventa.'</td>
                <td align="center">'.$item->stock.'</td>
              </tr>
            ';
}
 $htmlp.='</tbody></table>';
$pdf->writeHTML($htmlp, true, false, true, false, '');
  
if (isset($_GET['print'])) {
  $pdf->IncludeJS('print(true);');
}

$pdf->Output('Captura.pdf', 'I');

//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');

?>