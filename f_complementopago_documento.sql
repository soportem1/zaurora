-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 02-04-2022 a las 01:00:36
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `aurora_sicoi`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `f_complementopago_documento`
--

DROP TABLE IF EXISTS `f_complementopago_documento`;
CREATE TABLE IF NOT EXISTS `f_complementopago_documento` (
  `docId` bigint(20) NOT NULL AUTO_INCREMENT,
  `complementoId` bigint(20) NOT NULL,
  `facturasId` int(11) NOT NULL,
  `IdDocumento` text NOT NULL,
  `serie` varchar(10) DEFAULT NULL,
  `folio` varchar(10) DEFAULT NULL,
  `NumParcialidad` float NOT NULL,
  `ImpSaldoAnt` decimal(10,2) NOT NULL,
  `ImpPagado` decimal(10,2) NOT NULL,
  `ImpSaldoInsoluto` float NOT NULL,
  `MetodoDePagoDR` varchar(10) NOT NULL,
  `Estado` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`docId`),
  KEY `comp_fk_documento` (`complementoId`),
  KEY `comp_fk_factura` (`facturasId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `f_complementopago_documento`
--
ALTER TABLE `f_complementopago_documento`
  ADD CONSTRAINT `comp_fk_documento` FOREIGN KEY (`complementoId`) REFERENCES `f_complementopago` (`complementoId`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `comp_fk_factura` FOREIGN KEY (`facturasId`) REFERENCES `f_facturas` (`FacturasId`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
