-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 08-07-2019 a las 11:22:12
-- Versión del servidor: 5.7.26-0ubuntu0.18.04.1
-- Versión de PHP: 7.2.19-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `aurora`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE  PROCEDURE `SP_ADD_PERSONAL` (IN `_NOM` VARCHAR(120), IN `_SEX` INT(1))  NO SQL
INSERT INTO personal (nombre,sexo)VALUES (_NOM,_SEX)$$

CREATE  PROCEDURE `SP_DEL_PERSONAL` (IN `_ID` INT)  NO SQL
UPDATE personal SET estatus=0 where personalId=_ID$$

CREATE  PROCEDURE `SP_GET_ALL_ESTADOS` ()  NO SQL
SELECT * FROM Estado$$

CREATE  PROCEDURE `SP_GET_ALL_PERFILES` ()  NO SQL
SELECT *  FROM Perfiles$$

CREATE  PROCEDURE `SP_GET_ALL_PERSONAL` ()  NO SQL
SELECT per.personalId,per.nombre,per.apellidos,usu.Usuario 
FROM personal as per 
LEFT JOIN usuarios as usu on usu.personalId=per.personalId
WHERE per.tipo=1 AND per.estatus=1$$

CREATE  PROCEDURE `SP_GET_MENUS` (IN `PERFIL` INT)  NO SQL
SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, Perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId=PERFIL$$

CREATE  PROCEDURE `SP_GET_PERSONAL` (IN `_ID` INT)  NO SQL
SELECT *  FROM personal where personalId=_ID$$

CREATE  PROCEDURE `SP_GET_SESSION` (IN `USUA` VARCHAR(15))  NO SQL
SELECT usu.UsuarioID,per.personalId,per.nombre, usu.perfilId, usu.contrasena,per.idsucursal
FROM usuarios as usu
inner JOIN personal as per on per.personalId = usu.personalId
where per.estatus=1 AND usu.Usuario = USUA$$

CREATE  PROCEDURE `SP_GET_USUARIOS` ()  NO SQL
SELECT usu.UsuarioID,usu.Usuario,per.nombre as perfil FROM usuarios as usu, Perfiles as per WHERE usu.perfilId=per.perfilId$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `categoriaId` int(11) NOT NULL,
  `categoria` varchar(200) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`categoriaId`, `categoria`, `activo`) VALUES
(1, 'Tenis', 1),
(2, 'zapatos', 0),
(3, 's', 0),
(4, 'ARTICULOS DE BAÑO', 1),
(5, 'LAMPARAS', 1),
(6, 'ARTESANIAS', 1),
(7, 'CHAROLAS', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `ClientesId` int(11) NOT NULL,
  `Nom` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Calle` varchar(45) DEFAULT NULL,
  `noExterior` varchar(45) DEFAULT NULL,
  `Colonia` varchar(45) DEFAULT NULL,
  `Localidad` varchar(45) DEFAULT NULL,
  `Municipio` varchar(45) DEFAULT NULL,
  `Estado` varchar(45) DEFAULT NULL,
  `Pais` varchar(45) DEFAULT NULL,
  `CodigoPostal` varchar(45) DEFAULT NULL,
  `Correo` varchar(100) NOT NULL,
  `noInterior` varchar(45) NOT NULL,
  `nombrec` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `correoc` varchar(45) NOT NULL,
  `telefonoc` varchar(30) NOT NULL,
  `extencionc` varchar(20) NOT NULL,
  `nextelc` varchar(30) NOT NULL,
  `descripcionc` text NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1' COMMENT '1 activo 0 eliminado'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`ClientesId`, `Nom`, `Calle`, `noExterior`, `Colonia`, `Localidad`, `Municipio`, `Estado`, `Pais`, `CodigoPostal`, `Correo`, `noInterior`, `nombrec`, `correoc`, `telefonoc`, `extencionc`, `nextelc`, `descripcionc`, `activo`) VALUES
(45, 'PUBLICO EN GENERAL', '', '', '', '', '', '', 'Mexico', '', '', '', '', '', '', '', '', '', 1),
(46, 'DULCIBOTANASsx', 'GUADALAJARA', '', 'INDEPENDENCIA', 'PUEBLA', 'PUEBLA', 'PUEBLA', 'Mexico', '72150', 'edreimagdiel@gmail.com', '', 'EDREI', 'edreimagdiel@gmail.com', '2225464434', '', '', '', 1),
(47, 'xxxxxxxx11', 'calle', '1', 'colonia', 'localidad', 'municipio', 'estado', 'mexico', '94140', 'ddd@h.com', '1', 'contacto', 'ccc', '111111111', '11', '222222', 'des', 1),
(48, 'ñkñkñk', 'k', '0', 'k', 'k', 'k', 'k', 'México', '94140', 'cas@hot.com', '9', 'n', 'cas@h.com', '123456789', '123', '123456', 'fghjk', 1),
(49, 'hectoe lagunes loyo', 'conocida', '9', 'conocida', 'conocida', 'conocido', 'veracruz', 'México', '94140', 'lagunes@hotmal.com', '9', 'yo', 'lagunes@hotmal.com', '12356789', '34', '2345678', 'sdñokfsñldkfsd', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `id_compra` int(11) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `monto_total` double NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compra_detalle`
--

CREATE TABLE `compra_detalle` (
  `id_detalle_compra` int(11) NOT NULL,
  `id_compra` int(11) NOT NULL,
  `id_producto` bigint(20) NOT NULL,
  `cantidad` float NOT NULL,
  `precio_compra` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE `departamento` (
  `iddepartamento` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`iddepartamento`, `nombre`, `estatus`) VALUES
(1, 'Gilberto Hernández', 1),
(2, 'María Gómez', 1),
(3, 'Raúl Hernández', 1),
(4, 'Andrés Cuevas', 1),
(5, 'sdsdsdf', 0),
(6, 'dddd', 0),
(7, 'xxxx', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `EstadoId` int(11) NOT NULL,
  `Nombre` varchar(50) DEFAULT NULL,
  `Alias` varchar(10) DEFAULT NULL,
  `activo` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`EstadoId`, `Nombre`, `Alias`, `activo`) VALUES
(1, 'AGUASCALIENTES', 'AS', 1),
(2, 'BAJA CALIFORNIA', 'BC', 1),
(3, 'BAJA CALIFORNIA SUR', 'BS', 1),
(4, 'CAMPECHE', 'CC', 1),
(5, 'COAHUILA', 'CL', 1),
(6, 'COLIMA', 'CM', 1),
(7, 'CHIAPAS', 'CS', 1),
(8, 'CHIHUAHUA', 'CH', 1),
(9, 'DISTRITO FEDERAL', 'DF', 1),
(10, 'DURANGO', 'DG', 1),
(11, 'GUANAJUATO', 'GT', 1),
(12, 'GUERRERO', 'GR', 1),
(13, 'HIDALGO', 'HG', 1),
(14, 'JALISCO', 'JC', 1),
(15, 'ESTADO DE MEXICO', 'MC', 1),
(16, 'MICHOACAN', 'MN', 1),
(17, 'MORELOS', '', 1),
(18, 'NAYARIT', 'NT', 1),
(19, 'NUEVO LEON', 'NL', 1),
(20, 'OAXACA', 'OC', 1),
(21, 'PUEBLA', 'PL', 1),
(22, 'QUERETARO', 'QT', 1),
(23, 'QUINTANA ROO', 'QR', 1),
(24, 'SAN LUIS POTOSI', 'SP', 1),
(25, 'SINALOA', 'SL', 1),
(26, 'SONORA', 'SR', 1),
(27, 'TABASCO', 'TC', 1),
(28, 'TAMAULIPAS', 'TS', 1),
(29, 'TLAXCALA', 'TL', 1),
(30, 'VERACRUZ', 'VZ', 1),
(31, 'YUCATAN', 'YN', 1),
(32, 'ZACATECAS', 'ZS', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `MenuId` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Icon` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`MenuId`, `Nombre`, `Icon`) VALUES
(1, 'Catálogos', 'fa fa-book'),
(2, 'Operaciones', 'fa fa-folder-open'),
(3, 'Configuración\n', 'fa fa fa-cogs');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_sub`
--

CREATE TABLE `menu_sub` (
  `MenusubId` int(11) NOT NULL,
  `MenuId` int(11) NOT NULL,
  `Nombre` varchar(50) DEFAULT NULL,
  `Pagina` varchar(120) DEFAULT NULL,
  `Icon` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu_sub`
--

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES
(1, 1, 'Productos', 'Productos', 'fa fa-barcode'),
(2, 3, 'Categoria', 'Categoria', 'fa fa-cogs'),
(3, 1, 'Personal', 'Personal', 'fa fa-group'),
(4, 2, 'Ventas', 'Ventas', 'fa fa-shopping-cart'),
(5, 2, 'Compras', 'Compras', 'fa fa-cogs'),
(6, 1, 'Clientes', 'Clientes', 'fa fa-user'),
(7, 1, 'Proveedores', 'Proveedores', 'fa fa-truck'),
(8, 2, 'Corte de caja', 'Corte_caja', 'fa fa-cogs'),
(9, 2, 'Lista de ventas', 'ListaVentas', 'fa fa-cogs'),
(10, 2, 'Turno', 'Turno', 'fa fa-cogs'),
(11, 2, 'Lista de turnos', 'ListaTurnos', 'fa fa-cogs'),
(12, 2, 'Lista de compras', 'Listacompras', 'fa fa-shopping-cart'),
(13, 3, 'Config. de ticket', 'Config_ticket', 'fa fa-cogs'),
(14, 1, 'Socios', 'Socios', 'fa fa fa-cogs'),
(15, 1, 'Sucursales', 'Sucursales', 'fa fa fa-cogs');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notas`
--

CREATE TABLE `notas` (
  `id_nota` int(1) NOT NULL,
  `mensaje` text NOT NULL,
  `usuario` varchar(120) NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `notas`
--

INSERT INTO `notas` (`id_nota`, `mensaje`, `usuario`, `reg`) VALUES
(1, '<p>notass</p>\n', 'Administrador', '2018-05-10 19:01:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles`
--

CREATE TABLE `perfiles` (
  `perfilId` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `perfiles`
--

INSERT INTO `perfiles` (`perfilId`, `nombre`) VALUES
(1, 'Administrador'),
(2, 'Personal'),
(3, 'Residentes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles_detalles`
--

CREATE TABLE `perfiles_detalles` (
  `Perfil_detalleId` int(11) NOT NULL,
  `perfilId` int(11) NOT NULL,
  `MenusubId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `perfiles_detalles`
--

INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 2, 4),
(7, 2, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

CREATE TABLE `personal` (
  `personalId` int(11) NOT NULL,
  `nombre` varchar(300) NOT NULL,
  `apellidos` varchar(300) NOT NULL,
  `fechanacimiento` date DEFAULT NULL,
  `sexo` int(1) NOT NULL,
  `domicilio` varchar(500) NOT NULL,
  `ciudad` varchar(120) NOT NULL,
  `estado` int(11) NOT NULL,
  `codigopostal` int(5) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `turno` int(1) NOT NULL,
  `fechaingreso` date DEFAULT NULL,
  `fechabaja` date DEFAULT NULL,
  `sueldo` decimal(10,2) NOT NULL,
  `idsucursal` int(11) NOT NULL,
  `tipo` int(1) NOT NULL DEFAULT '1' COMMENT '0 administrador 1 normal',
  `estatus` int(1) NOT NULL DEFAULT '1' COMMENT '1 visible 0 eliminado'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `personal`
--

INSERT INTO `personal` (`personalId`, `nombre`, `apellidos`, `fechanacimiento`, `sexo`, `domicilio`, `ciudad`, `estado`, `codigopostal`, `telefono`, `celular`, `correo`, `turno`, `fechaingreso`, `fechabaja`, `sueldo`, `idsucursal`, `tipo`, `estatus`) VALUES
(1, 'Administrador', '', NULL, 0, '', '', 0, 0, '', '', '', 0, NULL, NULL, '0.00', 0, 0, 1),
(2, 'gerardo', 'bautista', '2018-05-02', 1, 'conocido', 'conocido', 30, 94140, '123456789', '1234567890', 'soporte@mangoo.mx', 1, '2018-05-03', '2018-05-17', '30000.00', 1, 1, 1),
(3, '_NOM', '', NULL, 0, '', '', 0, 0, '', '', '', 0, NULL, NULL, '0.00', 0, 1, 0),
(4, 'Mariana gutierrez', '', NULL, 2, '', '', 0, 0, '', '', '', 0, NULL, NULL, '0.00', 0, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_menu`
--

CREATE TABLE `personal_menu` (
  `personalmenuId` int(11) NOT NULL,
  `personalId` int(11) NOT NULL,
  `MenuId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `personal_menu`
--

INSERT INTO `personal_menu` (`personalmenuId`, `personalId`, `MenuId`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 1, 10),
(11, 1, 11),
(12, 1, 12),
(13, 1, 13),
(18, 1, 14),
(20, 2, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `productoid` bigint(20) NOT NULL,
  `codigo` varchar(20) NOT NULL,
  `productofiscal` int(1) NOT NULL COMMENT '0 no 1 si',
  `nombre` text NOT NULL,
  `descripcion` text NOT NULL,
  `categoria` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `preciocompra` decimal(10,2) NOT NULL,
  `gananciaefectivo` float NOT NULL,
  `porcentaje` int(11) NOT NULL,
  `precioventa` decimal(10,2) NOT NULL,
  `mediomayoreo` decimal(10,2) NOT NULL,
  `canmediomayoreo` int(11) NOT NULL,
  `mayoreo` decimal(10,2) NOT NULL,
  `canmayoreo` int(11) NOT NULL,
  `img` varchar(120) NOT NULL,
  `fecharegistro` date DEFAULT NULL,
  `iddepartamento` int(11) NOT NULL,
  `tipo_ganancia` int(11) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1' COMMENT '1 actual 0 eliminado',
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`productoid`, `codigo`, `productofiscal`, `nombre`, `descripcion`, `categoria`, `stock`, `preciocompra`, `gananciaefectivo`, `porcentaje`, `precioventa`, `mediomayoreo`, `canmediomayoreo`, `mayoreo`, `canmayoreo`, `img`, `fecharegistro`, `iddepartamento`, `tipo_ganancia`, `activo`, `reg`) VALUES
(82, '54345350', 0, 'a1', '', 0, 0, '500.00', 50, 5, '600.00', '0.00', 0, '650.00', 5, '', '2019-06-30', 1, 1, 0, '2019-06-21 16:58:09'),
(83, '54345351', 0, 'a2', '', 0, 0, '500.00', 50, 5, '600.00', '0.00', 0, '650.00', 5, '', '2019-08-01', 1, 1, 0, '2019-06-21 16:58:10'),
(84, '54345352', 0, 'a3', '', 0, 0, '500.00', 50, 5, '600.00', '0.00', 0, '650.00', 5, '', '2019-05-02', 1, 1, 0, '2019-06-21 16:58:10'),
(85, '54345353', 0, 'a4', '', 0, 0, '500.00', 50, 5, '600.00', '0.00', 0, '650.00', 5, '', '2019-05-03', 1, 1, 0, '2019-06-21 16:58:10'),
(86, '54345350', 0, 'a1', '', 0, 0, '500.00', 50, 5, '600.00', '0.00', 0, '650.00', 5, '', '2019-06-30', 1, 1, 0, '2019-06-21 17:06:00'),
(87, '54345351', 0, 'a2', '', 0, 0, '500.00', 50, 5, '600.00', '0.00', 0, '650.00', 5, '', '2019-08-01', 1, 1, 0, '2019-06-21 17:06:01'),
(88, '54345352', 0, 'a3', '', 0, 0, '500.00', 50, 5, '600.00', '0.00', 0, '650.00', 5, '', '2019-05-02', 1, 1, 0, '2019-06-21 17:06:01'),
(89, '54345353', 0, 'a4', '', 0, 0, '500.00', 50, 5, '600.00', '0.00', 0, '650.00', 5, '', '2019-05-03', 1, 1, 0, '2019-06-21 17:06:01'),
(90, '54345350', 0, 'a1', '', 0, 0, '500.00', 50, 5, '600.00', '0.00', 0, '650.00', 5, '', '2019-06-30', 2, 1, 1, '2019-06-21 17:14:58'),
(91, '54345351', 0, 'a2', '', 0, 0, '500.00', 50, 5, '600.00', '0.00', 0, '650.00', 5, '', '2019-08-01', 1, 1, 1, '2019-06-21 17:14:58'),
(92, '54345352', 0, 'a3', '', 0, -1, '500.00', 50, 5, '600.00', '0.00', 0, '650.00', 5, '', '2019-05-02', 1, 1, 1, '2019-06-21 17:14:58'),
(93, '54345353', 0, 'a4', '', 0, 0, '500.00', 50, 5, '600.00', '0.00', 0, '650.00', 5, '', '2019-05-03', 1, 1, 1, '2019-06-21 17:14:58'),
(94, 'A001', 0, 'Zapato Mangoo de prueba', '', 0, 0, '100.00', 20, 0, '120.00', '0.00', 0, '110.00', 10, '', '2019-07-05', 1, 1, 1, '2019-07-05 18:30:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_sucursales`
--

CREATE TABLE `productos_sucursales` (
  `idproductosucursal` int(11) NOT NULL,
  `idsucursal` int(11) NOT NULL,
  `idproducto` bigint(20) NOT NULL,
  `precio_venta` float NOT NULL,
  `mayoreo` float NOT NULL,
  `cuantos` int(11) NOT NULL,
  `existencia` int(11) NOT NULL,
  `minimo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos_sucursales`
--

INSERT INTO `productos_sucursales` (`idproductosucursal`, `idsucursal`, `idproducto`, `precio_venta`, `mayoreo`, `cuantos`, `existencia`, `minimo`) VALUES
(183, 1, 82, 300, 350, 5, 67, 2),
(184, 2, 82, 500, 1000, 5, 31, 5),
(185, 3, 82, 400, 250, 5, 100, 10),
(186, 1, 83, 360, 320, 7, 70, 3),
(187, 2, 83, 340, 100, 7, 24, 15),
(188, 3, 83, 560, 620, 7, 48, 56),
(189, 1, 84, 390, 400, 8, 40, 8),
(190, 2, 84, 290, 644, 8, 57, 60),
(191, 3, 84, 690, 800, 8, 40, 45),
(192, 1, 85, 390, 400, 8, 40, 8),
(193, 2, 85, 590, 700, 8, 64, 24),
(194, 3, 85, 490, 200, 8, 70, 7),
(195, 1, 86, 300, 350, 5, 67, 2),
(196, 2, 86, 500, 1000, 5, 31, 5),
(197, 3, 86, 400, 250, 5, 100, 10),
(198, 1, 87, 360, 320, 7, 70, 3),
(199, 2, 87, 340, 100, 7, 24, 15),
(200, 3, 87, 560, 620, 7, 48, 56),
(201, 1, 88, 390, 400, 8, 40, 8),
(202, 2, 88, 290, 644, 8, 57, 60),
(203, 3, 88, 690, 800, 8, 40, 45),
(204, 1, 89, 390, 400, 8, 40, 8),
(205, 2, 89, 590, 700, 8, 64, 24),
(206, 3, 89, 490, 200, 8, 70, 7),
(207, 1, 90, 300, 350, 5, 67, 2),
(208, 2, 90, 500, 1000, 5, 31, 5),
(209, 3, 90, 400, 250, 5, 100, 10),
(210, 1, 91, 360, 320, 7, 70, 3),
(211, 2, 91, 340, 100, 7, 24, 15),
(212, 3, 91, 560, 620, 7, 48, 56),
(213, 1, 92, 390, 400, 8, 40, 8),
(214, 2, 92, 290, 644, 8, 57, 60),
(215, 3, 92, 690, 800, 8, 40, 45),
(216, 1, 93, 390, 400, 8, 38, 8),
(217, 2, 93, 590, 700, 8, 64, 24),
(218, 3, 93, 490, 200, 8, 70, 7),
(219, 1, 94, 120, 110, 10, 99, 0),
(220, 2, 94, 120, 110, 10, 0, 0),
(221, 3, 94, 120, 110, 10, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `id_proveedor` int(11) NOT NULL,
  `razon_social` varchar(50) NOT NULL,
  `domicilio` varchar(30) NOT NULL,
  `ciudad` varchar(25) NOT NULL,
  `cp` varchar(8) NOT NULL,
  `id_estado` int(11) NOT NULL,
  `telefono_local` varchar(10) NOT NULL,
  `telefono_celular` varchar(10) NOT NULL,
  `contacto` varchar(100) NOT NULL,
  `email_contacto` varchar(60) NOT NULL,
  `rfc` varchar(13) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `obser` text NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`id_proveedor`, `razon_social`, `domicilio`, `ciudad`, `cp`, `id_estado`, `telefono_local`, `telefono_celular`, `contacto`, `email_contacto`, `rfc`, `fax`, `obser`, `activo`) VALUES
(23, 'BIMBO S.A DE C.V', 'AAA', 'AAA', '12345', 21, '123', '123', 'AAA', 'AAA', 'HERD890308UAA', '', '', 0),
(24, 'DULCERIA SUSY S.A DE C.V. SUCURSAL 3', '104 PTE NO 1720-A LOC 10 COL. ', 'PUEBLA', '', 21, '', '2227535616', 'HECTOR', '', 'DSU910312LSO', '', 'asdasdasdasdass', 1),
(25, 'laknsldknaklsdnlkasd', '', '', '', 0, '', '', '', '', '', '', '', 1),
(26, 'razon social2', 'domicilio2', 'ciudad2', '94140', 30, '2345678', '123456789', 'contacto', 'email', 'rfc', '123456789', 'obser', 0),
(27, 'razon', 'domi', 'ciudad', '94140', 30, '2345678', '23456789', 'contacto', 'email', 'rfc', '345678', 'obser', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursal`
--

CREATE TABLE `sucursal` (
  `idsucursal` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `estatus` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sucursal`
--

INSERT INTO `sucursal` (`idsucursal`, `nombre`, `estatus`) VALUES
(1, 'Matriz', 1),
(2, 'Sucursal 1', 1),
(3, 'Sucursal 2', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticket`
--

CREATE TABLE `ticket` (
  `id_ticket` int(11) NOT NULL,
  `titulo` text CHARACTER SET utf8 NOT NULL,
  `mensaje` text CHARACTER SET utf8 NOT NULL,
  `mensaje2` text CHARACTER SET utf8 NOT NULL,
  `fuente` int(1) NOT NULL,
  `tamano` int(2) NOT NULL,
  `margensup` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ticket`
--

INSERT INTO `ticket` (`id_ticket`, `titulo`, `mensaje`, `mensaje2`, `fuente`, `tamano`, `margensup`) VALUES
(1, 'APOYANDO LA ECONOMIA', 'VENUSTIANO CARRANZA 15<br>\nSAN BARTOLOME<br>\nSAN PABLO DEL MONTE TLAX.<br>\nGRACIAS POR SU PREFERENCIA<br>\n', 'SI LOS PRODUCTOS QUE COMPRASTE NO ESTAN EN EL TICKET, REPORTALO AL WHATSAPP 2226164890 Y TU COMPRA SERA GRATIS.\n', 2, 7, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `traspasos`
--

CREATE TABLE `traspasos` (
  `traspasoId` int(11) NOT NULL,
  `productoId` bigint(20) NOT NULL,
  `suc_salida` int(11) NOT NULL,
  `suc_entrada` int(11) NOT NULL,
  `cantidad` float NOT NULL,
  `personalId` int(11) NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `traspasos`
--

INSERT INTO `traspasos` (`traspasoId`, `productoId`, `suc_salida`, `suc_entrada`, `cantidad`, `personalId`, `reg`) VALUES
(1, 82, 1, 2, 10, 1, '2019-07-08 03:52:02'),
(2, 94, 1, 2, 1, 1, '2019-07-08 18:44:49'),
(3, 94, 1, 3, 10, 1, '2019-07-08 18:54:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `turno`
--

CREATE TABLE `turno` (
  `id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `horaa` time NOT NULL,
  `fechacierre` date NOT NULL,
  `horac` time NOT NULL,
  `cantidad` float NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `user` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `turno`
--

INSERT INTO `turno` (`id`, `fecha`, `horaa`, `fechacierre`, `horac`, `cantidad`, `nombre`, `status`, `user`) VALUES
(59, '2016-10-14', '00:37:05', '0000-00-00', '00:37:45', 100, 'x', 'cerrado', 'user'),
(60, '2016-10-14', '00:39:37', '0000-00-00', '06:11:06', 1, 'b', 'cerrado', 'user'),
(61, '2016-10-24', '00:38:48', '0000-00-00', '00:40:57', 100, 'dia', 'cerrado', 'user'),
(62, '2016-10-24', '00:43:15', '0000-00-00', '11:03:17', 100, '100', 'cerrado', 'user'),
(63, '2017-05-19', '11:03:33', '0000-00-00', '20:00:22', 100, 'aaa', 'cerrado', 'user'),
(64, '2018-01-10', '20:02:43', '0000-00-00', '20:11:01', 0, '', 'cerrado', 'user'),
(65, '2018-01-10', '20:19:13', '0000-00-00', '09:20:55', 0, '', 'cerrado', 'user'),
(66, '2018-01-15', '09:21:24', '0000-00-00', '00:00:00', 0, 'ESMERLDA', 'abierto', 'user'),
(67, '2018-01-15', '09:21:24', '0000-00-00', '00:00:00', 0, 'ESMERLDA', 'abierto', 'user'),
(68, '2018-01-15', '09:21:27', '0000-00-00', '10:01:26', 0, 'ESMERLDA', 'cerrado', 'user'),
(69, '2018-01-15', '10:01:28', '0000-00-00', '00:00:00', 0, '', 'abierto', 'user'),
(70, '2018-01-15', '10:01:43', '0000-00-00', '14:10:45', 500, 'ESMERALDA', 'cerrado', 'user'),
(71, '2018-01-15', '14:11:09', '0000-00-00', '13:49:39', 600, 'marijo', 'cerrado', 'user'),
(72, '2018-01-16', '13:49:46', '0000-00-00', '00:00:00', 0, '', 'abierto', 'user'),
(73, '2018-01-16', '13:50:01', '0000-00-00', '00:00:00', 500, 'MARIJO', 'abierto', 'user'),
(74, '2018-01-16', '13:50:11', '0000-00-00', '13:45:17', 500, 'MARIJO', 'cerrado', 'user'),
(75, '2018-01-17', '13:45:36', '0000-00-00', '14:03:13', 0, '', 'cerrado', 'user'),
(76, '2018-01-17', '14:03:34', '0000-00-00', '14:48:36', 500, 'MARI JOSE', 'cerrado', 'user'),
(77, '2018-01-19', '14:48:47', '0000-00-00', '18:19:52', 500, 'marijo', 'cerrado', 'user'),
(78, '2018-01-22', '18:24:30', '0000-00-00', '08:39:02', 500, 'marijo', 'cerrado', 'user'),
(79, '2018-01-23', '08:39:13', '0000-00-00', '00:00:00', 0, '', 'abierto', 'user'),
(80, '2018-01-23', '08:39:27', '0000-00-00', '14:02:30', 500, 'ESMERALDA', 'cerrado', 'user'),
(81, '2018-01-23', '14:02:40', '0000-00-00', '00:00:00', 0, '', 'abierto', 'user'),
(82, '2018-01-23', '14:02:49', '0000-00-00', '09:41:46', 0, 'MARIJO', 'cerrado', 'user'),
(83, '2018-01-24', '09:44:21', '0000-00-00', '14:04:56', 500, 'ESME', 'cerrado', 'user'),
(84, '2018-01-24', '14:05:03', '0000-00-00', '14:05:20', 0, '', 'cerrado', 'user'),
(85, '2018-01-24', '14:05:30', '0000-00-00', '00:00:00', 0, '', 'abierto', 'user'),
(86, '2018-01-24', '14:05:55', '0000-00-00', '00:00:00', 0, '', 'abierto', 'user'),
(87, '2018-01-24', '14:06:08', '0000-00-00', '14:07:24', 0, 'MARIJO', 'cerrado', 'user'),
(88, '2018-01-24', '14:07:32', '0000-00-00', '00:00:00', 0, '', 'abierto', 'user'),
(89, '2018-01-24', '14:07:44', '0000-00-00', '07:30:20', 500, 'MARIJO', 'cerrado', 'user'),
(90, '2018-01-25', '07:30:32', '0000-00-00', '00:00:00', 500, '', 'abierto', 'user'),
(91, '2018-01-25', '07:30:39', '0000-00-00', '14:10:39', 500, 'esmeralda', 'cerrado', 'user'),
(92, '2018-01-25', '14:10:55', '0000-00-00', '00:00:00', 500, 'MARIJO', 'abierto', 'user'),
(93, '2018-01-25', '14:10:58', '0000-00-00', '07:42:52', 500, 'MARIJO', 'cerrado', 'user'),
(94, '2018-01-26', '07:43:03', '0000-00-00', '11:23:32', 500, 'esme', 'cerrado', 'user'),
(95, '2018-05-08', '12:00:16', '2018-05-11', '17:58:24', 400, 'prueba', 'cerrado', 'user'),
(96, '2018-05-15', '13:29:48', '0000-00-00', '00:00:00', 100, 'xxx', 'abierto', 'user');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `UsuarioID` int(11) NOT NULL,
  `perfilId` int(11) NOT NULL,
  `personalId` int(11) NOT NULL,
  `Usuario` varchar(45) DEFAULT NULL,
  `contrasena` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`UsuarioID`, `perfilId`, `personalId`, `Usuario`, `contrasena`) VALUES
(1, 1, 1, 'admin', '$2y$10$.yBpfsTTEmhYhECki.qy3eL45XaLo3MinuIPgiPsjmXBYItZXHUga'),
(2, 2, 2, 'gerardo', '$2y$10$XE359fjs0SS/bB.L8ogiwefN8we7nm5ZfCDEGqC6gNNZy/06MTPlS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id_venta` int(11) NOT NULL,
  `id_personal` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `metodo` int(1) NOT NULL COMMENT '1 efectivo, 2 credito, 3 debito',
  `subtotal` double NOT NULL,
  `descuento` double NOT NULL COMMENT '0 % 5 % 7%',
  `descuentocant` double NOT NULL,
  `monto_total` double NOT NULL,
  `pagotarjeta` float NOT NULL,
  `efectivo` float NOT NULL,
  `sucursalid` int(11) NOT NULL,
  `cancelado` int(11) NOT NULL,
  `hcancelacion` date NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id_venta`, `id_personal`, `id_cliente`, `metodo`, `subtotal`, `descuento`, `descuentocant`, `monto_total`, `pagotarjeta`, `efectivo`, `sucursalid`, `cancelado`, `hcancelacion`, `reg`) VALUES
(1, 1, 45, 1, 1035, 0, 0, 1035, 0, 0, 0, 1, '2018-05-09', '2018-05-07 20:54:14'),
(2, 1, 45, 1, 3430, 0.05, 171.5, 3258.5, 0, 0, 0, 1, '2018-05-15', '2018-05-08 21:29:25'),
(3, 1, 45, 1, 3430, 0.05, 171.5, 3258.5, 0, 0, 0, 1, '2018-05-15', '2018-05-08 21:29:35'),
(4, 1, 45, 1, 7310.6, 0, 0, 7310.6, 0, 0, 0, 0, '0000-00-00', '2018-05-08 23:13:49'),
(5, 1, 45, 1, 690, 0, 0, 690, 0, 0, 0, 0, '0000-00-00', '2018-05-11 17:42:29'),
(6, 1, 45, 1, 116969.6, 0, 0, 116969.6, 0, 0, 0, 0, '0000-00-00', '2018-05-11 21:59:27'),
(7, 1, 45, 1, 690, 0, 0, 690, 0, 0, 0, 0, '0000-00-00', '2018-05-11 22:00:17'),
(8, 1, 45, 1, 1380, 0, 0, 1380, 0, 0, 0, 0, '0000-00-00', '2018-05-11 22:02:06'),
(9, 1, 45, 1, 33759, 0, 0, 33759, 0, 0, 0, 0, '0000-00-00', '2018-05-11 22:03:33'),
(10, 1, 45, 1, 67086.88, 0, 0, 67086.88, 0, 0, 0, 0, '0000-00-00', '2018-05-11 22:06:02'),
(11, 1, 45, 1, 345, 0, 0, 345, 0, 0, 0, 0, '0000-00-00', '2018-05-15 18:55:25'),
(12, 1, 45, 1, 345, 0, 0, 345, 0, 0, 0, 0, '0000-00-00', '2018-05-15 19:04:57'),
(13, 1, 45, 1, 345, 0, 0, 345, 0, 0, 0, 0, '0000-00-00', '2018-05-15 19:07:17'),
(14, 1, 45, 1, 345, 0, 0, 345, 0, 0, 0, 0, '0000-00-00', '2018-05-17 21:06:54'),
(15, 1, 45, 1, 6138, 0, 0, 6138, 5000, 1138, 0, 0, '0000-00-00', '2018-05-24 17:28:45'),
(16, 1, 45, 1, 0, 0, 0, 0, 0, 0, 0, 0, '0000-00-00', '2019-07-03 23:56:33'),
(17, 1, 45, 1, 4000, 0, 0, 4000, 0, 4000, 1, 0, '0000-00-00', '2019-07-04 17:43:59'),
(18, 1, 45, 1, 3200, 0, 0, 3200, 0, 3200, 1, 0, '0000-00-00', '2019-07-04 17:45:51'),
(19, 1, 45, 1, 390, 0, 0, 390, 0, 390, 1, 0, '0000-00-00', '2019-07-04 18:01:01'),
(20, 1, 45, 1, 390, 0, 0, 390, 0, 390, 1, 0, '0000-00-00', '2019-07-04 18:03:17'),
(21, 1, 45, 1, 390, 0, 0, 390, 0, 390, 1, 0, '0000-00-00', '2019-07-04 18:08:19'),
(22, 1, 45, 1, 780, 0, 0, 780, 0, 780, 1, 0, '0000-00-00', '2019-07-04 18:09:21'),
(23, 1, 45, 1, 1560, 0, 0, 1560, 0, 1560, 1, 0, '0000-00-00', '2019-07-04 18:10:55'),
(24, 1, 45, 1, 390, 0, 0, 390, 0, 390, 1, 0, '0000-00-00', '2019-07-04 18:12:33'),
(25, 1, 45, 1, 390, 0, 0, 390, 0, 390, 1, 0, '0000-00-00', '2019-07-04 18:18:34'),
(26, 1, 45, 1, 120, 0, 0, 120, 0, 120, 1, 0, '0000-00-00', '2019-07-05 18:32:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_detalle`
--

CREATE TABLE `venta_detalle` (
  `id_detalle_venta` int(11) NOT NULL,
  `id_venta` int(11) NOT NULL,
  `id_producto` bigint(20) NOT NULL,
  `cantidad` float NOT NULL,
  `precio` decimal(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `venta_detalle`
--

INSERT INTO `venta_detalle` (`id_detalle_venta`, `id_venta`, `id_producto`, `cantidad`, `precio`) VALUES
(1, 16, 92, 1, '0.00'),
(2, 17, 93, 10, '400.00'),
(3, 18, 93, 8, '400.00'),
(4, 19, 93, 1, '390.00'),
(5, 20, 93, 1, '390.00'),
(6, 21, 93, 1, '390.00'),
(7, 22, 93, 2, '390.00'),
(8, 23, 93, 4, '390.00'),
(9, 24, 93, 1, '390.00'),
(10, 25, 93, 1, '390.00'),
(11, 26, 94, 1, '120.00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`categoriaId`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`ClientesId`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`id_compra`),
  ADD KEY `constraint_fk_04` (`id_proveedor`);

--
-- Indices de la tabla `compra_detalle`
--
ALTER TABLE `compra_detalle`
  ADD PRIMARY KEY (`id_detalle_compra`),
  ADD KEY `constraint_fk_08` (`id_compra`),
  ADD KEY `constraint_fk_09` (`id_producto`);

--
-- Indices de la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`iddepartamento`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`EstadoId`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`MenuId`);

--
-- Indices de la tabla `menu_sub`
--
ALTER TABLE `menu_sub`
  ADD PRIMARY KEY (`MenusubId`),
  ADD KEY `fk_menu_sub_menu_idx` (`MenuId`);

--
-- Indices de la tabla `notas`
--
ALTER TABLE `notas`
  ADD PRIMARY KEY (`id_nota`);

--
-- Indices de la tabla `perfiles`
--
ALTER TABLE `perfiles`
  ADD PRIMARY KEY (`perfilId`);

--
-- Indices de la tabla `perfiles_detalles`
--
ALTER TABLE `perfiles_detalles`
  ADD PRIMARY KEY (`Perfil_detalleId`),
  ADD KEY `fk_Perfiles_detalles_Perfiles1_idx` (`perfilId`),
  ADD KEY `fk_Perfiles_detalles_menu_sub1_idx` (`MenusubId`);

--
-- Indices de la tabla `personal`
--
ALTER TABLE `personal`
  ADD PRIMARY KEY (`personalId`);

--
-- Indices de la tabla `personal_menu`
--
ALTER TABLE `personal_menu`
  ADD PRIMARY KEY (`personalmenuId`),
  ADD KEY `personal_fkpersona` (`personalId`),
  ADD KEY `personal_fkmenu` (`MenuId`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`productoid`),
  ADD KEY `producto_fk_departamento` (`iddepartamento`);

--
-- Indices de la tabla `productos_sucursales`
--
ALTER TABLE `productos_sucursales`
  ADD PRIMARY KEY (`idproductosucursal`),
  ADD KEY `existencia_fk_sucursal` (`idsucursal`),
  ADD KEY `existencia_fk_producto` (`idproducto`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`id_proveedor`),
  ADD KEY `constraint_fk_27` (`id_estado`);

--
-- Indices de la tabla `sucursal`
--
ALTER TABLE `sucursal`
  ADD PRIMARY KEY (`idsucursal`);

--
-- Indices de la tabla `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id_ticket`);

--
-- Indices de la tabla `traspasos`
--
ALTER TABLE `traspasos`
  ADD PRIMARY KEY (`traspasoId`),
  ADD KEY `traspaso_fk_salida` (`suc_salida`),
  ADD KEY `traspaso_fk_entrada` (`suc_entrada`),
  ADD KEY `traspaso_fk_personal` (`personalId`),
  ADD KEY `traspaso_fk_producto` (`productoId`);

--
-- Indices de la tabla `turno`
--
ALTER TABLE `turno`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`UsuarioID`),
  ADD KEY `fk_usuarios_Perfiles1_idx` (`perfilId`),
  ADD KEY `fk_usuarios_personal1_idx` (`personalId`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id_venta`),
  ADD UNIQUE KEY `id_venta_UNIQUE` (`id_venta`),
  ADD KEY `constraint_fk_30` (`id_cliente`),
  ADD KEY `constraint_fk_31` (`id_personal`);

--
-- Indices de la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD PRIMARY KEY (`id_detalle_venta`),
  ADD UNIQUE KEY `id_detalle_venta_UNIQUE` (`id_detalle_venta`),
  ADD KEY `constraint_fk_15` (`id_producto`),
  ADD KEY `constraint_fk_16` (`id_venta`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `categoriaId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `ClientesId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `id_compra` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `compra_detalle`
--
ALTER TABLE `compra_detalle`
  MODIFY `id_detalle_compra` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `departamento`
--
ALTER TABLE `departamento`
  MODIFY `iddepartamento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `EstadoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `MenuId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `menu_sub`
--
ALTER TABLE `menu_sub`
  MODIFY `MenusubId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `notas`
--
ALTER TABLE `notas`
  MODIFY `id_nota` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `perfiles`
--
ALTER TABLE `perfiles`
  MODIFY `perfilId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `perfiles_detalles`
--
ALTER TABLE `perfiles_detalles`
  MODIFY `Perfil_detalleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `personal`
--
ALTER TABLE `personal`
  MODIFY `personalId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `personal_menu`
--
ALTER TABLE `personal_menu`
  MODIFY `personalmenuId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `productoid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
--
-- AUTO_INCREMENT de la tabla `productos_sucursales`
--
ALTER TABLE `productos_sucursales`
  MODIFY `idproductosucursal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=222;
--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `id_proveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT de la tabla `sucursal`
--
ALTER TABLE `sucursal`
  MODIFY `idsucursal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id_ticket` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `traspasos`
--
ALTER TABLE `traspasos`
  MODIFY `traspasoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `turno`
--
ALTER TABLE `turno`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `UsuarioID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT de la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  MODIFY `id_detalle_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `compra_detalle`
--
ALTER TABLE `compra_detalle`
  ADD CONSTRAINT `fk_compra_compra` FOREIGN KEY (`id_compra`) REFERENCES `compras` (`id_compra`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_compra_producto` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`productoid`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `menu_sub`
--
ALTER TABLE `menu_sub`
  ADD CONSTRAINT `fk_menu_sub_menu` FOREIGN KEY (`MenuId`) REFERENCES `menu` (`MenuId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `perfiles_detalles`
--
ALTER TABLE `perfiles_detalles`
  ADD CONSTRAINT `fk_Perfiles_detalles_Perfiles1` FOREIGN KEY (`perfilId`) REFERENCES `perfiles` (`perfilId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Perfiles_detalles_menu_sub1` FOREIGN KEY (`MenusubId`) REFERENCES `menu_sub` (`MenusubId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `personal_menu`
--
ALTER TABLE `personal_menu`
  ADD CONSTRAINT `personal_fkmenu` FOREIGN KEY (`MenuId`) REFERENCES `menu_sub` (`MenusubId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `personal_fkpersona` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `producto_fk_departamento` FOREIGN KEY (`iddepartamento`) REFERENCES `departamento` (`iddepartamento`);

--
-- Filtros para la tabla `productos_sucursales`
--
ALTER TABLE `productos_sucursales`
  ADD CONSTRAINT `pro_suc_fk_producto` FOREIGN KEY (`idproducto`) REFERENCES `productos` (`productoid`),
  ADD CONSTRAINT `pro_suc_fk_sucursal` FOREIGN KEY (`idsucursal`) REFERENCES `sucursal` (`idsucursal`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `traspasos`
--
ALTER TABLE `traspasos`
  ADD CONSTRAINT `traspaso_fk_entrada` FOREIGN KEY (`suc_entrada`) REFERENCES `sucursal` (`idsucursal`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `traspaso_fk_personal` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `traspaso_fk_producto` FOREIGN KEY (`productoId`) REFERENCES `productos` (`productoid`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `traspaso_fk_salida` FOREIGN KEY (`suc_salida`) REFERENCES `sucursal` (`idsucursal`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_usuarios_Perfiles1` FOREIGN KEY (`perfilId`) REFERENCES `perfiles` (`perfilId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`);

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `fk_venta_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`ClientesId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_venta_presonal` FOREIGN KEY (`id_personal`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD CONSTRAINT `fk_detalle_producto` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`productoid`),
  ADD CONSTRAINT `fk_detalle_venta` FOREIGN KEY (`id_venta`) REFERENCES `ventas` (`id_venta`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


ALTER TABLE `clientes` ADD `rfcdf` TEXT NOT NULL AFTER `descripcionc`;
ALTER TABLE `ventas` ADD `facturado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1 = si' AFTER `reg`;
ALTER TABLE `clientes` ADD `razon_social` TEXT NOT NULL AFTER `rfcdf`;
ALTER TABLE `clientes` ADD `direccion_fiscal` TEXT NOT NULL AFTER `razon_social`, ADD `cp_fiscal` VARCHAR(5) NOT NULL AFTER `direccion_fiscal`;
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES (NULL, '2', 'Lista de Facturas', 'Facturaslis', 'fa fa-file');
INSERT INTO `personal_menu` (`personalmenuId`, `personalId`, `MenuId`) VALUES (NULL, '1', '21');

ALTER TABLE `f_facturas` ADD `f_relacion` TINYINT NOT NULL AFTER `facturaabierta`, ADD `f_r_tipo` VARCHAR(2) NOT NULL AFTER `f_relacion`, ADD `f_r_uuid` VARCHAR(60) NOT NULL AFTER `f_r_tipo`;
ALTER TABLE `f_facturas_servicios` ADD `descuento` DECIMAL(10,2) NOT NULL AFTER `iva`;
ALTER TABLE `f_facturas` ADD `CondicionesDePago` VARCHAR(100) NOT NULL AFTER `serie`;
