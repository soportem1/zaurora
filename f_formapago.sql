-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Servidor: mysql1002.mochahost.com:3306
-- Tiempo de generación: 28-03-2022 a las 13:21:50
-- Versión del servidor: 10.3.31-MariaDB
-- Versión de PHP: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sicoinet_kyocera2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `f_formapago`
--

CREATE TABLE `f_formapago` (
  `id` int(11) NOT NULL,
  `formapago` varchar(50) NOT NULL,
  `formapago_text` text NOT NULL,
  `activo` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `f_formapago`
--

INSERT INTO `f_formapago` (`id`, `formapago`, `formapago_text`, `activo`) VALUES
(1, 'Efectivo', '01 Efectivo', 1),
(2, 'ChequeNominativo', '02 Cheque nominativo', 1),
(3, 'TransferenciaElectronicaFondos', '03 Transferencia electrónica de fondos', 1),
(4, 'TarjetasDeCredito', '04 Tarjetas de Crédito', 1),
(5, 'MonederoElectronico', '05 Monedero Electrónico', 1),
(6, 'DineroElectronico', '06 Dinero Electrónico', 1),
(7, 'ValesDeDespensa', '08 Vales de despensa', 1),
(8, 'DacionPago', '12 Dación en pago', 1),
(9, 'PagoSubrogacion', '13 Pago por subrogación', 1),
(10, 'PagoConsignacion', '14 Pago por consignación', 1),
(11, 'Condonacion', '15 Condonación', 1),
(12, 'Compensacion', '17 Compensación', 1),
(13, 'Novacion', '23 Novación', 1),
(14, 'Confusion', '24 Confusión', 1),
(15, 'RemisionDeuda', '25 Remisión de deuda', 1),
(16, 'PrescripcionoCaducidad', '26 Prescripción o caducidad', 1),
(17, 'SatisfaccionAcreedor', '27 A satisfacción del acreedor', 1),
(18, 'TarjetaDebito', '28 Tarjeta de Débito', 1),
(19, 'TarjetaServicio', '29 Tarjeta de Servicio', 1),
(20, 'AplicacionAnticipos', '30 Aplicación de anticipos', 1),
(21, 'PorDefinir', '99 Por definir', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `f_formapago`
--
ALTER TABLE `f_formapago`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `f_formapago`
--
ALTER TABLE `f_formapago`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
