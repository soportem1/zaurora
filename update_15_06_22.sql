ALTER TABLE `venta_detalle` ADD `id_vendedor` INT NOT NULL AFTER `id_producto`;
ALTER TABLE `log_cambios` ADD `id_producto` INT NOT NULL AFTER `id_sucursal`;
ALTER TABLE `personal` ADD `num_checador` VARCHAR(25) NOT NULL AFTER `tipo`, ADD `salario_dia` DECIMAL(10,2) NOT NULL AFTER `num_checador`, ADD `hora_extra` DECIMAL(10,2) NOT NULL AFTER `salario_dia`, ADD `hora_doble` DECIMAL(10,2) NOT NULL AFTER `hora_extra`, ADD `hora_dia_feriado` DECIMAL(10,2) NOT NULL AFTER `hora_doble`, ADD `comision_venta` DECIMAL(6,2) NOT NULL AFTER `hora_dia_feriado`;
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES (NULL, '2', 'Boleta de Pago', 'Pagos', 'fa fa-usd');
INSERT INTO `personal_menu` (`personalmenuId`, `personalId`, `MenuId`) VALUES (NULL, '1', '22');
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES (NULL, '2', 'Bonos y Descuentos', 'Pagos/BonosDescuentos', 'fa fa-usd');
INSERT INTO `personal_menu` (`personalmenuId`, `personalId`, `MenuId`) VALUES (NULL, '1', '23');

/* ** add table bonos_descuentos ** */
ALTER TABLE `personal` ADD `dias_vaca` TINYINT NOT NULL AFTER `sueldo`;
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES (NULL, '3', 'Descuentos', 'Vacaciones/config', 'fa fa-gears');
INSERT INTO `personal_menu` (`personalmenuId`, `personalId`, `MenuId`) VALUES (NULL, '1', '24');
/* ** add table config_descuentos ** */
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES (NULL, '2', 'Vacaciones', 'Vacaciones', 'fa fa-briefcase');
INSERT INTO `personal_menu` (`personalmenuId`, `personalId`, `MenuId`) VALUES (NULL, '1', '25');
/* ** add table vacaciones_empleado ** */

/* **********************18-07-23******************************* */
ALTER TABLE `productos` ADD `notas` TEXT NOT NULL AFTER `tipo_prod`;
ALTER TABLE `log_cambios` ADD `id_producto` INT NOT NULL AFTER `id_sucursal`;

/* **********************04-12-23******************** */
ALTER TABLE `personal` ADD `almacen` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=si' AFTER `dias_vaca`;


/* **********************11-07-24******************** */
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES (NULL, '2', 'Puntos Aurora', 'PuntosAurora', 'fa fa-ticket')
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '26');
INSERT INTO `personal_menu` (`personalmenuId`, `personalId`, `MenuId`) VALUES (NULL, '1', '26');

ALTER TABLE `ventas` ADD `puntos_total` DOUBLE NOT NULL DEFAULT '0' AFTER `monto_total`;
ALTER TABLE `clientes` ADD `puntos` DOUBLE NOT NULL DEFAULT '0' AFTER `cp_fiscal`;

/* **********************17-07-24******************** */
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES (NULL, '3', 'Config. de puntos', 'ConfigPuntos', 'fa fa-cogs');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '27');
INSERT INTO `personal_menu` (`personalmenuId`, `personalId`, `MenuId`) VALUES (NULL, '1', '27');

CREATE TABLE `aurora`.`config_puntos` ( `id` INT NOT NULL AUTO_INCREMENT , `porcentaje` INT(11) NOT NULL , `checkFecha` INT(1) NOT NULL COMMENT '0=dosFechas, 1=unaFecha' , `fechaIni` DATE NOT NULL , `fechaFin` DATE NOT NULL , `id_personal` INT(11) NOT NULL , `reg` DATETIME NOT NULL , `activo` INT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;
INSERT INTO `config_puntos` (`id`, `porcentaje`, `checkFecha`, `fechaIni`, `fechaFin`, `id_personal`, `reg`, `activo`) VALUES (NULL, '5', '1', '2024-01-01', '0000-00-00', '1', '2024-01-01 00:00:00', '1')
ALTER TABLE `ventas` ADD `porcentaje_puntos` INT(11) NOT NULL DEFAULT '0' AFTER `puntos_total`;
ALTER TABLE `ventas` ADD `en_monedero` INT(1) NOT NULL DEFAULT '0' AFTER `porcentaje_puntos`;
ALTER TABLE `ventas` CHANGE `en_monedero` `en_monedero` INT(1) NOT NULL DEFAULT '0' COMMENT '1 = Ya calculo puntos, 0 = Sin calculo';

ALTER TABLE `ventas` ADD `pagopuntos` FLOAT NOT NULL AFTER `efectivo`;
ALTER TABLE `clientes` CHANGE `puntos` `puntos` DECIMAL(10,2) NOT NULL DEFAULT '0';
ALTER TABLE `ventas` CHANGE `puntos_total` `puntos_total` DECIMAL(10,2) NOT NULL DEFAULT '0', CHANGE `pagopuntos` `pagopuntos` DECIMAL(10,2) NOT NULL;


/*------------------------------04 10 24-------------------------------*/
ALTER TABLE `personal` ADD `salario_hora` DECIMAL(10,2) NOT NULL AFTER `salario_dia`;
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES (NULL, '2', 'Asistencias', 'Asistencias', 'fa fa-clipboard');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '28');
INSERT INTO `personal_menu` (`personalmenuId`, `personalId`, `MenuId`) VALUES (NULL, '1', '28');
CREATE TABLE `aurora`.`asistencias` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `personal_id` INT(11) NOT NULL , `fecha` DATE NOT NULL , `entrada` TIME NOT NULL , `salida` TIME NOT NULL , `reg` DATETIME NOT NULL , `estatus` INT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;
/*------------------------------09 10 24-------------------------------*/
ALTER TABLE `asistencias` ADD `check_in` INT(1) NOT NULL DEFAULT '1' AFTER `entrada`;
ALTER TABLE `asistencias` ADD `check_out` INT(1) NOT NULL DEFAULT '0' AFTER `salida`;
ALTER TABLE `asistencias` ADD `reg_out` DATE  TIME NOT NULL AFTER `reg`;
ALTER TABLE `asistencias` CHANGE `reg` `reg_in` DATETIME NOT NULL;

/*------------------------------10 10 24-------------------------------*/
ALTER TABLE `clientes` ADD `porcentaje` INT(11) NOT NULL AFTER `puntos`;

/*------------------------------14 10 24-------------------------------*/
ALTER TABLE `clientes` ADD `anio_puntos` YEAR NOT NULL DEFAULT '2024' AFTER `porcentaje`;

/*------------------------------17 10 24-------------------------------*/
CREATE TABLE `aurora`.`bitacora_pago` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `empleadoId` INT(11) NOT NULL , `fechaIni` DATE NOT NULL , `fechaFin` DATE NOT NULL , `pagoHora` FLOAT NOT NULL , `cantidadHoras` FLOAT NOT NULL , `usuarioId` INT(11) NOT NULL , `reg` DATETIME NOT NULL , `estatus` INT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `bitacora_pago` ADD `pagoTotal` FLOAT NOT NULL AFTER `pagoHora`;

ALTER TABLE `clientes` CHANGE `porcentaje` `porcentaje` FLOAT NOT NULL;
ALTER TABLE `config_puntos` CHANGE `porcentaje` `porcentaje` FLOAT NOT NULL;

/*--------------------------------31-10-24-------------------------------------*/
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES (NULL, '2', 'Kardex', 'Kardex', 'fa fa-list-ul');
INSERT INTO `personal_menu` (`personalmenuId`, `personalId`, `MenuId`) VALUES (NULL, '1', '29');

ALTER TABLE `venta_detalle` ADD `cantInicial` FLOAT NOT NULL AFTER `cantidad`;
ALTER TABLE `venta_detalle` ADD `cancela_personal` INT(11) NOT NULL AFTER `descuento`, ADD `hcancelacion` INT(11) NOT NULL AFTER `cancela_personal`;

ALTER TABLE `compra_detalle` ADD `cantInicial` FLOAT NOT NULL AFTER `cantidad`;
ALTER TABLE `compras` ADD `id_personal` INT(11) NOT NULL AFTER `id_proveedor`;

CREATE TABLE `aurora`.`bitacora_eliminacion` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `id_producto` INT(11) NOT NULL , `id_personal` INT(11) NOT NULL , `tipo` INT(1) NOT NULL COMMENT '0=Eliminacion, 1=Edicion' , `cantInicial` FLOAT NOT NULL , `cantidad` FLOAT NOT NULL , `id_sucursal` INT NOT NULL , `reg` DATETIME NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

/*--------------------------------23-11-24-------------------------------------*/
ALTER TABLE `venta_detalle` ADD `cantIniCancel` FLOAT NOT NULL AFTER `descuento`;
ALTER TABLE `venta_detalle` CHANGE `hcancelacion` `hcancelacion` DATETIME NOT NULL;
ALTER TABLE `venta_detalle` ADD `sucursalid` INT(11) NOT NULL AFTER `descuento`;

/*--------------------------------28-11-24-------------------------------------*/
ALTER TABLE `compras` ADD `idsucursal` INT(11) NOT NULL AFTER `monto_total`;
ALTER TABLE `compra_detalle` ADD `idsucursal` INT(11) NOT NULL AFTER `cantInicial`;


/*--------------------------------03-01-25-------------------------------------*/
ALTER TABLE `clientes` ADD `CurrentYPoints` DECIMAL(10,2) NOT NULL AFTER `puntos`, ADD `LastYPoints` DECIMAL(10,2) NOT NULL AFTER `CurrentYPoints`;