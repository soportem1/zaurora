-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Servidor: mysql1002.mochahost.com:3306
-- Tiempo de generación: 28-03-2022 a las 13:20:27
-- Versión del servidor: 10.3.31-MariaDB
-- Versión de PHP: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sicoinet_kyocera2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `f_metodopago`
--

CREATE TABLE `f_metodopago` (
  `id` int(11) NOT NULL,
  `metodopago` varchar(50) NOT NULL,
  `metodopago_text` text NOT NULL,
  `activo` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `f_metodopago`
--

INSERT INTO `f_metodopago` (`id`, `metodopago`, `metodopago_text`, `activo`) VALUES
(1, 'PUE', 'Pago en una sola exhibición', 1),
(2, 'PPD', 'Pago en parcialidades o diferido', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `f_metodopago`
--
ALTER TABLE `f_metodopago`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `f_metodopago`
--
ALTER TABLE `f_metodopago`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
