DROP TABLE IF EXISTS `venta_pagos_credito`;
CREATE TABLE IF NOT EXISTS `venta_pagos_credito` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_venta` int(11) NOT NULL,
  `metodo` int(11) NOT NULL,
  `referencia` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `monto` decimal(10,2) NOT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;