var base_url = $("#base_url").val();

$(document).ready(function () {
	$("#personal").select2({
		width: "resolve",
		minimumInputLength: 2,
		minimumResultsForSearch: 10,
		placeholder: "Buscar personal",
		ajax: {
			url: "Kardex/searchPersonal",
			dataType: "json",
			data: function (params) {
				var query = {
					search: params.term,
					type: "public",
				};
				return query;
			},
			processResults: function (data) {
				var personal = data;
				var itemsPersonal = [];
				data.forEach(function (element) {
					console.log(element);
					itemsPersonal.push({
						id: element.personalId,
						text: element.nombre + " " + element.apellidos,
					});
				});
				return {
					results: itemsPersonal,
				};
			},
		},
	});

	$("#chkFecha").change(function () {
		if (document.getElementById("chkFecha").checked) {
			f = new Date();
			if (f.getDate() < 10) {
				dia = "0" + f.getDate();
			} else {
				dia = f.getDate();
			}
			mes = Number(f.getMonth() + 1);
			if (f.getMonth() + 1 < 10) {
				mes = "0" + mes;
			} else {
				mes = mes;
			}

			fecha = f.getFullYear() + "-" + mes + "-" + dia;
			$("#fechaIni").val(fecha);
			$("#fechaFin").val(fecha);
			$("#fechaIni").attr("disabled", "disabled");
			$("#fechaFin").attr("disabled", "disabled");
		} else {
			$("#fechaIni").val("");
			$("#fechaFin").val("");
			$("#fechaIni").removeAttr("disabled");
			$("#fechaFin").removeAttr("disabled");
		}
	});
});

function load_table() {
	console.log("GET DATA-----");
	fInicio = $("#fechaIni").val();
	fFinal = $("#fechaFin").val();
	usuario = $("#personal option:selected").val();
	tipo = $("#tipo option:selected").val();
	sucursal = $("#sucursal option:selected").val();

	/*
	if (!usuario || usuario <= 0) {
		toastr.error("Seleccione un usuario valido.", "Error");
		return;
	}
	*/

	if (!tipo || tipo < 0 || tipo > 5) {
		toastr.error("Seleccione tipo de movimiento valido.", "Error");
		return;
	}

	if (!sucursal || sucursal < 0) {
		toastr.error("Seleccione una sucursal valida.", "Error");
		return;
	}

	if (fInicio != "" && fFinal != "") {
		if (fInicio > fFinal) {
			toastr.error("No existen fechas validas", "Error");
			return;
		}

		$.blockUI({
			message: "Espere por favor... ",
			css: {
				border: "none",
				padding: "15px",
				backgroundColor: "#000",
				"-webkit-border-radius": "10px",
				"-moz-border-radius": "10px",
				opacity: 0.5,
				color: "#fff",
			},
		});

		table = $("#data-tableK").DataTable({
			stateSave: false,
			//responsive: true,"bProcessing": true,
			serverSide: true,
			//searching: true,
			responsive: !0,
			destroy: true,
			ajax: {
				type: "POST",
				url: base_url + "Kardex/get_list",
				data: {
					id: usuario,
					tipo: tipo,
					sucursal: sucursal,
					fInicio: fInicio,
					fFinal: fFinal,
				},
			},
			columns: [
				{
					//0
					data: null,
					render: function (row) {
						return formatoFecha(row.fecha);
					},
				},
				{
					//1
					data: null,
					render: function (row) {
						return formato12Horas(row.hora);
					},
				},
				{ data: "personal" }, //2
				{ data: "codigo" }, //3
				{ data: "descripcion" }, //4
				{ data: "sucursal" }, //4.5
				{ data: "cantInicial" }, //5
				{
					//6
					data: null,
					render: function (row) {
						var html = "";

						if (row.tipo == "1") {
							html = "<span class='btn btn-primary'>Salida (por venta)</span>";
						} else if (row.tipo == "2") {
							html =
								"<span class='btn btn-success'>Entrada (por compra)</span>";
						} else if (row.tipo == "3") {
							html = "<span class='btn btn-secondary'>Devolución</span>";
						} else if (row.tipo == "4") {
							html = "<span class='btn btn-danger'>Eliminación</span>";
						} else if (row.tipo == "5") {
							html = "<span class='btn btn-warning'>Edición (ajuste)</span>";
						} else {
							html = "<span class='btn btn-info'>---</span>";
						}

						return html;
					},
				},
				//{ data: "cantidad" }, //7
				{ //7
					data: null,
					render: function (row) {
						//console.log("ROW: " + JSON.stringify(row));
						if (row.tipo == "1") {
							return '- ' + Math.abs(row.cantidad);

						} else if (row.tipo == "2") {
							return '+ ' + Math.abs(row.cantidad);

						} else if (row.tipo == "3") {
							return '+ ' + Math.abs(row.cantidad);
							
						} else if (row.tipo == "4") {
							return '- ' + Math.abs(row.cantidad);

						} else if (row.tipo == "5") {
							console.log("-----------------------------");
							console.log("Habia"+row.cantInicial);
							console.log("CantidadMov"+row.cantInicial);
							console.log("Hay"+row.stock);

							if ((row.cantInicial == Number(row.cantidad)) && (Number(row.stock) == 0)) {
								return '- ' + Math.abs(Number(row.cantidad));
							}else if ((Number(row.cantInicial) == Number(row.cantidad)) &&  (Number(row.stock) > 0)) {
								return '+ ' + Math.abs(Number(row.cantidad));
							}else if (Number(row.cantInicial) > Number(row.stock)) {
								return '- ' + Math.abs(Number(row.cantidad));
							}else if (Number(row.cantInicial) < Number(row.stock)) {
								return '+ ' + Math.abs(Number(row.cantidad));
							}else{
								return Math.abs(row.cantidad);
							}

						}else {
							return Math.abs(row.cantidad);
						}

					} 
				},
				{ data: "stock" }, //8
				/*
				{ //8
					data: null,
					render: function (row) {
						if (row.tipo == "1") {
							return parseFloat(row.cantInicial) - parseFloat(row.cantidad);
						} else if (row.tipo == "2") {
							return parseFloat(row.cantInicial) + parseFloat(row.cantidad);
						} else if (row.tipo == "3") {
							return 0;
						} else if (row.tipo == "4") {
							return 0;
						} else if (row.tipo == "5") {
							return 0;
						}else {
							return 0;
						}

					} 
				},
				*/
			],
			order: [[0, "desc"]],
			lengthMenu: [
				[10, 25, 50],//5, 
				[10, 25, 50],//5, 
			],
		});
		$.unblockUI();
	} else {
		toastr.error("No existen fechas validas", "Error");
	}
}



function formato12Horas(hora24) {
	let [horas, minutos, segundos] = hora24.split(":").map(Number);
	let periodo = horas >= 12 ? "PM" : "AM";

	horas = horas % 12 || 12;

	//return `${horas}:${String(minutos).padStart(2, '0')}:${String(segundos).padStart(2, '0')} ${periodo}`;
	return `${horas}:${String(minutos).padStart(2, '0')} ${periodo}`;
}


function formatoFecha(fecha) {
	let [soloFecha] = fecha.split(" "); // Separar fecha y hora
	let [anio, mes, dia] = soloFecha.split("-").map(Number); // Separar año, mes y día

	const meses = [
		"Enero", 
		"Febrero", 
		"Marzo", 
		"Abril", 
		"Mayo",
		"Junio", 
		"Julio", 
		"Agosto", 
		"Septiembre", 
		"Octubre", 
		"Noviembre", 
		"Diciembre"
	];

	return `${String(dia).padStart(2, '0')} de ${meses[mes - 1]} del ${anio}`;
}