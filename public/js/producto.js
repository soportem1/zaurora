var base_url = $('#base_url').val();
$(document).ready(function () {
        $.validator.addMethod("codigoexiste", function(value, element) {
        console.log(value);
        var respuestav;
        $.ajax({
            type: 'POST',
            url: base_url + 'Productos/validarcode',
            data: {
                codigo: value
            },
            async: false,
            statusCode: {
                404: function(data) {
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function() {
                    
                    toastr.error('Error!', '500');
                }
            },
            success: function(data) {
                if (data == 1) {
                    if ($('#productoid').val() > 0) {
                        respuestav = true;
                    } else {
                        respuestav = false;
                    }

                } else {
                    respuestav = true;
                }



            }
        });
        console.log(respuestav);
        return respuestav;
    });
        var form_register = $('#formproductos');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);

        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                pcodigo:{
                  codigoexiste: true,
                  required: true
                },
                productoname:{
                    required: true
                },
                txtUsuario:{
                    required: true
                },
                pstock:{
                    min: 0,
                    required: true
                },
                preciocompra:{
                    required: true
                },
                porcentaje:{
                    required: true
                },
                precioventa:{
                    required: true
                },
                preciommayoreo:{
                    required: true
                },
                cpmmayoreo:{
                    required: true
                },
                preciomayoreo:{
                    required: true
                },
                cpmayoreo:{
                    required: true
                }
                
            },
            messages: {
                pcodigo: {
                    codigoexiste: 'El codigo insertado ya se encuentra agregado'
                }
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });

        //=====================================================================================================
         $('#savepr').click(function(event) {
            var $valid = $("#formproductos").valid();
            console.log($valid);
            if($valid) {
                var datos = $('#formproductos').serialize(); 
                $.ajax({
                    type:'POST',
                    url: 'productoadd',
                    data:datos,
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){

                      var idpro = parseInt(data);
                        // Si existe el usuario, mostramos el mensaje de entrada y redirigimos
                        var DATA  = [];
                            var TABLA   = $("#tablesucursales tbody > tr");
                                TABLA.each(function(){         
                                    item = {};
                                    item ["idproducto"] = idpro;

                                    item ["idproductosucursal"] = $(this).find("input[id*='idproductosucursal']").val();
                                    item ["idsucursal"]  = $(this).find("input[id*='idsucursal_s']").val();
                                    item ["precio_venta"] = $(this).find("input[id*='precio_venta_s']").val();
                                    item ["mayoreo"] = $(this).find("input[id*='mayoreo_s']").val();
                                    //item ["cuantos"] = $(this).find("input[id*='cuantos_s']").val();
                                    //item ["cuantos"] = $(this).find("input[id*='cuantos_s']").val();
                                    item ["cuantos"]=$(this).find("input[id*='cuantos_s']").is(':checked')==false?0:1;
                                    item ["existencia"] = $(this).find("input[id*='existencia_s']").val();
                                    item ["minimo"] = $(this).find("input[id*='minimo_s']").val();
                                    DATA.push(item);
                                });
                                INFO  = new FormData();
                                aInfo   = JSON.stringify(DATA);
                                INFO.append('data', aInfo);
                                $.ajax({
                                    data: INFO,
                                    type: 'POST',
                                    url : 'productos_sucursales',
                                    processData: false, 
                                    contentType: false,
                                    async: false,
                                    statusCode:{
                                    },
                                    success: function(data){
                                    }
                                });
                        
                            toastr.success('Hecho!', 'Guardado Correctamente');
                            setTimeout(function () { window.location.href = base_url+"index.php/Productos" }, 1500);
                        //=====================
                        /*
                        	if ($('#imgpro')[0].files.length > 0) {
				            	var inputFileImage = document.getElementById('imgpro');
								var file = inputFileImage.files[0];
								var data = new FormData();
								data.append('img',file);
								data.append('carpeta',$('#carpetap').val());
								data.append('idpro',idpro);
								$.ajax({
									url:'imgpro',
									type:'POST',
									contentType:false,
									data:data,
									processData:false,
									cache:false,
									success: function(data) {
										var array = $.parseJSON(data);
							                if (array.ok=true) {
							                	$(".fileinput").fileinput("clear");
							                }else{
							                	toastr.error('Error', data.msg);
							                }
									},
									error: function(jqXHR, textStatus, errorThrown) {
							                var data = JSON.parse(jqXHR.responseText);
							                console.log(data);
							                if (data.ok=='true') {
							                	$(".fileinput").fileinput("clear");
							                }else{
							                	toastr.error('Error', data.msg);
							                }
							              
							            }
								});
				        	}
                        */    
                        //=====================
                       /* setInterval(function(){ 
                        	location.href=base_url+'Productos/';
                        }, 1500);*/
                        



                        
                    }
                });
            }
        });


    $("#exampleInputFile").fileinput({
            showCaption: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["xlsx","xls"],
            browseLabel: 'Cargar Excel',
            uploadUrl: base_url+"index.php/Productos/upload",
            maxFilePreviewSize: 5000,
            previewFileIconSettings: {
                'docx': '<i class="fa fa-file-word-o text-primary"></i>',
                'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                'xls': '<i class="fa fa-file-excel-o text-success"></i>',
                'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            }
        });
    $('.cargaarchivo').click(function() {
        $('#exampleInputFile').fileinput('upload');
    });

});
function calcular(){
	var costo = $('#preciocompra').val();
    var porcentaje = $('#porcentaje').val();
    var porcentaje2 = porcentaje/100;
    var costo2 = costo*porcentaje2;
    var cantitotal = parseFloat(costo)+parseFloat(costo2);
    $('#precioventa').val(cantitotal);
    //$('#preciommayoreo').val(cantitotal);
    //$('#cpmmayoreo').val('1');
    // $('#preciomayoreo').val(cantitotal);
    //$('#cpmayoreo').val('1');
}
function buscarproducto(){
    var search=$('#buscarpro').val();
    if (search.length>2) {
        $.ajax({
            type:'POST',
            url: base_url+'index.php/Productos/buscarpro',
            data: {
                 buscar: $('#buscarpro').val(),buscard: $('#depa').val(),buscars: $('#sucu').val()
            },
            async: false,
            statusCode:{
                404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
                500: function(){ toastr.error('Error', '500');}
            },
            success:function(data){
                $('#tbodyresultadospro2').html(data);
            }
        });
        $("#data-tables").css("display", "none");
        $("#data-tables2").css("display", "");
        $(".listado_p").css("display", "none");
    }else{
        $("#data-tables2").css("display", "none");
        $("#data-tables").css("display", "");
        $(".listado_p").css("display", "");
    }
}
function buscarproductos(){
    var searchs=$('#sucu').val();
    if (searchs.length>=1) {
        $.ajax({
            type:'POST',
            url: base_url+'index.php/Productos/buscarpro',
            data: {
                buscar: $('#buscarpro').val(),buscard: $('#depa').val(),buscars: $('#sucu').val()
            },
            async: false,
            statusCode:{
                404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
                500: function(){ toastr.error('Error', '500');}
            },
            success:function(data){
                $('#tbodyresultadospro2').html(data);
            }
        });
        $("#data-tables").css("display", "none");
        $("#data-tables2").css("display", "");
        $(".listado_p").css("display", "none");
    }else{
        $("#data-tables2").css("display", "none");
        $("#data-tables").css("display", "");
        $(".listado_p").css("display", "");
    }
}
function buscarproductod(){
    var searchs=$('#sucu').val();
    if (searchs.length>=1) {
        $.ajax({
            type:'POST',
            url: base_url+'index.php/Productos/buscarpro',
            data: {
                buscar: $('#buscarpro').val(),buscard: $('#depa').val(),buscars: $('#sucu').val()
            },
            async: false,
            statusCode:{
                404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
                500: function(){ toastr.error('Error', '500');}
            },
            success:function(data){
                $('#tbodyresultadospro2').html(data);
            }
        });
        $("#data-tables").css("display", "none");
        $("#data-tables2").css("display", "");
        $(".listado_p").css("display", "none");
    }else{
        $("#data-tables2").css("display", "none");
        $("#data-tables").css("display", "");
        $(".listado_p").css("display", "");
    }
}


function ganancigana(){
 var data = $('#tipo_ganancia option:selected').val();
 if(data==1){
    $('.efectivo_v').css('display','inline');
    $('.porcentaje_v').css('display','none');
    $('#porcentaje').val('');
 }else if(data==2){
    $('.efectivo_v').css('display','none');
    $('.porcentaje_v').css('display','inline');
    $('#gananciaefectivo').val('');
 }
} 
 function sumaventa(){
      var compra = parseFloat($('#preciocompra').val());
      var ganacia = parseFloat($('#gananciaefectivo').val()); 

      var total = ganacia + compra;
      $('#precioventa').val(total); 
}
function checksucursal() { 
        var p = $('#precioventa').val();
        var m = $('#preciomayoreo').val();
        var c = $('#cpmayoreo').val();
        if($('#test9').is(':checked'))
        {   $('.precio_venta_s').val(p);
            $('.mayoreo_s').val(m);
            $('.cuantos_s').val(c);
        }else
        {   $('.precio_venta_s').val('');
            $('.mayoreo_s').val('');
            $('.cuantos_s').val('');
        }       
}
function modal_sucursales(id){
          $('#modalSucursales').modal();
          $.ajax({
            url: 'Productos/tablasucursal/'+id,
            success: function (response) 
            {
                $('#tbodysucursal').html(response);
            },
            error: function(response)
            {
                toastr.error('Error!', 'Consulte al administrador');
            }
        });
}


