var base_url = $('#base_url').val();
$(document).ready(function(){

    $('#formsucursales').validate({
        rules: {
            nombre: "required",
            direccion: "required",
            telefono: "required",
            encargado: "required",
            
        },
        messages: {
            nombre: "Ingrese un nombre",
            direccion: "Ingrese una dirección",
            telefono: "Ingrese un telefono",
            encargado: "Ingrese un empleado",
        },
        submitHandler: function (form) {
             $.ajax({
                 type: "POST",
                 url: base_url+"Sucursales/insertar",
                 data: $(form).serialize(),
                 beforeSend: function(){
                    $(".btn_submit").attr("disabled",true);
                 },
                 success: function (result) {
                    swal("Exito!", "Se ha realizado la operación correctamente", "success");
                    //table.ajax.reload();
                    window.location.href = base_url+"Sucursales";
                    
                 }
             });
             return false; // required to block normal submit for ajax
         },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

    table=$('.table-striped').DataTable({
        responsive: true,
        "ajax": {
                "url": base_url+"Sucursales/datatable_records"
            },
            "columns": [
                {"data": "nombre"},
                {"data": "telefono"},
                {"data": "direccion"},
                {"data": "Usuario"},
                {
                    "data": null,
                    "defaultContent": "<button type='button' class='btn btn-xs waves-effect bg-teal edit'><i class='material-icons'>editar</i></button>"
                }
            ],
            //dom: 'Bfrtip',
            /*buttons: [
            {
                extend: 'print',
                exportOptions: {
                    columns: [0,1,2]
                }
            }
            ],*/
     
    });

    $('.table-striped').on('click', 'button.edit', function () {
        var tr = $(this).closest('tr');
        var data = table.row(tr).data();
        window.location.href = "Sucursales/sucursalAdd/"+data.idsucursal;
    });

    $('.table-striped').on('click', 'button.delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row(tr).data();
        delete_record(data.idsucursal);
    });

}); 


/*function delete_record(id) {
    swal({
        title: "¿Desea eliminar este elemento?",
        text: "Se eliminara del listado",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Eliminar",
        closeOnConfirm: false
    }, function () {
        $.ajax({
                 type: "POST",
                 url: "./Sucursales/deleteSucursal/"+id,
                 success: function (result) {
                    //console.log(result);
                    table.ajax.reload();
                    swal("Exito!", "Se ha eliminado correctamente", "success");
                 }
             });
    });
}*/