var base_url = $('#base_url').val();

$(document).ready(function () {
	$('#cliente').select2({
		width: 'resolve',
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un cliente',
		ajax: {
			url: 'Ventas/searchcli',
			dataType: "json",
			data: function (params) {
				var query = {
					search: params.term,
					type: 'public'
				}
				return query;
			},
			processResults: function (data) {
				var clientes = data;
				var itemscli = [];
				data.forEach(function (element) {
					//console.log(element);
					itemscli.push({
						id: element.ClientesId,
						text: element.Nom + ' - ' + element.telefonoc
					});
				});
				return {
					results: itemscli
				};
			},
		}
	});

	$('#chkFecha').change(function () {
		if (document.getElementById("chkFecha").checked) {
			f = new Date();
			if (f.getDate() < 10) {
				dia = "0" + f.getDate();
			} else {
				dia = f.getDate();
			}
			mes = Number(f.getMonth() + 1);
			if ((f.getMonth() + 1) < 10) {
				mes = "0" + mes;
			} else {
				mes = mes;
			}
			/*console.log("getDate: "+f.getDate());
			console.log("getMonth: "+f.getMonth());
			console.log("dia: "+dia);
			console.log("mes: "+mes);*/
			fecha = f.getFullYear() + "-" + mes + "-" + dia;
			$('#fechaIni').val(fecha);
			$('#fechaFin').val(fecha);
			$('#fechaIni').attr('disabled', 'disabled');
			$('#fechaFin').attr('disabled', 'disabled');
		} else {
			$('#fechaIni').val('');
			$('#fechaFin').val('');
			$('#fechaIni').removeAttr('disabled');
			$('#fechaFin').removeAttr('disabled');
		}
	});

});

function getTable() {
	fInicio = $('#fechaIni').val();
	fFinal = $('#fechaFin').val();
	cliente = $('#cliente option:selected').val();

	//console.log("Cliente: " + cliente);

	if (!cliente || cliente <= 0) {
		toastr.error('Seleccione cliente valido.', 'Error');
		return;
	}

	if (fInicio != '' && fFinal != '') {

		if (fInicio > fFinal) {
			toastr.error('No existen fechas validas', 'Error');
			return;
		}

		$(".wait").html("<img src='" + base_url + "public/img/loader.gif' width='64'/>");
		setTimeout(function () {
			$.ajax({
				type: 'POST',
				url: base_url + 'PuntosAurora/get_list',
				data: {
					id: cliente,
					fInicio: fInicio,
					fFinal: fFinal
				},
				async: false,
				beforeSend: function () {},
				success: function (data) {

					if ($.fn.DataTable.isDataTable('#data-tableM')) {
						$('#data-tableM').DataTable().destroy();
					}

					$('#data-tbody').empty();
					$('#data-tbody').append(data);

					var total = 0;
					$('#data-tbody input[id*="subtotal_"]').each(function () {
						var valor = parseFloat($(this).val().replace(',', '')) || 0;
						total += valor;	
					});
					$('#totalVenta').text('$ ' + total.toFixed(2));
					
					var puntos = 0;
					$('#data-tbody input[id*="puntos_"]').each(function () {
						var val = parseFloat($(this).val().replace(',', '')) || 0;
						puntos += val;
					});
					$('#totalPuntos').text('$ ' + puntos.toFixed(2));


				},
				complete: function () {
					//$(".wait").css("display", "none");
					$(".wait").empty();
					
					$('#data-tableM').DataTable({order: [[1, 'desc']]});

				}
			});

			$.ajax({
				type: 'POST',
				url: base_url + 'PuntosAurora/getTotalPuntos',
				data: {
					id: cliente
				},
				async: false,
				beforeSend: function () {},
				success: function (data) {
					$('#puntos').val('$ '+parseFloat(data).toFixed(2));
				}
			});

		}, 1500);
	} else {
		toastr.error('No existen fechas validas', 'Error');
	}

}


function ticket(id) {
	$("#iframeri").modal();
	$('#iframereporte').html('<iframe src="' + base_url + 'Ticket?&id=' + id + '"></iframe>');
}