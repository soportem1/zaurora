var base_url = $('#base_url').val();
var tabledata; 
var alm = $("#alm").val(); var vis=true;
var name_edit="Editar";
$(document).ready(function() {

    //$('#tipoProd').val('');
    if(alm=="1"){
        vis=false;
        name_edit="Ver";
    }

    $('#sucurs').on('change', function(){
        if( $("#sucurs").val()!="") {
            $("#tipostock").prop("disabled",false);
        }
    });

    $('#data-tabless').dataTable();
    $('#imprimiretiqueta').click(function(event) {
        var idp = $('#idproetiqueta').val();
        var nump = $('#numprint').val();
        var par = $('#pares').val();
        var tipo = $('#tipo option:selected').val();
        $('#iframeetiqueta').html('<iframe src="'+base_url+'Etiquetas?id='+idp+'&page='+nump+'&print=true&par='+par+'&tipo=' + tipo + '"></iframe>');
    });
    $('#sieliminar').click(function(event) {
        var idp = $('#hddIdpro').val();
        $.ajax({
            type: 'POST',
            url: base_url + 'Productos/deleteproductos',
            data: {
                id: idp
            },
            async: false,
            statusCode: {
                404: function(data) {
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function() {
                    toastr.error('Error', '500');
                }
            },
            success: function(data) {
                console.log(data);
                //location.reload();
                toastr.success('Hecho!', 'eliminado Correctamente');
                tabledata.destroy();
                loadtable();
                /*
                var row = document.getElementById('trpro_' + idp);
                row.parentNode.removeChild(row);
                */

            }
        });
    });
    loadtable();
    /*
    $('#pares').ckeditor(function() {

        }, { toolbar : [
            ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
            ['Undo','Redo'],
            ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
            ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
            ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
            ['Link','Unlink','Anchor', 'Image', 'Smiley'],
            ['Table','HorizontalRule','SpecialChar'],
            ['Styles','BGColor']
        ], toolbarCanCollapse:false, height: '300px', scayt_sLang: 'pt_PT', uiColor : '#EBEBEB' } );
    
    $('#pares').ckeditor(function() {
        }, { toolbar : [
            ['Paste','PasteText','-','Print', 'SpellChecker'],
            ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
            ['NumberedList','BulletedList','-'],
            
            

            ['Styles','BGColor']
        ], toolbarCanCollapse:false, height: '100px', scayt_sLang: 'pt_PT', uiColor : '#cfa424' } );
    */
    $("#exampleInputFile").fileinput({
            showCaption: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["xlsx","xls"],
            browseLabel: 'Cargar Excel',
            uploadUrl: base_url+"index.php/Productos/upload",
            maxFilePreviewSize: 5000,
            previewFileIconSettings: {
                'docx': '<i class="fa fa-file-word-o text-primary"></i>',
                'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                'xls': '<i class="fa fa-file-excel-o text-success"></i>',
                'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            }
        });
    $('.cargaarchivo').click(function() {
        $('#exampleInputFile').fileinput('upload');
    });

    $('#checkselect').on('click', function(){
        var DATA  = [];
        var TABLA   = $("#data-tables tbody > tr");
        TABLA.each(function(){    
                  if ($(this).find("input[id*='prodselect']").is(':checked')) {
                    item = {};     
                    item ["prodselect"] = $(this).find("input[id*='prodselect']").val();
                   DATA.push(item);
                  }
        });
        prodss   = JSON.stringify(DATA);
        //console.log(prodss);
        $.ajax({
            type: 'POST',
            url: base_url + 'Productos/delete_select',
            data: {
                productos:prodss,
            },
            success: function(data) {
               toastr.success('Hecho!', 'eliminado Correctamente');  
               tabledata.destroy();
               loadtable();
            },
        });
    });


  });
function productodelete(id) {
    $('#hddIdpro').val(id);
    $('#eliminacion').modal();

}

function etiquetas(id, codigo, nombre, categoria, precio) {
    $('#ecodigo').html(codigo);
    $('#eproducto').html(nombre);
    $('#ecategoria').html(categoria);
    $('#eprecio').html(precio);
    $('#idproetiqueta').val(id);

    $("#modaletiquetas").modal();
    $('#iframeetiqueta').html('<iframe src="' + base_url + 'Etiquetas?id=' + id + '&page=1"></iframe>');
}

function productosall() {
    $("#modalproductos").modal();
    $('#iframeproductos').html('<iframe src="' + base_url + 'Visorpdf?filex=Productosall&iden=id&id=0" style="height: 500px "></iframe>');
}

function modal_excel() {
    $('#importar_excel').modal();
}

function loadTableTipo(){
    tabledata.destroy();
    loadtable();
}

function loadtable() {
    var depa = $('#depa option:selected').val();
    var varrepartidor = $('#repartidor option:selected').val();
    var tipoProd = $('#tipoProd option:selected').val();
    tabledata = $('#data-tables').DataTable({
        "bProcessing": true,
        "serverSide": true,
        "search":true,
        destroy:true,
        responsive:false,
        /*search: {
            return: true
        },*/ 
        "ordering": true,
        "ajax": {
            url: base_url + "Productos/getData_productos",
            type: "post",
            //------------------------------------
            "data": {
                "departamento": depa,
                "tipoProd": tipoProd
            },
            //------------------------------------
            error: function() {
                $("#tabla_producto").css("display", "none");
            }
        },
        "columns": [
            {"data": "[]"},
            {"data": null,
                "render" : function ( url, type, full) {
                   //return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(full).html() + '">';
                    var productoId=full['productoid'];
                    var codigo="'"+full['codigo']+"'";
                    var nombre="'"+full['nombre']+"'";
                    var bttn='';
                    bttn+='<div class="btn-group mr-1 mb-1">';
                        bttn+='<button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>';
                        bttn+='<button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                            bttn+='<span class="sr-only">Toggle Dropdown</span>';
                        bttn+='</button>';
                        bttn+='<div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">';
                            bttn+='<a class="dropdown-item" href="'+base_url+'Productos/productosadd?id='+productoId+'">'+name_edit+'</a>';
                            bttn+='<a class="dropdown-item" onclick="etiquetas('+productoId+','+codigo+','+nombre+');"href="#">Etiquetas</a>';
                            bttn+='<a class="dropdown-item" onclick="productodelete('+productoId+');"href="#">Eliminar</a>';
                        bttn+='</div>';
                    bttn+='</div>   ';
                  return bttn;
                }
            }, 
            //{"data": "[]"},
           
            {"data": "codigo"}, 
            {"data": "nombre"}, 
            //{"data": "suc_1",className:'printcolor'},
            {"data": "suc_1"},
            /*{"data": "suc_1",
            render: function (row, data, index) {
                $('.printcolor').css('background-color', '#d0a41f9e');
                return index['suc_1'];
            }
            },*/
            {"data": "pv_1"},
            /*{"data": "my_1"},
            {"data": "ct_1"},*/
            {"data": "exi_1"},
            /*{"data": "min_1"},*/
            {"data": "suc_2"},
            {"data": "pv_2"},
            /*{"data": "my_2"},
            {"data": "ct_2"},*/
            {"data": "exi_2"},
            /*{"data": "min_2"},*/
            {"data": "suc_3"},
            {"data": "pv_3"},
            //{"data": "my_3"},
            //{"data": "ct_3"},
            {"data": "exi_3"},
            /*{"data": "min_3"}, */
            {"data": "depa"}, 
            {"data": "fecharegistro"},
            {"data": "productoid"},
            {"data": "tipo_prod"},
        ],
        'columnDefs': [{
                'targets': 0,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta){
                    productoId=full['productoid'];
                    return '<input type="checkbox" name="id[]" id="prodselect" value="' + productoId + '">'; 
                },
            },
            {
                'targets': [5,9,11],
                visible: vis
            },
            /*{
                'targets': 10,
                'render': function (data, type, full, meta){
                    if(alm=="1"){
                        {
                            visible: false
                        }
                    }
                }
            },
            {
                'targets': 13,
                'render': function (data, type, full, meta){
                    if(alm=="1"){
                        {
                            visible: false
                        }
                    }
                }
            }*/
            
        ],
        fixedColumns: true,
        'rowCallback': function(row, data, index){
            $(row).find('td:eq(6)').addClass('printcolor');
            $(row).find('td:eq(9)').addClass('printcolor');
            $(row).find('td:eq(12)').addClass('printcolor');
            /*if(alm==1){
                $(row).find('td:eq(7)').HiddenTables();
            }*/
        },
        'order': [[8, 'DESC']],

    }).on('draw',function(){
        //$('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
        
    });

    $(".dataTables_scrollHeadInner .filterhead").each( function ( i ) {
        var select = $('<input type="text" placeholder="Buscar" class="form-control-search"/>')
            .appendTo( $(this).empty() )
            .on( 'keyup', function () {
                tblSelected.column( i ).search(this.value).draw();
            } );
        }); 
}
