var base_url = $('#base_url').val();
var tabledata;
$(document).ready(function () {
    loadtable();
        var form_register = $('#formgasto');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);

        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                txtRazonSocial:{
                  required: true
                },
                
                
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });

        //=====================================================================================================
         $('#savegasto').click(function(event) {
            var $valid = $("#formgasto").valid();
            console.log($valid);
            if($valid) {
                $.ajax({
                    type:'POST',
                    url: 'insertarGasto',
                    data: {
                        id:$('#id').val(),
                        concepto:$('#concepto').val(),
                        monto:$('#monto').val()
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                        toastr.success('Hecho!', 'Guardado Correctamente');
                        location.href='../Gastos'; 
                        window.location.href = "../Gastos";
                    }
                });
            }
        });
  
});



function loadtable() {
    tabledata = $('#data-tables').DataTable({
        "bProcessing": true,
        "serverSide": true,
        "responsive": true,
        "ajax": {
            "url": base_url + "Gastos/getData_gastos",
            type: "post",
            error: function() {
                $("#tabla_gastos").css("display", "none");
            }
        },
        "columns": [
            {"data": "concepto"}, 
            //{"data": "monto"},

            {"data": null, type: 'formatted-num',
                "render" : function ( url, type, full) {
                    var montoT = 0;
                    montoT = new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(full.monto);
                    return montoT;
                }
            },  

            {"data": "sucursalP"},
            {"data": "fecha"},
            {"data": "Usuario"},
            //{"data": "personal"},
            {"data": null,
                "render" : function ( url, type, full) {
                    //return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(full).html() + '">';
            
                    var id=full['id'];
                    var concepto="'"+full['concepto']+"'";
                    var monto="'"+full['monto']+"'";
                    var fecha="'"+full['fecha']+"'";
                    var monto="'"+full['monto']+"'";
                    var usuario="'"+full['Usuario']+"'";
                    var sucursal="'"+full['nombre']+"'";
                    var bttn='';
                    bttn+='<div class="btn-group mr-1 mb-1">';
                        bttn+='<button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>';
                        bttn+='<button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                            bttn+='<span class="sr-only">Toggle Dropdown</span>';
                        bttn+='</button>';
                        bttn+='<div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">';
                            bttn+='<a class="dropdown-item" onclick="gastodelete('+id+');"href="#">Eliminar</a>';
                        bttn+='</div>';
                    bttn+='</div>   ';
                    return bttn;
                }
            },
        ],
        "order": [
            [3, "desc"]
        ],
        "lengthMenu": [
            [10, 25, 50], [10, 25, 50]
        ],
    });
}
function gastodelete(id){
          $.ajax({
                  type:'POST',
                  url: base_url+'Gastos/deletegasto',
                  data: {id:id},
                  async: false,
                  statusCode:{
                      404: function(data){
                          toastr.error('Error!', 'No Se encuentra el archivo');
                      },
                      500: function(){
                          toastr.error('Error', '500');
                      }
                  },
                  success:function(data){
                     tabledata.destroy();
                      toastr.success('Hecho!', 'eliminado Correctamente');
                      loadtable()
                      
                  }
              });
      }