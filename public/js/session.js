
$(document).ready(function () {
  var base_url = $('#base_url').val();
  var tabledata;

    tabledata = $('#data-tables').DataTable({
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "Reporte/getData_sessions",
            type: "post",
           
            error: function() {
                $("#tbodyresultadosess").css("display", "none");
            }
        },
        "columns": [
              {"data": "id"}, 
              {"data": "empleado"}, 
              {"data": "fecha_ini"},
              {"data": "fecha_fin"},
              {
                    "data": null,
                    "render" : function ( url, type, full) {
                        if (full['id_sucursal']==0) {
                            var msj="Matriz - Admin";
                        }else{
                            var msj= full['sucursal'];
                        }
                        return msj;
                    }
                    
                }
              //{"data": "sucursal"},             
        ],
    });

        //=====================================================================================================  
});

