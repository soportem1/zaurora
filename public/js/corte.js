var base_url = $('#base_url').val();
$(document).ready(function(){
	$('#chkFecha').change(function(){
		if(document.getElementById("chkFecha").checked){
			f = new Date();
			if(f.getDate()<10){
				dia = "0"+f.getDate();
			}else{dia = f.getDate();
			}
			mes = Number(f.getMonth()+1);
			if((f.getMonth()+1)<10){
				mes = "0"+mes;
			}else{
				mes = mes;
			}
			/*console.log("getDate: "+f.getDate());
			console.log("getMonth: "+f.getMonth());
			console.log("dia: "+dia);
			console.log("mes: "+mes);*/
			fecha = f.getFullYear()+"-"+mes+"-"+dia;
			$('#txtInicio').val(fecha);
			$('#txtFin').val(fecha);
			$('#txtInicio').attr('disabled','disabled');
			$('#txtFin').attr('disabled','disabled');	
		}
		else{
			$('#txtInicio').val('');
			$('#txtFin').val('');
			$('#txtInicio').removeAttr('disabled');
			$('#txtFin').removeAttr('disabled');
		}
	});
	$('#btnBuscar').click(function(){
		params = {};
		params.fecha1 = $('#txtInicio').val();
		params.fecha2 = $('#txtFin').val();
		params.sucursal = $('#sucursal option:selected').val();
		params.socios = $('#socios option:selected').val();
		params.cajeros = $('#cajeros option:selected').val();

		
		if(params.fecha1 != '' && params.fecha2 != ''){
			//$(".wait").html("<img src='"+base_url+"public/img/loader.gif' width='64'/>");
			$.blockUI({ 
                message: '<div class="spinner-grow" style="width: 3rem; height: 3rem;" role="status" id="cont_load">\
                    <span class="sr-only">Procesando...</span>\
                  </div><br>Espere por favor... <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>',
                css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff'
                } 
            });
			setTimeout(function () {  
				$.ajax({	
					type:'POST',
					url:base_url+'Corte_caja/corte',
					data:params,
					async:false,
					beforeSend: function() {
				       
				    },
					success:function(data){
						//console.log("data: "+data);
						var array = $.parseJSON(data);

						console.log("arraySub: "+array.dSubtotal);

						//$(".wait").css("display", "none");
						if($("#prf").val()==1 || $("#idper").val()==17){
							$('#tbCorte').html(array.tabla);
							$('#tbCorte2').html(array.tabla2);
							$('#tbCorte3').html(array.tabla10);
							$('.reportevendedores').html(array.tabla3);
							$('.reportevendedoresg').html(array.tabla4);
							$('.reportecajeros').html(array.tabla8);
							$('.reporteagresoscajero').html(array.tabla11);
							$('.reportecanceladas').html(array.tabla9);
							$('.reportesocios').html(array.tabla7);
							$('.puntosventas').html(array.tabla20);
							$('.sueldosPersonal').html(array.tabla21);

							$('#tbCorte_credito').html(array.tabla_credito);
							$('#tbCorte_credito_pagos').html(array.tabla_credito_tabla);
							$('#tbCorte_bono').html(array.tabla_bono);
						}

						$('.reportetablemenor').html(array.tabla6);
						$('.reportetablemayor').html(array.tabla5);

						if($("#prf").val()==1 || $("#idper").val()==17){
							$('#rowventas').html(array.totalventas);
							$('#totalutilidades').html(array.totalutilidad);
							$('#totalefectivo').html(array.totalefectivo);
							$('#totaltarjetas').html(array.totaltarjetas);
							//$('#totalmixto').html(array.totalmixto);
							$('#totalPuntosG').html(array.totalPuntos);
							$('#dTotal').html(''+array.dTotal);
							$('#dTotal_credito').html(''+array.total_credito);
							$('#dTotal_credito_pagos').html(''+array.total_credito_pagos);
		                    //$('#dImpuestos').html(''+array.dImpuestos);
		                    $('#totalegresos').html(''+array.totalegresos);
		                    $('#total_bonos').html(''+array.totalbonos);
		                    $('#total_descuentos').html(''+array.totaldescuentos);
												$('#total_pagoPersonal').html(''+array.totalPagoPersonal);
		                    $('#dSubtotal').html(''+array.dSubtotal);
	                	}
	                	if($("#prf").val()==1 || $("#idper").val()==17){
	                		$('#sample_2').DataTable();
		                    $('#tables_bono').DataTable();
		                    $('#sample_3').DataTable({dom: 'Blfrtip',
		                                buttons: [
		                                        //{extend: 'excel',title: 'Reporte de saldos'},
		                                        {extend: 'excel'},
		                                        {extend: 'pdf'},
		                                        {extend:'print'},
		                                        'pageLength'
		                                ],
																		lengthMenu: [[10, 25, 50,100], [10, 25, 50,100]]
																	});
		                    $('#sample_4').DataTable({dom: 'Blfrtip',
		                                buttons: [
		                                        //{extend: 'excel',title: 'Reporte de saldos'},
		                                        {extend: 'excel'},
		                                        {extend: 'pdf'},
		                                        {extend:'print'},
		                                        'pageLength'
		                    						],
																		lengthMenu: [[10, 25, 50,100], [10, 25, 50,100]]
																	});

												$('#sample_20').DataTable({dom: 'Blfrtip',
													buttons: [
														//{extend: 'excel',title: 'Reporte de saldos'},
														{extend: 'excel'},
														{extend: 'pdf'},
														{extend:'print'},
														'pageLength'
													],
													lengthMenu: [[10, 25, 50,100], [10, 25, 50,100]]
												});
		                }
		                if($("#alm").val()==1){
		                	$('.reportevendedores').html(array.tabla3);
		                    $('#sample_3').DataTable({dom: 'Blfrtip',
		                                buttons: [
		                                        //{extend: 'excel',title: 'Reporte de saldos'},
		                                        {extend: 'excel'},
		                                        {extend: 'pdf'},
		                                        {extend:'print'},
		                                        'pageLength'
		                                ],
																		lengthMenu: [[10, 25, 50,100], [10, 25, 50,100]]
																	});
		                }
	                    $('#sample_5').DataTable({dom: 'Blfrtip',
	                                buttons: [
	                                        {extend: 'excel'},
	                                        {extend: 'pdf'},
	                                        {extend:'print'},
	                                        'pageLength'
	                    						],
																	lengthMenu: [[10, 25, 50,100], [10, 25, 50,100]]
																});
	                    $('#sample_6').DataTable({dom: 'Blfrtip',
	                                buttons: [
	                                        {extend: 'excel'},
	                                        {extend: 'pdf'},
	                                        {extend:'print'},
	                                        'pageLength'
	                    						],
																	lengthMenu: [[10, 25, 50,100], [10, 25, 50,100]]
																});
	                    if($("#prf").val()==1 || $("#idper").val()==17){
		                    $('#sample_7').DataTable({dom: 'Blfrtip',
		                                buttons: [
		                                        //{extend: 'excel',title: 'Reporte de saldos'},
		                                        {extend: 'excel'},
		                                        {extend: 'pdf'},
		                                        {extend:'print'},
		                                        'pageLength'
		                    						],
																		lengthMenu: [[10, 25, 50,100], [10, 25, 50,100]]
												});
		                    $('#sample_8').DataTable({dom: 'Blfrtip',
		                                buttons: [
		                                        //{extend: 'excel',title: 'Reporte de saldos'},
		                                        {extend: 'excel'},
		                                        {extend: 'pdf'},
		                                        {extend:'print'},
		                                        'pageLength'
		                    						],
																		lengthMenu: [[10, 25, 50,100], [10, 25, 50,100]]});
		                    $('#sample_9').DataTable({dom: 'Blfrtip',
		                                buttons: [
		                                        //{extend: 'excel',title: 'Reporte de saldos'},
		                                        {extend: 'excel'},
		                                        {extend: 'pdf'},
		                                        {extend:'print'},
		                                        'pageLength'
		                    						],
																		lengthMenu: [[10, 25, 50,100], [10, 25, 50,100]]
																	});
		                    $('#sample_10').DataTable({dom: 'Blfrtip',
		                                buttons: [
		                                        //{extend: 'excel',title: 'Reporte de saldos'},
		                                        {extend: 'excel'},
		                                        {extend: 'pdf'},
		                                        {extend:'print'},
		                                        'pageLength'
		                    						],
																		lengthMenu: [[10, 25, 50,100], [10, 25, 50,100]]
																	});
		                    $('#sample_11').DataTable({dom: 'Blfrtip',
		                                buttons: [
		                                        {extend: 'excel'},
		                                        {extend: 'pdf'},
		                                        {extend:'print'},
		                                        'pageLength'
		                    						],
																		lengthMenu: [[10, 25, 50,100], [10, 25, 50,100]]
																	});
		                }
		                $.unblockUI();
					},
					complete: function(){
					    //$(".wait").css("display", "none");
					}
				})
			}, 1500); 
		}
		else{
			toastr.error('No existen fechas validas', 'Error');
			
		}
	});
});

function ticket(id) {
	//console.log("XD");
	//console.log('<iframe src="' + base_url + 'Ticket?&id=' + id + '"></iframe>');
	$("#iframeri").modal();
	$('#iframereporte').html('<iframe src="' + base_url + 'Ticket?&id=' + id + '"></iframe>');
}