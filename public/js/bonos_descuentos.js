var base_url = $('#base_url').val();
$(document).ready(function(){
	$('#empleado').select2({
		width: 'resolve',
		placeholder: 'Seleccione un empleado',
		allowClear: true,
		sorter: data => data.sort((a, b) => a.text.localeCompare(b.text)),
		ajax: {
            url: base_url+'Pagos/searchPersonal',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.personalId,
                        text: element.nombre + ' ' + element.apellidos,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
	}).on('select2:select', function (e) {
		var data = e.params.data;
		getDetallesBonosDescs();
    });

    $("#bono").on("click",function(){
    	if($("#bono").is(":checked")==true){
    		$(".bonos_").show("slow");
    		$(".descuentos_").hide("slow");
    	}
    });
    $("#descuento").on("click",function(){
    	if($("#descuento").is(":checked")==true){
    		$(".bonos_").hide("slow");
    		$(".descuentos_").show("slow");
    	}
    });

    if($("#bono").is(":checked")==true){
		$(".bonos_").show("slow");
		$(".descuentos_").hide("slow");
	}

    $("#save").on("click",function(){
    	guardar();
    });
});

function guardar(){
	var form_register = $('#formbonos');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            id_empleado:{
              required: true
            },
            fecha:{
              required: true
            },
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);
        },
        highlight: function (element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });

    var valid = $("#formbonos").valid();
    if(valid) {
        $.ajax({
            type:'POST',
            url: base_url+'Pagos/insertarBonoDesc',
            data: form_register.serialize(),
            async: false,
            beforeSend: function(){
                $("#save").attr("disabled",true);
            },
            success:function(data){
                toastr.success('Hecho!', 'Guardado Correctamente');
                setTimeout(function(){ 
	                location.reload(); 
	            }, 1500); 
            }
        });
    }
    
}

function getDetallesBonosDescs(tipo){
	$("#cont_lista").show("slow");
	$.ajax({
        type:'POST',
        url: base_url+'Pagos/detallesBonos',
        data: {
            id_emp: $('#empleado option:selected').val()
        },
        async: false,
        success:function(data){
        	$("#cont_tabla_bonos").html(data);
        	$("#tabla_det_bds").dataTable({
        		dom: 'Blfrtip',
        		buttons: [
                    {extend: 'excel'},
                    {extend: 'pdf'},
                    {extend:'print'},
            	]
        	});
        }
    });
}