var base_url = $('#base_url').val();
var d_vacas=0; var salario=0;
$(document).ready(function(){
	$('#empleado').select2({
		width: 'resolve',
		placeholder: 'Seleccione un empleado',
		allowClear: true,
		sorter: data => data.sort((a, b) => a.text.localeCompare(b.text)),
		ajax: {
            url: base_url+'Pagos/searchPersonal',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.personalId,
                        text: element.nombre + ' ' + element.apellidos,
                        d_vacas: element.dias_vaca,
                        salario: element.salario_dia
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
	}).on('select2:select', function (e) {
		var data = e.params.data;
        d_vacas = data.d_vacas;
        salario = data.salario;
        //console.log("d_vacas desde empleado: "+d_vacas);
        validaCantDias();
        validaParametros();
        verificarLista(data.id);
        if($("#tipo option:selected").val()==3 || $("#tipo option:selected").val()==4 || $("#tipo option:selected").val()==5){
            verficarConfig(salario); 
            $("#salario_dia").val(salario);
        }
    });
    $("#tipo").on("change",function(){
        validaCantDias();
        if($("#tipo option:selected").val()==3 || $("#tipo option:selected").val()==4 || $("#tipo option:selected").val()==5){
            $("#cont_info_salario").show("slow");
            verficarConfig(salario); 
        }else{
            $("#cont_info_salario").hide("slow");
        }
    });

    $('#inicio').change(function(){
        validaParametros();
        if($("#tipo option:selected").val()==3 || $("#tipo option:selected").val()==4 || $("#tipo option:selected").val()==5){
            verficarConfig(salario); 
        }
    });
    $('#fin').change(function(){
        validaParametros();
        if($("#tipo option:selected").val()==3 || $("#tipo option:selected").val()==4 || $("#tipo option:selected").val()==5){
            verficarConfig(salario); 
        }
    });


    $("#save").on("click",function(){
    	guardar();
    });
});

function verificarLista(id){
    $("#cont_lista").show("slow");
    tabledata = $('#tabla_vacaciones').DataTable({
        destroy:true,
        "ajax": {
            url: base_url+"Vacaciones/getData_vacaciones",
            type: "post",
            data: { id_emp:$("#empleado option:selected").val() }
        },
        "columns": [
            {"data": "empleado"}, 
            {"data": null,
                "render" : function ( data, type, row, meta) {
                    var html='';
                    if(row.tipo=="1"){
                        html="Día Libre";
                    }
                    else if(row.tipo=="2"){
                        html="Vacaciones";
                    }else if(row.tipo=="3"){
                        html="Día enfermo";
                    }else if(row.tipo=="4"){
                        html="Día descuento";
                    }else if(row.tipo=="5"){
                        html="Día suspendido";
                    }
                    return html;
                }
            },
            {"data": "inicio"},
            {"data": "fin"},
            {"data": "observaciones"},
            {"data": "reg"},         
        ],
    });
}

function validaParametros(){
    if($("#tipo option:selected").val()=="2"){ //vacaciones
        if($("#inicio").val()!="" && $("#fin").val()!=""){
            if(verificaFechas()<=d_vacas){
                $("#save").attr("disabled",false);
            }else{
                $("#save").attr("disabled",true);
                toastr.error('Álerta!', 'El empleado no cuenta con la cantidad de días de vacaciones seleccionados');
                $("#fin").val("");
            }
        }
    }else if($("#tipo option:selected").val()==3 || $("#tipo option:selected").val()==4 || $("#tipo option:selected").val()==5){
        if($("#inicio").val()!="" && $("#fin").val()!=""){
            $("#dias_tot").val(verificaFechas());
        }
    }
}

function validaCantDias(){
    //console.log("d_vacas desde validacant: "+d_vacas);
    if($("#tipo").val()=="2" && d_vacas>0){
        $("#inicio").attr("readonly",false);
        $("#fin").attr("readonly",false);
    }else if($("#tipo").val()=="2" && d_vacas==0){
        $("#inicio").attr("readonly",true);
        $("#fin").attr("readonly",true);
    }
    if($("#tipo").val()!="2"){
        $("#inicio").attr("readonly",false);
        $("#fin").attr("readonly",false);
    }
}

function verificaFechas(){
    dif_day = 0;
    var ini = moment($("#inicio").val());
    var fin = moment($("#fin").val());
    dif_day = fin.diff(ini, 'days');
    return dif_day+1;
}

function verficarConfig(salario){
    //console.log("salario: "+salario);
    $("#salario_dia").val(salario);
    $.ajax({
        type:'POST',
        url: base_url+'Vacaciones/getConfigDescs',
        async: false,
        success:function(data){
            var array = $.parseJSON(data);
            //console.log("% desc por dia enfermo: "+array.dia_enfermo);
            if($("#tipo option:selected").val()==3){ //dia enfermo
                tot = salario * Number($("#dias_tot").val());
                tot_dia = tot * Number(array.dia_enfermo);
                tot_dia = tot_dia / 100;
                $("#tot_desc").val(parseFloat(tot_dia).toFixed(2));
            }else if($("#tipo option:selected").val()==4){ //dia descuento
                tot = salario * Number($("#dias_tot").val());
                tot_dia = tot * Number(array.dia_desc);
                tot_dia = tot_dia / 100;
                $("#tot_desc").val(parseFloat(tot_dia).toFixed(2));
            }else if($("#tipo option:selected").val()==5){ //dia suspendido
                tot = salario * Number($("#dias_tot").val());
                tot_dia = tot * Number(array.dia_suspende);
                tot_dia = tot_dia / 100;
                $("#tot_desc").val(parseFloat(tot_dia).toFixed(2));
            }
        }
    });
}

function guardar(){
	var form_register = $('#form_vacaciones');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            id_empleado:{
              required: true
            },
            tipo:{
              required: true
            },
            inicio:{
              required: true
            },
            fin:{
              required: true
            },
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);
        },
        highlight: function (element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });

    var valid = $("#form_vacaciones").valid();
    if(valid) {
        $.ajax({
            type:'POST',
            url: base_url+'Vacaciones/insertarVacaciones',
            data: form_register.serialize()+"&observaciones="+$("#observaciones").val(),
            async: false,
            beforeSend: function(){
                $("#save").attr("disabled",true);
            },
            success:function(data){
                toastr.success('Hecho!', 'Guardado Correctamente');
                setTimeout(function(){ 
	                //location.reload(); 
	            }, 1500); 
                $("#form_vacaciones").trigger("reset");
                verificarLista($("#empleado option:selected").val());
                $("#empleado").val("").change();
                
                $("#save").attr("disabled",false);
            }
        });
    }
    
}
