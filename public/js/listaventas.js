var base_url = $('#base_url').val();

var IDG;
var id_pago=0;
var id_venta=0;
$(document).ready(function () {
	table();

	$('[data-toggle="tooltip"]').tooltip();
	$('#sicancelar').click(function () {
		params = {};
		params.id = $('#hddIdVenta').val();
		$.ajax({
			type: 'POST',
			url: base_url + 'ListaVentas/cancelarventa',
			data: params,
			async: false,
			beforeSend: function () {
				$("#sicancelar").prop('disabled', true);
			},
			success: function (data) {
				console.log("DT:"+data);
				if(data == "ERROR"){
					toastr.error('Imposible cancelar, no se permiten cancelaciones en ventas pagadas con puntos.', 'ERROR!');
					$("#sicancelar").prop('disabled', false);
					return;
				}

				toastr.success('Cancelado Correctamente', 'Hecho!');
				//$('#trven_'+params.id).remove();
				location.reload();
			}
		});
	});
	$('#confCancel').click(function () {
		location.reload();
	});

});

function ticket(id) {
	$("#iframeri").modal();
	$('#iframereporte').html('<iframe src="'+ base_url +'Ticket?&id=' + id + '"></iframe>');
}

function recarga() {
	//location.reload();
}

function cancelar(ID, CantidadTicket) {
	$('#cancelar').modal();
	$("#hddIdVenta").val(ID);
	$("#NoTicket").html(ID);
	IDG = ID;
	$("#hddIdVenta2").val(ID);
	$("#NoTicket2").html(ID);
	$.ajax({
		/*beforeSend: function() {
		  $("#sicancelar").prop('disabled',true); 
		},*/
		type: 'json',
		url: base_url + 'ListaVentas/consultaDetalleVenta/' + ID,
		async: false,
		statusCode: {
			404: function (data) {
				toastr.error('Error!', 'No Se encuentra el archivo');
			},
			500: function () {
				toastr.error('Error', '500');
			}
		},
		success: function (data) {
			$('#detVen').html(data);
		}
	});

	//$("#CantidadTicket").html(CantidadTicket);  
}

function cancelaParcial(id_det_ven) {
	$.ajax({
		type: 'POST',
		url: base_url + 'ListaVentas/cancelaParcial/' + id_det_ven + "/" + $('#hddIdVenta').val(),
		async: false,
		statusCode: {
			404: function (data) {
				toastr.error('Error!', 'No Se encuentra el archivo');
			},
			500: function () {
				toastr.error('Error', '500');
			}
		},
		success: function (data) {
			console.log("D: "+data);
			if(data == "ERROR"){
				toastr.error('Imposible cancelar, no se permiten cancelaciones en ventas pagadas con puntos.', 'ERROR!');
				return;
			}

			toastr.success('Cancelado Correctamente', 'Hecho!');
			$.ajax({
				type: 'json',
				url: base_url + 'ListaVentas/consultaDetalleVenta/' + IDG,
				async: false,
				statusCode: {
					404: function (data) {
						toastr.error('Error!', 'No Se encuentra el archivo');
					},
					500: function () {
						toastr.error('Error', '500');
					}
				},
				success: function (data) {
					$('#detVen').html(data);
				}
			});
			//location.reload(); // falta poner estatus de cancelado
		}
	});
	//$("#CantidadTicket").html(CantidadTicket);  
}


function buscarventa() {
	var search = $('#buscarvent').val();
	if (search.length > 2) {
		$.ajax({
			type: 'POST',
			url: base_url + 'ListaVentas/buscarvent',
			data: {
				buscar: $('#buscarvent').val()
			},
			async: false,
			statusCode: {
				404: function (data) {
					toastr.error('Error!', 'No Se encuentra el archivo');
				},
				500: function () {
					toastr.error('Error', '500');
				}
			},
			success: function (data) {
				$('#tbodyresultadosvent2').html(data);
			}
		});
		$("#data-tables").css("display", "none");
		$("#data-tables2").css("display", "");
	} else {
		$("#data-tables2").css("display", "none");
		$("#data-tables").css("display", "");
	}
}

function facturar(idv) {
	window.open(base_url + 'Ventas/facturar/' + idv, '_blank');
}

function loadTableTipo(){
    tabla.destroy();
    table();
}

function table() {
	tabla = $("#data-tables").DataTable({
		"bProcessing": true,
		"serverSide": true,
		"searching": true,
		responsive: !0,
		"ajax": {
			"url": base_url + "ListaVentas/get_table",
			type: "post",
			"data": {metodo:$('#ventas_credito').val()},
			error: function () {
				$("#data-tables").css("display", "none");
			}
		},
		"columns": [{
				"data": "id_venta"
			},
			{
				"data": "reg"
			},
			{
				"data": "vendedor"
			},
			{
				"data": "sucursal"
			},
			{"data": "monto_total",
                render:function(data,type,row){
                    var html='';
                    html=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(row.monto_total);
                    return html;
                }
            },
			{
				"data": "cliente"
			},
			{
				"data": null,
				"render": function (row) {
					//console.log("ROW: "+row.cancelado);
					let html = "";
					if (row.cancelado == 0) {
						html = '<span class="badge badge-success">Activa</span>';
					}

					if (row.cancelado == 1) {
						html = '<span class="badge badge-danger">Cancelado</span>';
					}

					if (row.cancelado == 2) {
						html = '<span class="badge badge-danger">Cancelado Parcial</span>';
					}
					return html;
				}
			},
			{
				"data": null,
				"render": function (row) {
					let html = "";
					if (row.cancelado == 0) {
						//html = '<button class="btn btn-raised gradient-blackberry white sidebar-shadow" onclick="facturar(' + row.id_venta + ')" title="Facturar Venta" data-toggle="tooltip" data-placement="top"> Facturar</button>';
					}
					return html;
				}
			},
			{
				"data": null,
				"render": function (row) {
					let disabled = '';
					if (row.cancelado == 1) {
						disabled = "disabled";
					}
					var btn_credito='';
                    if(row.metodo==6){
                    	btn_credito='<button class="btn btn-raised gradient-flickr white sidebar-shadow" onclick="pago_credito(' + row.id_venta + ')" title="Pagos" data-toggle="tooltip" data-placement="top">\
		                    <i class="fa fa-usd"></i>\
		                </button> ';
                    }
					let html = btn_credito+'<button class="btn btn-raised gradient-blackberry white sidebar-shadow" onclick="ticket(' + row.id_venta + ')" title="Ticket" data-toggle="tooltip" data-placement="top">\
                    <i class="fa fa-book"></i>\
                  </button>\
									\
                  <button class="btn btn-raised gradient-flickr white sidebar-shadow" onclick="cancelar(' + row.id_venta + ',' + row.monto_total + ')" title="Cancelar" data-toggle="tooltip" data-placement="top" ' + disabled + '>\
                    <i class="fa fa-times"></i>\
                  </button>';
					return html;
				}
			},
		],
		"order": [
			[0, "desc"]
		],
		"lengthMenu": [
			[10, 25, 50, 100],
			[10, 25, 50, 100]
		],
		// Cambiamos lo principal a Español
		"language": {
			"lengthMenu": "Mostrar _MENU_ entradas",
			"zeroRecords": "Lo sentimos - No se han encontrado elementos",
			"sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
			"info": "Mostrando página _PAGE_ de _PAGES_",
			"infoEmpty": "No hay registros disponibles",
			"infoFiltered": "(Filtrado de _MAX_ registros totales)",
			"search": "Buscar : _INPUT_",
			"paginate": {
				"previous": "Página previa",
				"next": "Siguiente página"
			}
		}

	});
}


function pago_credito(id) {
	$('#pagocredito').modal();
	$('.txt_credito').html('');
	$.ajax({
        type:'POST',
        url: base_url+"ListaVentas/detalles_credito",
        data: {
          id:id
        },
        success: function (data){
          $('.txt_credito').html(data);
        }
    });
	tabla_credito(id);
}

function agregar_pago_credito(id){
    var mpago=$('#mpago option:selected').val();
    var referencia_p=$('#referencia_p').val();
    var fecha_p=$('#fecha_p').val();
    var monto_p=$('#monto_p').val();

    if(mpago==0){
    	toastr.error('Error!', 'Falta seleccionar un método de pago');
    }else{
        if(referencia_p==''){
            toastr.error('Error!', 'Falta agregar una referencia');
        }else{
            if(fecha_p==''){
                toastr.error('Error!', 'Falta agregar una fecha');
            }else{
                if(monto_p==''){
	                toastr.error('Error!', 'Falta agregar monto');
	            }else{
	                $.ajax({
		                  type:'POST',
		                  url: base_url+'ListaVentas/add_pagos_credito',
		                  data: {id_venta:id,
							metodo:mpago,
							referencia:referencia_p,
							fecha:fecha_p,
							monto:monto_p
						  },
		                  async: false,
		                  statusCode:{
		                      404: function(data){
		                          toastr.error('Error!', 'No Se encuentra el archivo');
		                      },
		                      500: function(){
		                          toastr.error('Error', '500');
		                      }
		                  },
		                  success:function(data){
		                  	if(data==1){
		                  		toastr.error('Error', 'El monto es mayor a la cantidad restante');
                            }else{
		                        tabla_credito(id);
		                  	}
		                  }
		            });
	            }
            }
        } 
    }
	//
}

function tabla_credito(id) {
	$('.txt_credito_tabla').html('');
	id_venta=id;
	$.ajax({
        type:'POST',
        url: base_url+"ListaVentas/detalles_tabla_credito",
        data: {
          id:id
        },
        success: function (data){
          $('.txt_credito_tabla').html(data);
          $.ajax({
		        type:'POST',
		        url: base_url+"ListaVentas/detalles_credito",
		        data: {
		          id:id
		        },
		        success: function (data){
		          $('.txt_credito').html(data);
		        }
		   });
        }
    });
	
}

function eliminar_pago(id){
	$('#modaldeletepago').modal();
    id_pago=id;
}

function delete_pago(){
	$.ajax({
	    type:'POST',
	    url: base_url+'ListaVentas/delete_pago',
	    data: {
	        id: id_pago
	    },
	    async: false,
	    statusCode:{
	        404: function(data){
	            toastr.error('Error!', 'No Se encuentra el archivo');
	        },
	        500: function(){
	            toastr.error('Error', '500');
	        }
	    },
	    success:function(data){
	        toastr.success('Hecho!', 'Eliminado Correctamente');
	        $('.row_p'+id_pago).remove();
	        pago_credito(id_venta);
	    }
	});
}