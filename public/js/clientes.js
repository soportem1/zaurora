var base_url = $("#base_url").val();
$(document).ready(function () {
	var form_register = $("#formclientes");
	var error_register = $(".alert-danger", form_register);
	var success_register = $(".alert-success", form_register);

	var $validator1 = form_register.validate({
		errorElement: "div", //default input error message container
		errorClass: "vd_red", // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules: {
			nombrecli: {
				required: true,
			},
			telefonocli: {
				required: true,
				digits: true,
				minlength: 10,
				maxlength: 12,
			},
			porcentaje: {
				required: true,
				number: true,
				min: 0,
				max: 100,
			},
		},

		errorPlacement: function (error, element) {
			if (
				element.parent().hasClass("vd_checkbox") ||
				element.parent().hasClass("vd_radio")
			) {
				element.parent().append(error);
			} else if (element.parent().hasClass("vd_input-wrapper")) {
				error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		},

		invalidHandler: function (event, validator) {
			//display error alert on form submit
			success_register.fadeOut(500);
			error_register.fadeIn(500);
			scrollTo(form_register, -100);
		},

		highlight: function (element) {
			// hightlight error inputs

			$(element).addClass("vd_bd-red");
			$(element)
				.siblings(".help-inline")
				.removeClass("help-inline fa fa-check vd_green mgl-10");
		},

		unhighlight: function (element) {
			// revert the change dony by hightlight
			$(element).closest(".control-group").removeClass("error"); // set error class to the control group
		},

		success: function (label, element) {
			label
				.addClass("valid")
				.addClass("help-inline fa fa-check vd_green mgl-10") // mark the current input as valid and display OK icon
				.closest(".control-group")
				.removeClass("error")
				.addClass("success"); // set success class to the control group
			$(element).removeClass("vd_bd-red");
		},
	});

	//=====================================================================================================
	$("#savecl").click(function (event) {
		$("#savecl").addClass("disabled");
		var $valid = $("#formclientes").valid();
		console.log($valid);

		$.ajax({
			type: "POST",
			url: base_url + "Clientes/check_phone",
			data: {
				phone: $("#telefonocli").val(),
				id: $("#clienteid").val(),
			},
			success: function (data) {
				//console.log(data);
				if (data == false) {
					//console.log("NO Existe");

					if ($valid) {
						$.ajax({
							type: "POST",
							url: "clienteadd",
							data: {
								id: $("#clienteid").val(),
								nom: $("#nombrecli").val(),
								correo: $("#correocli").val(),
								calle: $("#callecli").val(),
								nint: $("#nexteriorcli").val(),
								next: $("#ninteriorcli").val(),
								col: $("#coloniacli").val(),
								loc: $("#localidadcli").val(),
								muni: $("#municipiocli").val(),
								cp: $("#codigopcli").val(),
								esta: $("#estadocli").val(),
								pais: $("#paiscli").val(),
								contac: $("#contactocli").val(),
								correoc: $("#correoccli").val(),
								tel: $("#telefonocli").val(),
								ext: $("#extenciocli").val(),
								nexte: $("#nextelcli").val(),
								des: $("#descripcioncli").val(),
								rfcdf: $("#rfcdf").val(),
								razon_social: $("#razon_social").val(),
								direccion_fiscal: $("#direccion_fiscal").val(),
								cp_fiscal: $("#cp_fiscal").val(),
								RegimenFiscalReceptor: $(
									"#RegimenFiscalReceptor option:selected"
								).val(),
								porcentaje: $("#porcentaje").val(),
							},
							async: false,
							statusCode: {
								404: function (data) {
									toastr.error("Error!", "No Se encuentra el archivo");
								},
								500: function () {
									toastr.error("Error", "500");
								},
							},
							success: function (data) {
								toastr.success("Hecho!", "Guardado Correctamente");
								setInterval(function () {
									window.location.href = base_url + "Clientes";
								}, 3000);
							},
						});
					}
				} else {
					//console.log("Existe");
					toastr.warning("Error!", "Intente nuevamente con otro teléfono");
					$("#savecl").removeClass("disabled");
				}
			},
		});
	});
});
function buscarcliente() {
	var search = $("#buscarcli").val();
	if (search.length > 2) {
		$.ajax({
			type: "POST",
			url: base_url + "Clientes/buscarcli",
			data: {
				buscar: $("#buscarcli").val(),
			},
			async: false,
			statusCode: {
				404: function (data) {
					toastr.error("Error!", "No Se encuentra el archivo");
				},
				500: function () {
					toastr.error("Error", "500");
				},
			},
			success: function (data) {
				$("#tbodyresultadoscli2").html(data);
			},
		});
		$("#data-tables").css("display", "none");
		$("#data-tables2").css("display", "");
	} else {
		$("#data-tables2").css("display", "none");
		$("#data-tables").css("display", "");
	}
}
