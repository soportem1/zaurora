var base_url = $('#base_url').val();
var table;
$(document).ready(function() {	
	table = $('#tabla_facturacion').DataTable();
	loadtable();

    $('#idcliente').select2({
        width: '90%',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un cliente',
        ajax: {
            url: base_url+'Facturaslis/searchcli',
            dataType: "json",
            data: function (params) {
            var query = {
                search: params.term,
                type: 'public'
            }
            return query;
        },

        processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                itemscli.push({
                    id: element.ClientesId,
                    text: element.Nom
                });
            });
            return {
                results: itemscli
            };          
        },  
      }
    }).on('select2:select', function (e) {
        var data = e.params.data;
    });
    $("#idcliente").on("change",function(){
        var id_cliente = $("#idcliente option:selected").val();
        if(id_cliente!=undefined){
            loadtable();
        }
    });
    $("#fecha_fact").on("change",function(){
        loadtable();
    });
    
});
function loadtable(){
    var id_cliente = $("#idcliente option:selected").val();
    if(id_cliente==undefined){
        id_cliente="";
    }
    //console.log("id_cliente: "+id_cliente);
	table = $('#tabla_facturacion').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        destroy:true,
        "ajax": {
            "url": base_url+"Facturaslis/getlistfacturas",
            type: "post",
            "data": {
                'idcliente': id_cliente,
                'fecha_fact':$("#fecha_fact").val()
            },
        },
        "columns": [
            {"data": "FacturasId",
            	render:function(data,type,row){
            		var html='';
            		if(row.Estado==1){
                        html='<input type="checkbox" class="filled-in facturas_checked" id="factura_'+row.FacturasId+'" value="'+row.FacturasId+'">\
				              <label for="factura_'+row.FacturasId+'"></label>';
                    }else{
                    	html='<input type="checkbox" class="filled-in facturas_checked" id="factura_'+row.FacturasId+'" value="'+row.FacturasId+'" disabled>\
				              <label for="factura_'+row.FacturasId+'"></label>';
                    }
                    return html;
            	}
        	},
            {"data": "Folio"},
            {"data": "Nombre"},
            {"data": "Rfc",},
            {"data": "total"},
            {"data": "Estado",
                render:function(data,type,row){
                    var html='';
                    if(row.Estado==0){
                        html='<span class="badge badge-danger mb-1 mr-2">Cancelada</span>';
                    }else if(row.Estado==1 && row.sellosat!=""){
                        html='<span class="badge badge-success mb-1 mr-2">Facturado</span>';
                    }else if(row.Estado==1 && row.sellosat==""){
                        html='<span class="badge badge-info mb-1 mr-2">Sin Facturar</span>';
                    }else if(row.Estado==2){
                        html='<span class="badge badge-info mb-1 mr-2">Sin Facturar</span>';
                    }else{
                        html='';
                    }
                    return html;
                }
            },
            {"data": "fechatimbre"},
            {"data": "Usuario"},
            {"data": null,
                 render:function(data,type,row){
                    //$('.tooltipped').tooltip();
                    var html='<div class="btn-group mr-1 mb-1">\
                        <button type="button" class="btn btn-raised gradient-back-to-earth white"><i class="fa fa-cog"></i></button>\
                          <button type="button" class="btn btn-raised gradient-back-to-earth dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                              <span class="sr-only ">Toggle Dropdown</span>\
                          </button>\
                          <div class="dropdown-menu  " x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">';
                    if(row.Estado==0){ // cancelado
                        html+='<a \
		                        class="dropdown-item" \
		                        href="'+base_url+'Facturaslis/generarfacturas/'+row.FacturasId+'/1" \
		                        target="_blank"\
		                        data-position="top" data-delay="50" data-tooltip="Factura"\
		                      >Factura\
                      		</a> ';
		                html+='<a\
			                        class="dropdown-item" \
			                        href="'+base_url+row.rutaXml+'" \
			                        target="_blank"\
			                        data-position="top" data-delay="50" data-tooltip="XML"\
			                      download >XML\
                      				</a> ';
                        html+='<a\
                                    class="dropdown-item" \
                                    href="'+base_url+'sat/facturas/'+row.rutaAcuseCancelacion+'" \
                                    target="_blank"\
                                    data-position="top" data-delay="50" data-tooltip="Acuse de cancelación"\
                                  download >Acuse de Cancelación\
                                    </a> ';
                    }else if(row.Estado==1 && row.sellosat!=""){
                        html+='<a \
		                        class="dropdown-item" \
		                        href="'+base_url+'Facturaslis/generarfacturas/'+row.FacturasId+'/1" \
		                        target="_blank"\
		                        data-tooltip="Factura">Factura\
                      		</a> ';
                        
		                html+='<a\
			                      class="dropdown-item" \
			                        href="'+base_url+row.rutaXml+'" \
			                        target="_blank"\
			                        data-tooltip="XML"\
			                      download>XML\
                      			</a> ';
                        html+='<a \
                                class="dropdown-item modal_envio_'+row.FacturasId+'" \
                                data-urlxml="'+base_url+row.rutaXml+'" \
                                data-correo="'+row.Correo+'" \
                                onclick="modal_envio('+row.FacturasId+')"\
                                data-tooltip="Envio factura">Envio Email\
                            </a> ';
                        html+='<a\
                                    class="dropdown-item" \
                                    onclick="complementop('+row.FacturasId+')"\
                                     data-tooltip="Complemento de pago"\
                                  download >Complemento de Pago\
                                    </a> ';
                    }else if(row.Estado==2 || row.Estado==1 && row.sellosat==""){
                        html+='<button type="button"\
                          class="dropdown-item" \
                          onclick="retimbrar('+row.FacturasId+','+row.id_sucursal+')"\
                          data-position="top" data-delay="50" data-tooltip="Retimbrar" id="refacturar">\
                          <span class="fa-stack">Refacturar\
                          </span>\
                        </button> ';
                    }else{
                    	html+='';
                    }
                    html+='</div></div>';
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]]
        
    });
}

function retimbrar(idfactura,sucur){
    
    url="Ventas/retimbrar";
    

    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de generar el retimbrado?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+url,
                    data: {
                        factura:idfactura
                    },
                    success:function(response){  
                        console.log(response);
                        var array = $.parseJSON(response);
                        if (array.resultado=='error') {
                            swal({    title: "Error "+array.CodigoRespuesta+"!",
                                          text: array.MensajeError,
                                          type: "warning",
                                          showCancelButton: false
                            });
                        }else{
                            swal("Éxito!", "Se ha creado la factura", "success");
                            loadtable();
                        }

                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
/*function cancelarfacturas(){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de realizar la Cancelación de una o mas facturas?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
            	//===================================================
            	var facturaslis = $("#tabla_facturacion tbody > tr");
            	var DATAa  = [];
            	var num_facturas_selected=0;
		        facturaslis.each(function(){  
		        	if ($(this).find("input[class*='facturas_checked']").is(':checked')) {
		        		num_facturas_selected++;
		        		item = {};                    
		            	item ["FacturasIds"]  = $(this).find("input[class*='facturas_checked']").val();
		            	DATAa.push(item);
		        	}       
		            
		        });
		        INFOa  = new FormData();
		        aInfoa   = JSON.stringify(DATAa);
		        //console.log(aInfoa);
		        if (num_facturas_selected==1) {
                    $.ajax({
                        type:'POST',
                        url: base_url+"Ventas/cancelarCfdi",
                        data: {
                            facturas:aInfoa
                        },
                        success:function(response){  
                            console.log(response);
                            var array = $.parseJSON(response);
                            if (array.resultado=='error') {
                                swal({    title: "Error "+array.CodigoRespuesta+"!",
                                              text: array.MensajeError,
                                              type: "warning",
                                              showCancelButton: false
                                 });
                            }else{
                                swal("Éxito!", "Se ha Cancelado la factura", "success");
                                loadtable();
                            }

                        }
                    });
		        }else{
		        	alertfunction('Atención!','Seleccione solo una factura');
		        }
                
                
                //================================================
            },
            cancelar: function () 
            {
                
            }
        }
    });
}*/

function cancelarfacturas(){
    var html='<div class="row col-md-12">\
                <div class="col-md-12">\
                    ¿Está seguro de realizar la Cancelacion de la factura?<br>Se necesita permisos de administrador\
                </div>\
                <div class="col-md-12">\
                    <form method="post" action="#" autocomplete="off">\
                    <input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>\
                    </form>\
                </div>\
              </div>\
              <div class="row col-md-12">\
                <div class="col-md-6">\
                    <label>Motivo Cancelación</label>\
                    <select id="motivocancelacion" class="form-control" onchange="mocance()">\
                        <option value="00" disabled selected> Seleccione una opción</option>\
                        <option value="01" >01 Comprobante emitido con errores con relación</option>\
                        <option value="02" >02 Comprobante emitido con errores sin relación</option>\
                        <option value="03" >03 No se llevó a cabo la operación</option>\
                        <option value="04" >04 Operación nominativa relacionada en una factura global</option>\
                    </select>\
                </div>\
                <div class="col-md-6 class_foliorelacionado" style="display:none">\
                    <label>Folio Relacionado</label>\
                    <input type="text" id="foliorelacionado" class="form-control" placeholder="111AAA1A-1AA1-1A11-11A1-11A1AA111A11">\
                </div>\
              </div>';
    $.confirm({
        boxWidth: '55%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $('body').loading({theme: 'dark',message: 'Cancelando factura...'});
                var pass=$('#contrasena').val();
                var motivo = $('#motivocancelacion option:selected').val();
                var uuidrelacionado=$('#foliorelacionado').val();
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"Facturaslis/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                            var respuesta = parseInt(response);
                            if (respuesta==1) {
                                //===================================================
                                var facturaslis = $("#tabla_facturacion tbody > tr");
                                var DATAa  = [];
                                var num_facturas_selected=0;
                                facturaslis.each(function(){  
                                    if ($(this).find("input[class*='facturas_checked']").is(':checked')) {
                                        num_facturas_selected++;
                                        item = {};                    
                                        item ["FacturasIds"]  = $(this).find("input[class*='facturas_checked']").val();
                                        
                                        DATAa.push(item);
                                    }       
                                    
                                });
                                INFOa  = new FormData();
                                aInfoa   = JSON.stringify(DATAa);
                                //console.log(aInfoa);
                                if (num_facturas_selected==1) {
                                    $.ajax({
                                        type:'POST',
                                        url: base_url+"Ventas/cancelarCfdi",
                                        data: {
                                            facturas:aInfoa,
                                            motivo:motivo,
                                            uuidrelacionado:uuidrelacionado
                                        },
                                        success:function(response){  
                                            console.log(response);
                                            var array = $.parseJSON(response);
                                            if (array.resultado=='error') {
                                                swal({    title: "Error "+array.CodigoRespuesta+"!",
                                                              text: array.MensajeError,
                                                              type: "warning",
                                                              showCancelButton: false
                                                 });
                                            }else{
                                                swal("Éxito!", "Se ha Cancelado la factura", "success");
                                                loadtable();
                                            }
                                            $('body').loading('stop');
                                        }
                                    });
                                }else{
                                    alertfunction('Atención!','Seleccione solo una factura');
                                }
                                //================================================
                                }else{;
                                    swal("Error!", "No tiene permiso para esta operación", "warning");
                                }
                        },
                        error: function(response){ 
                            swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "warning");
                        }
                    });
                    
                }else{
                    swal("Error!", "Ingrese una contraseñan", "warning");
                }
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function mocance(){
    var motivo=$('#motivocancelacion option:selected').val();
    if(motivo=='01'){
        $('.class_foliorelacionado').show('show');
    }else{
        $('.class_foliorelacionado').hide('show');
        $('#foliorelacionado').val('');
    }
}

function alertfunction(titlea,contenta){
    $.alert({boxWidth: '30%',useBootstrap: false,title: titlea,theme: 'bootstrap',content: contenta});  
}
var facturacomplemtoid;
function complementop(facturacomplemto){
    facturacomplemtoid=facturacomplemto;
    $('#modalcomplementos').modal();
    //$('#modalcomplementos').modal('open');
    $('.listadocomplementos').html('');
    $.ajax({
        type:'POST',
        url: base_url+"Facturaslis/listasdecomplementos",
        data: {
            facturas:facturacomplemto
        },
        success:function(response){  
           $('.listadocomplementos').html(response);
           $('#tableliscomplementos').DataTable();
        }
    });
}
function addcomplemento(){
    window.location.href = base_url+'Facturaslis/complemento/'+facturacomplemtoid;
}
function modal_envio(id){
    $('#f_id').val(id);
    var urlxml = $('.modal_envio_'+id).data('urlxml');
    var correo = $('.modal_envio_'+id).data('correo');
    $('#modalenvio').modal();
    $('#f_email').val(correo);
    $('.iframepdf').html('<iframe src="'+base_url+'Facturaslis/generarfacturas/'+id+'/1" class="iframepdf_doc"></iframe>');
    $('.iframexml').html('<a href="'+urlxml+'"><img src="'+base_url+'public/img/xml.svg" style="width: 237px;"></a>');
}
function enviocorreo(){
    var form = $('#form_envio');
    if(form.valid()){
        $('#modalenvio').modal('hide');
        $('body').loading({theme: 'dark',message: 'Enviando factura...'});
        var datos=form.serialize();
        $.ajax({
            type:'POST',
            url: base_url+"Envio/enviocorreo",
            data: datos,
            success:function(response){
            if(response==1){
                toastr["success"]("Correo enviado", "Alerta!");
            }else{
                toastr["error"]("No se envio", "Alerta!");
            }  
               $('body').loading('stop');
            }
        }); 
    }
    
}