var base_url = $('#base_url').val();
$(document).ready(function(){
	$('#empleado').select2({
		width: 'resolve',
		placeholder: 'Seleccione un empleado',
		allowClear: true,
		sorter: data => data.sort((a, b) => a.text.localeCompare(b.text)),
		ajax: {
            url: base_url+'Pagos/searchPersonal',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.personalId,
                        text: element.nombre + ' ' + element.apellidos,
                        salario_dia: element.salario_dia,
												salario_hora: element.salario_hora,
                        hora_extra: element.hora_extra,
                        hora_doble: element.hora_doble,
                        hora_dia_feriado: element.hora_dia_feriado,
                        comision: element.comision_venta
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
	}).on('select2:select', function (e) {
		var data = e.params.data;
        $("#salario_dia").val(data.salario_dia);
				$("#salario_hora").val(data.salario_hora);
				$("#salario_minuto").val(data.salario_hora / 60);
        $("#hora_extra").val(data.hora_extra);
        $("#hora_doble").val(data.hora_doble);
        $("#hora_feriado").val(data.hora_dia_feriado);
				$("#comision").val(data.comision);
				if($("#del").val()!="" && $("#al").val()!=""){
					calcular();
				}
    });

	$('#del').change(function(){
		if($("#del").val()!="" && $("#al").val()!=""){
			calcular();
		}
	});
	$('#al').change(function(){
		if($("#del").val()!="" && $("#al").val()!=""){
			calcular();
		}
	});
	$("#generar").on("click",function(){
    	if($('#empleado option:selected').val()!=undefined && $("#del").val()!="" && $("#al").val()!=""){
			generarPDF();
		}
    });

    $("#det_prods").on("click",function(){
    	if($('#empleado option:selected').val()!=undefined && $("#del").val()!="" && $("#al").val()!=""){
			getDetallesProds();
		}
    });

    $("#ver_bonos").on("click",function(){
    	if($('#empleado option:selected').val()!=undefined && $("#del").val()!="" && $("#al").val()!=""){
			getDetallesBonosDescs(1);
		}
    });
    $("#ver_descs").on("click",function(){
    	if($('#empleado option:selected').val()!=undefined && $("#del").val()!="" && $("#al").val()!=""){
			getDetallesBonosDescs(2);
		}
    });
    $("#ver_descs_dias").on("click",function(){
    	if($('#empleado option:selected').val()!=undefined && $("#del").val()!="" && $("#al").val()!=""){
			getDetallesNoLaboral();
		}
    });

		$("#confirmar").on("click",function(){
			if($('#empleado option:selected').val()!=undefined && $("#del").val()!="" && $("#al").val()!=""){
				confirmarPagos();
			}
    });
});

function calcular(){
	$("#cant_dias").val(verificaFechas());
	verificaHoras();

	//tot_salario = parseFloat($("#cant_dias").val()) * parseFloat($("#salario_dia").val());

	//tot_salario = (parseFloat($("#cant_horas").val()) * parseFloat($("#salario_hora").val()));
	tot_salario = (parseFloat($("#cant_horas").val()) * parseFloat($("#salario_hora").val()))+(parseFloat($("#cant_min").val()) * parseFloat($("#salario_minuto").val()));
	//console.log("tot_salario"+tot_salario);
	$("#total_salario").val(tot_salario.toFixed(2));

	////////////
	if($("#cant_extras").val()==""){
		tot_ext = 0;
		$("#total_extra").val("0.00");
	}else{
		tot_ext = parseFloat($("#cant_extras").val()) * parseFloat($("#hora_extra").val());
		$("#total_extra").val(tot_ext);
	}	
	///////////
	if($("#cant_dobles").val()==""){
		tot_doble = 0;
		$("#total_doble").val("0.00");
	}else{
		tot_doble = parseFloat($("#cant_dobles").val()) * parseFloat($("#hora_doble").val());
		$("#total_doble").val(tot_doble);
	}	
	///////////
	if($("#cant_feriado").val()==""){
		tot_feria = 0;
		$("#total_feriado").val("0.00");
	}else{
		tot_feria = parseFloat($("#cant_feriado").val()) * parseFloat($("#hora_feriado").val());
		$("#total_feriado").val(tot_feria);
	}	
	///////////
	var total_bonos=0;
	var total_descs=0;
	$.ajax({
        type:'POST',
        url: base_url+'Pagos/getVentas',
        data: {
            id_emp: $('#empleado option:selected').val(), ini:$("#del").val(), fin:$("#al").val()
        },
        async: false,
        success:function(data){
        	var array = $.parseJSON(data);
        	result = array.data;
        	result2 = array.descs;

        	//console.log("tot_prods: "+result.tot_prods);
        	//console.log("tot_venta: "+result.tot_venta);
        	//console.log("tot_venta: "+result2.tot_descs);
        	var tot_desc_venta = result2.tot_descs == null ? 0 : parseFloat(result2.tot_descs).toFixed(2);
        	var tot_ventaf = parseFloat(result.tot_venta).toFixed(2)-tot_desc_venta;

        	$("#cant_prods").val(result.tot_prods);
        	$("#total_ventas").val(tot_ventaf);
        	tot_comision = parseFloat(tot_ventaf) * parseFloat($("#comision").val());
        	$("#total_comision").val(parseFloat(tot_comision).toFixed(2));

        	// traer los datos de bonos y descuentos
        	$.ajax({
		        type:'POST',
		        url: base_url+'Pagos/getTotalBD',
		        data: {
		            id_emp: $('#empleado option:selected').val(), ini:$("#del").val(), fin:$("#al").val()
		        },
		        async: false,
		        success:function(data){
		        	var array = $.parseJSON(data);
		        	var datos = array.results;
		        	var datos2 = array.resultsnl;
		        	total_bonos = parseFloat(datos[0].tot_bonos).toFixed(2);
		        	total_descs = parseFloat(datos[0].tot_descs).toFixed(2);
		        	tot_no_labora = parseFloat(datos2[0].tot_no_labora).toFixed(2);
		        	$("#total_bonos").val(total_bonos);
		        	$("#total_descs").val(total_descs);
		        	$("#total_no_laboral").val(tot_no_labora);

		        	//console.log("total_bonos: "+total_bonos);
		        	//console.log("total_descs: "+total_descs);
		        	super_total = parseFloat(tot_comision) + parseFloat(tot_salario) + parseFloat(tot_ext) + parseFloat(tot_doble) + parseFloat(tot_feria) + parseFloat(total_bonos);
					//console.log("super_total: "+super_total);
				    super_total = parseFloat(super_total)-parseFloat(total_descs)-parseFloat(tot_no_labora);
				    //console.log("super_total: "+super_total);
		        	$("#total_pagar").val(super_total.toFixed(2));
		        }
		    });

        }
    });
    // //////////////

}

function getDetallesProds(){
	$("#modal_detalles_prods").modal();
	$.ajax({
        type:'POST',
        url: base_url+'Pagos/detallesVentas',
        data: {
            id_emp: $('#empleado option:selected').val(), ini:$("#del").val(), fin:$("#al").val()
        },
        async: false,
        success:function(data){
        	$(".cont_tabla").html(data);
        	$("#tabla_det_prods").dataTable({
        		dom: 'Blfrtip',
        		buttons: [
                    {extend: 'excel'},
                    {extend: 'pdf'},
                    {extend:'print'},
            	]
        	});
        }
    });
}

function getDetallesBonosDescs(tipo){
	$("#modal_bonos_descs").modal();
	$.ajax({
        type:'POST',
        url: base_url+'Pagos/detallesExtras',
        data: {
            id_emp: $('#empleado option:selected').val(), ini:$("#del").val(), fin:$("#al").val(), tipo:tipo
        },
        async: false,
        success:function(data){
        	$(".cont_tabla_bonos_descs").html(data);
        	$("#tabla_det_bds").dataTable({
        		dom: 'Blfrtip',
        		buttons: [
                    {extend: 'excel'},
                    {extend: 'pdf'},
                    {extend:'print'},
            	]
        	});
        }
    });
}
function getDetallesNoLaboral(){
	$("#modal_no_laboral").modal();
	$.ajax({
        type:'POST',
        url: base_url+'Pagos/detallesExtrasNoLaboral',
        data: {
            id_emp: $('#empleado option:selected').val(), ini:$("#del").val(), fin:$("#al").val()
        },
        async: false,
        success:function(data){
        	$(".cont_tabla_no_laboral").html(data);
        	$("#tabla_det_nl").DataTable({
        		dom: 'Blfrtip',
        		buttons: [
                    {extend: 'excel'},
                    {extend: 'pdf'},
                    {extend:'print'},
            	]
        	});
        }
    });
}

function verificaFechas(){
	dif_day = 0;
	var del = moment($("#del").val());
	var al = moment($("#al").val());
	dif_day = al.diff(del, 'days');
	
	return dif_day+1;
}

function generarPDF(){
	id_emp = $("#empleado option:selected").val();
	ini=$("#del").val(); fin=$("#al").val();
	
	dif_day = 0;
	var del = moment($("#del").val());
	var al = moment($("#al").val());
	dif_day = al.diff(del, "days");

	if (dif_day + 1 > 0) {
		$.ajax({
			type: "POST",
			url: base_url + "Pagos/getHorasLaboradas",
			data: {
				id_emp: $("#empleado option:selected").val(),
				ini: $("#del").val(),
				fin: $("#al").val(),
			},
			async: false,
			success: function (data) {
				//console.log("DATA H: " + data);

				data = JSON.parse(data);
				var arrayHrs = data[0];

				console.log("Total Minutos: " + arrayHrs.total_minutos);
				window.open(base_url+'Pagos/generarBoleta/'+id_emp+"/"+ini+"/"+fin+"/"+(dif_day + 1)+"/"+arrayHrs.total_horas+"/"+arrayHrs.total_minutos);

			},
		});
	}else{
		window.open(base_url+'Pagos/generarBoleta/'+id_emp+"/"+ini+"/"+fin+"/"+0+"/"+0+"/"+0);
	}

}

function verificaHoras() {
	dif_day = 0;
	var del = moment($("#del").val());
	var al = moment($("#al").val());
	dif_day = al.diff(del, "days");

	if (dif_day + 1 > 0) {
		$.ajax({
			type: "POST",
			url: base_url + "Pagos/getHorasLaboradas",
			data: {
				id_emp: $("#empleado option:selected").val(),
				ini: $("#del").val(),
				fin: $("#al").val(),
			},
			async: false,
			success: function (data) {
				//console.log("DATA H: " + data);

				data = JSON.parse(data);
				var arrayHrs = data[0];

				//console.log("Total Horas: " + arrayHrs.total_horas);
				//console.log("Total Horas: " + arrayHrs.total_minutos);
				$("#cant_horas").val(arrayHrs.total_horas);
				$("#cant_min").val(arrayHrs.total_minutos);
				
				$("#cant_h_m").val(arrayHrs.total_horas +':'+arrayHrs.total_minutos);

			},
		});
	}else{
		$("#cant_horas").val(0);
		$("#cant_min").val(0);
	}
}


function confirmarPagos() {
	$.confirm({
		boxWidth: "30%",
		useBootstrap: true,
		icon: "fa fa-warning",
		title: "¡Atención!",
		content:"¿Confirmar pagos?",
		type: "red",
		typeAnimated: true,
		buttons: {
			Confirmar: function () {
				$.ajax({
					type: "POST",
					url: base_url + "Pagos/confirmarPagos",
					data: {
						id_emp: $("#empleado option:selected").val(),
						ini: $("#del").val(),
						fin: $("#al").val(),
						salarioHora: $("#salario_hora").val(),
						horasTrabajadas: $("#cant_horas").val(),
						//salarioMinutos: $("#salario_minuto").val(),
						//minutosTrabajados: $("#cant_min").val(),
						pagoTotal: $("#total_pagar").val(),
					},
					async: false,
					success: function (data) {
						//console.log("DATA CP: " + data);
						data = $.parseJSON(data);

						if(data > 0){
							swal({
								position: "Éxito",
								icon: "success",
								title: "Exito!",
								type: "success",
								text: "Se ha registrado correctamente!",
								showConfirmButton: false,
								timer: 3500
							});
						}else if(data == "E"){
							swal({
								position: "Éxito",
								icon: "error",
								title: "Error!",
								type: "error",
								text: "Ya existe pago con este periodo de fechas",
								showConfirmButton: false,
								timer: 5000
							});
						}else{
							swal({
								position: "Éxito",
								icon: "error",
								title: "Error!",
								type: "error",
								text: "No se ha registrado",
								showConfirmButton: true,
								//timer: 3500
							});
						}
		
					},
				});
			},
			Cancelar: function () {
			},
		},
	});
}