var base_url = $("#base_url").val();
let canChangeDates = false;
//let isSalida = false;

$(document).ready(function () {
	$("#empleado")
		.select2({
			width: "resolve",
			placeholder: "Seleccione un empleado",
			allowClear: true,
			sorter: (data) => data.sort((a, b) => a.text.localeCompare(b.text)),
			//minimumInputLength: 2,
			ajax: {
				url: base_url + "Asistencias/searchPersonal",
				dataType: "json",
				data: function (params) {
					var query = {
						search: params.term,
						type: "public",
					};
					return query;
				},
				processResults: function (data) {
					//console.log(data);
					var itemscli = [];

					data.forEach(function (element) {
						itemscli.push({
							id: element.personalId,
							text: element.nombre + " " + element.apellidos,
						});
					});
					return {
						results: itemscli,
					};
				},
			},
		})
		.on("select2:select", function (e) {
			//console.log("Data: "+JSON.stringify(data));
			var data = e.params.data;

			$("#id").val(0);
			$("#existIn").val(0);
			getList(data.id);
			$("#entrada").val(getHoraE());
			$("#salida").val("");
			$("#salida").attr("readonly", "readonly");
			$("#fecha").val(getFecha());
		});

	$("#entrada").change(function () {
		$("#entrada").val(getHoraE());
	});

	$("#salida").change(function () {
		//$("#salida").val(getHoraS());
	});

	$("#fecha").change(function () {
		$("#fecha").val(getFecha());
	});

	$("#add").on("click", function () {
		if (!canChangeDates) {
			$("#entrada").val(getHoraE());
			$("#fecha").val(getFecha());

			if ($("#existIn").val() != 0 && $("#id").val() != 0) {
				//$("#salida").val(getHoraS());
			} else {
				$("#salida").val("");
			}
		}

		//setTimeout(function () {
		if ($("#existIn").val() == 0) {
			checkRecordForToday();
		} else if ($("#existIn").val() != 0 && $("#id").val() != 0) {
			var hs = $("#salida").val();
			hs = formato12Horas(hs);

			$.confirm({
				boxWidth: "20%",
				useBootstrap: false,
				icon: "fa fa-warning",
				title: "¡Atención!",
				content: '¿La hora "' + hs + '" es correcta?',
				type: "red",
				typeAnimated: true,
				buttons: {
					Cancelar: function () {},
					Aceptar: function () {
						$("#salida").attr("readonly", "readonly");
						checkRecordForToday();
					},
				},
			});

		} else {

			$.confirm({
				boxWidth: "20%",
				useBootstrap: false,
				icon: "fa fa-warning",
				title: "¡Atención!",
				content: "Se requiere el registro de salida.",
				type: "orange",
				typeAnimated: true,
				buttons: {
					Aceptar: function () {},
				},
			});
		}
		//}, 500);
	});

	function getList(id) {
		checkEntradas(id);
		$("#cont_lista").show("slow");
		tabledata = $("#tabla_asistencia").DataTable({
			destroy: true,
			searching: false,
			ajax: {
				url: base_url + "Asistencias/getList_asistencias",
				type: "post",
				data: {
					id_emp: $("#empleado option:selected").val(),
				},
			},
			columns: [
				{
					data: "empleado",
				},
				{
					data: "fechaF",
				},
				{
					data: "entradaF",
				},
				{
					data: null,
					render: function (data, type, row, meta) {
						if (row.check_out == 0) {
							return "--------";
						} else {
							return row.salidaF;
						}
					},
				},
				{
					data: null,
					render: function (data, type, row, meta) {
						//console.log("ROW: "+JSON.stringify(row));
						if (row.check_out == 0) {
							var html =
								'<button type="buton" onclick="check_salida(' +
								row.id +
								"," +
								row.personal_id +
								",'" +
								row.fecha +
								"','" +
								row.entrada +
								'\')" class="btn btn-warning white shadow-z-1-hover" >Registrar salida</button>';
						} else {
							var html =
								'<button type="buton" class="btn btn-warning white shadow-z-1-hover" disabled>Registrar salida</button>';
						}
						//console.log('existIn D: '+$('#existIn').val());
						return html;
					},
				},
			],
		});
	}

	function checkEntradas(id) {
		$.ajax({
			type: "POST",
			url: base_url + "Asistencias/checkEntradas",
			data: { id: id },
			success: function (data) {
				if (data == "1") {
					$("#existIn").val(1);
				}
			},
		});
	}

	function checkRecordForToday() {
		$.ajax({
			type: "POST",
			url: base_url + "Asistencias/checkInOutToday",
			data: { id: $("#empleado option:selected").val() },
			success: function (data) {
				if (data == 0) {
					guardar();
				} else {
					toastr.error(
						"Error!",
						"Se ha registrado tanto la entrada como la salida correspondientes al día de hoy."
					);
				}
			},
		});
	}

	function guardar() {
		var form_register = $("#form_asistencias");
		var error_register = $(".alert-danger", form_register);
		var success_register = $(".alert-success", form_register);
		var $validator1 = form_register.validate({
			errorElement: "div", //default input error message container
			errorClass: "vd_red", // default input error message class
			focusInvalid: false, // do not focus the last invalid input
			ignore: "",
			rules: {
				personal_id: {
					required: true,
				},
				fecha: {
					required: true,
				},
			},

			errorPlacement: function (error, element) {
				if (
					element.parent().hasClass("vd_checkbox") ||
					element.parent().hasClass("vd_radio")
				) {
					element.parent().append(error);
				} else if (element.parent().hasClass("vd_input-wrapper")) {
					error.insertAfter(element.parent());
				} else {
					error.insertAfter(element);
				}
			},
			invalidHandler: function (event, validator) {
				//display error alert on form submit
				success_register.fadeOut(500);
				error_register.fadeIn(500);
				scrollTo(form_register, -100);
			},
			highlight: function (element) {
				// hightlight error inputs
				$(element).addClass("vd_bd-red");
				$(element)
					.siblings(".help-inline")
					.removeClass("help-inline fa fa-check vd_green mgl-10");
			},
			unhighlight: function (element) {
				// revert the change dony by hightlight
				$(element).closest(".control-group").removeClass("error"); // set error class to the control group
			},
			success: function (label, element) {
				label
					.addClass("valid")
					.addClass("help-inline fa fa-check vd_green mgl-10") // mark the current input as valid and display OK icon
					.closest(".control-group")
					.removeClass("error")
					.addClass("success"); // set success class to the control group
				$(element).removeClass("vd_bd-red");
			},
		});

		var valid = $("#form_asistencias").valid();
		//console.log("DATA: " + form_register.serialize());
		if (valid) {
			$.ajax({
				type: "POST",
				url: base_url + "Asistencias/insertar",
				data: form_register.serialize(),
				async: false,
				beforeSend: function () {
					$("#save").attr("disabled", true);
				},
				success: function (data) {
					toastr.success("Hecho!", "Guardado Correctamente");

					setTimeout(function () {
						//$("#form_asistencias").trigger("reset");
						//$("#empleado").val("").change();

						$("#id").val(0);
						$("#existIn").val(0);
						$("#fecha").val(getFecha());
						$("#entrada").val(getHoraE());
						$("#salida").val("");
						getList($("#empleado option:selected").val());

						$("#save").attr("disabled", false);
					}, 1500);
				},
			});
		}
	}
});

function check_salida(id, cliente, fecha, hora_e) {
	var HoraS = getHoraS();
	//$("#empleado").val(cliente).change();
	$("#id").val(id);
	$("#fecha").val(fecha);
	$("#entrada").val(hora_e);
	$("#salida").val(HoraS);
	$("#salida").removeAttr("readonly");

	//console.log("Hora Sal: "+HoraS);
}

function getHoraE() {
	var e_fecha = new Date();
	var e_horas = e_fecha.getHours();
	var e_minutos = e_fecha.getMinutes();
	var e_segundos = e_fecha.getSeconds();

	// Asegura que siempre tengan dos dígitos
	e_horas = e_horas < 10 ? "0" + e_horas : e_horas;
	e_minutos = e_minutos < 10 ? "0" + e_minutos : e_minutos;
	e_segundos = e_segundos < 10 ? "0" + e_segundos : e_segundos;

	//console.log("Hora Entrada: "+e_horas + ':' + e_minutos + ':' + e_segundos);

	return e_horas + ":" + e_minutos + ":" + e_segundos;
}

function getHoraS() {
	var s_fecha = new Date();
	var s_horas = s_fecha.getHours();
	var s_minutos = s_fecha.getMinutes();
	var s_segundos = s_fecha.getSeconds();

	// Asegura que siempre tengan dos dígitos
	s_horas = s_horas < 10 ? "0" + s_horas : s_horas;
	s_minutos = s_minutos < 10 ? "0" + s_minutos : s_minutos;
	s_segundos = s_segundos < 10 ? "0" + s_segundos : s_segundos;

	//console.log("Hora Salida: " + s_horas + ':' + s_minutos + ':' + s_segundos);

	return s_horas + ":" + s_minutos + ":" + s_segundos;
}

function getFecha() {
	var f_fecha = new Date();
	var f_dia = f_fecha.getDate();
	var f_mes = f_fecha.getMonth() + 1; // Los meses empiezan desde 0
	var f_ano = f_fecha.getFullYear();

	// Asegura que día y mes siempre tengan dos dígitos
	f_dia = f_dia < 10 ? "0" + f_dia : f_dia;
	f_mes = f_mes < 10 ? "0" + f_mes : f_mes;

	//console.log("Fecha1: " + f_dia + '/' + f_mes + '/' + f_ano);
	//console.log("Fecha2: " + f_ano + '-' + f_mes + '-' + f_dia);

	("yyyy-MM-dd");

	return f_ano + "-" + f_mes + "-" + f_dia;
}

function formato12Horas(timeString) {
	var timeParts = timeString.split(":");
	var hours = parseInt(timeParts[0]);
	var minutes = timeParts[1];
	var period = hours >= 12 ? "PM" : "AM";
	hours = hours % 12 || 12;

	return hours + ":" + minutes + " " + period;
}


function modal_export_excel(){
	$('#modal_export_excel').modal("show");

	$('#f_ini').val(getFecha());
	$('#f_fin').val(getFecha());
	if($('#empleado').val() != null){
		$('#id_pers').val($('#empleado').val());
	}

}

function export_excel(){
	//console.log('xxx');
	var fIni = $('#f_ini').val();
	var fFin = $('#f_fin').val();
	var idP = $('#id_pers').val();

	//console.log('FIN: ' + fIni);

	/*
	if(fIni == '' || fFin == ''){
		toastr.error("Error!","Fechas no validas.");
		return;
	}
	*/

	if(fIni > fFin){
		toastr.error("Error!","La fecha de inicio no puede ser superior a la final.");
		return;
	}

	if (fIni == '') {
		fIni = 0;
	}

	if (fFin == '') {
		fFin = 0;
	}

	window.open(base_url + "Asistencias/exportExcel/" + fIni + "/" + fFin + "/" + idP, '_blank');
	$('#modal_export_excel').modal("hide");
}