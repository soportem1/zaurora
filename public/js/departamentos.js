var base_url = $('#base_url').val();
var table;
var iddepartamento=0;
function load() {
    
    table = $('#tabla').DataTable({
        "ajax": {
            "url": base_url + "index.php/Departamentos/getDepartamentos"
        },
        "columns": [{
            "data": "iddepartamento"
        }, {
            "data": "nombre"
        },
        {"data": null,
            "render" : function ( url, type, full) {
                var btton='';
                    btton += "<button type='button' class='btn btn-sm btn-icon btn-info white edit'><i class='ft-edit'></i></button>";
                    btton += "<button type='button' class='btn btn-sm btn-icon btn-danger white delete'><i class='fa fa-times'></i></button>";
                
                return btton;
            }
        }
        ]
    });

}
$(document).ready(function() {
    //table = $('#tabla').DataTable();

    //Listener para edicion
    $('#tabla tbody').on('click', 'button.edit', function() {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        functionedit(data.iddepartamento,data.nombre);

    });
    $('#tabla tbody').on('click', 'button.delete', function() {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        functiondelete(data.iddepartamento);

    });

    load();
    $('.addsocio').click(function(event) {
        $('#modalsocios').modal();
        $('#idsocio').val(0);
        $('#namesocio').val('');
    });
    $('.guardarsocio').click(function(event) {
        $.ajax({
                    type:'POST',
                    url: base_url+'Departamentos/add',
                    data: {
                        iddepartamento: $('#idsocio').val(),
                        nombre: $('#namesocio').val()
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                        toastr.success('Hecho!', 'Guardado Correctamente');
                        setTimeout(function(){ 
                            table.destroy();
                            load();
                        }, 3000);
                        
                    }
                });
    });
    $('.eliminarsocio').click(function(event) {
        $.ajax({
                    type:'POST',
                    url: base_url+'Departamentos/delete',
                    data: {
                        iddepartamento: iddepartamento
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                        toastr.success('Hecho!', 'Guardado Correctamente');
                        setTimeout(function(){ 
                            table.destroy();
                            load();
                        }, 3000);
                        
                    }
                });
    });
});
function functionedit(id,name){
        $('#modalsocios').modal();
        $('#idsocio').val(id);
        $('#namesocio').val(name);
}
function functiondelete(id){
    $('#modaldeletesocios').modal();
    iddepartamento=id;
}