var base_url = $('#base_url').val();
var tabledata;
$(document).ready(function() {
	loadtable();
	$('.newtraspaso').click(function(event) {
		$('#modaltraspaso').modal();
	});
	$('#sproducto').select2({
		
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un Producto',
		dropdownParent: $("#modaltraspaso"),//solo para modales
		width: 'element',
	  	ajax: {
	    	url: base_url+'Ventas/searchproducto',
	    	dataType: "json",
	    	data: function (params) {
	      	var query = {
	        	search: params.term,
	        	type: 'public'
	      	}
	      	return query;
	    },
	    processResults: function(data){
	    	var clientes=data;
	    	var itemscli = [];
	    	data.forEach(function(element) {
                itemscli.push({
                    id: element.productoid,
                    text: element.codigo+' / '+element.nombre
                });
            });
            return {
                results: itemscli
            };	    	
	    },  
	  }
	}).on('select2:select', function (e) {
	    //var data = e.params.data;
	    //console.log(data);
	    //addproducto();

	});
	$('.aceptartraspaso').click(function(event) {
		var corigen= $('#stocks').val();

		var cantidd= $('#cantidadtraspaso').val();
		var prod=$('#sproducto option:selected').val();
		var sucursals =$('#sucursal_salida option:selected').val();
		var sucursale =$('#sucursal_entrada option:selected').val();
	
		if (cantidd>0) {
			if (cantidd<=corigen) {
				params = {};
				params.producto = prod;
				params.sucursals = sucursals;
				params.sucursale = sucursale;
				params.cantidad = cantidd;

				$.ajax({
					type:'POST',
					url:base_url+'Traspasos/traspasar',
					data:params,
					async:false,
					success:function(data){
						
						toastr.success('Traspaso realizado','Hecho!');
						$('#modaltraspaso').modal('hide');
						$("#sproducto").val(0).change();
						$('#stocks').val(0);
						$('#stocke').val(0);
						$('#cantidadtraspaso').val('');
						recargarlistado();

						
					}
				});

			}else{
				toastr.error('No se permite cantidad mayor a la de origen', 'Error');
			}

		}else{
			toastr.error('Especifique cantidad', 'Error');
		}
	});
	

	//$(".selectsucursalt").val(0).change();
});
function loadtable() {
    tabledata = $('#data-tables').DataTable({
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "Traspasos/getData_traspasos",
            type: "post",
            //------------------------------------
            /*
            "data": {
                "departamento": depa
            },
            */
            //------------------------------------
            error: function() {
                $("#tabla_producto").css("display", "none");
            }
        },
        "columns": [
              {"data": "traspasoId"},
              {"data": "nombre"}, 
              {"data": "suc_salida"}, 
              {"data": "suc_entrada"},
              {"data": "cantidad"},
              {"data": "personal"},
              {"data": "reg"},
        ],
        order: [
            [1, "desc"]
        ]



    });
}
function recargarlistado(){
  tabledata.destroy();
    loadtable();
}
function consultarstock(tipo){
	var prod=$('#sproducto option:selected').val();
	if (tipo==0) {
		var sucursal =$('#sucursal_salida option:selected').val();
	}else{
		var sucursal =$('#sucursal_entrada option:selected').val();
	}
	if (prod!=undefined) {
		if (sucursal!=0) {
			params = {};
			params.producto = prod;
			params.sucursal = sucursal;
			$.ajax({
				type:'POST',
				url:base_url+'Traspasos/cstock',
				data:params,
				async:false,
				success:function(data){
					if (tipo==0) {
						$('#stocks').val(data);
					}else{
						$('#stocke').val(data);
					}
					
				}
			});
		}else{
			toastr.error('Seleccione sucursal', 'Error');
		}

	}else{
		toastr.error('Seleccione producto', 'Error');
	}
}