var base_url = $('#base_url').val();
$(document).ready(function () {
	console.log("JS");

	//=============================================================
	var form_register = $('#formConfig');
	var error_register = $('.alert-danger', form_register);
	var success_register = $('.alert-success', form_register);

	var $validator1 = form_register.validate({
		errorElement: 'div', //default input error message container
		errorClass: 'vd_red', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules: {
			fechaIni: {
				required: true
			},
			porcentaje: {
				required: true,
				number: true,
				min: 0,
				max: 100
			},
		},

		errorPlacement: function (error, element) {
			if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
				element.parent().append(error);
			} else if (element.parent().hasClass("vd_input-wrapper")) {
				error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		},

		invalidHandler: function (event, validator) { //display error alert on form submit              
			success_register.fadeOut(500);
			error_register.fadeIn(500);
			scrollTo(form_register, -100);

		},

		highlight: function (element) { // hightlight error inputs

			$(element).addClass('vd_bd-red');
			$(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

		},

		unhighlight: function (element) { // revert the change dony by hightlight
			$(element)
				.closest('.control-group').removeClass('error'); // set error class to the control group
		},

		success: function (label, element) {
			label
				.addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
				.closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
			$(element).removeClass('vd_bd-red');
		}
	});

	//=====================================================================================================

	$('#guardar').click(function (event) {
		event.preventDefault(); // Previene la recarga del formulario

		var porcentaje = $('#porcentaje').val();
		var sinFinal = $('#chkFecha').is(':checked');
		var fInicio = $('#fechaIni').val();
		var fFinal = $('#fechaFin').val();

		if (!sinFinal) {
			if (fInicio != '' && fFinal != '') {
				if (fInicio > fFinal) {
					toastr.error('No existen fechas validas', 'Error');
					return;
				}
			} else {
				toastr.error('No existen fechas validas', 'Error');
				return;
			}
		} else {
			if (fInicio == '') {
				toastr.error('No existen fechas validas', 'Error');
				return;
			}
		}

		if (porcentaje == '' || porcentaje < 0 || porcentaje > 100) {
			toastr.error('Porcentaje no valido', 'Error');
			return;
		}


		$('#guardar').prop('disabled', true);
		var $valid = $('#formConfig').valid();
		if ($valid) {
			$.ajax({
				type: 'POST',
				url: base_url + 'ConfigPuntos/updateConfigPuntos',
				data: {
					porcentaje: porcentaje,
					sinFinal: sinFinal ? 1 : 0,
					fInicio: fInicio,
					fFinal: fFinal
				},
				async: false,
				statusCode: {
					404: function (data) {
						toastr.error('Error!', 'No Se encuentra el archivo');
					},
					500: function () {
						toastr.error('Error', '500');
					}
				},
				success: function (data) {
					toastr.success('Hecho!', 'Guardado Correctamente');
					setInterval(function () {
						window.location.href = base_url + "ConfigPuntos";
					}, 3000);
				}
			});

		} else {
			console.log("NO Existe");
			//toastr.success('Guardado Correctamente','Hecho!');
			//toastr.warning('Error!', 'Intente nuevamente');
			$('#guardar').prop('disabled', false);
		}
	});

	$('#chkFecha').change(function () {
		console.log('CHECK');
		if (document.getElementById("chkFecha").checked) {
			$('#fechaFin').val('');
			$('#fechaFin').attr('disabled', 'disabled');
		} else {
			$('#fechaFin').val('');
			$('#fechaFin').removeAttr('disabled');
		}
	});
});
