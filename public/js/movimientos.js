var base_url = $('#base_url').val();
$(document).ready(function () {
  //console.log("ready movimientos");
  var tabledata;
  var starttime; var start;
  var endtime; var end;
  $("#color").spectrum({
      color: '#cfa520',
      flat: false,
      showInput: true,
      preferredFormat: "hex",
      showPalette: true,
      showPaletteOnly: true,
      palette: [["#cfa520", "#f90", "#ff0", "#0f0", "#0ff", "#00f", "#90f", "#f0f"], ["#f4cccc", "#fce5cd", "#fff2cc", "#d9ead3", "#d0e0e3", "#cfe2f3", "#d9d2e9", "#ead1dc"], ["#ea9999", "#f9cb9c", "#ffe599", "#b6d7a8", "#a2c4c9", "#9fc5e8", "#b4a7d6", "#d5a6bd"], ["#e06666", "#f6b26b", "#ffd966", "#93c47d", "#76a5af", "#6fa8dc", "#8e7cc3", "#c27ba0"], ["#c00", "#e69138", "#f1c232", "#6aa84f", "#45818e", "#3d85c6", "#674ea7", "#a64d79"], ["#900", "#b45f06", "#bf9000", "#38761d", "#134f5c", "#0b5394", "#351c75", "#741b47"], ["#600", "#783f04", "#7f6000", "#274e13", "#0c343d", "#073763", "#20124d", "#4c1130"]]
  });


  $('#calendar').fullCalendar({
    lang: 'es',
    header: {
      center: 'month,agendaWeek,agendaDay' // buttons for switching between views
    },
    defaultView: 'agendaDay',
    timeFormat: 'h(:mm)',
    timeZone: 'America/Mexico_City',
    locale: 'es',
      views: {
      month: {buttonText: 'Mes'},
      agendaWeek: {buttonText: 'Semana'},
      agendaDay: {buttonText: 'Día'}
    },
    select: function(start, end, jsEvent) {  // click on empty time slot
      endtime = $.fullCalendar.moment(end).format('h:mm');
      starttime = $.fullCalendar.moment(start).format('dddd, MMMM Do YYYY, h:mm');
      var mywhen = starttime + ' - ' + endtime;
      start = moment(start).format();
      end = moment(end).format();
      $('#createEventModal #startTime').val(start);
      $('#createEventModal #endTime').val(end);
      $('#createEventModal #when').text(mywhen);
      $('#createEventModal').modal('toggle');
    },
            
    defaultDate: $("#fecha").val(),
    editable: true,
    eventLimit: true, // allow "more" link when too many events
    events: base_url+'ReporteMovs/registrosLogAgenda/',
    /*eventClick: function (calEvent) {
      informacion_evento(calEvent.id_ultimo,calEvent.tabla);
    }*/
    eventClick:  function(event, jsEvent, view) {  // when some one click on any event
      endtime = $.fullCalendar.moment(event.end).format('h:mm');
      starttime = $.fullCalendar.moment(event.start).format('dddd, MMMM Do YYYY, h:mm');
      var mywhen = starttime ;
      var modificacion = event.modificacion;
      var tabla = event.tabla;
      if(modificacion=="insertar"){ modificacion="Registro"; $("#modalSubTitle").removeClass("red_prod"); }
      if(modificacion=="modifica"){ modificacion="Edición"; $("#modalSubTitle").removeClass("red_prod"); }
      if(modificacion=="elimina multiple") { modificacion="Eliminación multiple"; $("#modalSubTitle").addClass("red_prod"); }
      if(modificacion=="elimina") { modificacion="Eliminación"; $("#modalSubTitle").addClass("red_prod"); }
      if(modificacion=="cancelado total") { modificacion="Cancelación de venta"; $("#modalSubTitle").removeClass("red_prod"); }
      if(modificacion=="cancelado parcial") { modificacion="Cancelación de concepto en venta"; $("#modalSubTitle").removeClass("red_prod"); }

      $('#modalTitle').html("Tabla: "+event.title);
      $('#modalSubTitle').html("Tipo: " +modificacion);
      $('#modalUser').html("Realizó: " +event.Usuario);
      $('#modalWhen').text(mywhen);
      $('#eventID').val(event.id);

      if(tabla=="productos"){
        $.ajax({
          type:'POST',
          url: base_url+'ReporteMovs/detalleLog',
          data: { id:event.id_reg },
          async: false,
          success:function(data){
            var array = $.parseJSON(data);
            $('#modalProd').html("Producto: "+ array.codigo+" / "+array.nombre);
          }
        });
      }else{
        $('#modalProd').html("");
      }

      $('#calendarModal').modal();
    },

  });

  $('#chkFecha').change(function(){
    if(document.getElementById("chkFecha").checked){
      f = new Date();
      if(f.getDate()<10){
        dia = "0"+f.getDate();
      }else{dia = f.getDate();
      }
      if(f.getMonth()<10){
        mes = "0"+(f.getMonth()+1);
      }else{
        mes = (f.getMonth+1);
      }
      fecha = f.getFullYear()+"-"+mes+"-"+dia;
      $('#txtInicio').val(fecha);
      $('#txtFin').val(fecha);
      $('#txtInicio').attr('disabled','disabled');
      $('#txtFin').attr('disabled','disabled'); 
    }
    else{
      $('#txtInicio').val('');
      $('#txtFin').val('');
      $('#txtInicio').removeAttr('disabled');
      $('#txtFin').removeAttr('disabled');
    }
  });

    /*tabledata = $('#data-tables').DataTable({
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "ReporteMovs/getData_sessions",
            type: "post",
           
            error: function() {
                $("#tbodyresultadosess").css("display", "none");
            }
        },
        "columns": [
              {"data": "id"}, 
              {"data": "id_reg"}, 
              {"data": "tabla"}, 
              {"data": "modificacion"},
              {"data": "empleado"},
              {"data": "fecha"},
              {
                    "data": null,
                    "render" : function ( url, type, full) {
                        if (full['id_sucursal']==0) {
                            var msj="Matriz - Admin";
                        }else{
                            var msj= full['sucursal'];
                        }
                        return msj;
                    }
                    
                }
              //{"data": "sucursal"},             
        ],
    });*/

  $('#btnBuscar').click(function(){
    params = {};
    params.fecha1 = $('#txtInicio').val();
    params.fecha2 = $('#txtFin').val();
    
    if(params.fecha1 != '' && params.fecha2 != ''){
      tabledata.destroy();
      /*
      var tabla='<table class="table table-striped responsive" id="data-tables" style="width: 100%">\
                    <thead>\
                      <tr>\
                        <th>#</th>\
                        <th># Registro</th>\
                        <th>Tabla</th>\
                        <th>Modificación</th>\
                        <th>Empleado</th>\
                        <th>Fecha</th>\
                        <th>Sucursal</th>\
                        <th></th>\
                      </tr>\
                    </thead>\
                    <tbody id="tbodyresultadosess">\
                    </tbody>\
                  </table>';
                  $('.table-movs').html(tabla);
        */
      tabledata = $('#data-tables').DataTable({
        "bProcessing": true,
        "serverSide": true,
        "retrieve": true,
        "ajax": {
            url: base_url + "ReporteMovs/movsBusqueda",
            type: "post",
            //------------------------------------
            "data": {
                 "txtInicio": $('#txtInicio').val(),
                 "txtFin": $('#txtFin').val()
            },
            //------------------------------------
            error: function() {
                $("#data-tables").css("display", "none");
            }
        },
        "columns": [
          {"data": "id"}, 
          {"data": "id_reg"}, 
          {"data": "tabla"}, 
          {"data": "modificacion"},
          {"data": "empleado"},
          {"data": "fecha"},
          {
                "data": null,
                "render" : function ( url, type, full) {
                    if (full['id_sucursal']==0) {
                        var msj="Matriz - Admin";
                    }else{
                        var msj= full['sucursal'];
                    }
                    return msj;
                }
                
            }       
        ],
      });
      
    }
    else{
      toastr.error('No existen fechas validas', 'Error');
      
    }
  });

        //=====================================================================================================  
});

