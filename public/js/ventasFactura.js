var base_url = $('#base_url').val();
var total=0;
var trunquearredondear=0;//0 redondeo 1 trunquear (dos dijitos)
$(document).ready(function(){
	//$('.chosen-select').chosen({width: "91%"});

	$('.sunidadsat').select2({
        width: '90%',
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar Unidad',
          ajax: {
            url: base_url+'Ventas/searchunidadsat',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.Clave,
                        text: element.Clave+' / '+element.nombre
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });
	$('.sconseptosat').select2({
        width: '90%',
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar Concepto',
          ajax: {
            url: base_url+'Ventas/searchconceptosat',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.Clave,
                        text: element.nombre
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });

    $('#idcliente').select2({
        width: '90%',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un cliente',
        ajax: {
            url: base_url+'Ventas/searchcli',
            dataType: "json",
            data: function (params) {
            var query = {
                search: params.term,
                type: 'public'
            }
            return query;
        },

        processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                itemscli.push({
                    id: element.ClientesId,
                    text: element.Nom
                });
            });
            return {
                results: itemscli
            };          
        },  
      }
    }).on('select2:select', function (e) {
    	var data = e.params.data;
        obtenerrfc(data.id)
    });

    $('.registrofac').click(function(event) {
        $( ".registrofac" ).prop( "disabled", true );
        setTimeout(function(){ 
            $( ".registrofac" ).prop( "disabled", false );
        }, 5000);
		var form =$('#validateSubmitForm');
		var valid =form.valid();
        var exento=0;
		if (valid) {
            
			var datos = form.serialize();
			var productos = $("#table_conceptos tbody > tr");
            var band_vacio=0;
			//==============================================
				var DATAa  = [];
		        productos.each(function(){   
		            item = {};                    
                    item ["Cantidad"] = $(this).find("input[id*='cantidad']").val();
		            item ["Unidad"]  = $(this).find("select[id*='unidadsat'] option:selected").val();
		            item ["servicioId"]  = $(this).find("select[id*='conseptosat'] option:selected").val();
		            item ["Descripcion"]  = $(this).find("select[id*='conseptosat'] option:selected").text();
		            item ["Descripcion2"]  = $(this).find("input[id*='descripcion']").val();
		            item ["Cu"]  = $(this).find("input[id*='precio']").val();
                    //item ["descuento"]   = $(this).find("input[id*='descuento']").val();
                    item ["descuento"]   = 0;
		            item ["Importe"]  = $(this).find("input[id*='subtotal']").val();
		            item ["iva"]  = $(this).find("input[id*='tiva']").val();
		            DATAa.push(item);
                    if($(this).find("select[id*='unidadsat'] option:selected").val()=="" || $(this).find("select[id*='conseptosat'] option:selected").val()=="" ||
                        $(this).find("select[id*='unidadsat'] option:selected").val()==undefined || $(this).find("select[id*='conseptosat'] option:selected").val()==undefined)
                        band_vacio=1;
		        });
		        INFOa  = new FormData();
		        aInfoa   = JSON.stringify(DATAa);
                console.log("band_vacio: "+band_vacio);
			if (productos.length>0 && band_vacio==0) {
                $.blockUI({ 
                    message: '<div class="spinner-grow" style="width: 3rem; height: 3rem;" role="status" id="cont_load">\
                        <span class="sr-only">Procesando...</span>\
                      </div><br>Espere por favor... <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>',
                    css: { 
                        border: 'none', 
                        padding: '15px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: .5, 
                        color: '#fff'
                    } 
                });

				//var tiporelacion = $('input:radio[name=tiporelacion]:checked').val();
				//var ventaviculada = $('#ventaviculada').val();
                //var ventaviculadatipo = $('#ventaviculadatipo').val();
				var Subtotal=$('#Subtotal').val();
				var iva=$('#iva').val();
				var total=$('#total').val();  

                var f_r = $('#facturarelacionada').is(':checked')==true?1:0; //agregados version 4.0
                var f_r_t = $('#TipoRelacion option:selected').val();
                var f_r_uuid = $('#uuid_r').val();

                /*var visr= $('#isr').val();
                var vriva= $('#ivaretenido').val();
                var v5millar= $('#5almillarval').val();
                var outsourcing= $('#outsourcing').val();*/

				
				//datos=datos+'&subtotal='+Subtotal+'&iva='+iva+'&visr='+visr+'&vriva='+vriva+'&v5millar='+v5millar+'&outsourcing='+outsourcing+'&total='+total+'&conceptos='+aInfoa+'&id_venta='+$("#id_venta").val();
                datos=datos+'&subtotal='+Subtotal+'&iva='+iva+'&total='+total+'&conceptos='+aInfoa+'&id_venta='+$("#id_venta").val()+'&f_r='+f_r+'&f_r_t='+f_r_t+'&f_r_uuid='+f_r_uuid;
                $.ajax({
		            type:'POST',
		            url: base_url+"Ventas/generafacturarabierta",
		            data: datos,
		            success: function (response){
                        console.log(response);
                        
                        var array = $.parseJSON(response);
                        if (array.resultado=='error') {
                            /*
                            swal({    title: "Error "+array.CodigoRespuesta+"!",
                                          text: array.MensajeError,
                                          type: "warning",
                                          showCancelButton: false
                             });
                            */
                            retimbrar(array.facturaId,$("#id_venta").val());
                            $.unblockUI();
                        }else{
                            $.unblockUI();
                            swal("Éxito!", "Se ha creado la factura", "success");
                            //modificar la venta por facturada
                        }
		                setTimeout(function(){ 
		                    window.location.href = base_url+"Facturaslis/"; 
		                }, 4500);
                        
		            },
		            error: function(response){
		                swal('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema','warning'); 
                        $.unblockUI();
		            }
		        });
			}else{
				swal('Atención!','Agregar por lo menos un concepto con unidad/concepto SAT','warning');
			}
		}else{
			swal('Atención!','Faltan campos obligatorios','warning');
		}
	});
    calculartotales_set(1000);

    $('#facturarelacionada').click(function(event) { //agregado version 4.0
        if($('#facturarelacionada').is(':checked')){
            $('.divfacturarelacionada').show('show');
        }else{
            $('.divfacturarelacionada').hide('show');
            $('#uuid_r').val('');
        }
        /* Act on the event */
    });
    $('#FormaPago').change(function(event) {
        if($('#FormaPago').val()=='PPD'){
            $('#MetodoPago').val(99);
        }
    });
});

function calculartotal(){
    //console.log("calculartotal");
	var cantidad=$('#scantidad').val()==''?0:$('#scantidad').val();
	var precio=$('#sprecio').val()==''?0:$('#sprecio').val();
	total=parseFloat(cantidad)*parseFloat(precio);
	total=total.toFixed(2);
	//$('.montototal').html(new Intl.NumberFormat('es-MX').format(total))
}
function agregarconcepto(){
	if(total>0){
		var cantidad = $('#scantidad').val();
		var unidad = $('#sunidadsat').val();
		var unidadt = $('#sunidadsat').text();
		var concepto = $('#sconseptosat').val();
		var conceptot = $('#sconseptosat').text();
		var descripcion = $('#sdescripcion').val();
		var precio = $('#sprecio').val();
		var aiva = $('#aplicariva').is(':checked')==true?1:0;

		agregarconceptovalor(cantidad,unidad,unidadt,concepto,conceptot,descripcion,precio,aiva);
		$('#sdescripcion').val('');
		$('#scantidad').val(1);
		$('#sprecio').val(0);
	}
}
var rowcobrar=0;
function agregarconceptovalor(cantidad,unidad,unidadt,concepto,conceptot,descripcion,precio,aiva){
	var subtotal=parseFloat(cantidad)*parseFloat(precio);
    //console.log("aiva: "+aiva);
    var chk_ai="";
	if (aiva==1) {
		/*var siva =subtotal*0.16;
		siva = siva.toFixed(4);*/
    
        var siva =subtotal*0.16;
        if(trunquearredondear==0){
            siva = siva.toFixed(4);
        }else{
            siva=Math.floor(siva * 100) / 100;
        }
        chk_ai="checked";
	}else{
		var siva =0.00;
	}
	//console.log("siva: "+siva);
	var subtotal=parseFloat(subtotal)+parseFloat(siva);
		subtotal=subtotal.toFixed(2);
        console.log("subtotal: "+subtotal);
	var datoscobrar ='	<tr class="rowcobrar_'+rowcobrar+'">\
                          <td>\
                            <input type="number" name="cantidad" id="cantidad" class="form-control" value="'+cantidad+'" style="background: transparent !important; border: 0px !important;" readonly>\
                          </th>\
                          <th >\
                            <select name="sunidadsat" id="unidadsat" class="form-control browser-default form-control unidadsat">\
                            	<option value="'+unidad+'">'+unidadt+'</option>\
                            </select>\
                          </th>\
                          <th >\
                            <select name="conseptosat" id="conseptosat" class="form-control browser-default conseptosat">\
                            	<option value="'+concepto+'">'+conceptot+'</option>\
                            </select>\
                          </th>\
                          <th >\
                            <input type="text" name="descripcion" id="descripcion" class="form-control" value="'+descripcion+'" style="background: transparent !important; border: 0px !important;" readonly>\
                          </th>\
                          <th >\
                            <input type="number" name="precio" id="precio" class="form-control" value="'+precio+'" style="background: transparent !important; border: 0px !important;" readonly>\
                          </th>\
                          <th>\
                            <input type="number"\
                            name="descuento"\
                            id="descuento"\
                            data-diva="'+aiva+'"\
                            class="form-control cdescuento cdescuento_'+rowcobrar+'"\
                            value="0" readonly>\
                          </th>\
                          <th>\
                          	<input type="number" name="subtotal" id="subtotal" class="form-control csubtotal" value="'+subtotal+'" style="background: transparent !important; border: 0px !important;" readonly>\
                          </th>\
                          <th>\
                            <input type="number" id="tiva" value="'+siva+'" class="form-control siva" style="background: transparent !important; border: 0px !important;" readonly>\
                          </th>\
                          <th>\
                            <input type="checkbox" class="filled-in" id="aplicariva" onclick="calculartotales()" '+chk_ai+'>\
                            <label for="aplicariva"></label>\
                          </th>\
                          <!--<th >\
                            <a class="btn btn-danger" onclick="deleteconcepto('+rowcobrar+')"><i class="fa fa-trash"></i></a>\
                          </th>-->\
                        </tr>';

	$('.addcobrar').append(datoscobrar);
	rowcobrar++;
	calculartotales();
}
function deleteconcepto(row){
	$('.rowcobrar_'+row).remove();
	calculartotales();
}
function calculartotales_set(tiempo){
    setTimeout(function(){ 
        calculartotales();
    }, tiempo);
}

/*function exentoIva(idv){
    $(".siva_"+idv+"").val('0');
    $(".chk_pi_"+idv+"").attr("checked",false);
}*/

function calculartotales(){
	var totales = 0;
    /*
    $(".preciorow").each(function() {
        var vtotales = parseFloat($(this).val());
        totales += Number(vtotales);
    });
    */
    var TABLApr   = $("#table_conceptos tbody > tr");
    TABLApr.each(function(){ 
        var cant = $(this).find("input[name*='scantidad']").val();
        if(cant==undefined){
                cant = $(this).find("input[id*='cantidad']").val();
            }
        if($(this).find("#aplicariva").is(":checked")==true){
            var calc_iva = parseFloat($(this).find("input[id*='precio']").val())*parseFloat($(this).find("input[id*='cantidad']").val());
            calc_iva = calc_iva.toFixed(2);
            var precalc_iva = calc_iva * 0.16;
            precalc_iva = precalc_iva.toFixed(4);
            $(this).find("input[id*='tiva']").attr("readonly",false);
            $(this).find("input[id*='tiva']").val(precalc_iva);
            $(this).find("input[id*='tiva']").attr("readonly",true);
            var precioi= parseFloat($(this).find("input[id*='precio']").val());
            //var sub = precioi - calc_iva;
            var sub = calc_iva;
            $(this).find("input[name*='sprecio']").val(sub);
            $(this).find("input[name*='subtotal']").val(sub);
        }else{
            $(this).find("input[id*='tiva']").val("0");
            var sub_siniva=parseFloat($(this).find("input[id*='precio']").val())*cant;
            //$(this).find("input[name*='sprecio']").val($(this).find("input[id*='precio']").val());
            $(this).find("input[name*='subtotal']").val(sub_siniva);
        }

        var vtotales = parseFloat($(this).find("input[id*='cantidad']").val())*parseFloat($(this).find("input[id*='precio']").val());
            vtotales = vtotales.toFixed(2);
            totales += Number(vtotales);

    });

    //=================================================
    var ivas = 0;
    $(".siva").each(function() {
        var vivas = parseFloat($(this).val());
        ivas += Number(vivas);
    });
    var dest = 0;
    $(".cdescuento").each(function() {
        var destc = parseFloat($(this).val());
        dest += Number(destc);
    });


    var subtotalc = parseFloat(totales);
        subtotalc = subtotalc.toFixed(2);
        //ivas=ivas.toFixed(2);

        totales=totales.toFixed(2);
        ivas = parseFloat(ivas);
        ivas =ivas.toFixed(2);

    var Subtotalinfo=parseFloat(subtotalc);
    var subtotalc=parseFloat(subtotalc);

    /*if ($('#risr').is(':checked')) {
        var v_isr=Subtotalinfo*0.1;
            v_isr =v_isr.toFixed(2);
            $('#isr').val(v_isr);
            totales=totales-v_isr;
            totales=totales.toFixed(2);
    }else{
        var v_isr=0;
        $('#isr').val(0.00);
    }
    if ($('#riva').is(':checked')) {
        var v_riva=Subtotalinfo*0.106666;
            v_riva =v_riva.toFixed(2);
            $('#ivaretenido').val(v_riva);
            totales=totales-v_riva;
            totales=totales.toFixed(2);
    }else{
        $('#ivaretenido').val(0.00);
    }
    if ($('#5almillar').is(':checked')) {
        var v_5millar=(Subtotalinfo/1000)*5;
            v_5millar =v_5millar.toFixed(2);
            $('#5almillarval').val(v_5millar);
            totales=totales-v_5millar;
            totales=totales.toFixed(2);
    }else{
        $('#5almillarval').val(0.00);
    }
    if ($('#aplicaout').is(':checked')) {
        var aplicaout = Subtotalinfo*0.06;
            aplicaout = aplicaout.toFixed(2);
        
            $('#outsourcing').val(aplicaout);
            totales=totales-aplicaout;
            totales=totales.toFixed(2);
    }else{
        $('#outsourcing').val(0.00);
    }*/

    totales=totales-parseFloat(dest)+parseFloat(ivas);
    totales=totales.toFixed(2);
    $('#Subtotalinfo').val(Subtotalinfo);

    $('#Subtotal').val(subtotalc);
    $('#descuentof').val(dest);
    $('#iva').val(ivas);
    $('#total').val(totales);

}
function obtenerrfc(cliente){
    //console.log("cliente: "+cliente);
	$.ajax({
        type:'POST',
        url: base_url+"Ventas/obtenerrfc",
        data: {
        	id:cliente
        },
        success: function (data){
            //console.log("data: "+data);
        	$('#rfc').val(data).change(); 
            //$('#rfc').trigger("chosen:updated");
        }
    });
}
function alertfunction(titlea,contenta){
    $.alert({boxWidth: '30%',useBootstrap: false,title: titlea,theme: 'bootstrap',content: contenta});  
}
function detalle(id,tipo){
    if (tipo==0) {
        window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
    }else{
        window.open(base_url+"Prefactura/viewc/"+id, "Prefactura", "width=780, height=612");
    }
}
function detallep(id){
    window.open(base_url+"PolizasCreadas/view/"+id, "Prefactura", "width=780, height=612");
}

function calculardescuento(idrow){
    var costo = $('.precio_'+idrow).val();
    var descuento = $('.cdescuento_'+idrow).val();
    var rowtotal =parseFloat(costo)-parseFloat(descuento);
    var ivaif = $('.cdescuento_'+idrow).data('diva');
    if (ivaif==1) {
        var siva =rowtotal*0.16;
        if(trunquearredondear==0){
            siva = siva.toFixed(4);
        }else{
            siva=Math.floor(siva * 100) / 100;
        }
    }else{
        var siva =0.00;
    }
    $('.tiva_'+idrow).val(siva);
    var totalg = parseFloat(rowtotal)+parseFloat(siva);
    $('.csubtotal_'+idrow).val(totalg);
    calculartotales();
}

function retimbrar(idfactura,id_venta){
    $.ajax({
        type:'POST',
        url: base_url+"Ventas/retimbrar",
        data: {
            factura:idfactura, id_venta: id_venta
        },
        success:function(response){  
            console.log(response);
            $.unblockUI();
            var array = $.parseJSON(response);
            if (array.resultado=='error') {
                swal({    title: array.CodigoRespuesta+"!",
                              text: array.MensajeError,
                              type: "warning",
                              showCancelButton: false
                 });
            }else{
                swal("Éxito!", "Se ha creado la factura", "success");
            }

        }
    });       
}

function verPrefactura(){
    var form =$('#validateSubmitForm');
    var valid =form.valid();
    //if (valid) {
        //$( ".registrofac" ).prop( "disabled", true );
        var datos = form.serialize();
        var productos = $("#table_conceptos tbody > tr");
         //==============================================
        var DATAa  = [];
        productos.each(function(){         
            item = {};                    
            item ["Cantidad"]   = $(this).find("input[id*='cantidad']").val();
            item ["Unidad"]  = $(this).find("select[id*='unidadsat']").val();
            console.log($(this).find("select[id*='unidadsat']").val());
            if ($(this).find("select[id*='unidadsat']").val()==null || $(this).find("select[id*='unidadsat']").val()=='') {
                blockearfact=0;
            }
            item ["servicioId"]  = $(this).find("select[id*='conseptosat']").val();
            console.log($(this).find("select[id*='conseptosat']").val());
            if ($(this).find("select[id*='conseptosat']").val()==null || $(this).find("select[id*='conseptosat']").val()=='') {
                blockearfact=0;
            }
            item ["Descripcion"]  = $(this).find("select[id*='conseptosat']").text();
            item ["Descripcion2"]  = $(this).find("input[id*='descripcion']").val();
            item ["Cu"]  = $(this).find("input[id*='precio']").val();
            item ["descuento"]  = $(this).find("input[id*='descuento']").val();
            item ["Importe"]  = $(this).find("input[id*='subtotal']").val();
            item ["iva"]  = $(this).find("input[id*='tiva']").val();
            DATAa.push(item);
        });
        INFOa  = new FormData();
        aInfoa   = JSON.stringify(DATAa);
        var Subtotal=$('#Subtotal').val();
        var iva=$('#iva').val();

        var total=$('#total').val();               
        datos=datos+'&id_venta'+$("#id_venta").val()+'&subtotal='+Subtotal+'&iva='+iva+'&total='+total+'&conceptos='+aInfoa;
        if($("#uso_cfdi option:selected").val()!=""){
            window.open(base_url+"Ventas/prefactura?"+datos, "Prefactura", "width=780, height=612");
        }else{
            swal("Error!", "Seleccione uso de CFDI", "warning");
        }
    //}
}
function verficartiporfc(){
    var rfc = $('#rfc').val();
    if(rfc=='XAXX010101000'){
        var html='<div class="col-md-4">\
                    <label>Periodicidad</label>\
                    <select class="form-control" name="pg_periodicidad" id="pg_periodicidad" onchange="v_periocidad()">\
                      <option value="01">01 Diario</option>\
                      <option value="02">02 Semanal</option>\
                      <option value="03">03 Quincenal</option>\
                      <option value="04">04 Mensual</option>\
                      <option value="05">05 Bimestral</option>\
                    </select>\
                  </div>\
                  <div class="col-md-4">\
                    <label>Mes</label>\
                    <select class="form-control" name="pg_meses" id="pg_meses">\
                      <option value="01" class="select_no_bimestral">01 Enero</option>\
                      <option value="02" class="select_no_bimestral">02 Febrero</option>\
                      <option value="03" class="select_no_bimestral">03 Marzo</option>\
                      <option value="04" class="select_no_bimestral">04 Abril</option>\
                      <option value="05" class="select_no_bimestral">05 Mayo</option>\
                      <option value="06" class="select_no_bimestral">06 Junio</option>\
                      <option value="07" class="select_no_bimestral">07 Julio</option>\
                      <option value="08" class="select_no_bimestral">08 Agosto</option>\
                      <option value="09" class="select_no_bimestral">09 Septiembre</option>\
                      <option value="10" class="select_no_bimestral">10 Octubre</option>\
                      <option value="11" class="select_no_bimestral">11 Noviembre</option>\
                      <option value="12" class="select_no_bimestral">12 Diciembre</option>\
                      <option value="13" class="select_bimestral">13 Enero-Febrero</option>\
                      <option value="14" class="select_bimestral">14 Marzo-Abril</option>\
                      <option value="15" class="select_bimestral">15 Mayo-Junio</option>\
                      <option value="16" class="select_bimestral">16 Julio-Agosto</option>\
                      <option value="17" class="select_bimestral">17 Septiembre-Octubre</option>\
                      <option value="18" class="select_bimestral">18 Noviembre-Diciembre</option>\
                    </select>\
                  </div>\
                  <div class="col-md-4">\
                    <label>Año</label>\
                    <input type="number" name="pg_anio" id="pg_anio" class="form-control" readonly>\
                  </div>';
        $('.agregardatospublicogeneral').html(html);
        setTimeout(function(){ 
            var mesactual = $('#mesactual').val();
            var anioactual = $('#anioactual').val();
            console.log(mesactual);
            console.log(anioactual);
            $('#pg_meses').val(mesactual);
            $('#pg_anio').val(anioactual);
            $('.select_bimestral').hide('show');
        }, 1000);
    }else{
        $('.agregardatospublicogeneral').html('');
    }
}
function v_periocidad(){
    var mesactual = $('#mesactual').val();
    var pg_periodicidad = $('#pg_periodicidad').val();
    if(pg_periodicidad=='05'){
        $('.select_no_bimestral').hide('show');
        $('.select_bimestral').show('show');
        if(mesactual=='01' || mesactual=='02'){
            var mesactual_n=13;
        }
        if(mesactual=='03' || mesactual=='04'){
            var mesactual_n=14;
        }
        if(mesactual=='05' || mesactual=='06'){
            var mesactual_n=15;
        }
        if(mesactual=='07' || mesactual=='08'){
            var mesactual_n=16;
        }
        if(mesactual=='09' || mesactual=='10'){
            var mesactual_n=17;
        }
        if(mesactual=='11' || mesactual=='12'){
            var mesactual_n=18;
        }
        $('#pg_meses').val(mesactual_n);
    }else{
        $('#pg_meses').val(mesactual);
        $('.select_no_bimestral').show('show');
        $('.select_bimestral').hide('show');
    }
    
}