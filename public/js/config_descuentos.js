var base_url = $('#base_url').val();
$(document).ready(function(){
    $("#guardar").on("click",function(){
    	guardar();
    });
});

function guardar(){
	var form_register = $('#formconfig');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            dia_enfermo:{
              required: true
            },
            dia_desc:{
              required: true
            },
            dia_suspende:{
              required: true
            },
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);
        },
        highlight: function (element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });

    var valid = $("#formconfig").valid();
    if(valid) {
        $.ajax({
            type:'POST',
            url: base_url+'Vacaciones/insertarConfigDesc',
            data: form_register.serialize(),
            async: false,
            beforeSend: function(){
                $("#guardar").attr("disabled",true);
            },
            success:function(data){
                toastr.success('Hecho!', 'Guardado Correctamente');
                setTimeout(function(){ 
                    location.reload(); 
                }, 1500); 
            }
        });
    }
    
}
