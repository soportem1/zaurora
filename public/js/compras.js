$(document).ready(function() {	
	$('#cproveedor').select2({
		width: 'resolve',
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un proveedor',
	  	ajax: {
	    	url: 'Compras/searchpro',
	    	dataType: "json",
	    	data: function (params) {
	      	var query = {
	        	search: params.term,
	        	type: 'public'
	      	}
	      	return query;
	    },
	    processResults: function(data){
	    	var itemspro = [];
	    	data.forEach(function(element) {
                itemspro.push({
                    id: element.id_proveedor,
                    text: element.razon_social
                });
            });
            return {
                results: itemspro
            };	    	
	    },  
	  }
	});

	$('#vproducto').select2({
		width: 'resolve',
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un Producto',
	  	ajax: {
	    	url: 'Ventas/searchproducto',
	    	dataType: "json",
	    	data: function (params) {
	      	var query = {
	        	search: params.term,
	        	type: 'public'
	      	}
	      	return query;
	    },
	    processResults: function(data){
	    	var clientes=data;
	    	var itemscli = [];
	    	data.forEach(function(element) {
                itemscli.push({
                    id: element.productoid,
                    text: element.codigo+' / '+element.nombre,
                    precio: element.preciocompra,
										cantidad: element.preciocompra,
                });
            });
            return {
                results: itemscli
            };	    	
	    },  
	  }
	}).on('select2:select', function (e) {
	    var data = e.params.data;
	    console.log(data.precio);
	    $('#cprecio').val(data.precio);
	});

	



	$('#ingresaventa').click(function(event) {
            
                $.ajax({
                    type:'POST',
                    url: 'Compras/ingresarcompra',
                    data: {
                    	uss: $('#ssessius').val(),
                        prov: $('#cproveedor').val(),
                        desc: $('#mdescuento').val(),
                        total: $('#vtotal').val()
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                    	var idcompra=data;
                    	var DATA  = [];
            			var TABLA   = $("#productosv tbody > tr");
            				TABLA.each(function(){         
				                item = {};
				                item ["idcompra"] = idcompra;
				                item ["producto"]   = $(this).find("input[id*='vsproid']").val();
				                item ["cantidad"]  = $(this).find("input[id*='vscanti']").val();
												item ["cantIni"] = $(this).find("input[id*='cantIni']").val();
				                item ["precio"]  = $(this).find("input[id*='vsprecio']").val();
				                DATA.push(item);
				            });
            				INFO  = new FormData();
            				aInfo   = JSON.stringify(DATA);
            				INFO.append('data', aInfo);
				            $.ajax({
				                data: INFO,
				                type: 'POST',
				                url : 'Compras/ingresarcomprapro',
				                processData: false, 
				                contentType: false,
				                success: function(data){
				                }
				            });
				            toastr.success('Hecho!', 'Guardado Correctamente');
				            limpiar();
				            $('#vcantidad').val('')
							$('#vproducto').html('');
							$('#cprecio').val('');
						    $("#vproducto").val(0).change();
						    $('#vtotal').val(0);

                      




                    }
                });
            
        });
	


});

function addproducto(){
	if ($('#vcantidad').val()>0) {
		$.ajax({
	        type:'POST',
	        url: 'Compras/addproducto',
	        data: {
	            cant: $('#vcantidad').val(),
	            prod: $('#vproducto').val(),
	            prec: $('#cprecio').val()
	            },
	            async: false,
	            statusCode:{
	                404: function(data){
	                    toastr.error('Error!', 'No Se encuentra el archivo');
	                },
	                500: function(){
	                    toastr.error('Error', '500');
	                }
	            },
	            success:function(data){
	                var array = $.parseJSON(data);
	                console.log(array);
	                var selecttotal = Number((parseFloat(array.cant)*parseFloat(array.precio)).toFixed(2));
	                var producto='<tr class="producto_'+array.id+'">                                        <td><input type="hidden" name="vsproid" id="vsproid" value="'+array.id+'">'+array.codigo+'</td>                                        <td><input type="number" name="vscanti" id="vscanti" value="'+array.cant+'" readonly style="background: transparent;border: 0px;width: 80px;"> <input type="hidden" name="cantInicial" id="cantInicial" value="0" readonly > </td>                                        <td>'+array.producto+'</td>                                        <td>$ <input type="text" name="vsprecio" id="vsprecio" value="'+array.precio+'" readonly style="background: transparent;border: 0px;width: 100px;"></td>                                        <td>$ <input type="text" class="vstotal" name="vstotal" id="vstotal" value="'+selecttotal+'" readonly style="background: transparent;border: 0px;width: 100px;"></td>                                        <td>                                            <a class="danger" data-original-title="Eliminar" title="Eliminar" onclick="deletepro('+array.id+')">                                                <i class="ft-trash font-medium-3"></i>                                            </a>                                        </td>                                    </tr>';
	                $('#class_productos').append(producto);
									checkCant(array.id);
	            }
	        });
		$('#vcantidad').val('')
		$('#vproducto').html('');
		$('#cprecio').val('');
	    $("#vproducto").val(0).change();
	}





	
    calculartotal();
}
function calculartotal(){
	var addtp = 0;
	$(".vstotal").each(function() {
        var vstotal = $(this).val();
        addtp += Number(vstotal);
    });
    
    $('#vtotal').val(Number(addtp).toFixed(2));
}
function limpiar(){
	$('#class_productos').html('');
}
function deletepro(id){
	$('.producto_'+id).remove();
}

function checkCant(id = 0) {
	console.log("Check: "+id);
	
	$.ajax({
		type: "POST",
		url: "Compras/checkCantproducto",
		data: {
			id: id
		},
		async: false,
		statusCode: {
			404: function (data) {
				toastr.error("Error!", "No Se encuentra el archivo");
			},
			500: function () {
				toastr.error("Error", "500");
			},
		},
		success: function (data) {
			console.log(data);
			//if(data > 0){
				$('.producto_'+id+' #cantInicial').val(data)
			//}
		},
	});
	
}