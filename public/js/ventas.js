var base_url = $('#base_url').val();
var aprobado = 0;
$(document).ready(function() {	
	$('#vvendedores').select2({
		width: 'resolve',
		placeholder: 'Seleccionar un vendedor',
		allowClear: true
	});
	$("#vvendedores").val(0).change();
	$('#vcliente').select2({
		width: 'resolve',
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un cliente',
	  	ajax: {
	    	url: 'Ventas/searchcli',
	    	dataType: "json",
	    	data: function (params) {
	      	var query = {
	        	search: params.term,
	        	type: 'public'
	      	}
	      	return query;
	    },
	    processResults: function(data){
	    	var clientes=data;
	    	var itemscli = [];
	    	data.forEach(function(element) {
					//console.log(element);
                itemscli.push({
                    id: element.ClientesId,
                    text: element.Nom+' - '+element.telefonoc
                });
            });
            return {
                results: itemscli
            };	    	
	    },  
	  }
	}).on('select2:select', function (e) {
		fillOutCard();
});

	$('#vproducto').select2({
		width: 'resolve',
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un Producto',
	  	ajax: {
	    	url: 'Ventas/searchproducto',
	    	dataType: "json",
	    	data: function (params) {
	      	var query = {
	        	search: params.term,
	        	type: 'public'
	      	}
	      	return query;
	    },
	    processResults: function(data){
	    	var clientes=data;
	    	var itemscli = [];
	    	data.forEach(function(element) {
                itemscli.push({
                    id: element.productoid,
                    text: element.codigo+' / '+element.nombre
                });
            });
            return {
                results: itemscli
            };	    	
	    },  
	  }
	}).on('select2:select', function (e) {
	    //var data = e.params.data;
	    //console.log(data);
	    addproducto();
	});
	$('#vproducto2').focus();
	$('#ingresaventa').click(function(event) {
			addventas();
  
		});/*
    $('#btnabrirt').click(function(){
			$.ajax({
				type:'POST',
				url:'Ventas/abrirturno',
				data:{
					cantidad: $('#cantidadt').val(),
					nombre: $('#nombredelturno').val()
				},
				async:false,
				success:function(data){
					toastr.success('Turno Abierto','Hecho!');
					$('#modalturno').modal('hide');
					
				}
			});
		
	});*/
	$( "#vingreso" ).keypress(function(e) {
	  	if(e.which == 13) {
       		addventas();
    	}
	});
	$("body").keypress(function(e) {
		//console.log(e);
	  	if(e.which == 10 && e.ctrlKey == true) {
	  		$('.infochecador').html('');
       		$('#modalchecador').modal();
       		$('#inputchecador').focus();
    	}
	});
	$( "#vingresot" ).keypress(function(e) {
	  	if(e.which == 13) {
       		addventas();
    	}
	});
	$('.modalchecador').click(function(event) {
		$('#modalchecador').modal();
		$("#vproducto2").attr('type','text');
	});

	$('#mdescuento').click(function(event) {
		if(aprobado==0){
			$('#modalPass').modal();
			$('#mdescuento').prop('disabled',true);
			$("#passAD").attr('type','password');
		}
		else{
			$('#mdescuento').attr('disabled',false);
		}
		
	});
	if($('#modalPass').is(':hidden')){ //saber si el modal esta oculto
		//console.log('modal dismiss');
		//$('#mdescuento').attr('disabled',false);
	}

	$('#envPassAD').click(function(event) {
		var passAut = $('#passAD').val();
        $.ajax({
			type:'POST',
			url:base_url+'Login/verificaPass',
			data:{
				passAut: passAut,
				usuario: 'admin'
			},
			async:false,
			success:function(data){
				//console.log("data verifica: "+data);
				if(data==1){
					toastr.success('Contraseña correcta', 'Autorización aprobada');
					$("#modalPass").modal('hide');
					$('#mdescuento').attr('disabled',false);
					aprobado = 1;
					$('#passAD').val('');
				}
				else{
					toastr.warning('Contraseña incorrecta', 'Autorización no aprobada');
				}

			}
		});
	});

	if(aprobado==1){
		$('#mdescuento').attr('disabled',false);	
	}

	$('#inputchecador').keypress(function (e) {
	    if(e.which ==13){
	        $.ajax({
				type:'POST',
				url:base_url+'Ventas/checador',
				data:{
					codigo: $('#inputchecador').val()
				},
				async:false,
				success:function(data){
					$('.infochecador').html('');
					$('.infochecador').html(data);
					$('#inputchecador').val('');
					$('#inputchecador').focus();
					setTimeout(function(){ 
						$('#inputchecador').val('');
					}, 10000);

					
				}
			});
	    }
	});
	$( "#vproducto2" ).keypress(function(e) {
        if (e.which==13) {
            buscarcodigobar();
        }
    });
    $('.btn_modal_gastos').click(function(event) {
    	$('#modalgastos').modal();
    });
    $('.guardargasto').click(function(event) {
    	$.ajax({
            type:'POST',
            url: base_url+'Gastos/insertarGasto',
            data: {
                id:0,
                concepto:$('#concepto').val(),
                monto:$('#monto').val()
            },
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
            	$("#concepto").val('');
            	$("#monto").val('');
                toastr.success('Hecho!', 'Guardado Correctamente');
            }
        });
    });

    $('#generacodigo').click(function(event) {
		var d = new Date();
		var dia=d.getDate();//1 31
		var dias=d.getDay();//0 6
		var mes = d.getMonth();//0 11
		var yy = d.getFullYear();//9999
		var hr = d.getHours();//0 24
		var min = d.getMinutes();//0 59
		var seg = d.getSeconds();//0 59
		var yyy = 18;
		var ram = Math.floor((Math.random() * 11) + 1);
		const  generateRandomString = (num) => {
            const characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            let result1= Math.random().toString(36).substring(0,num);       
            return result1;
        }
		var codigo=seg+''+min+''+hr+''+yyy+''+ram+''+mes+''+dia+''+dias+generateRandomString(7).toUpperCase();
		codigo=codigo.replace(".", "V");
		$('#codigo').val(codigo);
  	});
});

function addproducto(){
	if ($('#vcantidad').val()>0) {
		$.ajax({
	        type:'POST',
	        url: 'Ventas/addproducto',
	        data: {
	            cant: $('#vcantidad').val(),
	            prod: $('#vproducto').val(),
	            vendedor: $('#vvendedores option:selected').val(),
	            vendedor_name: $('#vvendedores option:selected').text()
	        },
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
            	//console.log(data);
                var array = $.parseJSON(data);
				$('#class_productos').html(array.datos);
				if(array.validar==1){
                    //toastr.error('Error!', 'Producto sin existencias');
                    swal("¡Atención!","Producto sin existencias","error");
				}
            }
        });
		$('#vcantidad').val(1);
		$('#vproducto').html('');
		$('#vproducto2').val('');
	    $("#vproducto").val(0).change();
	    $('#vproducto2').focus();
	}
    calculartotal();
}
function addproductocheck(id){
	if ($('#vcantidad').val()>0) {
		$.ajax({
	        type:'POST',
	        url: 'Ventas/addproducto',
	        data: {
	            cant: 1,
	            prod: id
	            },
	            async: false,
	            statusCode:{
	                404: function(data){
	                    toastr.error('Error!', 'No Se encuentra el archivo');
	                },
	                500: function(){
	                    toastr.error('Error', '500');
	                }
	            },
	            success:function(data){
	            	//console.log(data);
	                var array = $.parseJSON(data);
					$('#class_productos').html(array.datos);
	            }
	        });
		$('#vcantidad').val(1);
		$('#vproducto').html('');
	    $("#vproducto").val(0).change();
	    $('#vproducto').select2('open').on('focus');
	}
    calculartotal();
    $('#modalchecador').modal('hide');
}
function calculartotal(){
	var addtp = 0;
	$(".vstotal").each(function() {
        var vstotal = $(this).val();
        addtp += Number(vstotal);
    });
    //var descuento1=100/$('#mdescuento').val();
    $('#vsbtotal').val(addtp);
    var descuento=addtp*$('#mdescuento').val();
    var decimales = descuento - Math.floor(descuento)
    //console.log("decimales: "+decimales);
    /*if(decimales==0.5)
    	descuento= descuento;*/
    if(decimales==0.5)
    	descuento=Math.round(descuento); //convertirlo a menor
    else if(decimales>0.50) //me restan mas en decimal para llegar el precio en entero
    	descuento=Math.ceil(descuento); //convertirlo a mayor
    else if(decimales<=0.49)
    	descuento=Math.round(descuento); //convertirlo a menor

    $('#cantdescuento').val(descuento);

    var total=parseFloat(addtp)-parseFloat(descuento);
    $('#vtotal').val(Number(total).toFixed(2));

		fillOutCard();
    ingreso();
}

function ingreso(){
	var ingresoe= $('#vingreso').val();
	var ingresot= $('#vingresot').val();
	var ingreso=parseFloat(ingresoe)+parseFloat(ingresot);
	var totals=$('#vtotal').val();

	var cambio = parseFloat(ingreso)-parseFloat(totals);
	if (cambio<0) {
		cambio=0;
	}
	$('#vcambio').val(Number(cambio).toFixed(2));

	calcRest();
}

function limpiar(){
	$('#class_productos').html('');
	$.ajax({
	        type:'POST',
	        url: 'Ventas/productoclear',
	        async: false,
	        statusCode:{
	            404: function(data){
	            	toastr.error('Error!', 'No Se encuentra el archivo');
	        	},
	        	500: function(){
	                toastr.error('Error', '500');
	            }
	        },
	        success:function(data){
	            	
	        }
	});
}
function deletepro(id){
	
	$.ajax({
        type:'POST',
        url: 'Ventas/deleteproducto',
        data: {
            idd: id
            },
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
							$('.producto_'+id).remove();
							calculartotal()
            }
        });
}


function addventas() {
	// Revision pago >= total ----------------------------
	let sel = $('#mpago').find("option:selected").val();
	let ingreT = parseFloat($('#vingresot').val() != '' ? $('#vingresot').val() : 0);
	let ingreE = parseFloat($('#vingreso').val() != '' ? $('#vingreso').val() : 0);
	let ingreP = parseFloat($('#vingresop').val() != '' ? $('#vingresop').val() : 0);
	let currentP = parseFloat($('#currentP').val() != '' ? $('#currentP').val() : 0);
	let totalC = parseFloat($('#vtotal').val() != '' ? $('#vtotal').val() : 0);

	if (sel == 1 && ingreT != 0) {
		toastr.error('Error', 'El tipo de pago no es correcto.');
		return;
	} else if (sel == 1 && ingreE < totalC) {
		toastr.error('Error', 'El pago es menor al costo.');
		return;
	}

	if ((sel == 2 || sel == 3) && ingreE != 0) {
		toastr.error('Error', 'El tipo de pago no es correcto.');
		return;
	} else if ((sel == 2 || sel == 3) && ingreT < totalC) {
		toastr.error('Error', 'El pago es menor al costo.');
		return;
	} else if ((sel == 2 || sel == 3) && ingreT > totalC) {
		toastr.error('Error', 'El pago es mayor al costo.');
		return;
	}


	if (sel == 4) {
		const inputConDatos = [ingreE, ingreT, ingreP].filter(inputs => inputs !== 0).length;

		if (inputConDatos >= 2) {
			if ((ingreE + ingreT + ingreP) < totalC) {
				toastr.error('Error', 'El pago es menor al costo.');
				return;
			}

			/*
			if ((ingreT + ingreP) > totalC) {
				toastr.error('Error', 'El pago es mayor al costo.');
				return;
			}
			*/

			if ((ingreE + ingreT + ingreP) > totalC) {
				toastr.error('Error', 'El pago es mayor al costo.');
				return;
			}

		} else {
			toastr.error('Error', 'El tipo de pago no es correcto.');
			return;
		}
	}


	if (sel == 5 && (ingreE != 0 || ingreT != 0)) {
		toastr.error('Error', 'El tipo de pago no es correcto.');
		return;
	} else if (sel == 5 && ingreP < totalC) {
		toastr.error('Error', 'El pago es menor al costo.');
		return;
	}else if (sel == 5 && ingreP > currentP) {
		toastr.error('Error', 'Los puntos ingresados superan a los poseidos por el cliente.');
		return;
	}

	//-----------------------------------

	$('#vproducto2').focus();

	var vend = false;
	var cli = false;

	if ($('#vvendedores option:selected').val() != undefined) {
		vend = false;
	} else {
		toastr.error('Error', 'Elige un vendedor');
		vend = true;
	}

	if ($('#vcliente option:selected').val() != undefined) {
		cli = false;
	} else {
		toastr.error('Error', 'Elige un cliente');
		cli = true;
	}

	if (vend == false && cli == false) {
		if ($('#vcambio').val() >= 0) {
			var vingresop = $('#vingresop').val() == '' ? 0 : $('#vingresop').val();
			var vingresot = $('#vingresot').val() == '' ? 0 : $('#vingresot').val();
			var vingreso = $('#vingreso').val() == '' ? 0 : $('#vingreso').val();
			var vendedor = $('#vvendedores').val() == null ? 0 : $('#vvendedores').val();

			var vtotal = $('#vtotal').val();
			var vcambio = $('#vcambio').val();
			//var efec = $('#vingreso').val();
			//var tar = $('#vingresot').val();
			//var punt = $('#vingresop').val();
			//var id_prod = $("#vsproid").val();
			var factura = 0;
			var checkfact = document.getElementById("factura").checked;
			if (checkfact == true) {
				factura = 1;
			} else {
				factura = 0;
			}

			//console.log("cambio js: "+vcambio);
			var efectivo = parseFloat(vtotal) - parseFloat(vingresot);

			//console.log("efectivo?: " + efectivo);

			var vpagacon = parseFloat(vingreso) + parseFloat(vingresot) + parseFloat(vingresop);

			//console.log("paga con: " + vpagacon);

			if (vtotal > 0) {
				$.ajax({
					type: 'POST',
					url: 'Ventas/ingresarventa',
					data: {
						vend: vendedor,
						uss: $('#ssessius').val(),
						cli: $('#vcliente').val(),
						mpago: $('#mpago').val(),
						desc: $('#mdescuento').val(),
						descu: $('#cantdescuento').val(),
						sbtotal: $('#vsbtotal').val(),
						total: vtotal,
						//efectivo: efectivo,
						efectivo: vingreso,
						tarjeta: vingresot,
						puntos: vingresop,
						factura: factura
					},
					async: false,
					statusCode: {
						404: function (data) {
							toastr.error('Error!', 'No Se encuentra el archivo');
						},
						500: function () {
							toastr.error('Error', '500');
						}
					},
					beforeSend: function () {
						$("#ingresaventa").attr("disabled", true);
					},
					success: function (data) {
						console.log("data: "+data);
						if(data == "Error1"){
							toastr.error('Error', 'Los puntos ingresados superan a los poseidos por el cliente.');
							showPuntos($('#vcliente').val());
							return;
						}

						if(data == "Error2"){
							toastr.error('Error', 'Los puntos ingresados superan a los poseidos por el cliente.');
							return;
						}

						var idventa = data;
						var DATA = [];
						var TABLA = $("#productosv tbody > tr");
						var precio_venta;
						var totalV;

						TABLA.each(function () {
							item = {};
							item["idventa"] = idventa;
							item["producto"] = $(this).find("input[id*='vsproid']").val();
							item["id_vendedor"] = $(this).find("input[id*='id_vendedor']").val();
							item["cantidad"] = $(this).find("input[id*='vscanti']").val();
							item["cantIni"] = $(this).find("input[id*='cantIni']").val();
							item["precio"] = $(this).find("input[id*='vsprecio']").val();
							item["descuento"] = $(this).find("input[id*='vsdesc']").val();
							DATA.push(item);
						});
						INFO = new FormData();
						aInfo = JSON.stringify(DATA);
						INFO.append('data', aInfo);
						$.ajax({
							data: INFO,
							type: 'POST',
							url: 'Ventas/ingresarventapro',
							processData: false,
							contentType: false,
							async: false,
							statusCode: {
								404: function (data) {
									toastr.error('Error!', 'No Se encuentra el archivo');
								},
								500: function () {
									toastr.error('Error', '500');
								}
							},
							success: function (data) {}
						});
						checkprint = document.getElementById("checkimprimir").checked;
						checkfact = document.getElementById("factura").checked;

						if (checkprint == true) {
							$("#iframeri").modal();
							$('#iframereporte').html('<iframe src="Ticket?id=' + idventa + '&vcambio=' + vcambio + '&vpagacon=' + vpagacon + '"></iframe>');

						} else {
							toastr.success('Venta Realizada', 'Hecho!');
						}
						if (checkfact == true) {
							//window.open('Ventas/facturar/'+idventa, "_blank");	
							setTimeout(function () {
								window.location.href = "Ventas/facturar/" + idventa + ""
							}, 1000);
						}

						limpiar();
						$("#vcliente").val("").change();
						$("#vvendedores").val("").change();
						$('#vtotal').val(0);
						$('#vcambio').val(0);
						$('#vingreso').val(0);
						$("#vingresot").val(0);
						aprobado = 0;
						$('#mdescuento').val("0")
						$('#mdescuento').attr('value', '0');
						$('#vproducto2').focus();
						setInterval(function () {
							window.location.href = base_url + "Ventas";
						}, 10000);
					}
				});
			} else
				toastr.error('Ingresa un producto a vender', 'Error!');
		} else {
			toastr.error('No se puede realizar la venta debido a que no ha ingresado el saldo para liquidar la venta', 'Error!');
		}
	}
	$('#vproducto2').focus();
}

/*
function addventas(){
	$('#vproducto2').focus();
	var vend=false;
	var cli=false;
	if($('#vvendedores option:selected').val()!=undefined){
		vend=false;
	}else{
		toastr.error('Error', 'Elige un vendedor');
		vend=true;
	}
	if($('#vcliente option:selected').val()!=undefined){
		cli=false;
	}else{
		toastr.error('Error', 'Elige un cliente');
		cli=true;
	}
	if(vend==false && cli==false){
		if ($('#vcambio').val()>=0 ) {
			var vingresot =$('#vingresot').val()==''?0:$('#vingresot').val();
			var vendedor = $('#vvendedores').val()==null?0:$('#vvendedores').val();
			var vtotal = $('#vtotal').val();
			var vcambio = $('#vcambio').val();
			var efec = $('#vingreso').val();
			var tar = $('#vingresot').val();
			var id_prod = $("#vsproid").val();
			var factura=0;
	        var checkfact = document.getElementById("factura").checked;
	        if (checkfact==true) {   
	            factura=1;      
	        }else{
	            factura=0;
	        }

			//console.log("cambio js: "+vcambio);
			var efectivo = parseFloat(vtotal)-parseFloat(vingresot);
			var vpagacon = parseFloat(efec)+parseFloat(tar);
			console.log("paga con: " +vpagacon);
			if(vtotal>0){
		        $.ajax({
		            type:'POST',
		            url: 'Ventas/ingresarventa',
		            data: {
		            	vend:vendedor,
		            	uss: $('#ssessius').val(),
		                cli: $('#vcliente').val(),
		                mpago: $('#mpago').val(),
		                desc: $('#mdescuento').val(),
		                descu: $('#cantdescuento').val(),
		                sbtotal:$('#vsbtotal').val(),
		                total: vtotal,
		                efectivo: efectivo,
		                tarjeta: vingresot,
		                factura:factura
		            },
		            async: false,
		            statusCode:{
		                404: function(data){
		                    toastr.error('Error!', 'No Se encuentra el archivo');
		                },
		                500: function(){
		                    toastr.error('Error', '500');
		                }
		            },
		            beforeSend: function() {
			        	$("#ingresaventa").attr("disabled", true);
			    	},
		            success:function(data){
		            	var idventa=data;
		            	var DATA  = [];
		    			var TABLA   = $("#productosv tbody > tr");
		    			var precio_venta;
		    			var totalV;
		    			
		    				TABLA.each(function(){         
				                item = {};
				                item ["idventa"] = idventa;
				                item ["producto"]   = $(this).find("input[id*='vsproid']").val();
				                item ["id_vendedor"]   = $(this).find("input[id*='id_vendedor']").val();
				                item ["cantidad"]  = $(this).find("input[id*='vscanti']").val();
				                item ["precio"]  = $(this).find("input[id*='vsprecio']").val();
				                item ["descuento"]  = $(this).find("input[id*='vsdesc']").val();
				                DATA.push(item);  
				            });
		    				INFO  = new FormData();
		    				aInfo   = JSON.stringify(DATA);
		    				INFO.append('data', aInfo);
				            $.ajax({
				                data: INFO,
				                type: 'POST',
				                url : 'Ventas/ingresarventapro',
				                processData: false, 
				                contentType: false,
				                async: false,
			                    statusCode:{
			                        404: function(data){
			                            toastr.error('Error!', 'No Se encuentra el archivo');
			                        },
			                        500: function(){
			                            toastr.error('Error', '500');
			                        }
			                    },
				                success: function(data){
				                }
				            });
				            checkprint = document.getElementById("checkimprimir").checked;
				            checkfact = document.getElementById("factura").checked;

							if (checkprint==true) {
								$("#iframeri").modal();
								$('#iframereporte').html('<iframe src="Ticket?id='+idventa+'&vcambio='+vcambio+'&vpagacon='+vpagacon+'"></iframe>');
								
							}else{
								toastr.success( 'Venta Realizada','Hecho!');
							}
							if (checkfact==true) {
				            	//window.open('Ventas/facturar/'+idventa, "_blank");	
								setTimeout(function(){window.location.href = "Ventas/facturar/"+idventa+""},1000);
							}
							
							limpiar();
						    $("#vcliente").val("").change();
						    $("#vvendedores").val("").change();
						    $('#vtotal').val(0);
						    $('#vcambio').val(0);
						    $('#vingreso').val(0);
						    $("#vingresot").val(0);
						    aprobado=0;
						    $('#mdescuento').val("0")
						    $('#mdescuento').attr('value','0');
						    $('#vproducto2').focus();
						    setInterval(function(){ 

	                            window.location.href = base_url+"Ventas";
	                        }, 15000);
		            }
		        });
			}else
				toastr.error('Ingresa un producto a vender','Error!');
		}else{
			toastr.error('No se puede realizar la venta debido a que no ha ingresado el saldo para liquidar la venta','Error!');
		}
	}
	$('#vproducto2').focus();
}
*/

function buscarcodigobar(){
	var barcode =$('#vproducto2').val();
	//console.log(barcode);
	$.ajax({
            type: 'POST',
            url: base_url + 'Ventas/searchproductobar',
            data: {
                prod: barcode
            },
            async: false,
            statusCode: {
                404: function(data) {
                	toastr.error('Error', 'No se encuentra archivo');
                },
                500: function() {
                	toastr.error('Error', '500');
                }
            },
            success: function(data) {
                //console.log(data);
                var array = $.parseJSON(data);
                var pro = array.pro;
                var proid = array.proid;
                var prorow = array.prorow;
                if (pro==1) {
                    if (prorow==1) {
                        productselected(proid);
                    }else{
                    	toastr.error('Error', 'Codigo duplicado');
                    }

                }else{
                	toastr.error('Error', 'No existe producto');
                }

            }
        });
}
function productselected(proid){
	if ($('#vcantidad').val()>0) {
		$.ajax({
	        type:'POST',
	        url: 'Ventas/addproducto',
	        data: {
	            cant: $('#vcantidad').val(),
	            prod: proid
	            },
	            async: false,
	            statusCode:{
	                404: function(data){
	                    toastr.error('Error!', 'No Se encuentra el archivo');
	                },
	                500: function(){
	                    toastr.error('Error', '500');
	                }
	            },
	            success:function(data){
	            	//console.log(data);
	               var array = $.parseJSON(data);
					$('#class_productos').html(array.datos);
	            }
	        });
		$('#vcantidad').val(1);
		$('#vproducto2').val('');
	    $('#vproducto2').focus();
	}
    calculartotal();
}


function fillOutCard(){

	let seleccion = $('#mpago').find("option:selected").val();
	let cliente = $('#vcliente').val();
	$("#cont_rest").hide("slow");
	//console.log("seleccion "+seleccion);

	if(seleccion == 2 || seleccion == 3){
		if($('#vtotal').val() != ''){
			$('#vingresot').val($('#vtotal').val());
			$('#vingresot').attr('readonly', false);

			$('#vingreso').val(0);
			$('#vingreso').attr('readonly', true);
		}else{
			$('#vingresot').val(0);
			$('#vingresot').attr('readonly', false);

			$('#vingreso').attr('readonly', true);
		}
		$('#vingresop').val(0);
		$('#vingresop').attr('readonly', true);
		$('#labelPuntos').text('');
		$('#currentP').val(0);
		

	}else if(seleccion == 1){
		if($('#vtotal').val() != ''){
			$('#vingreso').val($('#vtotal').val());
			$('#vingreso').attr('readonly', false);

			$('#vingresot').val(0);
			$('#vingresot').attr('readonly', true);
		}else{
			$('#vingreso').val(0);
			$('#vingreso').attr('readonly', false);

			$('#vingresot').attr('readonly', true);
		}

		$('#vingresop').val(0);
		$('#vingresop').attr('readonly', true);
		$('#labelPuntos').text('');
		$('#currentP').val(0);

	}else if(seleccion == 4){
		$('#vingresot').val(0);
		$('#vingresot').attr('readonly', false);

		$('#vingreso').val(0);
		$('#vingreso').attr('readonly', false);

		$('#vingresop').val(0);
		$('#vingresop').attr('readonly', false);
		
		$('#labelPuntos').text('');
		$('#currentP').val(0);
		
		showPuntos(cliente);

		if(cliente){
			$("#cont_rest").show("slow");
		}

	}else if(seleccion == 5){
		$('#vingresot').val(0);
		$('#vingresot').attr('readonly', true);

		$('#vingreso').val(0);
		$('#vingreso').attr('readonly', true);

		$('#vingresop').val(0);
		$('#vingresop').attr('readonly', false);

		showPuntos(cliente);
	}else if(seleccion == 6){

		$('#vingresot').val(0);
		$('#vingresot').attr('readonly', true);

		$('#vingreso').val(0);
		$('#vingreso').attr('readonly', true);

		$('#vingresop').val(0);
		$('#vingresop').attr('readonly', true);
		show_cliente_credito(cliente);
	}

	ingreso()
}

function showPuntos(id){
	let cliente = "";
	let puntos = 0

	if(id != null){
		$.ajax({
			type:'POST',
			url:'Ventas/getPuntosCliente',
			data:{
				id: id,
			},
			async:false,
			success:function(data){
				//console.log(data);
				let datos = JSON.parse(data);
				cliente = datos.cliente;
				puntos = parseFloat(datos.puntos).toFixed(2);
				if(puntos <= 0){
					$('#vingresop').attr('readonly', true);
				}
			}
		});
	}else{
		toastr.error('Error!', 'Seleccione un cliente');
		$('#mpago').val(1);
		fillOutCard();
		$('#labelPuntos').text('');
		$('#currentP').val(0);
		return;
	}

    $.confirm({
        boxWidth: '45%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: 'El cliente <b>'+cliente+'</b> cuenta con <b>$'+puntos+'</b> puntos,',//, ¿desea usarlos?
        type: 'yellow',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
							$('#labelPuntos').text('($'+Number(puntos).toFixed(2)+')');
							$('#currentP').val(puntos);
            },
            cancelar: function () 
            {
							
							if($('#mpago').find("option:selected").val() == 4){
								$('#labelPuntos').text('($'+Number(puntos).toFixed(2)+')');
								$('#currentP').val(puntos);
								$('#vingresop').val(0);
								$('#vingresop').attr('readonly', true);

							}else{
								$('#mpago').val(1);
								fillOutCard();
								$('#labelPuntos').text('');
								$('#currentP').val(0);
							}
							
            }
        }
    });


}

function show_cliente_credito(id){
	if(id != null){
	}else{
		toastr.error('Error!', 'Seleccione un cliente');
		$('#mpago').val(1);
		fillOutCard();
		$('#labelPuntos').text('');
		$('#currentP').val(0);
		return;
	}
}


function calcRest() {
	var total = $('#vtotal').val();
	var efe = $('#vingreso').val();
	var tar = $('#vingresot').val();
	var pun = $('#vingresop').val();
	var ingreso = parseFloat(pun) + parseFloat(tar) + parseFloat(efe);
	var rest = parseFloat(total) - parseFloat(ingreso);

	console.log("REST: "+rest);
	if(isNaN(rest)){
		rest = 0;
	}

	$("#rest").text("($" + Number(rest).toFixed(2) + ")");
}