var base_url = $("#base_url").val();

$(document).ready(function () {
	$("#btn_submit").click(function (event) {
		event.preventDefault();
		consultar();
	});

	$("#btn_return").click(function() {
		retornar();
	});
});

function consultar() {
	var form_register = $("#form_data");
	var error_register = $(".alert-danger", form_register);
	var success_register = $(".alert-success", form_register);
	var $validator1 = form_register.validate({
		errorElement: "div", //default input error message container
		errorClass: "vd_red", // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules: {
			num_tel: {
				required: true,
				minlength: 10,
				maxlength: 10,
				min: 1,
				number: true,
			},
		},
		messages: {
			num_tel: {
					required: " Por favor, ingresa tu número de teléfono.",
					minlength: " El número debe tener 10 dígitos.",
					maxlength: " El número debe tener 10 dígitos.",
					number: " Por favor, ingresa un número teléfonico valido.",
			},
		},

		errorPlacement: function (error, element) {
			if ( element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
				element.parent().append(error);
			} else if (element.parent().hasClass("vd_input-wrapper")) {
				error.insertAfter(element.parent());	
			} else {
				error.insertAfter(element);
			}
		},
		invalidHandler: function (event, validator) {
			//display error alert on form submit
			success_register.fadeOut(500);
			success_register.empty();
			error_register.fadeIn(500);
			scrollTo(form_register, -100);
		},
		highlight: function (element) {
			// hightlight error inputs
			$(element).addClass("vd_bd-red");
			$(element)
				.siblings(".help-inline")
				.removeClass("help-inline fa fa-check vd_green mgl-10");
		},
		unhighlight: function (element) {
			// revert the change dony by hightlight
			$(element).closest(".control-group").removeClass("error"); // set error class to the control group
		},
		success: function (label, element) {
			error_register.empty()
			label
				.addClass("valid")
				//.addClass("help-inline fa fa-check vd_green mgl-10") // mark the current input as valid and display OK icon
				.addClass("help-inline mgl-10") // mark the current input as valid and display OK icon
				.closest(".control-group")
				.removeClass("error")
				.addClass("success"); // set success class to the control group
			$(element).removeClass("vd_bd-red");
		},
	});

	var $valid = $("#form_data").valid();

	if ($valid) {
		$("#btn_submit").attr("disabled", true);

		var datos = form_register.serialize();
		//console.log("datos: " + datos);

		$.ajax({
			type: "POST",
			url: base_url + "PuntosClientes/searchCliente",
			data: datos,
			statusCode: {
				404: function (data) {
					swal("Error!", "No Se encuentra el archivo", "error");
				},
				500: function () {
					swal("Error!", "500", "error");
				},
			},
			success: function (data) {
				//console.log("data" + data);
				if (data == 1) {
					swal({
						title: "Éxito!",
						text: "Cliente válido!",
						type: "success",
						showConfirmButton: false,
						timer: 1600 
					});
					setTimeout(function () {
						redireccionar(1, $("#num_tel").val());
					}, 1500);

				} else {
					swal({
						title: "Error!",
						text: "Cliente no válido!",
						type: "error",
						showConfirmButton: true,
						timer: 1500 
					});
					$("#btn_submit").attr("disabled", false);
				}
			},
		});
	}
}

function redireccionar(type = 0, tel = 0) {
	//1 -> Existe Cliente | 0 -> No Existe Cliente

	if (type == 1) {
		window.location.href = base_url+'PuntosClientes/consulta_puntos/'+tel;
	} else {
		window.location.href = base_url+'PuntosClientes';
	}
}

function retornar() {
	window.location.href = base_url+'PuntosClientes';
}
